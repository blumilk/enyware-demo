<?php
	/*
		Plugin Name: Iykaa DMS
		Plugin URI: http://www.blumilk.com
		Description: Document Management System
		Author: James George Dunn
		Version: 0.1
	*/
	
	function iykaa_dms_shortcode_init() {
		// include('dms/init.php');
		$path = ABSPATH . '/init.php';
		include($path);
	}
	add_shortcode('iykaa_dms_init', 'iykaa_dms_shortcode_init');
?>