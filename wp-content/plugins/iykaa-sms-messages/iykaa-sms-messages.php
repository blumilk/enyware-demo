<?php
	/*
		Plugin Name: Iykaa SMS Messages
		Plugin URI: http://www.blumilk.com
		Description: Sends out SMS messages to members
		Version: 0.1
		Author: Blumilk
		Author URI: http://www.blumilk.com
	*/
	
	function iykaa_send_sms_messages_meta_boxes($post) {
		add_meta_box(
			'send-sms-messages-meta-box',
			__('Send an SMS Message'),
			'iykaa_send_sms_messages_content',
			array(
				'iykaa_notifications',
			),
			'side',
			'high'
		);
	}
	add_action('add_meta_boxes', 'iykaa_send_sms_messages_meta_boxes', 10, 2);
	
	function iykaa_send_sms_messages_content() {
		echo '<input id="iykaa_send_sms_messages" name="iykaa_send_sms_messages" value="true" type="checkbox"><label for="iykaa_send_sms_messages">Send SMS Messages</label>';
	}
	
	function iykaa_send_sms_messages_function($postsPage) {
		$notificationTitle = get_field('temp_title', 'options');
		$notificationPermalink = get_field('temp_link', 'options');
		
		$sendCount = 0;
		
		if($postsPage > 200) {
			$postOffset = $postsPage - 200;
		} else {
			$postOffset = 0;
		}
		
		$args = array(
			'post_type' => 'iykaa_members',
			'orderby' => 'title',
			'order' => 'ASC',
			'posts_per_page' => 200,
			'post_parent' => 0,
			'post_status' => array('pending', 'draft', 'publish'),
			'offset' => $postOffset,
		);
		
		$emailSearch = new WP_Query($args);
		
		$numbers = array();
		
		while($emailSearch->have_posts()) {
			$emailSearch->the_post();
			
			$membersNumber = ltrim(get_field('mobile_phone'), '0');
			
			$number = '44' . $membersNumber;
			
			array_push($numbers, $number);
		}
		
		// Textlocal account details
		$username = 'james@blumilk.com';
		$hash = 'fe52a50bcd06d6bbc4c722459d62d0ab05b1f80f';
		
		// Message details
		$sender = urlencode('Iykaa');
		$message = $notificationTitle . ' ' . $notificationPermalink;
		
		$numbers = implode(',', $numbers);
		
		// Prepare data for POST request
		$data = array(
			'username' => $username,
			'hash' => $hash,
			'numbers' => $numbers,
			"sender" => $sender,
			"message" => $message
		);
		
		// Send the POST request with cURL
		$ch = curl_init('http://api.txtlocal.com/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
	}
	add_action('iykaa_send_sms_messages_function_execute', 'iykaa_send_sms_messages_function');
	
	function iykaa_send_sms_messages($ID, $post) {
		$notificationTitle = $post->post_title;
		$notificationPermalink = get_permalink($ID);
		
		if(isset($_REQUEST['iykaa_send_sms_messages'])) {
			// Temporarily stores the title and link of the posts information
			update_field('temp_title', $notificationTitle, 'options');
			update_field('temp_link', $notificationPermalink, 'options');
			
			// Sets a 10 second delay before
			// This allows for the notification to get publish without delay so the tasks just run in the background
			date_default_timezone_set('Europe/London');
			
			$timeDelay = 60; // 60 seconds
			
			$memberCount = wp_count_posts('iykaa_members');
			$memberCount = $memberCount->publish;
			
			$runCount = $memberCount / 200;
			$runCount = ceil($runCount);
			
			for($notificationCounter = 1; $notificationCounter <= $runCount; $notificationCounter++) {
				$timeDelay = $timeDelay * $notificationCounter;
				$postsPage = 200 * $notificationCounter;
				
				wp_schedule_single_event(time() + $timeDelay, 'iykaa_send_sms_messages_function_execute', array($postsPage));
			}
		}
	}
	add_action('publish_iykaa_notifications', 'iykaa_send_sms_messages', 10, 2);
?>