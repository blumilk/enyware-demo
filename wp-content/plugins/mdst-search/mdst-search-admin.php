<div class="wrap">
	<?php
		echo '<h2>Searches</h2>';
	?>
	<style>
		.warnings .tab {
			vertical-align: middle;
		}
		
		.warnings .tab span {
			background-color: #ed4a56;
			width: 10px;
			height: 10px;
			display: block;
			border-radius: 50%;
		}
		
		/* Style the tab */
		div.tab {
			overflow: hidden;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
		}
		
		/* Style the buttons inside the tab */
		div.tab button {
			background-color: inherit;
			float: left;
			border: none;
			outline: none;
			cursor: pointer;
			padding: 14px 16px;
			transition: 0.3s;
		}
		
		/* Change background color of buttons on hover */
		div.tab button:hover {
			background-color: #ddd;
		}
		
		/* Create an active/current tablink class */
		div.tab button.active {
			background-color: #ccc;
		}
		
		/* Style the tab content */
		.tabcontent {
			display: none;
			padding: 6px 12px;
			border: 1px solid #ccc;
			border-top: none;
		}
	</style>
	
	<div class="postbox">
		<div class="inside">
			<h3>Popular Searches</h3>
			<ul>
				<?php
					global $wpdb;
					$table_name = $wpdb->prefix . 'mdst_search';
					$posts = $wpdb->get_results("SELECT search,
					COUNT(search) AS search_count
					FROM $table_name
					GROUP BY search
					ORDER BY search_count DESC");
					
					$searchDays = 30;
					$maxSearchCount = 5;
					$postCount = 1;
					
					foreach($posts as $post) {
						if(!empty($post->search)) {
							echo '<li>' . $post->search_count . ': ' . $post->search . '</li>';
						}
					    
					    if($maxSearchCount == $postCount) {
					    	break;
					    }
					    
						if(!empty($post->search)) {
					    	$postCount++;
					    }
					}
				?>
			</ul>
			<h3>Popular Document Types</h3>
			<ul>
				<?php
					global $wpdb;
					$table_name = $wpdb->prefix . 'mdst_search';
					$posts = $wpdb->get_results("SELECT document_type,
					COUNT(document_type) AS document_type_count
					FROM $table_name
					GROUP BY document_type
					ORDER BY document_type_count DESC");
					
					$searchDays = 30;
					$maxSearchCount = 5;
					$postCount = 1;
					
					foreach($posts as $post) {
						if(!empty($post->document_type)) {
							if($post->document_type != '–') {
					    		echo '<li>' . $post->document_type_count . ': ' . $post->document_type . '</li>';
					    	}
					    }
					    
					    if($maxSearchCount == $postCount) {
					    	break;
					    }
					    
						if(!empty($post->document_type)) {
							if($post->document_type != '–') {
								$postCount++;
							}
						}
					}
				?>
			</ul>
		</div>
	</div>
	
	<?php
		$order = 'ASC';
		$orderQuery = 'ORDER BY time DESC';
		
		if(isset($_GET['results-found'])) {
			if($_GET['results-found'] == 'ASC') {
				$order = 'DESC';
			} else {
				$order = 'ASC';
			}
			
			$orderQuery = 'ORDER BY CAST(result AS UNSIGNED) ' . $order;
		}
		
		if(isset($_GET['date'])) {
			if($_GET['date'] == 'ASC') {
				$order = 'DESC';
			} else {
				$order = 'ASC';
			}
			
			$orderQuery = 'ORDER BY time ' . $order;
		}
	?>
	
	<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'today')">Today</button>
		<button class="tablinks" onclick="openCity(event, 'week')">Week</button>
		<button class="tablinks" onclick="openCity(event, 'month')">Month</button>
	</div>
	<div id="today" class="tabcontent">
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					<a href="<?php echo $_SERVER['PHP_SELF']; ?>/?post_type=iykaa_library_custom&page=mdst-search-access-admin&results-found=<?php echo $order; ?>">
						Results Found
					</a>
				</th>
				<th>
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>/?post_type=iykaa_library_custom&page=mdst-search-access-admin&date=<?php echo $order; ?>">
						Date
					</a>
				</th>
			</thead>
			<tbody>
				<?php
					global $wpdb;
					$table_name = $wpdb->prefix . 'mdst_search';
					$posts = $wpdb->get_results("
						SELECT * FROM $table_name
						WHERE (time BETWEEN DATE_SUB(NOW(), INTERVAL 1 DAY) AND DATE_SUB(NOW(), INTERVAL -1 HOUR))
						$orderQuery
					");
					
					foreach($posts as $post) {
						echo '<tr>';
						echo '<td>';
						echo $post->member_login;
						echo '</td>';
						echo '<td>';
						echo $post->post_type;
						echo '</td>';
						echo '<td>';
						echo $post->search;
						echo '</td>';
						echo '<td>';
						echo $post->document_type;
						echo '</td>';
						echo '<td>';
						echo $post->result;
						echo '</td>';
						echo '<td>';
						echo $post->time;
						echo '</td>';
						echo '</tr>';
					}
				?>
			</tbody>
			<tfooter>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					Results Found
				</th>
				<th>
					Date
				</th>
			</tfooter>
		</table>
	</div>
	<div id="week" class="tabcontent">
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					Results Found
				</th>
				<th>
					Date
				</th>
			</thead>
			<tbody>
				<?php
					global $wpdb;
					$table_name = $wpdb->prefix . 'mdst_search';
					$posts = $wpdb->get_results("
						SELECT * FROM $table_name
						WHERE (time BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND DATE_SUB(NOW(), INTERVAL -1 HOUR))
						ORDER BY time DESC
					");
					
					foreach($posts as $post) {
						echo '<tr>';
						echo '<td>';
						echo $post->member_login;
						echo '</td>';
						echo '<td>';
						echo $post->post_type;
						echo '</td>';
						echo '<td>';
						echo $post->search;
						echo '</td>';
						echo '<td>';
						echo $post->document_type;
						echo '</td>';
						echo '<td>';
						echo $post->result;
						echo '</td>';
						echo '<td>';
						echo $post->time;
						echo '</td>';
						echo '</tr>';
					}
				?>
			</tbody>
			<tfooter>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					Results Found
				</th>
				<th>
					Date
				</th>
			</tfooter>
		</table>
	</div>
	<div id="month" class="tabcontent">
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					Results Found
				</th>
				<th>
					Date
				</th>
			</thead>
			<tbody>
				<?php
					global $wpdb;
					$table_name = $wpdb->prefix . 'mdst_search';
					$posts = $wpdb->get_results("
						SELECT * FROM $table_name
						WHERE (time BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND DATE_SUB(NOW(), INTERVAL -1 HOUR))
						ORDER BY time DESC
					");
					
					foreach($posts as $post) {
						echo '<tr>';
						echo '<td>';
						echo $post->member_login;
						echo '</td>';
						echo '<td>';
						echo $post->post_type;
						echo '</td>';
						echo '<td>';
						echo $post->search;
						echo '</td>';
						echo '<td>';
						echo $post->document_type;
						echo '</td>';
						echo '<td>';
						echo $post->result;
						echo '</td>';
						echo '<td>';
						echo $post->time;
						echo '</td>';
						echo '</tr>';
					}
				?>
			</tbody>
			<tfooter>
				<th>
					Member
				</th>
				<th>
					Section
				</th>
				<th>
					Search Term
				</th>
				<th>
					Document Type
				</th>
				<th>
					Results Found
				</th>
				<th>
					Date
				</th>
			</tfooter>
		</table>
	</div>
</div>
<script>
	function openCity(evt, cityName) {
		// Declare all variables
		var i, tabcontent, tablinks;
		
		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		
		// Get all elements with class="tablinks" and remove the class "active"
		tablinks = document.getElementsByClassName("tablinks");
		
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		
		// Show the current tab, and add an "active" class to the button that opened the tab
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>