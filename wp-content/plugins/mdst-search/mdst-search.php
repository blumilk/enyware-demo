<?php
	/*
		Plugin Name: Modest Search
		Plugin URI: http://www.blumilk.com
		Description: Simple popular searches.
		Version: 1.0
		Author: James George Dunn
		Author URI: http://www.blumilk.com
	*/
	
	function mdst_search_admin_actions() {
		add_submenu_page('edit.php?post_type=iykaa_library_custom', 'Search Results', 'Search Results', 'manage_options', 'mdst-search-access-admin', 'mdst_search_access_admin', 'dashicons-groups');
		add_submenu_page('edit.php?post_type=nursing_competencies', 'Search Results', 'Search Results', 'manage_options', 'mdst-search-access-admin', 'mdst_search_access_admin', 'dashicons-groups');
	}
	add_action('admin_menu', 'mdst_search_admin_actions');
	
	function mdst_search_access_admin() {
		include('mdst-search-admin.php');
	}
	
	global $mdst_db_version;
	$mdst_db_version = '1.0';
	
	function mdst_install() {
		global $wpdb;
		global $mdst_db_version;
	
		$table_name = $wpdb->prefix . 'mdst_search';
		
		$charset_collate = $wpdb->get_charset_collate();
	
		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			search text NOT NULL,
			result text NOT NULL,
			document_type text NOT NULL,
			member_login text NOT NULL,
			post_type text NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";
	
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	
		add_option('mdst_db_version', $mdst_db_version);
	}
	register_activation_hook(__FILE__, 'mdst_install');
	
	function mdst_search_execute(&$results, &$documentType, &$postTypeName) {
		global $wpdb;
		$table_name = $wpdb->prefix . 'mdst_search';
		$memberData = wp_get_current_user();
		
		$wpdb->insert( 
			$table_name, 
			array( 
				'time' 				=> current_time('mysql'),
				'search' 			=> $_REQUEST['keywords'],
				'result' 			=> $results,
				'document_type' 	=> $documentType,
				'member_login' 		=> $memberData->user_login,
				'post_type' 		=> $postTypeName,
			) 
		);
	}
	
	function mdst_search_results() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'mdst_search';
		$posts = $wpdb->get_results("SELECT search,
		COUNT(search) AS search_count
		FROM $table_name
		GROUP BY search
		ORDER BY search_count DESC");
		
		$searchDays = 30;
		$maxSearchCount = 5;
		$postCount = 0;
		
		foreach($posts as $post) {
		    echo '<a href="' . home_url() . '/clinical-document?paged=1&keywords=' . $post->search . '">' . $post->search . '</a>';
		    
		    if($maxSearchCount == $postCount) {
		    	break;
		    }
		    
		    $postCount++;
		}
		
		$cleanSearch = $wpdb->get_results("DELETE FROM $table_name WHERE time < CURRENT_DATE - INTERVAL $searchDays DAY");
	}
?>