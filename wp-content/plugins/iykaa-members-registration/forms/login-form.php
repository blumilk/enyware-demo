<?php global $validHash; ?>

<div class="inner">
	<?php if(isset($validHash) || isset($_REQUEST['enyware-email-hash'])) { ?>
		<div class="errors soft">
			<?php if(isset($_POST['enyware-email-verify'])) { ?>
				<p>Thank you. Please check your inbox and click the verification link.</p>
				
				<?php
					$signupSubject = get_bloginfo('name') . ' Email Verification Link';
					$memberEmail = $_POST['enyware-email-verify'];
					$verifyHash = md5($memberEmail);
					$signupMessage = '<a class="cta" href="' . home_url() . '/login?email=' . $memberEmail . '&hash=' . $verifyHash . '">Please Verify Your Email Address</a>';
					
					$headers = array('Content-Type: text/html; charset=UTF-8');
					@wp_mail($memberEmail, $signupSubject, $signupMessage, $headers);
				?>
			<?php } else { ?>
				<p>You still need to verify your email address.</p>
				<form action="" method="post">
					<input type="hidden" name="enyware-email-verify" value="<?php echo $_REQUEST['iykaa-member-email']; ?>">
					<input type="hidden" name="enyware-email-hash" value="<?php echo true; ?>">
					<button type="submit" class="cta primary">Send Verification Email</button>
				</form>
			<?php } ?>
		</div>
	<?php } ?>
	
	<?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'lostpassword') { ?>
		<h1>Reset</h1>
		<hr class="white size-s width-s">
		<p>Please enter your Email Address.</p>
		<p>You will receive a link to create a new password via email.</p>
		<fieldset>
			<form class="dropdown login-form" name="lostpasswordform" id="lostpasswordform" action="<?= site_url("/wp-login.php?action=lostpassword") ?>" method="post">
				<label>
					Email Address
					<input type="text" name="user_login" id="user_login" value="" size="20" required>
				</label>
				<input type="hidden" name="redirect_to" value="<?php echo home_url(); ?>/login">
				<button class="cta primary">Reset Password</button>
			</form>
		</fieldset>
	<?php } elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'rp' || isset($_REQUEST['action']) && $_REQUEST['action'] == 'resetpass') { ?>
		<h1>Reset!</h1>
		<hr class="white size-s width-s">
		<p>Enter your new password below.</p>
		<fieldset>
			<form id="iykaa-login-form" class="dropdown login-form" action="<?php echo site_url('wp-login.php?action=resetpass', 'login_post') ?>" method="post">
				<label>
				Password
					<input name="pass1-text" id="iykaa-member-password" class="required" type="password" placeholder="Password" <?php if(isset($_GET['email'])) { echo 'autofocus'; } ?>>
				</label>
				<div class="reveal-password">
					<label>
						<input name="show-password" type="checkbox">
						Show Password
					</label>
				</div>
				<?php
					$rp_key = '';
					$rp_cookie = 'wp-resetpass-' . COOKIEHASH;
					if(isset( $_COOKIE[ $rp_cookie ]) && 0 < strpos($_COOKIE[ $rp_cookie ], ':')) {
						list($rp_login, $rp_key) = explode(':', wp_unslash($_COOKIE[ $rp_cookie ]), 2);
					}
				?>
				<input type="hidden" name="rp_key" value="<?php echo $_REQUEST['key']; ?>">
				<input type="hidden" name="rp_login" value="<?php echo $_REQUEST['login']; ?>">
				<button class="cta primary">Reset Password</button>
			</form>
		</fieldset>
	<?php } else { ?>
		<h1>Hello!</h1>
		<hr class="white size-s width-s">
		<?php if(isset($_GET['redirect_to'])) { ?>
			<div class="errors">
				<p>You are required to be logged in to view this content.</p>
				<p>Please login to continue.</p>
			</div>
		<?php } else { ?>
			<?php if(isset($_REQUEST['password']) && $_REQUEST['password'] == 'changed') { ?>
				<p>Your password has been successfully changed.</p>
			<?php } else { ?>
				<p>I'm Enyware and I'm here to make your work life easier.</p>
			<?php } ?>
			<p>Please login to continue.</p>
		<?php } ?>
		<fieldset>
			<form id="iykaa-login-form" class="dropdown login-form" action="" method="post">
				<div class="row">
					<div class="ml-6">
						<label>
							Email Address
							<input name="iykaa-member-email" id="iykaa-member-email" class="required" type="text" value="<?php if(isset($_GET['email'])) { echo $_GET['email'] . $_GET['domain']; } ?>">
						</label>
					</div>
					<div class="ml-6">
						<label>
							Password <a href="<?php echo home_url(); ?>/login?action=lostpassword">(Forgot password?)</a>
							<input name="iykaa-member-password" id="iykaa-member-password" class="required" type="password" placeholder="Password" <?php if(isset($_GET['email'])) { echo 'autofocus'; } ?>>
						</label>
					</div>
				</div>
				<label class="shared" for="enyware-shared">
					<input name="enyware-shared" id="enyware-shared" type="checkbox">
					<?php
						if(get_field('shared_computer', 'options')) {
							$sharedMinutes = get_field('shared_computer', 'options');
						} else {
							$sharedMinutes = 600;
						}
						
						if($sharedMinutes <= 60) {
							$timeUnit = 'seconds';
						} else {
							$timeUnit = 'minutes';
							$sharedMinutes = $sharedMinutes / 60;
						}
					?>
					<span>I am using a shared computer (you will be automatically logged out after <?php echo $sharedMinutes . ' ' . $timeUnit; ?>)</span>
				</label>
				<button class="cta primary">Login</button>
				<input type="hidden" name="iykaa-login-nonce" value="<?php echo wp_create_nonce('iykaa-login-nonce'); ?>">
			</form>
		</fieldset>
	<?php } ?>
</div>
<script>
	jQuery(document).ready(function($){
		// Reveals the passwords, if needed
		$('.reveal-password input').click(function() {
			if($('.reveal-password input').is(':checked')) {
				$('#iykaa-member-password').attr('type', 'text');
			} else {
				$('#iykaa-member-password').attr('type', 'password');
			}
		});
	});
</script>