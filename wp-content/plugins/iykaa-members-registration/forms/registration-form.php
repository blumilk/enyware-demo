<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.css">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.min.js" type="text/javascript" charset="utf-8"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet">-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
	.extra-inner {
		display: none;
	}
	
	.checked {
		text-align: left;
	}
	
	.checked label {
		margin: 5px 20px 5px 0 !important;
		display: inline-block;
	}
	
	input[type="checkbox"] {
		margin: 0;
	}
	
	.password-hint {
		margin-bottom: 10px;
	}
	
	.password-hint p {
		font-size: 14px !important;
		padding-bottom: 0px !important;
		margin-bottom: 5px;
		font-style: italic;
	}
	
	.password-hint p span {
		font-weight: bold;
	}
	
	.required {
		transition: 0.4s;
	}
	
	.errors {
		background-color: #ed4a56;
		text-align: left;
		margin-top: 0;
		margin-bottom: 20px;
	}
	
	.error {
		border-color: #ed4a56 !important;
	}
</style>

<?php
	function valueSet($postValue) {
		if(isset($_POST[$postValue])) {
			echo 'value="' . $_POST[$postValue] . '"';
		}
	}
	
	function inputError($error, $boolean) {
		if(in_array($error, iykaa_errors()->get_error_codes())) {
			if($boolean == true) {
				return 'error';
			} else {
				echo 'error';
			}
		}
	}
?>
<form id="iykaa-registration-form" action="" method="POST" enctype="multipart/form-data">
	<div class="wrapper">
		<div class="login-slides">
			<!-- Starting Slide -->
			<div class="slide pre-slide">
				<div class="row">
					<div class="tl-8 indent-tl-2">
						<h2>Welcome to <?php bloginfo('name');?> Enyware...</h2>
						<hr class="secondary large">
						<img src="<?php bloginfo('url');?>/images/enyware-i-icon.svg" />
						<p class="first">A new online platform for your staff.</p>
						<p>Register now to access trust information any time, any place, any device, Enyware.</p>
						<hr class="secondary large">
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button next full">Get Started</div>	
						</div>
					</div>
				</div>
			</div>
			<!-- Pre-slide 1 -->
			<div class="slide pre-slide">
				<div class="row">
					<div class="tl-8 indent-tl-2">
						<h2>Let's create your login...</h2>
						<hr class="secondary large">
						<p class="icon">v</p>
						<p class="first">First up, we're going to create your login for the system.</p>
						<p>Remember, strong passwords are the best way to stay safe online.<br/>The strongest passwords are made using multiple words and numbers.</p>
						<p>For example, '<span>Monday French Pine 1066</span>'.</p>
						<hr class="secondary large">
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button next full">Next Step</div>	
						</div>
					</div>
				</div>
			</div>
			<!-- Slide 1 -->
			<div class="slide">
				<div class="row">
					<div class="tl-6 indent-tl-3">
						<h2>Let's create your login...</h2>
						<hr class="secondary large">
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-user"></div>
							</div>
							<div class="mp-10 ml-11">
								<input type="text" placeholder="First Name *" class="required required-forename half-field left <?php inputError('username_invalid', false); ?>" name="iykaa-forename" <?php valueSet('iykaa-forename'); ?>>
								<input type="text" placeholder="Last Name *" class="required required-surname half-field <?php inputError('username_invalid', false); ?>" name="iykaa-surname" <?php valueSet('iykaa-surname'); ?>>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-envelope"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-member-email" id="iykaa-member-email" type="text" placeholder="Email Address *" class="required required-email <?php inputError('email_used', false); ?> <?php inputError('email_invalid', false); ?>" <?php valueSet('iykaa-member-email'); ?>>
								<label>Please use one of the following email domains:
								<?php
									if(have_rows('email_domains', 'options')) {
										while(have_rows('email_domains', 'options')) {
											the_row();
											the_sub_field('domain_name');
											echo '<br />';
										}
									}
								?>
								</label>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-key"></div>
							</div>
							<div class="mp-10 ml-11">
								<div class="row">
									<div class="ml-6">
										<input name="iykaa-member-password" type="password" id="password-one" placeholder="Password *" class="required required-password <?php inputError('password_empty', false); ?> <?php inputError('password_strength', false); ?> <?php inputError('password_mismatch', false); ?>" autocomplete="off" autocorrect="off" autocapitalize="off">
										<div class="progress-bar"></div>
									</div>
									<div class="ml-6">
										<input name="iykaa-member-password-confirm" type="password" id="password-two" placeholder="Repeat Password *" <?php inputError('password_empty', false); ?> class="required required-password-confirm <?php inputError('password_empty', false); ?> <?php inputError('password_strength', false); ?> <?php inputError('password_mismatch', false); ?>" autocomplete="off" autocorrect="off" autocapitalize="off">
										<div class="password-match"></div>
									</div>
									<div class="ml-12">
										<div class="reveal-password">
											<label>
												<input name="show-password" type="checkbox">
												Show Passwords
											</label>
										</div>
									</div>
								</div>
								<div class="password-hint">
									<p><span>Passwords Minimum Requirements:</span> A password must have minimum of 9 characters of which 1 must be UPPERCASE and 2 must be NUMBERS.<br />For example, '<span>Enyware99</span>'.</p>
									<br />
									<p>But a more robust password would be the following: three memorable words including spaces and a number. For example, '<span>Monday French Pine 1066</span>'.</p>
								</div>
							</div>
							<input class="password-strength" type="hidden" name="password-strength">
						</div>
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev">Previous Step</div>
							<div class="button next">Next Step</div>			
						</div>
					</div>
				</div>
			</div>
			<!-- Pre-slide 2 -->
			<div class="slide pre-slide">
				<div class="row">
					<div class="tl-8 indent-tl-2">
						<h2>Your profile...</h2>
						<hr class="secondary large">
						<p class="icon">t</p>
						<p class="first">Next up we'll create your public profile.</p>
						<p>The information in this section will be seen by all registered colleagues in the system and are the details they'll use to contact you.</p>
						<p>You can update this information at anytime, from any device.</p>
						<hr class="secondary large">
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev">Previous Step</div>
							<div class="button next">Next Step</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Slide 2 -->
			<div class="slide">
				<div class="row">
					<div class="tl-6 indent-tl-3">
						<h2>Your profile...</h2>
						<hr class="secondary large">
						<p>Please fill in the fields below to create your public profile.</p>
						<hr class="secondary large">
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-user"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-member-job-title" id="iykaa-job-title" type="text" placeholder="Job Title *" <?php valueSet('iykaa-member-job-title'); ?> class="required required-job-title <?php inputError('job_title', false); ?>">
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-user-group"></div>
							</div>
							<div class="mp-10 ml-11">
								<?php
									if(isset($_REQUEST['department'])) {
										$department = $_REQUEST['department'];
									} else {
										$department = 'department';
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Department *',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $department,
										'hierarchical'       => 0,
										'name'               => 'department',
										'id'                 => '',
										'class'              => inputError('department', 'true'),
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'department_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<?php wp_dropdown_categories($args); ?>
								<p>If your department does not appear, please contact the communications department with a request to add into the Enyware system.</p>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-location"></div>
							</div>
							<div class="mp-10 ml-11">
								<?php
									if(isset($_REQUEST['location'])) {
										$location = $_REQUEST['location'];
									} else {
										$location = 'location';
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Location *',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $location,
										'hierarchical'       => 0,
										'name'               => 'location',
										'id'                 => '',
										'class'              => inputError('location', 'true'),
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'location_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<?php wp_dropdown_categories($args); ?>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-location"></div>
							</div>
							<div class="mp-10 ml-11">
								<?php
									if(isset($_REQUEST['business'])) {
										$businessUnit = $_REQUEST['business'];
									} else {
										$businessUnit = 'business';
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Business Unit *',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $businessUnit,
										'hierarchical'       => 0,
										'name'               => 'business',
										'id'                 => '',
										'class'              => inputError('business', 'true'),
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'business_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<?php wp_dropdown_categories($args); ?>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-phone"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-telephone" id="iykaa-telephone" type="phone" placeholder="Telephone Number *" <?php valueSet('iykaa-telephone'); ?> class="required required-telephone <?php inputError('telephone', false); ?>">
							</div>
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-phone"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-mobile-phone" id="iykaa-mobile-phone" type="phone" placeholder="Mobile Phone Number (if applicable)" <?php valueSet('iykaa-mobile-phone'); ?> class="<?php inputError('mobile', false); ?>">
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-notification"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-bleep" id="iykaa-bleep" type="text" placeholder="Bleep Number (if applicable)" title="You can add your Bleep number here if you have one." <?php valueSet('iykaa-bleep'); ?> class="<?php inputError('bleep', false); ?>">
							</div>
						</div>
						<div class="row nested">
							<div class="mp-12">
								<div class="button extra-info">
									Working Hours
								</div>
								<div class="extra-inner border working-hours">
									<p>You can opt to skip this and update it on your profile at a convenient time in the future.</p>
									<div class="row">
										<div class="mp-6">
											<input type="checkbox" name="flexi-hours" value="flexi-hours" />Flexi Hours
										</div>
										<div class="mp-6">
											<input type="checkbox" name="irregular-hours" value="irregular-shift-patterns" />Irregular Shift Patterns
										</div>
									</div>
									<?php for($h = 0; $h < 7; $h++) { ?>
										<?php
											$dayWeek = jddayofweek($h, 1);
											
											echo $dayWeek;
										?>
										<div class="row">
											<div class="mp-6">
												<select name="start-<?php echo strtolower($dayWeek); ?>">
													<option value="false">Start Time</option>
													<?php
														for($i = 7; $i <= 23; $i++) {
															for($j = 0; $j <= 45; $j += 15) {
																$time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . str_pad($j, 2, '0', STR_PAD_LEFT);
																
																if(isset($_POST['start-' . strtolower($dayWeek)]) && $_POST['start-' . strtolower($dayWeek)] == $time) {
																	$selected = 'selected';
																} else {
																	$selected = '';
																}
																
																echo '<option ' . $selected . ' value="' . $time . '">' . $time . '</option>';
															}
														}
														
														for($i = 0; $i <= 6; $i++) {
															for($j = 0; $j <= 45; $j += 15) {
																$time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . str_pad($j, 2, '0', STR_PAD_LEFT);
																
																if(isset($_POST['start-' . strtolower($dayWeek)]) && $_POST['start-' . strtolower($dayWeek)] == $time) {
																	$selected = 'selected';
																} else {
																	$selected = '';
																}
																
																echo '<option ' . $selected . ' value="' . $time . '">' . $time . '</option>';
															}
														}
													?>
												</select>
											</div>
											<div class="mp-6">
												<select name="end-<?php echo strtolower($dayWeek); ?>">
													<option value="false">End Time</option>
													<?php
														for($i = 16; $i <= 23; $i++) {
															for($j = 0; $j <= 45; $j += 15) {
																$time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . str_pad($j, 2, '0', STR_PAD_LEFT);
																
																if(isset($_POST['end-' . strtolower($dayWeek)]) && $_POST['end-' . strtolower($dayWeek)] == $time) {
																	$selected = 'selected';
																} else {
																	$selected = '';
																}
																
																echo '<option ' . $selected . ' value="' . $time . '">' . $time . '</option>';
															}
														}
														
														for($i = 0; $i <= 15; $i++) {
															for($j = 0; $j <= 45; $j += 15) {
																$time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . str_pad($j, 2, '0', STR_PAD_LEFT);
																
																if(isset($_POST['end-' . strtolower($dayWeek)]) && $_POST['end-' . strtolower($dayWeek)] == $time) {
																	$selected = 'selected';
																} else {
																	$selected = '';
																}
																
																echo '<option ' . $selected . ' value="' . $time . '">' . $time . '</option>';
															}
														}
													?>
												</select>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-12">
								<div class="button extra-info">
									Add your profile picture
								</div>
								<div class="extra-inner border">
								<!--<h2>A bit about you...</h2>
									<hr class="secondary large">-->
									<div class="row nested">
										<div class="mp-2 ml-1 icon"></div>
										<div class="mp-10 ml-11">
											<p>It's nice to put a face to a name. Please add an appropriate image of yourself that you will be happy for your colleagues to see.</p>
											<p>You can opt to skip this and update it on your profile at a convenient time in the future.</p>
										</div>
										<div class="mp-2 ml-1 icon">
											<div class="i-icon large icon-ii-face"></div>
										</div>
										<div class="mp-10 ml-11">
											<input name="iykaa-profile" id="iykaa-profile" type="file" placeholder="Profile Picture">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev">Previous Step</div>
							<div class="button next">Next Step</div>		
						</div>
					</div>
				</div>
			</div>
			<!-- Pre-slide 3 -->
			<div class="slide pre-slide">
				<div class="row">
					<div class="tl-8 indent-tl-2">
						<h2>Tell us more about you...</h2>
						<hr class="secondary large">
						<p class="icon">C</p>
						<p class="first">Lastly, please tell us a little bit more about you and your role to help us only send you information that is relevant.</p>
						<p>The information on the next screen is for optimising communication, so you only receive information that is relevant to the job you do / the place you work.</p>
						<p>This information can only be seen and used by the communication department and will not show on your public profile.</p>
						<hr class="secondary large">
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev">Previous Step</div>
							<div class="button next">Next Step</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Slide 3 -->
			<div class="slide">
				<div class="row">
					<div class="tl-6 indent-tl-3">
						<h2>Tell us more about you...</h2>
						<hr class="secondary large">
						<p>Help us to send information relevant to you by completing the fields below. This information will only be used by trust admin and will not be visible to colleagues.</p>
						<hr class="secondary large">
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-phone"></div>
							</div>
							<div class="mp-10 ml-11">
								<input name="iykaa-member-emergency" id="iykaa-member-emergency" type="text" placeholder="Emergency mobile number" title="This will only be used to text you in the case of a major incident / emergency. This can be your work or personal mobile number. This will only be visible to the communications department.">
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-location"></div>
							</div>
							<div class="mp-10 ml-11">
								<?php
									if(isset($_REQUEST['occupational'])) {
										$occupationalUnit = $_REQUEST['occupational'];
									} else {
										$occupationalUnit = 'occupational';
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Occupational Unit *',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $occupationalUnit,
										'hierarchical'       => 0,
										'name'               => 'occupational',
										'id'                 => '',
										'class'              => 'occupational',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'occupational_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<div class="row">
									<div class="tp-8">
										<?php wp_dropdown_categories($args); ?>
									</div>
									<div class="tp-4">
										<input class="other-occupational" type="text" name="other-occupational" placeholder="Other" <?php valueSet('other-occupational'); ?>>
									</div>
								</div>
							</div>
						</div>
						<div class="row nested">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large icon-ii-location"></div>
							</div>
							<div class="mp-10 ml-11" title="This will only be used to send you relevant information and won’t be visible to colleagues.">
								<?php
									if(isset($_REQUEST['band'])) {
										$band = $_REQUEST['band'];
									} else {
										$band = 'band';
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Band *',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $band,
										'hierarchical'       => 0,
										'name'               => 'band',
										'id'                 => '',
										'class'              => 'band',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'band_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<?php wp_dropdown_categories($args); ?>
							</div>
						</div>
						<div class="row nested checked">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large"></div>
							</div>
							<div class="mp-10 ml-11">
								<p>Please tick the following boxes if any of them apply to you...</p>
								<label class="iam">I am a...</label>
								<?php
									$args = array(
										'type'                     => 'iykaa_members',
										'child_of'                 => 0,
										'parent'                   => '',
										'orderby'                  => 'Title',
										'order'                    => 'ASC',
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '',
										'include'                  => '',
										'number'                   => '',
										'taxonomy'                 => 'you_are_tax',
										'pad_counts'               => false
									);
								 	
									$categories = get_categories($args);
									
									if(isset($_POST['you-are'])) {
										$selected = $_POST['you-are'];
										
										if(!empty($selected)) {
											foreach($selected as $select) {
												$selection[] = $select;
											}
										}
									}
								?>
								<div class="">
									<div class="row">
										<ul class="options">
										<?php foreach($categories as $category) { ?>
											<?php if($category->slug != 'all') { // Ignores the 'All' option as this is only to be used within the DMS. I would have done this by the exclude but ID's will be changing when it comes to different deployments ?>
												<li>
													<div class="mp-12">
														<label for="tax-<?php echo $category->term_id; ?>">
															<input type="checkbox" id="tax-<?php echo $category->term_id; ?>" name="you-are[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
															<?php echo $category->cat_name; ?>
														</label>
													</div>
												</li>
											<?php } ?>
										<?php } ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<hr class="secondary large">
						<div class="row nested checked">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large"></div>
							</div>
							<div class="mp-10 ml-11">
								<p>Please tick the following boxes if any of them apply to you...</p>
								<?php
									$args = array(
										'type'                     => 'iykaa_members',
										'child_of'                 => 0,
										'parent'                   => '',
										'orderby'                  => 'Title',
										'order'                    => 'ASC',
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '',
										'include'                  => '',
										'number'                   => '',
										'taxonomy'                 => 'apply_to_you_tax',
										'pad_counts'               => false
									);
								 	
									$categories = get_categories($args);
									
									if(isset($_POST['apply-to-you'])) {
										$selected = $_POST['apply-to-you'];
										
										if(!empty($selected)) {
											foreach($selected as $select) {
												$selection[] = $select;
											}
										}
									}
								?>
								<div class="">
									<div class="row">
										<ul class="options">
										<?php foreach($categories as $category) { ?>
											<?php if($category->slug != 'all') { // Ignores the 'All' option as this is only to be used within the DMS. I would have done this by the exclude but ID's will be changing when it comes to different deployments ?>
												<li>
													<div class="mp-12">
														<label for="tax-<?php echo $category->term_id; ?>">
															<input type="checkbox" id="tax-<?php echo $category->term_id; ?>" name="apply-to-you[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
															<?php echo $category->cat_name; ?>
														</label>
													</div>
												</li>
											<?php } ?>
										<?php } ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<hr class="secondary large">
						<div class="row nested checked">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon large"></div>
							</div>
							<div class="mp-10 ml-11">
								<p>Please tick the systems that you regularly use...</p>
								<?php
									$args = array(
										'type'                     => 'iykaa_members',
										'child_of'                 => 0,
										'parent'                   => '',
										'orderby'                  => 'Title',
										'order'                    => 'ASC',
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '',
										'include'                  => '',
										'number'                   => '',
										'taxonomy'                 => 'systems_used',
										'pad_counts'               => false
									);
								 	
									$categories = get_categories($args);
									
									if(isset($_POST['systems'])) {
										$selected = $_POST['systems'];
										
										if(!empty($selected)) {
											foreach($selected as $select) {
												$selection[] = $select;
											}
										}
									}
								?>
								<div class="row">
									<ul class="options">
									<?php foreach($categories as $category) { ?>
										<?php if($category->slug != 'all') { // Ignores the 'All' option as this is only to be used within the DMS. I would have done this by the exclude but ID's will be changing when it comes to different deployments ?>
											<li>
												<div class="mp-12">
													<label for="tax-<?php echo $category->term_id; ?>">
														<input type="checkbox" id="tax-<?php echo $category->term_id; ?>" name="systems[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
														<?php echo $category->cat_name; ?>
													</label>
												</div>
											</li>
										<?php } ?>
									<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev">Previous Step</div>
							<div class="button next">Next Step</div>		
						</div>
					</div>
				</div>
			</div>
			<!-- Pre-slide 4 -->
			<!-- Slide 4 -->
			<div class="slide">
				<div class="row">
					<div class="tl-6 indent-tl-3">
						<h2>Almost done...</h2>
						<?php
							/*
								<p>By signing up you are are agreeing to our:</p>
								<ul>
									<li><a href="">Terms and Conditions</a></li>
									<li><a href="">Data Protection Policy</a></li>
									<li><a href="">Cookie Policy</a></li>
									<li><a href="">Privacy Policy</a></li>
									<li><a href="">EULA</a></li>
								</ul>
								<label>
									<input name="iykaa-agreement" type="checkbox" placeholder="I Agree" id="agreement" value="consent-given">
									I agree to the above terms and conditions and approve <?php echo bloginfo('name'); ?> to send me appropriate information from the system.
								</label>
							*/
						?>
						
						<label>
							<input name="enyware-opt-out" type="checkbox" placeholder="Opt out" id="opt-out" value="opt-out">
							Tick this box if you wish to opt out of receiving email notifications from the system, you can opt back in again from your 'edit profile' to be kept up to date.
						</label>
						<input type="hidden" name="iykaa-register-nonce" value="<?php echo wp_create_nonce('iykaa-register-nonce'); ?>">
						<input class="button sign-up" type="submit" value="<?php _e('Register'); ?>">
					</div>
				</div>
				<div class="controls">
					<div class="row">
						<div class="ml-6 indent-ml-3">
							<div class="button prev full">Previous Step</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
	$(function() {
		$(document).tooltip();
	});
	
	jQuery(document).ready(function($){
		// Reveals the passwords, if needed
		$('.reveal-password input').click(function() {
			if($('.reveal-password input').is(':checked')) {
				$('#password-one').attr('type', 'text');
				$('#password-two').attr('type', 'text');
			} else {
				$('#password-one').attr('type', 'password');
				$('#password-two').attr('type', 'password');
			}
		});
		
		var sliderPage = 1;
		var errorCount = new Array();
		var errorMessages = new Array();
		
		function errorOccurred(field, message, usermessage) {
			if(jQuery.inArray(field, errorCount) != -1) {
				if(message === 'no-error') {
					errorCount.splice(errorCount.indexOf(field), 1);
				}
			} else {
				errorCount.push(field);
				
				if(message === 'no-error') {
					errorCount.splice(errorCount.indexOf(field), 1);
				}
			}
			
/*
			if(jQuery.inArray('<span class="member-error">' + usermessage + '</span>', errorMessages) != -1) {
				if(message === 'no-error') {
					errorMessages.splice(errorMessages.indexOf('<span class="member-error">' + usermessage + '</span>'), 1);
				}
			} else {
				errorMessages.push('<span class="member-error">' + usermessage + '</span>');
				$('.errors').css('display', 'block');
				$('.errors').html(errorMessages);
			}
*/
		}
		
		function errorCheck() {
			if(errorCount <= 0) {
				errorCount = 0;
			} else {
				errorCount--;
			}
		}
		
		// Slider Controls
		$('.login-slides').slick({
			infinite: false,
			dots: false,
			arrows: false,
			slidesToShow: 1,
			speed: 600,
			adaptiveHeight: false,
			swipe: false,
		});

		$('#alt-payer').click(function() {
			$('.main-payer').toggle('slow');
			$('.alternate-payer').toggle('slow');
		});
		
		$('.extra-info').click(function() {
			if($(this).hasClass('open')) {
				$(this).removeClass('open');
			} else {
				$(this).addClass('open');
			}
			
			$(this).next().slideToggle();
		});
		
		$('.next').click(function() {
			var slidesTop = $('.login-slides').position().top;
			
			if(sliderPage === 3) {
				if($('.required-forename').val().length === 0) {
					$('.required-forename').addClass('error');
					errorOccurred('required-forename', 'error', 'Your first name is required.');
				} else {
					$('.required-forename').removeClass('error');
					errorOccurred('required-forename', 'no-error', 'Your first name is required.');
				}
				
				if($('.required-surname').val().length === 0) {
					$('.required-surname').addClass('error');
					errorOccurred('required-surname', 'error', 'Your surname is required.');
				} else {
					$('.required-surname').removeClass('error');
					errorOccurred('required-surname', 'no-error', 'Your surname is required.');
				}
				
				if($('.required-email').val().length === 0) {
					$('.required-email').addClass('error');
					errorOccurred('required-email', 'error', 'A valid email address is required.');
				} else {
					$('.required-email').removeClass('error');
					errorOccurred('required-email', 'no-error', 'A valid email address is required.');
				}
				
				if($('.required-password').val().length === 0) {
					$('.required-password').addClass('error');
					errorOccurred('required-password', 'error', 'A password is required.');
				} else {
					$('.required-password').removeClass('error');
					errorOccurred('required-password', 'no-error', 'A password is required.');
				}
				
				if($('.required-password-confirm').val().length === 0) {
					$('.required-password-confirm').addClass('error');
					errorOccurred('required-password-confirm', 'error', '');
				} else {
					$('.required-password-confirm').removeClass('error');
					errorOccurred('required-password-confirm', 'no-error', '');
				}
				
				if(!$('.progress-bar').hasClass('good')) {
					$('.required-password-confirm').addClass('error');
					errorOccurred('required-password-not-strong', 'error', 'A strong password is required.');
				} else {
					$('.required-password-confirm').removeClass('error');
					errorOccurred('required-password-not-strong', 'no-error', 'A strong password is required.');
				}
				
				if(!$('.password-match').hasClass('match')) {
					$('.required-password-confirm').addClass('error');
					errorOccurred('required-password-no-match', 'error', 'Your passwords need to match.');
				} else {
					$('.required-password-confirm').removeClass('error');
					errorOccurred('required-password-no-match', 'no-error', 'Your passwords need to match.');
				}
			}
			
			if(sliderPage === 5) {
				if($('.required-job-title').val().length === 0) {
					$('.required-job-title').addClass('error');
					errorOccurred('required-job-title', 'error');
				} else {
					$('.required-job-title').removeClass('error');
					errorOccurred('required-job-title', 'no-error');
				}
				
				if($('.required-telephone').val().length === 0) {
					$('.required-telephone').addClass('error');
					errorOccurred('required-telephone', 'error');
				} else {
					$('.required-telephone').removeClass('error');
					errorOccurred('required-telephone', 'no-error');
				}
				
				if($('#select2-department-container').text() == 'Department *') {
					$('#select2-department-container').addClass('error');
					errorOccurred('department', 'error');
				} else {
					$('#select2-department-container').removeClass('error');
					errorOccurred('department', 'no-error');
				}
				
				if($('#select2-location-container').text() == 'Location *') {
					$('#select2-location-container').addClass('error');
					errorOccurred('location', 'error');
				} else {
					$('#select2-location-container').removeClass('error');
					errorOccurred('location', 'no-error');
				}
				
				if($('#select2-business-container').text() == 'Business Unit *') {
					$('#select2-business-container').addClass('error');
					errorOccurred('business', 'error');
				} else {
					$('#select2-business-container').removeClass('error');
					errorOccurred('business', 'no-error');
				}
			}
			
			if(sliderPage === 7) {
				if($('.other-occupational').val().length === 0) {
					if($('.occupational').val() == 'false') {
						$('.occupational').addClass('error');
						errorOccurred('occupational', 'error');
					} else {
						$('.occupational').removeClass('error');
						errorOccurred('occupational', 'no-error');
					}
				} else {
					$('.occupational').removeClass('error');
					errorOccurred('occupational', 'no-error');
				}
				
				if($('.occupational').val() != 157) { // Skips if Medical & Dental
					if($('.band').val() == 'false') {
						$('.band').addClass('error');
						errorOccurred('band', 'error');
					} else {
						$('.band').removeClass('error');
						errorOccurred('band', 'no-error');
					}
				} else {
					$('.band').removeClass('error');
					errorOccurred('band', 'no-error');
				}
			}
			
			if(errorCount.length === 0) {
				$('.login-slides').slick('slickNext');
				sliderPage++;
			}
			
			console.log(errorCount);
			
			// Creates a smooth scroll back to the top when a slide is quite tall.
			$('html,body').animate(
				{
					scrollTop: slidesTop,
				},
				'slow'
			);
		});
		
		$('.prev').click(function() {
			var slidesTop = $('.login-slides').position().top;
			
			if(errorCount.length === 0) {
				$('.login-slides').slick('slickPrev');
				sliderPage--;
			}
			
			// Creates a smooth scroll back to the top when a slide is quite tall.
			$('html,body').animate(
				{
					scrollTop: slidesTop,
				},
				'slow'
			);
		});
		
		$('#agreement').click(function() {
			$('.sign-up').toggleClass('blanked');
			
			if($('.sign-up').hasClass('blanked')) {
				$('.sign-up').attr('disabled', true);
			} else {
				$('.sign-up').attr('disabled', false);
			}
		});
		
		$('#department').select2();
		$('#location').select2();
		$('#business').select2();
	});
</script>