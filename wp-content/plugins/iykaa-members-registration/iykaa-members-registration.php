<?php
	/*
		Plugin Name: Iykaa Members Registration
		Plugin URI: https://www.blumilk.com
		Description: Provides simple front end registration and login forms
		Version: 0.1
		Author: Blumilk
		Author URI: https://www.blumilk.com
	*/
	
	date_default_timezone_set('Europe/London');
	
	if(!isset($_SESSION)) {
		session_start();
	}
	
	// User registration login form
	function iykaa_registration_form() {
		// Only show the registration form to non-logged-in members
		// Check to make sure user registration is enabled
		$registrationEnabled = get_option('users_can_register');
		
		// Only show the registration form if allowed
		if($registrationEnabled) {
			$output = iykaa_registration_form_fields();
		} else {
			$output = __('User registration is not enabled.');
		}
		
		return $output;
	}
	add_shortcode('register_form', 'iykaa_registration_form');
	
	// User login form
	function iykaa_login_form() {
		if(!is_user_logged_in()) {
			$output = iykaa_login_form_fields();
		} else {
			$output = 'You\'re already logged in.';
		}
		
		return $output;
	}
	add_shortcode('login_form', 'iykaa_login_form');
	
	// Registration form fields
	function iykaa_registration_form_fields() {
		ob_start();
		// Show any error messages after form submission
		iykaa_show_error_messages();
		include('forms/registration-form.php');
		
		return ob_get_clean();
	}
	
	// Login form fields
	function iykaa_login_form_fields() {
		ob_start();
		
		// Show any error messages after form submission
		iykaa_show_error_messages();
		
		include('forms/login-form.php');
		
		return ob_get_clean();
	}
	
	// Logs a member in after submitting a form
	function iykaa_login_member() {
		if(isset($_POST['iykaa-member-email'])) {
			$memberEmailJoin = $_POST['iykaa-member-email'];
		}
		
		if(isset($memberEmailJoin) && array_key_exists('iykaa-login-nonce', $_POST) && wp_verify_nonce($_POST['iykaa-login-nonce'], 'iykaa-login-nonce')) {
			$user = get_user_by('login', $memberEmailJoin);
			
			if(!$user) {
				// If the user name doesn't exist
				iykaa_errors()->add('empty_username', __('Invalid email.'));
			} else {
				// Check the user's login with their password
				if(!wp_check_password($_POST['iykaa-member-password'], $user->user_pass, $user->ID)) {
					// If the password is incorrect for the specified user
					iykaa_errors()->add('empty_password', __('Incorrect password.'));
				}
			}
			
			if(!isset($_POST['iykaa-member-password']) || $_POST['iykaa-member-password'] == '') {
				// If no password was entered
				iykaa_errors()->add('empty_password', __('Please enter a password.'));
			}
			
			// Retrieve all error messages
			$errors = iykaa_errors()->get_error_messages();
			
			// Checks to see whether the user would like to stay logged in
			if(empty($_POST['enyware-shared'])) {
				$remain = true;
			} else {
				$remain = false;
			}
			
			// Only log the user in if there are no errors
			if(empty($errors)) {
				$args = array(
					// Since a user can only have one member assigned we will get the post that belongs to that user.
					'author' => $user->ID,
					'post_type' => 'iykaa_members',
					'post_status' => array('pending', 'draft', 'publish'),
				);
				
				$memberDetails = new WP_Query($args);
				
				while($memberDetails->have_posts()) {
					$memberDetails->the_post();
					$memberID = $memberDetails->post->ID;
				}
				
				if(get_field('member_first_login', $memberID)) {
					update_post_meta($memberID, 'member_last_login', date('YmdHis'));
					
					wp_set_auth_cookie($user->ID, $remain);
					wp_set_current_user($user->ID, $memberEmailJoin);
					do_action('wp_login', $memberEmailJoin);
					
					if(isset($_GET['redirect_to'])) {
						$redirectURL = $_GET['redirect_to'];
						
						wp_redirect(home_url('/' . $redirectURL));
					} else {
						wp_redirect(home_url('/dashboard'));
					}
					
					exit;
				} else {
					if(isset($_GET['hash']) && $_GET['hash'] === get_field('verify_hash', $memberID)) {
						// First time login for the member
						update_post_meta($memberID, 'member_first_login', 'true');
						update_post_meta($memberID, 'member_first_login_date', date('Ymd'));
						update_post_meta($memberID, 'verify_hash', '');
						
						wp_set_auth_cookie($user->ID, $remain);
						wp_set_current_user($user->ID, $memberEmailJoin);
						do_action('wp_login', $memberEmailJoin);
						wp_redirect(home_url('introduction'));
						exit;
					} else {
						global $validHash;
						$validHash = true;
					}
				}
			}
		}
	}
	add_action('init', 'iykaa_login_member');
	
	global $profileImage;
	
	// Register a new member
	function iykaa_add_new_member() {
		if(isset($_POST['iykaa-register-nonce']) && wp_verify_nonce($_POST['iykaa-register-nonce'], 'iykaa-register-nonce')) {
			$memberForename = trim($_POST['iykaa-forename']);
			$memberSurname = trim($_POST['iykaa-surname']);
			
			$memberFullName = $memberForename . ' ' . $memberSurname;
			
			$memberLogin = $memberFullName;
			
			$memberEmail = $_POST['iykaa-member-email'];
			$emailParts = explode('@', $memberEmail);
			
			$memberEmailLocal = $emailParts[0];
			$memberEmailDomain = '@' . $emailParts[1];
			
			$memberEmail = $memberEmailLocal . $memberEmailDomain;
			$memberPassword = $_POST['iykaa-member-password'];
			$passwordConfirm = $_POST['iykaa-member-password-confirm'];
			$verifyHash = md5($memberEmail);
			
			if(username_exists($memberEmail)) {
				// Username already registered
				iykaa_errors()->add('username_unavailable', __('Your email address has already been registered by another user. Perhaps you have already registered to ' . get_bloginfo('name') . '?'));
			}
				
			// Email domains that will need approval
			$approvedDomains = array();
			$approvalEmails = array();
			while(have_rows('email_domains', 'options')) {
				the_row();
				array_push($approvedDomains, get_sub_field('domain_name'));
				
				if(get_sub_field('approval_required')) {
					array_push($approvalEmails, get_sub_field('domain_name'));
				}
			}
			
			if(!in_array($memberEmailDomain, $approvedDomains)) {
				iykaa_errors()->add('invalid_domain', __('This domain name is not currently on the approval list.'));
			}
			
			if(!validate_username($memberLogin)) {
				// Invalid username
				iykaa_errors()->add('username_invalid', __('Invalid name.'));
			}
			
			if($memberLogin == '') {
				// Empty username
				iykaa_errors()->add('username_empty', __('Please enter your name.'));
			}
			
			if(!is_email($memberEmail)) {
				// Invalid email
				iykaa_errors()->add('email_invalid', __('Invalid email.'));
			}
			
			if(email_exists($memberEmail)) {
				// Email address already registered
				iykaa_errors()->add('email_used', __('Email already registered.'));
			}
			
			if($memberPassword == '') {
				// Passwords do not match
				iykaa_errors()->add('password_empty', __('Please enter a password.'));
			}
			
			if($memberPassword != $passwordConfirm) {
				// Passwords do not match
				iykaa_errors()->add('password_mismatch', __('Passwords do not match.'));
			}
			
			/*
			if($_POST['password-strength'] != 'strong') {
				iykaa_errors()->add('password_strength', __('Your password is too weak.'));
			}
			*/
			
			if($_POST['iykaa-member-job-title'] == '') {
				iykaa_errors()->add('job_title', __('Please enter your job title.'));
			}
			
			if($_POST['department'] == 'false') {
				iykaa_errors()->add('department', __('Please select your department.'));
			}
			
			if($_POST['location'] == 'false') {
				iykaa_errors()->add('location', __('Please select your location.'));
			}
			
			if($_POST['business'] == 'false') {
				iykaa_errors()->add('business', __('Please select your business unit.'));
			}
			
			if($_POST['iykaa-telephone'] == '') {
				iykaa_errors()->add('telephone', __('Please enter your telephone number.'));
			}
			
			if(!isset($_FILES['iykaa-profile']) && $_FILES['iykaa-profile']['size'] != 0) {
				iykaa_errors()->add('member_image', __('Please upload your profile image again.'));
			}
			
			$errors = iykaa_errors()->get_error_messages();
			
			// Only create the member if there are no errors
			if(empty($errors)) {
				$newMemberID = wp_insert_user(
					array(
						'user_login'		=> $memberEmail,
						'user_pass'	 		=> $memberPassword,
						'user_email'		=> $memberEmail,
						'nickname' 			=> $memberLogin,
						'user_registered'	=> date('Y-m-d H:i:s'),
						'role'				=> 'member_iykaa',
						'first_name' 		=> $memberForename,
						'last_name' 		=> $memberSurname,
						'display_name' 		=> $memberFullName,
					)
				);
				
				$meta = array();
				$meta['ID'] = $newMemberID;
				$memberID = wp_update_user($meta);
				
				// Registers the member to a custom post type
				$register = array(
					'post_type' => 'iykaa_members',
					'post_title' => $memberLogin,
					'post_status' => 'publish',
					'post_author'   => $newMemberID,
				);
				
				$registerDetails = wp_insert_post($register);
				
				// Custom fields
				update_post_meta($registerDetails, 'member_name', $memberLogin);
				update_post_meta($registerDetails, 'email_address', $memberEmail);
				update_post_meta($registerDetails, 'member_register_date', date('Ymd'));
				update_post_meta($registerDetails, 'member_forename', $memberForename);
				update_post_meta($registerDetails, 'member_surname', $memberSurname);
				update_post_meta($registerDetails, 'job_title_full', $_POST['iykaa-member-job-title']);
				update_post_meta($registerDetails, 'member_telephone', $_POST['iykaa-telephone']);
				update_post_meta($registerDetails, 'mobile_phone', $_POST['iykaa-mobile-phone']);
				update_post_meta($registerDetails, 'bleep_number', $_POST['iykaa-bleep']);
				update_post_meta($registerDetails, 'member_emergency_number', $_POST['iykaa-member-emergency']);
				
				if(isset($_POST['iykaa-agreement'])) {
					update_post_meta($registerDetails, 'member_agreement', $_POST['iykaa-agreement']);
				} else {
					update_post_meta($registerDetails, 'member_agreement', true);
				}
				
				if(isset($_POST['enyware-opt-out'])) {
					update_post_meta($registerDetails, 'member_opt_out', true);
				} else {
					update_post_meta($registerDetails, 'member_opt_out', false);
				}
				
				update_post_meta($registerDetails, 'verify_hash', $verifyHash);
				
				$fieldKey = 'email_addresses';
				$value = array(
					array(
						'email_address'		=> $memberEmail,
						'label'				=> 'Default'
					)
				);
				update_field($fieldKey, $value, $registerDetails);
				
				$fieldKey = 'contact_numbers';
				$value = array(
					array(
						'contact_number'	=> $_POST['iykaa-telephone'],
						'label'				=> 'Telephone'
					),
					array(
						'contact_number'	=> $_POST['iykaa-mobile-phone'],
						'label'				=> 'Mobile'
					),
					array(
						'contact_number'	=> $_POST['iykaa-bleep'],
						'label'				=> 'Bleep Number'
					)
				);
				update_field($fieldKey, $value, $registerDetails);
				
				$fieldKey = 'field_596f4bc957dff';
				
				for($i = 0; $i < 7; $i++) {
					$startKey = strtolower(jddayofweek($i, 1)) . '_start';
					$endKey = strtolower(jddayofweek($i, 1)) . '_end';
					$startValue = 'start-' . strtolower(jddayofweek($i, 1));
					$endValue = 'end-' . strtolower(jddayofweek($i, 1));
					
					if($_POST[$startValue] != 'false') {
						$value = array(
							array(
								$startKey 	=> $_POST[$startValue],
							)
						);
						update_field($fieldKey, $value, $registerDetails);
					}
					
					if($_POST[$endValue] != 'false') {
						$value = array(
							array(
								$endKey 	=> $_POST[$endValue],
							)
						);
						update_field($fieldKey, $value, $registerDetails);
					}
				}
				
				// Status of the member defaults to available
				wp_set_object_terms($registerDetails, 'Available', 'status_tax', true);
				
				if(isset($_POST['location'])) {
					wp_set_object_terms($registerDetails, (int)$_POST['location'], 'location_tax', true);
				}
				
				if(isset($_POST['position'])) {
					$setTax = $_POST['position'];
					
					foreach($setTax as $value) {
						wp_set_object_terms($registerDetails, (int)$value, 'position_tax', true);
					}
				}
				
				if(isset($_POST['department'])) {
					wp_set_object_terms($registerDetails, (int)$_POST['department'], 'department_tax', true);
				}
				
				if(isset($_POST['you-are'])) {
					$setTax = $_POST['you-are'];
					
					foreach($setTax as $value) {
						wp_set_object_terms($registerDetails, (int)$value, 'you_are_tax', true);
					}
				}
				
				if(isset($_POST['apply-to-you'])) {
					$setTax = $_POST['apply-to-you'];
					
					foreach($setTax as $value) {
						wp_set_object_terms($registerDetails, (int)$value, 'apply_to_you_tax', true);
					}
				}
				
				if(isset($_POST['systems'])) {
					$setTax = $_POST['systems'];
					
					foreach($setTax as $value) {
						wp_set_object_terms($registerDetails, (int)$value, 'systems_used', true);
					}
				}
				
				if(isset($_POST['transport'])) {
					$setTax = $_POST['transport'];
					
					foreach($setTax as $value) {
						wp_set_object_terms($registerDetails, (int)$value, 'transport_tax', true);
					}
				}
				
				if(isset($_POST['job-type'])) {
					wp_set_object_terms($registerDetails, (int)$_POST['job-type'], 'job_type_tax', true);
				}
				
				if(isset($_POST['band'])) {
					wp_set_object_terms($registerDetails, (int)$_POST['band'], 'band_tax', true);
				}
				
				// Helps to prevent double entries of the occupational unit
				if(!empty($_POST['other-occupational'])) {
					wp_set_object_terms($registerDetails, $_POST['other-occupational'], 'occupational_tax', true);
				} else {
					if(isset($_POST['occupational'])) {
						wp_set_object_terms($registerDetails, (int)$_POST['occupational'], 'occupational_tax', true);
					}
				}
				
				if(isset($_POST['business'])) {
					wp_set_object_terms($registerDetails, (int)$_POST['business'], 'business_tax', true);
				}
				
				// Deal with images uploaded from the front-end while allowing ACF to do it's thing
				if(!function_exists('wp_handle_upload')) {
					require_once(ABSPATH . 'wp-admin/includes/file.php');
				}
				
				// Move file to media library
				$movefile = wp_handle_upload($_FILES['iykaa-profile'], array('test_form' => false));
				
				// If move was successful, insert WordPress attachment
				if($movefile && !isset($movefile['error'])) {
					$wp_upload_dir = wp_upload_dir();
					$attachment = array(
						'guid' => $wp_upload_dir['url'] . '/' . basename($movefile['file']),
						'post_mime_type' => $movefile['type'],
						'post_title' => preg_replace('/\.[^.]+$/', '', basename($movefile['file'])),
						'post_content' => '',
						'post_status' => 'inherit'
					);
					$attach_id = wp_insert_attachment($attachment, $movefile['file']);
					
					// Assign the file as the featured image
					set_post_thumbnail($registerDetails, $attach_id);
					update_field('profile_image', $attach_id, $registerDetails);
				}
				
				// Setting up of the emails
				$headers = array('Content-Type: text/html; charset=UTF-8');
				
				$commsEmail = get_field('approval_email_address', 'options');
				
				if(in_array($memberEmailDomain, $approvalEmails)) {
					$approvalProcessSubject = get_field('member_approval_process_subject', 'options');
					$approvalProcessSubject = str_replace('[company-name]', get_bloginfo('name'), $approvalProcessSubject);
					$approvalProcessSubject = str_replace('[full-name]', get_userdata($newMemberID)->display_name, $approvalProcessSubject);
					$approvalProcessSubject = str_replace('[forename]', get_userdata($newMemberID)->first_name, $approvalProcessSubject);
					$approvalProcessSubject = str_replace('[surname]', get_userdata($newMemberID)->last_name, $approvalProcessSubject);
					$approvalProcessMessage = get_field('member_approval_process_message', 'options');
					$approvalProcessMessage = str_replace('[company-name]', get_bloginfo('name'), $approvalProcessMessage);
					$approvalProcessMessage = str_replace('[full-name]', $memberFullName, $approvalProcessMessage);
					$approvalProcessMessage = str_replace('[email]', $memberEmail, $approvalProcessMessage);
					
					$linkText = get_string_between($approvalProcessMessage, '[login-link]', '[/login-link]');
					
					$approvalProcessMessage = str_replace('[login-link]', '<a class="cta" href="' . home_url() . '/approve-member?email=' . $memberEmailLocal . '&domain=' . $memberEmailDomain . '&hash=' . $verifyHash . '">', $approvalProcessMessage);
					$approvalProcessMessage = str_replace('[/login-link]', '</a>', $approvalProcessMessage);
					
					@wp_mail($commsEmail, $approvalProcessSubject, $approvalProcessMessage, $headers);
				} else {
					$signupSubject = get_field('member_signup_subject', 'options');
					$signupSubject = str_replace('[company-name]', get_bloginfo('name'), $signupSubject);
					$signupSubject = str_replace('[full-name]', get_userdata($newMemberID)->display_name, $signupSubject);
					$signupSubject = str_replace('[forename]', get_userdata($newMemberID)->first_name, $signupSubject);
					$signupSubject = str_replace('[surname]', get_userdata($newMemberID)->last_name, $signupSubject);
					$signupMessage = get_field('member_signup_message', 'options');
					$signupMessage = str_replace('[company-name]', get_bloginfo('name'), $signupMessage);
					$signupMessage = str_replace('[full-name]', get_userdata($newMemberID)->display_name, $signupMessage);
					$signupMessage = str_replace('[forename]', get_userdata($newMemberID)->first_name, $signupMessage);
					$signupMessage = str_replace('[surname]', get_userdata($newMemberID)->last_name, $signupMessage);
					
					$linkText = get_string_between($signupMessage, '[login-link]', '[/login-link]');
					
					$signupMessage = str_replace('[login-link]', '<a class="cta" href="' . home_url() . '/login?email=' . $memberEmailLocal . '&domain=' . $memberEmailDomain . '&hash=' . $verifyHash . '" style="background-color: #28333C; color: #ffffff; font-size: 24px; font-weight: bold; text-align: center; display: block; margin-top: 20px; padding: 20px; border-radius: 3px;">', $signupMessage);
					$signupMessage = str_replace('[/login-link]', '</a>', $signupMessage);
					
					// TO DELETE
					// $memberEmail = 'james@blumilk.co.uk';
					
					@wp_mail($memberEmail, $signupSubject, $signupMessage, $headers);
				}
				
				// Setting up of the admin email confirmation
				$adminEmails = array();
				while(have_rows('admin_email_addresses', 'options')) {
					the_row();
					array_push($adminEmails, get_sub_field('email'));
				}
				
				$adminMessage = 'A member by the name of ' . $memberLogin . ' has registered to ' . get_bloginfo('name') . ' (' . get_bloginfo('url') . ').';
				$adminMessage .= '<br /><br />';
				$adminMessage .= $_SERVER['HTTP_USER_AGENT'];
				$adminMessage .= '<br /><br />';
				$adminMessage .= $_SERVER['REMOTE_ADDR'];
				@wp_mail($adminEmails, 'A new member has registered to ' . get_bloginfo('name'), $adminMessage, $headers);
				
				$joined = 'true';
				
				$_SESSION['joined'] = $joined;
				
				if($newMemberID) {
					if(in_array($memberEmailDomain, $approvalEmails)) {
						wp_redirect(home_url('/approval-process'));
						exit;
					} else {
						wp_redirect(home_url('/thank-you'));
						exit;
					}
				}
			}
		}
	}
	add_action('init', 'iykaa_add_new_member');
	
	// Used for tracking error messages
	function iykaa_errors() {
		static $wp_error;
		return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}
	
	// Displays error messages from form submissions
	function iykaa_show_error_messages() {
		if($codes = iykaa_errors()->get_error_codes()) {
			echo '<div class="errors">';
			
			// Loop error codes and display errors
			foreach($codes as $code) {
				$message = iykaa_errors()->get_error_message($code);
				echo '<span class="member-error"><strong>' . __('Error:') . '</strong> ' . $message . '</span>';
			}
			
			echo '</div>';
		}
	}
	
	function do_password_reset() {
		if('POST' == $_SERVER['REQUEST_METHOD']) {
			$rp_key = $_REQUEST['rp_key'];
			$rp_login = $_REQUEST['rp_login'];
			
			$user = check_password_reset_key( $rp_key, $rp_login );
			
			if(!$user || is_wp_error($user)) {
				if($user && $user->get_error_code() === 'expired_key') {
					wp_redirect(home_url('login?login=expiredkey'));
				} else {
					wp_redirect(home_url('login?login=invalidkey'));
				}
				
				exit;
			}
			
			if(isset($_POST['pass1-text'])) {
				if(empty($_POST['pass1-text'])) {
					// Password is empty
					$redirect_url = home_url('member-password-reset');
					$redirect_url = add_query_arg('key', $rp_key, $redirect_url);
					$redirect_url = add_query_arg('login', $rp_login, $redirect_url);
					$redirect_url = add_query_arg('error', 'password_reset_empty', $redirect_url);
					
					wp_redirect($redirect_url);
					exit;
				}
				
				// Parameter checks OK, reset password
				reset_password($user, $_POST['pass1-text']);
				wp_redirect(home_url('login?password=changed'));
			} else {
				echo "Invalid request.";
			}
			
			exit;
		}
	}
	add_action('login_form_rp', 'do_password_reset');
	add_action('login_form_resetpass', 'do_password_reset');
?>