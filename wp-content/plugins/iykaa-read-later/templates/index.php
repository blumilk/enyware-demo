<div class="wrapper read-later page-content">
	<div class="row">
		<div class="dt-12">
			<a href="<?php echo home_url(); ?>/read-later">
				<h1>My Favourites List</h1>
			</a>
			<hr class="secondary">
			<?php
				include(get_template_directory() . '/includes/config/member-details.php');
				
				while($membersProfile->have_posts()) {
					$membersProfile->the_post();
					$memberID = get_the_ID();
				}
				
				$readLaterID = trim(get_field('iykaa_read_later_ids', $memberID));
				$readLaterID = preg_replace('!\s+!', ' ', $readLaterID); // Removes any double spaces
				$readLaterPieces = explode(' ', $readLaterID);
				
				wp_reset_query();
				
				$readLaterCount = count($readLaterPieces);
			?>
			
			<?php if($readLaterID) { ?>
				<div class="results">
					<div class="row">
						<div class="dt-12">
							<table width="100%">
								<thead>
									<tr>
										<td class="name">
											Name
										</td>
										<td class="mp-hide type">
											Item Type
										</td>
										<td class="actions">
											Options
										</td>
									</tr>
								</thead>
								<tbody>
									<?php for($i = 0; $i < $readLaterCount; $i++) { ?>
										<?php
											if(count($_REQUEST) > 0 && isset($_REQUEST['removeID'])) {
												$removeID = $_REQUEST['removeID'];
												
												$newID = str_replace($removeID, '', $readLaterID);
												$newID = preg_replace('!\s+!', ' ', $newID); // Removes any double spaces
												
												update_field('iykaa_read_later_ids', $newID, $memberID);
											} else {
												$removeID = '';
											}
											
											if($readLaterPieces[$i] == $removeID) {
												$priorityLevel = 'high';
											} else {
												$priorityLevel = 'low';
											}
										?>
										
										<tr class="<?php echo $priorityLevel; ?>">
											<td class="name">
												<a href="<?php the_permalink($readLaterPieces[$i]); ?>">
													<p><?php echo get_the_title($readLaterPieces[$i]); ?></p>
												</a>
											</td>
											<td class="mp-hide type">
												<?php
													$postType = get_post_type_object(get_post_type($readLaterPieces[$i]));
													
													echo esc_html($postType->labels->singular_name);
												?>
											</td>
											<td class="actions">
												<a href="<?php the_permalink($readLaterPieces[$i]); ?>" class="visit" alt="Visit" title="Visit"></a>
												<a href="?removeID=<?php echo $readLaterPieces[$i]; ?>" class="remove" alt="Remove" title="Remove"></a>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			<?php } else { ?>
				<p>There's nothing in your reading list yet.</p>
			<?php } ?>
		</div>
	</div>
</div>