<?php
	$currentID = get_the_ID();
	
	if(count($_GET) > 0 && isset($_REQUEST['postid'])) {
		include(get_template_directory() . '/includes/config/member-details.php');
		
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberID = get_the_ID();
		}
		
		$currentID = $_REQUEST['postid'];
		
		$currentReadLater = get_field('iykaa_read_later_ids', $memberID);
		
		$readLaterPieces = explode(' ', $currentReadLater);
		
		if(!in_array($currentID, $readLaterPieces)) {
			$readLaterUpdate = $currentID . ' ' . $currentReadLater;
			
			update_field('iykaa_read_later_ids', $readLaterUpdate, $memberID);
			
			$theHTML = '
			<div class="read-later edit">
				<a href="' . home_url() . '/read-later">
					<div class="wrapper no-gaps nested">
						<div class="row">
							<div class="mp-2">
								<div class="i-icon large icon-ii-information"></div>
							</div>
							<div class="mp-10">
							<p>Success!<br/>Click here to go your favourites</p>
							</div>
						</div>
					</div>
				</a>
			</div>';
			
			// Bringing it all together
			echo $theHTML;
		} else {
			$theHTML = '
			<div class="read-later edit">
				<a href="' . home_url() . '/read-later">
					<div class="wrapper no-gaps nested">
						<div class="row">
							<div class="mp-2">
								<div class="i-icon large icon-ii-information"></div>
							</div>
							<div class="mp-10">
							<p>This is already saved in your favourites list</p>
							</div>
						</div>
					</div>
				</a>
			</div>';
			
			// Bringing it all together
			echo $theHTML;
		}
		
		wp_reset_query();
	} else {
		$theHTML = '
		<div class="read-later edit">
			<a href="?postid=' . $currentID . '">
				<div class="wrapper no-gaps nested">
					<div class="row">
						<div class="mp-2">
							<div class="i-icon large icon-ii-information"></div>
						</div>
						<div class="mp-10">
						<p>Save to favourites</p>
						</div>
					</div>
				</div>
			</a>
		</div>';
		
		// Bringing it all together
		echo $theHTML;
	}
?>