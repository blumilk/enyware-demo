<?php
	/*
		Plugin Name: Iykaa Read Later
		Plugin URI: https://www.blumilk.com
		Description: Add the read later feature to articles and pages.
		Author: James George Dunn
		Version: 0.3
	*/
	
	// Registers a custom fields but requires ACF to be activated
	function iykaa_read_later_acf_register() {
		if(function_exists('acf_add_local_field_group')) {
			acf_add_local_field_group(array(
					'key' => 'group_1',
					'title' => 'Iykaa Read Later',
					'fields' => array(
						array(
							'key' => 'field_1',
							'label' => 'IDs',
							'name' => 'iykaa_read_later_ids',
							'type' => 'text',
							'prefix' => '',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						)
					),
					'location' => array (
						array(
							array(
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'iykaa_members',
							),
						),
					),
					'menu_order' => 0,
					'position' => 'normal',
					'style' => 'default',
					'label_placement' => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen' => '',
					'active' => 1,
				)
			);
		}
	}
	add_action('init', 'iykaa_read_later_acf_register');
	
	function iykaa_read_later_shortcode_list() {
		include('templates/index.php');
	}
	add_shortcode('iykaa_read_later_list', 'iykaa_read_later_shortcode_list');
	
	function iykaa_read_later_shortcode_add() {
		include('templates/add.php');
	}
	add_shortcode('iykaa_read_later_add', 'iykaa_read_later_shortcode_add');
	
	// Creates the Read Later page when the plugin is activated
	function iykaa_read_later_add_page() {
		$pageTitle = 'Read Later';
		$pageContent = '[iykaa_read_later_list]'; // Inserts the shortcode for the read later list
		
		$pageCheck = get_page_by_title($pageTitle);
		
		$newPage = array(
			'post_type' 	=> 'page',
			'post_title' 	=> $pageTitle,
			'post_content' 	=> $pageContent,
			'post_status' 	=> 'publish',
			'post_author' 	=> 1,
		);
		
		if(!isset($pageCheck->ID)) { // Checks to see if the page already exists
			$newPageID = wp_insert_post($newPage);
		}
	}
	add_action('init', 'iykaa_read_later_add_page');
?>