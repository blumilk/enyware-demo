<?php
	/*
		Plugin Name: Iykaa Support Documentation
		Plugin URI: http://www.blumilk.com
		Description: Sets up the support documentation for Iykaa admins.
		Author: James George Dunn
		Version: 1.0
	*/
	
	require ABSPATH . '/wp-content/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'http://repo.iykaa.com/iykaa-support-documentation/iykaa-support-documentation.json',
		__FILE__,
		'iykaa-support-documentation'
	);
	
	function iykaa_support_documentation_admin_actions() {
		add_menu_page('Iykaa Support', 'Iykaa Support', 'manage_options', 'iykaa-support-documentation', 'iykaa_support_documentation_admin', 'dashicons-sos');
	}
	add_action('admin_menu', 'iykaa_support_documentation_admin_actions');
	
	function iykaa_support_documentation_admin() {
		include('includes/iykaa-support-documentation-admin.php');
	}
?>