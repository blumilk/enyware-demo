<?php
	/*
		Plugin Name: Enyware Email Notifications
		Plugin URI: http://www.blumilk.com
		Description: Sends out email notifications to members.
		Author: James George Dunn
		Version: 1.0
	*/
	
	function send_email_notifications_meta_boxes($post) {
		add_meta_box('send-email-notifications-meta-box', __('Send an Email Notification'), 'send_email_notifications_content', array('iykaa_notifications'), 'side', 'high');
	}
	add_action('add_meta_boxes', 'send_email_notifications_meta_boxes', 10, 2);
	
	function send_email_notifications_content() {
		echo '<input id="send_email_notification" name="send_email_notification" value="true" type="checkbox"><label for="send_email_notification">Send as an Email Notification</label>';
	}
	
	global $enyware_db_version;
	$enyware_db_version = '1.0';
	
	function enyware_email_notifications_install() {
		global $wpdb;
		global $enyware_db_version;
		
		$table_name = $wpdb->prefix . 'enyware_email_notifications';
		
		$charset_collate = $wpdb->get_charset_collate();
		
		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			notification_id text NOT NULL,
			sent_email text NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";
		
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		
		add_option('enyware_db_version', $enyware_db_version);
	}
	register_activation_hook(__FILE__, 'enyware_email_notifications_install');
	
	function send_email_notification_function($postsPage, $postID) {
		// Builds the content that the email will send
		$notificationTitle = get_the_title($postID);
		$content_post = get_post($postID);
		$content = $content_post->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		$notificationParagraph = $content;
		$notificationPermalink = get_permalink($postID);
		
		$notificationSubject = $notificationTitle;
		$notificationContent .= '<h1>' . $notificationTitle . '</h1>';
		$notificationContent .= $notificationParagraph;
		$notificationContent .= '<a class="cta" href="' . $notificationPermalink . '">View Notification</a>';
		
		$sendCount = 0;
		
		if($postsPage > 200) {
			$postOffset = $postsPage - 200;
		} else {
			$postOffset = 0;
		}
		
		// Sets the taxonomies and checks against the members
		$taxQuery = array();
		
		$locations = get_the_terms($postID, 'location_tax');
		if(isset($locations) && !empty($locations)) {
			$locationIDs = array();
			
			foreach($locations as $location) {
				array_push($locationIDs, $location->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'location_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $locationIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$positions = get_the_terms($postID, 'position_tax');
		if(isset($positions) && !empty($positions)) {
			$positionIDs = array();
			
			foreach($positions as $position) {
				array_push($positionIDs, $position->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'position_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $positionIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$departments = get_the_terms($postID, 'department_tax');
		if(isset($departments) && !empty($departments)) {
			$departmentIDs = array();
			
			foreach($departments as $department) {
				array_push($departmentIDs, $department->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'department_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $departmentIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$bands = get_the_terms($postID, 'band_tax');
		if(isset($bands) && !empty($bands)) {
			$bandIDs = array();
			
			foreach($bands as $band) {
				array_push($bandIDs, $band->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'band_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $bandIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$occupationals = get_the_terms($postID, 'occupational_tax');
		if(isset($occupationals) && !empty($occupationals)) {
			$occupationalIDs = array();
			
			foreach($occupationals as $occupational) {
				array_push($occupationalIDs, $occupational->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'occupational_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $occupationalIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$businesss = get_the_terms($postID, 'business_tax');
		if(isset($businesss) && !empty($businesss)) {
			$businessIDs = array();
			
			foreach($businesss as $business) {
				array_push($businessIDs, $business->term_id);
			}
			
			array_push($taxQuery,
				array('relation' => 'OR',
					array(
						'taxonomy' 	=> 'business_tax',
						'field'    	=> 'term_id',
						'terms'    	=> $businessIDs,
						'operator' 	=> 'IN',
					)
				)
			);
		}
		
		$args = array(
			'post_type' => 'iykaa_members',
			'orderby' => 'title',
			'order' => 'ASC',
			'posts_per_page' => 200,
			'post_parent' => 0,
			'post_status' => array(
				'publish'
			),
			'offset' => $postOffset,
			'tax_query' => $taxQuery,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => 'member_opt_out',
					'value' => false,
				),
				array(
					'key' => 'member_opt_out',
					'compare' => 'NOT EXISTS',
				),
			),
		);
		
		$emailSearch = new WP_Query($args);
		
		if($emailSearch->have_posts()) {
			while($emailSearch->have_posts()) {
				$emailSearch->the_post();
				
				$sendCount++;
				
				// Sends out to all of the email addresses that a member has
				// This means a member may get an email more than once depending on how many email addresses they have assigned to themselves
				while(have_rows('email_addresses')) {
					the_row();
					$notificationEmail .= get_sub_field('email_address') . ',';
				}
				
				global $wpdb;
				$table_name = $wpdb->prefix . 'enyware_email_notifications';
				
				$wpdb->insert($table_name, 
					array( 
						'date' 				=> current_time('mysql'),
						'notification_id' 	=> $postID,
						'sent_email' 		=> $notificationEmail,
					)
				);
				
				// Send the emails
				wp_mail($notificationEmail, $notificationSubject, $notificationContent);
				
				// Clears the email address ready for the next batch of sends
				$notificationEmail = '';
			}
		}
		
		// Testing purposes. Can be removed once happy with the functionality
		wp_mail('james@blumilk.com', 'Sending: Enyware Send', 'Sending to ' . $sendCount . ' members.');
	}
	add_action('send_email_notification_function_execute', 'send_email_notification_function', 1, 2);
	
	function send_email_notifications($ID, $post) {
		$postID = $post->ID;
		
		if(isset($_REQUEST['send_email_notification'])) {
			date_default_timezone_set('Europe/London');
			
			$memberCount = wp_count_posts('iykaa_members');
			$memberCount = $memberCount->publish;
			
			$runCount = $memberCount / 200;
			$runCount = ceil($runCount);
			
			if($runCount === 0) {
				$runCount = 1;
			}
			
			for($notificationCounter = 1; $notificationCounter <= $runCount; $notificationCounter++) {
				$timeDelay = 5 * $notificationCounter;
				$postsPage = 200 * $notificationCounter;
				
				// Testing purposes. Can be removed once happy with the functionality
				wp_mail('james@blumilk.com', 'Queueing: Enyware Email Notifications', 'Queueing the email notifications.');
				
				wp_schedule_single_event(time() + $timeDelay, 'send_email_notification_function_execute', array($postsPage, $postID));
			}
		}
	}
	add_action('publish_iykaa_notifications', 'send_email_notifications', 10, 2);
?>