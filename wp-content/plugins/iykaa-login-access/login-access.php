<?php
	/*
		Plugin Name: Iykaa Login Access
		Plugin URI: http://www.blumilk.com
		Description: Provides simple front end importing of members
		Version: 1.0
		Author: Blumilk
		Author URI: http://www.blumilk.com
	*/
	
	function login_access_admin_actions() {
		add_menu_page('Login Access', 'Login Access', 'manage_options', 'iykaa-login-access', 'login_access_admin', 'dashicons-groups');
	}
	add_action('admin_menu', 'login_access_admin_actions');
	
	function login_access_admin() {
		include('login-access-admin.php');
	}
?>