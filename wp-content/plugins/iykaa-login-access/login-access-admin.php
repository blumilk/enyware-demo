<div class="wrap">
	<?php
		echo '<h2>' . __('Login Access', 'iykaa_login_access') . '</h2>';
	?>
	<style>
		.warnings .tab {
			vertical-align: middle;
		}
		
		.warnings .tab span {
			background-color: #ed4a56;
			width: 10px;
			height: 10px;
			display: block;
			border-radius: 50%;
		}
		
		/* Style the tab */
		div.tab {
			overflow: hidden;
			border: 1px solid #ccc;
			background-color: #f1f1f1;
		}
		
		/* Style the buttons inside the tab */
		div.tab button {
			background-color: inherit;
			float: left;
			border: none;
			outline: none;
			cursor: pointer;
			padding: 14px 16px;
			transition: 0.3s;
		}
		
		/* Change background color of buttons on hover */
		div.tab button:hover {
			background-color: #ddd;
		}
		
		/* Create an active/current tablink class */
		div.tab button.active {
			background-color: #ccc;
		}
		
		/* Style the tab content */
		.tabcontent {
			display: none;
			padding: 6px 12px;
			border: 1px solid #ccc;
			border-top: none;
		}
	</style>
	
	<div class="tab">
		<button class="tablinks" onclick="openCity(event, 'day')">Day</button>
		<button class="tablinks" onclick="openCity(event, 'week')">Week</button>
		<button class="tablinks" onclick="openCity(event, 'month')">Month</button>
		<button class="tablinks" onclick="openCity(event, 'never')">Never</button>
	</div>
	<div id="day" class="tabcontent">
		<?php
			$args = array(
				'post_type' => 'iykaa_members',
				'orderby' => 'meta_value_num',
				'meta_key' => 'member_last_login',
				'order' => 'DESC',
				'posts_per_page' => -1,
				'post_parent' => 0,
				'post_status' => 'publish',
				'meta_query' => array(
					'relation' => 'OR',
					array(
						'key' 		=> 'member_last_login',
						'value' 	=> date('Ymd0001'),
						'compare' 	=> '>=',
						'type' 		=> 'CHAR',
					)
				)
			);
			
			$memberSearch = new WP_Query($args);
			
			$count = 0;
			
			while($memberSearch->have_posts()) {
				$memberSearch->the_post();
				$count++;
			}
		?>
		<h3><?php echo $count; ?> logins today.</h3>
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_members',
					'orderby' => 'meta_value_num',
					'meta_key' => 'member_last_login',
					'order' => 'DESC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish',
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' 		=> 'member_last_login',
							'value' 	=> date('Ymd0001'),
							'compare' 	=> '>=',
							'type' 		=> 'CHAR',
						)
					)
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<tr class="<?php if(!get_field('member_last_login')) { echo 'warnings'; } ?>">
					<td class="tab" width="10px"><span></span></td>
					<td>
						<?php the_title(); ?>
					</td>
					<td>
						<?php the_field('email_address'); ?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_last_login');
							
							if($originalDate) {
								$newDate = date("d/m/Y H:i", strtotime($originalDate));
								echo $newDate;
							} else {
								echo '<strong>This member has not logged in.</strong>';
							}
						?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_register_date');
							
							if($originalDate) {
								$newDate = date("d/m/Y", strtotime($originalDate));
								echo $newDate;
							}
						?>
					</td>
				</tr>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</tfooter>
		</table>
	</div>
	<div id="week" class="tabcontent">
		<?php
			$args = array(
				'post_type' => 'iykaa_members',
				'orderby' => 'meta_value_num',
				'meta_key' => 'member_last_login',
				'order' => 'DESC',
				'posts_per_page' => -1,
				'post_parent' => 0,
				'post_status' => 'publish',
				'meta_query' => array(
					'relation' => 'OR',
					array(
						'key' 		=> 'member_last_login',
						'value' 	=> date('Ymd', strtotime('-7 days')),
						'compare' 	=> '>=',
						'type' 		=> 'CHAR',
					)
				)
			);
			
			$memberSearch = new WP_Query($args);
			
			$count = 0;
			
			while($memberSearch->have_posts()) {
				$memberSearch->the_post();
				$count++;
			}
		?>
		<h3><?php echo $count; ?> logins this week.</h3>
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_members',
					'orderby' => 'meta_value_num',
					'meta_key' => 'member_last_login',
					'order' => 'DESC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish',
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' 		=> 'member_last_login',
							'value' 	=> date('Ymd', strtotime('-7 days')),
							'compare' 	=> '>=',
							'type' 		=> 'CHAR',
						)
					)
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<tr class="<?php if(!get_field('member_last_login')) { echo 'warnings'; } ?>">
					<td class="tab" width="10px"><span></span></td>
					<td>
						<?php the_title(); ?>
					</td>
					<td>
						<?php the_field('email_address'); ?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_last_login');
							
							if($originalDate) {
								$newDate = date("d/m/Y H:i", strtotime($originalDate));
								echo $newDate;
							} else {
								echo '<strong>This member has not logged in.</strong>';
							}
						?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_register_date');
							
							if($originalDate) {
								$newDate = date("d/m/Y", strtotime($originalDate));
								echo $newDate;
							}
						?>
					</td>
				</tr>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</tfooter>
		</table>
	</div>
	<div id="month" class="tabcontent">
		<?php
			$args = array(
				'post_type' => 'iykaa_members',
				'orderby' => 'meta_value_num',
				'meta_key' => 'member_last_login',
				'order' => 'DESC',
				'posts_per_page' => -1,
				'post_parent' => 0,
				'post_status' => 'publish',
				'meta_query' => array(
					'relation' => 'OR',
					array(
						'key' 		=> 'member_last_login',
						'value' 	=> date('Ymd', strtotime('-31 days')),
						'compare' 	=> '>=',
						'type' 		=> 'CHAR',
					)
				)
			);
			
			$memberSearch = new WP_Query($args);
			
			$count = 0;
			
			while($memberSearch->have_posts()) {
				$memberSearch->the_post();
				$count++;
			}
		?>
		<h3><?php echo $count; ?> logins this month.</h3>
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_members',
					'orderby' => 'meta_value_num',
					'meta_key' => 'member_last_login',
					'order' => 'DESC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish',
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' 		=> 'member_last_login',
							'value' 	=> date('Ymd', strtotime('-31 days')),
							'compare' 	=> '>=',
							'type' 		=> 'CHAR',
						)
					)
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<tr class="<?php if(!get_field('member_last_login')) { echo 'warnings'; } ?>">
					<td class="tab" width="10px"><span></span></td>
					<td>
						<?php the_title(); ?>
					</td>
					<td>
						<?php the_field('email_address'); ?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_last_login');
							
							if($originalDate) {
								$newDate = date("d/m/Y H:i", strtotime($originalDate));
								echo $newDate;
							} else {
								echo '<strong>This member has not logged in.</strong>';
							}
						?>
					</td>
					<td>
						<?php
							$originalDate = get_field('member_register_date');
							
							if($originalDate) {
								$newDate = date("d/m/Y", strtotime($originalDate));
								echo $newDate;
							}
						?>
					</td>
				</tr>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</tfooter>
		</table>
	</div>
	<div id="never" class="tabcontent">
		<?php
			$args = array(
				'post_type' => 'iykaa_members',
				'orderby' => 'title',
				'order' => 'ASC',
				'posts_per_page' => -1,
				'post_parent' => 0,
				'post_status' => 'publish',
			);
			
			$memberSearch = new WP_Query($args);
			
			$count = 0;
			
			while($memberSearch->have_posts()) {
				$memberSearch->the_post();
				if(!get_field('member_last_login')) {
					$count++;
				}
			}
		?>
		<h3><?php echo $count; ?> members have not logged in before.</h3>
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_members',
					'orderby' => 'title',
					'order' => 'ASC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish',
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<?php if(!get_field('member_last_login')) { ?>
					<tr class="<?php if(!get_field('member_last_login')) { echo 'warnings'; } ?>">
						<td class="tab" width="10px"><span></span></td>
						<td>
							<?php the_title(); ?>
						</td>
						<td>
							<?php the_field('email_address'); ?>
						</td>
						<td>
							<?php
								$originalDate = get_field('member_last_login');
								
								if($originalDate) {
									$newDate = date("d/m/Y H:i", strtotime($originalDate));
									echo $newDate;
								} else {
									echo '<strong>This member has not logged in.</strong>';
								}
							?>
						</td>
						<td>
							<?php
								$originalDate = get_field('member_register_date');
								
								if($originalDate) {
									$newDate = date("d/m/Y", strtotime($originalDate));
									echo $newDate;
								}
							?>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Email Address
				</th>
				<th>
					Lasted Logged In
				</th>
				<th>
					Registration Date
				</th>
			</tfooter>
		</table>
	</div>
</div>
<script>
	function openCity(evt, cityName) {
		// Declare all variables
		var i, tabcontent, tablinks;
		
		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		
		// Get all elements with class="tablinks" and remove the class "active"
		tablinks = document.getElementsByClassName("tablinks");
		
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		
		// Show the current tab, and add an "active" class to the button that opened the tab
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>