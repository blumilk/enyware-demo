<?php
	/*
		Plugin Name: Iykaa Member Documents
		Plugin URI: http://www.blumilk.com
		Description: Add member documents.
		Author: James George Dunn
		Version: 0.1
	*/
	
	require ABSPATH . '/wp-content/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://repo.iykaa.com/iykaa-member-documents/iykaa-member-documents.json',
		__FILE__,
		'iykaa-member-documents'
	);
	
	// Registers a custom fields but requires ACF to be activated
	function iykaa_member_documents_acf_register() {
		if(function_exists('acf_add_local_field_group')) {
			acf_add_local_field_group(array(
					'key' => 'iykaa_member_documents_1',
					'title' => 'Iykaa Member Documents',
					'fields' => array(
						array(
							'key' => 'iykaa_member_documents_2',
							'label' => 'Iykaa Member Documents',
							'name' => 'iykaa_member_documents',
							'type' => 'repeater',
							'prefix' => '',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						)
					),
					'location' => array (
						array(
							array(
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'iykaa_members',
							),
						),
					),
					'menu_order' => 0,
					'position' => 'normal',
					'style' => 'default',
					'label_placement' => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen' => '',
					'active' => 1,
				)
			);
			
			acf_add_local_field(
				array(
					'key' => 'iykaa_member_documents_document_id',
					'label' => 'Document ID',
					'name' => 'document_id',
					'type' => 'text',
					'parent' => 'iykaa_member_documents_2'
				)
			);
			
			acf_add_local_field(
				array(
					'key' => 'iykaa_member_documents_document_title',
					'label' => 'Document Title',
					'name' => 'document_title',
					'type' => 'text',
					'parent' => 'iykaa_member_documents_2'
				)
			);
			
			acf_add_local_field(
				array(
					'key' => 'iykaa_member_documents_document_timestamp',
					'label' => 'Document Timestamp',
					'name' => 'document_timestamp',
					'type' => 'text',
					'parent' => 'iykaa_member_documents_2'
				)
			);
			
			acf_add_local_field(
				array(
					'key' => 'iykaa_member_documents_document_description',
					'label' => 'Document Description',
					'name' => 'document_description',
					'type' => 'textarea',
					'parent' => 'iykaa_member_documents_2'
				)
			);
			
			acf_add_local_field(
				array(
					'key' => 'iykaa_member_documents_document_url',
					'label' => 'Document URL',
					'name' => 'document_url',
					'type' => 'text',
					'parent' => 'iykaa_member_documents_2'
				)
			);
		}
	}
	add_action('init', 'iykaa_member_documents_acf_register');
	
	function iykaa_member_documents_shortcode__uploader() {
		include('templates/uploader.php');
	}
	add_shortcode('iykaa_member_documents_uploader', 'iykaa_member_documents_shortcode__uploader');
	
	function iykaa_member_documents_shortcode_add() {
		include('templates/add.php');
	}
	add_shortcode('iykaa_member_documents_add', 'iykaa_member_documents_shortcode_add');
	
	// Creates the Member Document Uploader page when the plugin is activated
	function iykaa_member_documents_add_page() {
		$pageTitle = 'Member Document Uploader';
		$pageContent = '[iykaa_member_documents_uploader]'; // Inserts the shortcode
		
		$pageCheck = get_page_by_title($pageTitle);
		
		$newPage = array(
			'post_type' 	=> 'page',
			'post_title' 	=> $pageTitle,
			'post_content' 	=> $pageContent,
			'post_status' 	=> 'publish',
			'post_author' 	=> 1,
		);
		
		if(!isset($pageCheck->ID)) { // Checks to see if the page already exists
			$newPageID = wp_insert_post($newPage);
		}
	}
	add_action('init', 'iykaa_member_documents_add_page');
?>