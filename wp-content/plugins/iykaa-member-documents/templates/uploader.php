<?php
	function upload_user_file($file = array()) {
		require_once(ABSPATH . 'wp-admin/includes/admin.php');
		
		$file_return = wp_handle_upload($file, array('test_form' => false));
		
		if(isset($file_return['error']) || isset($file_return['upload_error_handler'])) {
			return false;
		} else {
			$filename = $file_return['file'];
			
			$attachment = array(
				'post_mime_type' 	=> $file_return['type'],
				'post_title' 		=> preg_replace('/\.[^.]+$/', '', basename($filename)),
				'post_content' 		=> '',
				'post_status' 		=> 'inherit',
				'guid' 				=> $file_return['url']
			);
			
			$attachmentID = wp_insert_attachment($attachment, $file_return['url']);
			
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			
			$attachment_data = wp_generate_attachment_metadata($attachmentID, $filename);
			
			wp_update_attachment_metadata($attachmentID, $attachment_data);
			
			if(0 < intval($attachmentID)) {
				return $attachmentID;
			}
		}
		
		return false;
	}
?>

<div class="wrapper page-content">
	<div class="row">
		<div class="dt-12">
			<a href="<?php echo home_url(); ?>/member-document-uploader">
				<h1>Document Uploader</h1>
			</a>
			<hr class="secondary">
			<?php if(empty($_FILES)) { ?>
				<form action="" method="post" enctype="multipart/form-data">
					<script>
						function showHint(str) {
							if(str.length == 0) {
								document.getElementById("get-user").innerHTML = "";
								return;
							} else {
								var xmlhttp = new XMLHttpRequest();
								xmlhttp.onreadystatechange = function() {
									if(this.readyState == 4 && this.status == 200) {
										document.getElementById("get-user").innerHTML = this.responseText;
									}
								};
								
								xmlhttp.open("GET", "<?php echo home_url(); ?>/user-list?resource-url=<?php the_permalink(); ?>&resource-name=<?php the_title(); ?>&keywords=" + str, true);
								xmlhttp.send();
							}
						}
					</script>
					
					<div class="row">
						<div class="tp-12">
							<section class="get-user document">
								<div class="wrapper no-gaps nested">
									<div class="row">
										<div class="mp-12">
											<input type="text" onkeyup="showHint(this.value)" placeholder="Search for a member" autocomplete="off">
											<div>
												<ul id="search">
													<select id="get-user" name="member">
													</select>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div class="tp-12">
							<input type="file" name="member-document" id="member-document">
							<div class="row">
								<div class="tp-4">
									<label>Document Title</label>
									<input type="text" name="document-title" placeholder="Document Title">
								</div>
								<div class="tp-4">
									<label>Document Timestamp</label>
									<input type="text" name="document-timestamp" placeholder="Document Timestamp" value="<?php echo date('d/m/Y - H:i:s'); ?>">
								</div>
								<div class="tp-4">
									<label>Notify the member by email?</label>
									<select name="notify">
										<option value="no">No</option>
										<option value="yes">Yes</option>
									</select>
								</div>
							</div>
							<label>Document Description</label>
							<textarea name="document-description" placeholder="Document Description" rows="6"></textarea>
						</div>
					</div>
					<input type="submit" value="Upload File" name="submit">
				</form>
			<?php } ?>
			
			<?php
				if(count($_POST) > 0) {
					if(!$_FILES['member-document']['error'] > 0) {
						foreach($_FILES as $file) {
							if(is_array($file)) {
								$attachmentID = upload_user_file($file);
							}
						}
						
						$attachmentURL = wp_get_attachment_url($attachmentID);
						
						echo '<h4>Your file has been uploaded.</h4>';
					} else {
						
					}
				}
				
				if(count($_POST) > 0) {
					
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					
					if(isset($_POST['member'])) {
						$args = array(
							'post_type' => 'iykaa_members',
							'orderby' => 'modified',
							'order' => 'DESC',
							'posts_per_page' => -1,
							'post_parent' => 0,
							'post_status' => 'publish',
							'paged' => $paged,
							'p' => $_REQUEST['member'],
						);
					} else {
						$args = array(
							'post_type' => 'iykaa_members',
							'orderby' => 'modified',
							'order' => 'DESC',
							'posts_per_page' => -1,
							'post_parent' => 0,
							'post_status' => 'publish',
							'paged' => $paged,
						);
					}
				} else {
					$args = array(
						'post_type' => 'iykaa_members',
						'orderby' => 'modified',
						'order' => 'DESC',
						'posts_per_page' => -1,
						'post_parent' => 0,
						'post_status' => 'publish',
					);
				}
				
				if(count($_POST) > 0) {
					$memberSearch = new WP_Query($args);
					
					if($memberSearch->have_posts()) {
						while($memberSearch->have_posts()) {
							$memberSearch->the_post();
							
							$notifyMember = '';
							
							if(empty($_POST['document-title'])) {
								$documentTitle = get_the_title($attachmentID);
							} else {
								$documentTitle = $_POST['document-title'];
							}
							
							if(empty($_POST['document-timestamp'])) {
								$documentTimestamp = date('d/m/Y - H:i:s');
							} else {
								$documentTimestamp = $_POST['document-timestamp'];
							}
							
							$documentDescription = $_POST['document-description'];
							
							// Emails the member an notification about the document upload
							if($_POST['notify'] == 'yes') {
								while(have_rows('email_addresses')) {
									the_row();
									$notifyMember .= get_sub_field('email_address') . ',';
								}
								
								$message = 'You have a new document assigned to you.<br /><br /><hr><strong>' . $documentTitle . '</strong><br />' . $documentDescription . '<hr><br /><a href="' . get_permalink() . '">View it now</a>.';
								
								wp_mail($notifyMember, 'You have a new document assigned to you.', $message);
							}
							
							if(count($_POST) > 0 && !$_FILES['member-document']['error'] > 0) {
								$fileCount = count(get_field('iykaa_member_documents_2'));
								$key = 'iykaa_member_documents_2';
								$value = array(
									'iykaa_member_documents_document_id'			=> $attachmentID,
									'iykaa_member_documents_document_title' 		=> $documentTitle,
									'iykaa_member_documents_document_timestamp' 	=> $documentTimestamp,
									'iykaa_member_documents_document_description' 	=> $documentDescription,
									'iykaa_member_documents_document_url'			=> $attachmentURL,
								);
								
								update_row(
									$key,
									$fileCount + 1,
									$value
								);
							}
						}
					}
				}
			?>
		</div>
	</div>
</div>