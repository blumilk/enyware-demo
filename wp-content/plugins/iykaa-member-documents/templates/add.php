<?php
	$currentID = get_the_ID();
	
	if(count($_GET) > 0) {
		include(get_template_directory() . '/includes/config/member-details.php');
		
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberID = get_the_ID();
		}
		
		$currentReadLater = get_field('iykaa_read_later_ids', $memberID);
		
		$readLaterPieces = explode(' ', $currentReadLater);
		
		if(!in_array($currentID, $readLaterPieces)) {
			$readLaterUpdate = $currentID . ' ' . $currentReadLater;
			
			update_field('iykaa_read_later_ids', $readLaterUpdate, $memberID);
			
			echo '<a class="cta primary size-s" href="' . home_url() . '/read-later">';
			echo 'Success! View your Read Later List';
			echo '</a>';
		} else {
			echo '<a class="cta primary size-s" href="' . home_url() . '/read-later">';
			echo 'This is already saved in your Read Later list';
			echo '</a>';
		}
		
		wp_reset_query();
	} else {
		echo '<a class="cta secondary size-s" href="?pageID=' . $currentID . '">';
		echo 'Add Read Later List';
		echo '</a>';
	}
?>