<?php
	/*
		Plugin Name: Iykaa Plugin Experiments
		Plugin URI: http://www.iykaa.com
		Description: A sandbox to play and experiment with plugins.
		Author: James George Dunn
		Version: 2.2.5
	*/
	
	require ABSPATH . '/wp-content/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'http://repo.iykaa.com/iykaa-plugin-experiments/iykaa-plugin-experiments.json',
		__FILE__,
		'iykaa-plugin-experiments'
	);
?>