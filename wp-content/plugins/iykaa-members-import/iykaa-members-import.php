<?php
	/*
		Plugin Name: Iykaa Members Import
		Plugin URI: http://www.blumilk.com
		Description: Provides simple front end importing of members
		Version: 1.0
		Author: Blumilk
		Author URI: http://www.blumilk.com
	*/
	
	function member_import_admin_actions() {
		add_menu_page('Member Importer', 'Member Importer', 'manage_options', 'iykaa-member-importer', 'member_import_admin', 'dashicons-groups');
	}
	add_action('admin_menu', 'member_import_admin_actions');
	
	function member_import_admin() {
		include('iykaa-import-admin.php');
	}
	
	function iykaa_parse_member_import_function($postsPage) {
		$sendCount = 0;
		
		if($postsPage > 200) {
			$postOffset = $postsPage - 200;
		} else {
			$postOffset = 0;
		}
		
		$args = array(
			'post_type' => 'iykaa_members',
			'orderby' => 'title',
			'order' => 'ASC',
			'posts_per_page' => 200,
			'post_parent' => 0,
			'post_status' => array(
				'pending',
				'draft',
				'publish'
			),
			'offset' => $postOffset,
		);
		
		$memberJoin = new WP_Query($args);
		
		while($memberJoin->have_posts()) {
			$memberJoin->the_post();
			$emailSent = get_field('email_schedule_sent');
			
			if($emailSent != 'true' && !empty($emailSent)) {
				$memberEmail = get_field('email_address');
				$signupSubject = get_field('member_signup_subject', 'options');
				$signupSubject = str_replace('[company-name]', get_bloginfo('name'), $signupSubject);
				$signupSubject = str_replace('[full-name]', get_field('member_name'), $signupSubject);
				$signupSubject = str_replace('[forename]', get_field('member_forename'), $signupSubject);
				$signupSubject = str_replace('[surname]', get_field('member_surname'), $signupSubject);
				$signupMessage = get_field('member_signup_message', 'options');
				$signupMessage = str_replace('[company-name]', get_bloginfo('name'), $signupMessage);
				$signupMessage = str_replace('[full-name]', get_field('member_name'), $signupMessage);
				$signupMessage = str_replace('[forename]', get_field('member_forename'), $signupMessage);
				$signupMessage = str_replace('[surname]', get_field('member_surname'), $signupMessage);
				$signupMessage .= '<br />Your username is <b>' . $memberEmail . '</b>';
				$signupMessage .= '<br />Your password is <b>' . get_field('password_temp') . '</b>';
				$signupMessage .= '<br />Login to your account by visiting here <b>' . home_url() . '/?email=' . $memberEmail . '</b>';
				
				update_field('password_temp', '');
				update_field('email_schedule_sent', 'true');
				
				wp_mail($memberEmail, $signupSubject, $signupMessage);
				
				// Updates the post status so the member will appear in the member search results
				wp_update_post(
					array(
						'post_status' => 'publish'
					)
				);
			} else {
				update_field('email_schedule_sent', 'true');
			}
		}
	}
	add_action('iykaa_parse_member_import_function_execute', 'iykaa_parse_member_import_function');
	
	function iykaa_parse_member_import($parsedFile, $emailScheduleDate) {
		include 'classes/simplexlsx.class.php';
		
		$xlsx = new SimpleXLSX($parsedFile);
		$rows = $xlsx->rows();
		
		unset($rows[0]);
		
		$usersAdded = 0;
		
		$memberRoleCount = 0;
		$adminRoleCount = 0;
		
		foreach($rows as $row) {
			if(!empty($row[0])) {
				$memberEmail = $row[0];
				
				if(!username_exists($memberEmail)) {
					if(empty($row[1])) {
						$memberPassword = wp_generate_password();
					} else {
						$memberPassword = $row[1];
					}
					
					$memberForename 	= $row[2];
					$memberMiddleName 	= $row[3];
					$memberSurname 		= $row[4];
					$locations 			= $row[5];
					$positions 			= $row[6];
					$departments 		= $row[7];
					$memberMobile 		= $row[8];
					
					// Sets the members role
					$memberRole 		= $row[9];
					
					if(strcasecmp($memberRole, 'Member') == 0) {
						$setMemberRole = 'member_iykaa';
						$memberRoleCount++;
					} elseif(strcasecmp($memberRole, 'Admin') == 0) {
						$setMemberRole = 'admin_iykaa';
						$adminRoleCount++;
					} else {
						$setMemberRole = 'member_iykaa';
						$memberRoleCount++;
					}
					
					// Structures the members full name
					if(!empty($memberMiddleName)) {
						$memberFullName = trim($memberForename) . ' ' . trim($memberMiddleName) . ' ' . trim($memberSurname);
					} else {
						$memberFullName = trim($memberForename) . ' ' . trim($memberSurname);
					}
					
					$memberLogin = $memberFullName;
					
					$newMemberID = wp_insert_user(
						array(
							'user_login'		=> $memberEmail,
							'user_pass'	 		=> $memberPassword,
							'user_email'		=> $memberEmail,
							'nickname' 			=> $memberLogin,
							'user_registered'	=> date('Y-m-d H:i:s'),
							'role'				=> $setMemberRole,
							'first_name' 		=> $memberForename,
							'last_name' 		=> $memberSurname,
							'display_name' 		=> $memberFullName,
						)
					);
					
					$meta = array();
					$meta['ID'] = $newMemberID;
					$memberID = wp_update_user($meta);
					
					// Registers the member to a custom post type
					$register = array(
						'post_type' 	=> 'iykaa_members',
						'post_title' 	=> $memberLogin,
						'post_status' 	=> 'pending',
						'post_author' 	=> $newMemberID,
					);
					
					$registerDetails = wp_insert_post($register);
					
					// Custom fields
					update_post_meta($registerDetails, 'member_name', $memberLogin);
					update_post_meta($registerDetails, 'email_address', $memberEmail);
					update_post_meta($registerDetails, 'member_register_date', date('Ymd'));
					update_post_meta($registerDetails, 'member_forename', $memberForename);
					update_post_meta($registerDetails, 'member_middle_name', $memberMiddleName);
					update_post_meta($registerDetails, 'member_surname', $memberSurname);
					update_post_meta($registerDetails, 'mobile_phone', $memberMobile);
					update_post_meta($registerDetails, 'email_schedule_sent', 'false');
					
					if($emailScheduleDate != 'false') {
						$emailScheduleDateNew = strtotime($emailScheduleDate);
					} else {
						$emailScheduleDateNew = $emailScheduleDate;
					}
					update_post_meta($registerDetails, 'email_schedule', $emailScheduleDateNew);
					
					update_post_meta($registerDetails, 'password_temp', $memberPassword);
					
					// ACF
					$fieldKey = 'email_addresses';
					$value = array(
						array(
							'email_address' => $memberEmail,
							'label' => 'Work',
						)
					);
					update_field($fieldKey, $value, $registerDetails);
					
					$fieldKey = 'contact_numbers';
					$value = array(
						array(
							'contact_number' => $memberMobile,
							'label' => 'Mobile',
						)
					);
					update_field($fieldKey, $value, $registerDetails);
					
					// Taxonomies
					$locationCat = explode(', ', $locations);
					foreach($locationCat as $location) {
						wp_set_object_terms($registerDetails, $location, 'location_tax', true);
					}
					
					$positionCat = explode(', ', $positions);
					foreach($positionCat as $position) {
						wp_set_object_terms($registerDetails, $position, 'position_tax', true);
					}
					
					$departmentsCat = explode(', ', $departments);
					foreach($departmentsCat as $department) {
						wp_set_object_terms($registerDetails, $department, 'team_tax', true);
					}
					
					// Status of the member defaults to Status Not Set
					wp_set_object_terms($registerDetails, 'Status Not Set', 'status_tax', true);
					
					// Setting up the background tasks
					date_default_timezone_set('Europe/London');
					$memberCount = wp_count_posts('iykaa_members');
					$memberCount = $memberCount->publish;
					$runCount = $memberCount / 200;
					$runCount = ceil($runCount);
					
					for($notificationCounter = 1; $notificationCounter <= $runCount; $notificationCounter++) {
						$timeDelay = 60 * $notificationCounter;
						$postsPage = 200 * $notificationCounter;
						
						if($emailScheduleDateNew != 'false') {
							wp_clear_scheduled_hook('iykaa_parse_member_import_function_execute', array($postsPage));
							wp_schedule_single_event($emailScheduleDateNew, 'iykaa_parse_member_import_function_execute', array($postsPage));
						} else {
							wp_clear_scheduled_hook('iykaa_parse_member_import_function_execute', array($postsPage));
							wp_schedule_single_event(time(), 'iykaa_parse_member_import_function_execute', array($postsPage));
						}
					}
					
					$usersAdded++;
				}
			}
		}
		
		$userTotal = $usersAdded;
		
		echo '<div class="updated">';
		
		echo '<p>Members Added: ' . $memberRoleCount . '</p>';
		echo '<p>Admins Added: ' . $adminRoleCount . '</p>';
		echo '<p>Total Added: ' . $userTotal . '</p>';
		
		echo '</div>';
	}
	add_action('iykaa_parse_member_import_execute', 'iykaa_parse_member_import');
?>