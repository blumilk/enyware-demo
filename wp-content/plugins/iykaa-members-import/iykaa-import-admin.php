<div class="wrap">
	<?php
		if(isset($_POST['iykaa_hidden']) && $_POST['iykaa_hidden'] == 'Y') {
			$allowedExtensions = array(
				'xlsx',
			);
			
			$extension = pathinfo($_FILES['upload-file']['name'], PATHINFO_EXTENSION);
			
			if(in_array($extension, $allowedExtensions)) {
				if(count($_FILES)!= 0) {
					if(!function_exists('wp_handle_upload')) {
						require_once(ABSPATH . 'wp-admin/includes/file.php');
					}
					
					$uploadedfile = $_FILES['upload-file'];
					$upload_overrides = array('test_form' => false);
					$moveFile = wp_handle_upload($uploadedfile, $upload_overrides, date('Y/m'));
					
					if(file_exists($moveFile['file'])) {
						if($_REQUEST['email-time'] !== 'false') {
							$emailScheduleDate = $_REQUEST['email-date-year'] . $_REQUEST['email-date-month'] . $_REQUEST['email-date-day'] . $_REQUEST['email-time'] . '00';
						} else {
							$emailScheduleDate = 'false';
						}
						
						$parsedFile = iykaa_parse_member_import($moveFile['file'], $emailScheduleDate);
						
						// Removes the Excel file so it doesn't fill the uploads folder
						unlink($moveFile['file']);
					}
					
					remove_filter('upload_dir', 'custom_import');
				} else {
					echo '<div class="error">';
					echo '<p>Please select a file to import.</p>';
					echo '</div>';
				}
			} else {
				echo '<div class="error">';
				echo '<p>Sorry but only the file extension .xlsx is supported at the moment.</p>';
				echo '</div>';
			}
		}
		
		echo '<h2>' . __('Member Importer', 'iykaa_member_import') . '</h2>';
	?>
	
	<form method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>" enctype="multipart/form-data">
		<input type="hidden" name="iykaa_hidden" value="Y">
		<p>
			<label for="email-date">
				Select a date and time when you would like your members to be informed about their login details.
			</label>
			<br />
			<select name="email-date-day">
				<option value="false">--Day--</option>
				<?php
					$currentDay = date('j');
					
					for($i = 1; $i <= 31; $i++) {
						if($currentDay == $i) {
							if($i < 10) {
								echo '<option value="0' . $i . '" selected>0' . $i . '</option>';
							} else {
								echo '<option value="' . $i . '" selected>' . $i . '</option>';
							}
						} else {
							if($i < 10) {
								echo '<option value="0' . $i . '">0' . $i . '</option>';
							} else {
								echo '<option value="' . $i . '">' . $i . '</option>';
							}
						}
					}
				?>
			</select>
			<select name="email-date-month">
				<option value="false">--Month--</option>
				<?php
					$currentMonth = date('n');
					
					for($i = 1; $i <= 12; $i++) {
						if($currentMonth == $i) {
							if($i < 10) {
								echo '<option value="0' . $i . '" selected>0' . $i . '</option>';
							} else {
								echo '<option value="' . $i . '" selected>' . $i . '</option>';
							}
						} else {
							if($i < 10) {
								echo '<option value="0' . $i . '">0' . $i . '</option>';
							} else {
								echo '<option value="' . $i . '">' . $i . '</option>';
							}
						}
					}
				?>
			</select>
			<select name="email-date-year">
				<option value="false">--Year--</option>
				<?php
					$currentYear = date('Y');
					
					for($i = $currentYear; $i <= $currentYear + 2; $i++) {
						if($currentYear == $i) {
							echo '<option value="' . $i . '" selected>' . $i . '</option>';
						} else {
							echo '<option value="' . $i . '">' . $i . '</option>';
						}
					}
				?>
			</select>
			<br /><br />
			<label for="email-date">
				Leave the time blank if you would just like to send the email notifications now.
			</label>
			<br />
			<select id="email-time" name="email-time">
				<option value="false">SEND NOW</option>
				<?php
					// Set 24 hour format between 0 and 23
					$startTime 			= 6;
					$endTime 			= 20;
					// Minute start time. Default is just 00
					$minuteStart 		= 0;
					$minuteDifference 	= 15;
					$minuteGap 			= 60 / $minuteDifference;
					$finalTime 			= '';
					
					for($i = $startTime; $i <= $endTime; $i++) {
						for($j = 0; $j < $minuteGap; $j++) {
							if($i < 10) {
								$finalTime .= '0' . $i;
							} else {
								$finalTime .= $i;
							}
							
							$finalTime .= ':';
							
							if($minuteStart <= 0) {
								$finalTime .= $minuteStart . '0';
								$minuteStart = $minuteStart + $minuteDifference;
							} else {
								if($minuteStart < 60) {
									$finalTime .= $minuteStart;
									$minuteStart = $minuteStart + $minuteDifference;
								} else {
									$minuteStart = 0;
									$finalTime .= $minuteStart . '0';
									$minuteStart = $minuteStart + $minuteDifference;
								}
							}
							
							echo '<option value="' . str_replace(':', '', $finalTime) . '">' . $finalTime . '</option>';
							$finalTime = '';
						}
					}
				?>
			</select>
			<br /><br />
			<label for="upload-file">
				Select a file to upload. Only the file extension .xlsx is supported at the moment.
			</label>
			<br />
			<input type="file" id="upload-file" name="upload-file" value="">
			<?php wp_nonce_field(plugin_basename(__FILE__), 'example-jpg-nonce'); ?>
		</p>
		<input type="submit" name="Submit" class="button button-primary button-large" value="<?php _e('Start Import', 'iykaa_member_import'); ?>">
	</form>
</div>