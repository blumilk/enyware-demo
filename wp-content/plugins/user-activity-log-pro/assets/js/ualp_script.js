jQuery(window).load(function() {
    jQuery('#subscribe_thickbox').trigger('click');
    jQuery("#TB_closeWindowButton").click(function() {
        jQuery.post(ajaxurl, {
            'action': 'close_tab'
        });
    });
});
jQuery(window).resize(function(){
    var windowWidth = jQuery(window).width();
    var responsive = false;
    var title;
    if (windowWidth < 783) {
        responsive = true;
    }
    jQuery('#the-list tr').each(function() {
        title = jQuery(this).find('td.title.column-title').text();
        if(responsive == true) {
            jQuery(this).find('td.check').find('span').remove();
            jQuery(this).find('td.check input').after('<span>'+title+'</span>');
        } else {
            jQuery(this).find('td.check').find('span').remove();
        }
    });
});
jQuery(document).ready(function() {
    jQuery('script').each(function () {
        var src = jQuery(this).attr('src');
        if (typeof src !== typeof undefined && src !== false) {
            if (src.search('bootstrap.js') !== -1 || src.search('bootstrap.min.js') !== -1) {
                var bootstrapButton = jQuery.fn.button.noConflict();
                jQuery.fn.bootstrapBtn = bootstrapButton;
            }
        }
    });
    jQuery(".user-activity-log_page_ualp_settings #dialogPassword #deleteLogPassword").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
            validPassword();
        }
    });
    jQuery(".toplevel_page_ual_pro #dialogPassword #deleteLogPassword").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
            validBulkPassword();
        }
    });
    jQuery(".toplevel_page_ual_pro #dialogSinglePassword #deleteSingleLogPassword").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
            validSinglePassword();
        }
    });

    jQuery('.reset_delete_log_passwd').click(function () {
        if (!confirm(ualpJSObject.reset_confirm)) {
            return false;
        }
        var btn = this;
        setTimeout(function () { jQuery(btn).attr('disabled', 'disabled'); }, 1);
        return true;
    });

    jQuery('.ualp_view').on("click", ualpViewAjaxCall);
    var ualp_detail_view = jQuery(".ualp_view_details_ajax").dialog({
        resizable: false,
        autoOpen: false,
        height: "auto",
        width: '600', // overcomes width:'auto' and maxWidth bug
        maxWidth: '100%',
        modal: true,
        draggable: false,
        dialogClass: 'ualp-view-ui-dialog',
        hide: {
            effect: "fadeOut",
            duration: 500
        },
        close: function() {
            ualp_detail_view.dialog("close");
        }
    });
    jQuery('.ualpFavorite').click(function() {
        var selected = 'yes';
        var logID = jQuery(this).attr('id');
        if (jQuery(this).hasClass('selected')) {
            jQuery(this).removeClass('selected');
            selected = 'no';
            jQuery(this).attr('title', 'No');
        } else {
            jQuery(this).addClass('selected');
            jQuery(this).attr('title', 'Yes');
        }
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'ualpFavoriteLog',
                post: logID,
                featured: selected
            },
            success: function(data) {
                location.reload();
            },
        });
    });
    var windowWidth = jQuery(window).width();
    var responsive = false;
    if (windowWidth < 783) {
        responsive  = true;
    }
    var title;
    jQuery('#the-list tr').each(function() {
        title = jQuery(this).find('td.title.column-title').text();
        if(responsive == true) {
            jQuery(this).find('td.check').find('span').remove();
            jQuery(this).find('td.check input').after('<span>'+title+'</span>');
        } else {
            jQuery(this).find('td.check').find('span').remove();
        }
    });

    ualpHookGroup('post_type');
    ualpHookGroup('taxonomy');
    ualpHookGroup('plugin');
    ualpHookGroup('contactForm');
    ualpHookGroup('ualp');
    ualpHookGroup('users');
    ualpHookGroup('theme');
    ualpHookGroup('comment');
    ualpHookGroup('menu');
    ualpHookGroup('view');
    ualpHookGroup('other');
    jQuery('#ualpHookSettings tr.post_type input[type="checkbox"]').click(function() {
        ualpHookGroup('post_type');
    });
    jQuery('#ualpHookSettings tr.taxonomy input[type="checkbox"]').click(function() {
        ualpHookGroup('taxonomy');
    });
    jQuery('#ualpHookSettings tr.plugin input[type="checkbox"]').click(function() {
        ualpHookGroup('plugin');
    });
    jQuery('#ualpHookSettings tr.contactForm input[type="checkbox"]').click(function() {
        ualpHookGroup('contactForm');
    });
    jQuery('#ualpHookSettings tr.ualp input[type="checkbox"]').click(function() {
        ualpHookGroup('ualp');
    });
    jQuery('#ualpHookSettings tr.users input[type="checkbox"]').click(function() {
        ualpHookGroup('users');
    });
    jQuery('#ualpHookSettings tr.theme input[type="checkbox"]').click(function() {
        ualpHookGroup('theme');
    });
    jQuery('#ualpHookSettings tr.comment input[type="checkbox"]').click(function() {
        ualpHookGroup('comment');
    });
    jQuery('#ualpHookSettings tr.menu input[type="checkbox"]').click(function() {
        ualpHookGroup('menu');
    });
    jQuery('#ualpHookSettings tr.view input[type="checkbox"]').click(function() {
        ualpHookGroup('view');
    });
    jQuery('#ualpHookSettings tr.other input[type="checkbox"]').click(function() {
        ualpHookGroup('other');
    });
    jQuery('#ualpHookSettings tr.groupHeader input[type="checkbox"]').click(function() {
        var hookGroup = jQuery(this).attr('data-hookGroup');
        if (jQuery(this).is(':checked')) {
            jQuery('#ualpHookSettings .' + hookGroup + ' input[type="checkbox"]').prop('checked', true);
        } else {
            jQuery('#ualpHookSettings .' + hookGroup + ' input[type="checkbox"]').prop('checked', false);
        }
    });
    jQuery('#ualpHookSettings input[name="hook[]"]').click(function() {
        var object_type = jQuery(this).attr('data-type');
        if (!jQuery(this).is(':checked')) {
            alert(ualpJSObject.note_for_uncheck+' ' + object_type);
        }
    });
    jQuery('#activity-filter #check input[type="checkbox"]').click(function() {
        if (jQuery(this).is(':checked')) {
            jQuery('#activity-filter input[type="checkbox"]').prop('checked', true);
        } else {
            jQuery('#activity-filter input[type="checkbox"]').prop('checked', false);
        }
    });
    var Count = 0;
    var Check = 0;
    var Uncheck = 0;
    jQuery('.toplevel_page_ual_pro #activity-filter input[name="chkual[]"]').each(function() {
        Count++;
        if (jQuery(this).is(':checked')) {
            Check++;
        } else {
            Uncheck++;
        }
    });
    jQuery('.toplevel_page_ual_pro #activity-filter input[name="chkual[]"]').click(function() {
        var Count = 0
        var Check = 0;
        var Uncheck = 0;
        jQuery('.toplevel_page_ual_pro #activity-filter input[name="chkual[]"]').each(function() {
            Count++;
            if (jQuery(this).is(':checked')) {
                Check++;
            } else {
                Uncheck++;
            }
        });
        if (Count == Check) {
            jQuery('#activity-filter th.column-check input[type="checkbox"]').prop('checked', true);
        } else {
            jQuery('#activity-filter th.column-check input[type="checkbox"]').prop('checked', false);
        }
    });
    //settings tab script
    if (window.localStorage.getItem("lasttab") == null ||
        (window.localStorage.getItem("lasttab") != 'ualpGeneralSettings' &&
            window.localStorage.getItem("lasttab") != 'ualpUsersSettings' &&
            window.localStorage.getItem("lasttab") != 'ualpEmailSettings' &&
            window.localStorage.getItem("lasttab") != 'ualpPwdSettings' &&
            window.localStorage.getItem("lasttab") != 'ualpCustomEventSettings' &&
            window.localStorage.getItem("lasttab") != 'ualpHookSettings')) {
        window.localStorage.setItem("lasttab", 'ualpPwdSettings');
        jQuery('.ualpParentTabs .nav-tab-wrapper a.nav-tab').removeClass('nav-tab-active');
        jQuery('.ualpParentTabs .ualpPwdSettings').addClass('nav-tab-active');
        jQuery('.ualpContentDiv').hide();
        jQuery('#ualpPwdSettings').show();
    } else {
        jQuery('.ualpParentTabs .nav-tab-wrapper a').removeClass('nav-tab-active');
        jQuery('.' + window.localStorage.getItem("lasttab")).addClass('nav-tab-active');
        jQuery('.ualpContentDiv').hide();
        jQuery('#' + window.localStorage.getItem("lasttab")).show();
    }
    jQuery('.ualpParentTabs .nav-tab-wrapper a').click(function(e) {
        e.preventDefault();
        jQuery('.ualpAdminNotice.is-dismissible').hide();
        var this_tab = jQuery(this);
        var data_href = jQuery(this).attr('data-href');
        jQuery('.ualpContentDiv').hide();
        jQuery('#' + data_href).show();
        jQuery('.nav-tab-wrapper a.nav-tab').removeClass('nav-tab-active');
        this_tab.addClass('nav-tab-active');
        if (window.localStorage) {
            window.localStorage.setItem("lasttab", data_href);
        }
    });
    // Enable email notification start
    if (jQuery('.ualpSettingsForm input[name="rdoualpEnableEmail"]:checked').val() == 0) {
        jQuery('.emailNotificationCover .ui-button.ui-corner-right').addClass('active');
        jQuery('.emailNotificationCover .ui-button.ui-corner-left').removeClass('active');
        jQuery('.ualpSettingsForm .fromEmailTr,.ualpSettingsForm .toEmailTr,.ualpSettingsForm .messageTr,div.selectUserForEmail').hide();
    } else {
        jQuery('.emailNotificationCover .ui-button.ui-corner-left').addClass('active');
        jQuery('.emailNotificationCover .ui-button.ui-corner-right').removeClass('active');
        jQuery('.ualpSettingsForm .fromEmailTr,.ualpSettingsForm .toEmailTr,.ualpSettingsForm .messageTr,div.selectUserForEmail').show();
    }
    jQuery('.ualpSettingsForm input[name="rdoualpEnableEmail"]').click(function() {
        if (jQuery('.ualpSettingsForm input[name="rdoualpEnableEmail"]:checked').val() == 0) {
            jQuery('.emailNotificationCover .ui-button.ui-corner-right').addClass('active');
            jQuery('.emailNotificationCover .ui-button.ui-corner-left').removeClass('active');
            jQuery('.ualpSettingsForm .fromEmailTr,.ualpSettingsForm .toEmailTr,.ualpSettingsForm .messageTr,div.selectUserForEmail').hide();
        } else {
            jQuery('.emailNotificationCover .ui-button.ui-corner-left').addClass('active');
            jQuery('.emailNotificationCover .ui-button.ui-corner-right').removeClass('active');
            jQuery('.ualpSettingsForm .fromEmailTr,.ualpSettingsForm .toEmailTr,.ualpSettingsForm .messageTr,div.selectUserForEmail').show();
        }
    });
    // Enable email notification end
    // For delete all the logs
    var pwddialog, downloadDialogbox, downloadFormat, password;
    jQuery('.pwdErrorMsg').text('');
    jQuery('#setDeleteDataPassword').on('click', function(e) {
        e.preventDefault();
        setLogDeletePassword();
    });
    jQuery('#deleteLogData').on('click', function(e) {
        e.preventDefault();
        passwordDialog();
    });
    function downloadLogFile() {
        jQuery('.pwdErrorMsg').text('');
        var downloadLogs = false;
        var sendLog = false;
        downloadFormat = jQuery('input[name="downloadLogs"]:checked').val();
        if (jQuery('input[name="downloadLogs"]').prop("checked")) {
            downloadLogs = true;
        }
        if (jQuery('input[name="sendLog"]').prop("checked")) {
            sendLog = true;
        }
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'downloadLogFile',
                downloadLogs: downloadLogs,
                sendLog: sendLog
            },
            success: function(data) {
                if (data == '') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.select_option_delete_log);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogs') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_success);
                    location.reload();
                }
                if (data == 'mailnotsent') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.email_send_error);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogsmailnotsent') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_error_email);
                    location.reload();
                }
                if (data == 'sendLog') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.email_send_and_log_delete);
                    location.reload();
                }
                if (data == 'downloadLogssendLog') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_send_email);
                    location.reload();
                }
            },
        });
    }
    function validPassword() {
        jQuery('.pwdErrorMsg').text('');
        password = jQuery('#deleteLogPassword').val();
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'compareLogPassword',
                logPwd: password
            },
            success: function(data) {
                if (data == 'valid') {
                    downloadDialog();
                    pwddialog.dialog("close");
                } else {
                    jQuery('.pwdErrorMsg').text(ualpJSObject.enter_valid_pwd);
                }
            },
        });
    }
    function downloadDialog() {
        jQuery('.pwdErrorMsg').text('');
        downloadDialogbox = jQuery("#dialogDownload").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                       downloadLogFile();
                        downloadDialogbox.dialog("close");
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        downloadDialogbox.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                downloadDialogbox.dialog("close");
            }
        });
    }
    function passwordDialog() {
        jQuery('.pwdErrorMsg').text('');
        pwddialog = jQuery("#dialogPassword").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                        validPassword();
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        pwddialog.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                pwddialog.dialog("close");
            }
        });
    }
    jQuery('#setDeletePassword a').click(function(e){
        window.localStorage.setItem("lasttab",'ualpPwdSettings');
    });
    jQuery('.setDeletePassword').on('click', function(e) {
        e.preventDefault();
        setLogDeletePassword();
    });
    jQuery('.deleteSingleLog').on('click', function(e) {
        e.preventDefault();
        passwordSingleDialog();
        jQuery('.deleteSingleLog').removeClass('currentlyDelete');
        jQuery(this).addClass('currentlyDelete');
    });
    function setLogDeletePassword() {
        var setpwddialog;
        setpwddialog = jQuery("#setDeletePassword").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            close: function() {
                setpwddialog.dialog("close");
            }
        });
    }
    function downloadSingleLogFile() {
        jQuery('.pwdErrorMsg').text('');
        jQuery('#deleteLogPassword').val('');
        var downloadLogs = false;
        var sendLog = false;
        downloadFormat = jQuery('input[name="downloadLogs"]:checked').val();
        if (jQuery('input[name="downloadLogs"]').prop("checked")) {
            downloadLogs = true;
        }
        if (jQuery('input[name="sendLog"]').prop("checked")) {
            sendLog = true;
        }
        var logid = jQuery('.currentlyDelete').attr('data-id');
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'downloadSingleLog',
                downloadLogs: downloadLogs,
                sendLog: sendLog,
                logid: logid
            },
            success: function(data) {
                if (data == '') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.select_option_for_delete_log);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogs') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_log_delete_success);
                    location.reload();
                }
                if (data == 'mailnotsent') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.email_send_error);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogsmailnotsent') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_error_email);
                    location.reload();
                }
                if (data == 'sendLog') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.email_send_and_delete_success);
                    location.reload();
                }
                if (data == 'downloadLogssendLog') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_email_send_and_success_delete_log);
                    location.reload();
                }
            },
        });
    }
    function validSinglePassword() {
        jQuery('.pwdErrorMsg').text('');
        password = jQuery('#deleteSingleLogPassword').val();
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'compareLogPassword',
                logPwd: password
            },
            success: function(data) {
                if (data == 'valid') {
                    downloadSingleDialog();
                    pwddialog.dialog("close");
                } else {
                    jQuery('.pwdErrorMsg').text(ualpJSObject.enter_valid_pwd);
                }
            },
        });
    }
    function downloadSingleDialog() {
        jQuery('.pwdErrorMsg').text('');
        downloadDialogbox = jQuery("#dialogDownload").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                       downloadSingleLogFile();
                       downloadDialogbox.dialog("close");
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        downloadDialogbox.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                downloadDialogbox.dialog("close");
            }
        });
    }
    function passwordSingleDialog() {
        jQuery('.pwdErrorMsg').text('');
        pwddialog = jQuery("#dialogSinglePassword").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                        validSinglePassword();
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        pwddialog.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                pwddialog.dialog("close");
            }
        });
    }
    jQuery('.frm-user-activity input[name="bulk_action"],#activity-filter input[name="bulk_action"]').on('click', function(e) {
        var i = 0;
        var logid = [];
        jQuery('.frm-user-activity input.cb-bulk-delete:checked,#activity-filter td.column-check input:checked').each(function() {
            logid[i++] = jQuery(this).val();
        });
        if (jQuery('.deleteBulkLog').val() == 'delete') {
            e.preventDefault();
            if (logid.length <= 0) {
                alert(ualpJSObject.log_delete);
            } else {
                passwordBulkDialog();
            }
        }
        if (jQuery('.setBulkDeletePassword').val() == 'delete') {
            e.preventDefault();
            setLogDeletePassword();
        }
    });
    function downloadBulkLogFile() {
        jQuery('.pwdErrorMsg').text('');
        jQuery('#deleteLogPassword').val('');
        var downloadLogs = false;
        var sendLog = false;
        downloadFormat = jQuery('input[name="downloadLogs"]:checked').val();
        if (jQuery('input[name="downloadLogs"]').prop("checked")) {
            downloadLogs = true;
        }
        if (jQuery('input[name="sendLog"]').prop("checked")) {
            sendLog = true;
        }
        var i = 0;
        var logid = [];
        jQuery('.toplevel_page_ual_pro #activity-filter input[name="chkual[]"]:checked').each(function() {
            logid[i++] = jQuery(this).val();
        });
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'downloadBulkLog',
                downloadLogs: downloadLogs,
                sendLog: sendLog,
                logid: logid
            },
            success: function(data) {
                if (data == '') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.select_option_for_delete_log);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogs') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_log_delete_success);
                    location.reload();
                }
                if (data == 'mailnotsent') {
                    jQuery('#dialogDownload p.pwdErrorMsg').html(ualpJSObject.email_send_error);
                    downloadDialogbox.dialog("open");
                }
                if (data == 'downloadLogsmailnotsent') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.file_save_and_error_email);
                    location.reload();
                }
                if (data == 'sendLog') {
                    downloadDialogbox.dialog("close");
                    alert(ualpJSObject.email_send_and_delete_success);
                    location.reload();
                }
                if (data == 'downloadLogssendLog') {
                    alert(ualpJSObject.file_save_email_send_and_success_delete_log);
                    location.reload();
                }
            },
        });
    }
    function validBulkPassword() {
        jQuery('.pwdErrorMsg').text('');
        password = jQuery('#deleteLogPassword').val();
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'compareLogPassword',
                logPwd: password
            },
            success: function(data) {
                if (data == 'valid') {
                    downloadBulkDialog();
                    pwddialog.dialog("close");
                } else {
                    jQuery('.pwdErrorMsg').text(ualpJSObject.enter_valid_pwd);
                }
            },
        });
    }
    function downloadBulkDialog() {
        jQuery('.pwdErrorMsg').text('');
        downloadDialogbox = jQuery("#dialogDownload").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                        downloadBulkLogFile();
                        downloadDialogbox.dialog("close");
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        downloadDialogbox.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                downloadDialogbox.dialog("close");
            }
        });
    }
    function passwordBulkDialog() {
        jQuery('.pwdErrorMsg').text('');
        pwddialog = jQuery("#dialogPassword").dialog({
            resizable: true,
            height: "auto",
            width: 400,
            modal: true,
            draggable: false,
            buttons: [
                {
                    text: ualpJSObject.submit,
                    click: function() {
                        validBulkPassword();
                    }
                    },{
                    text: ualpJSObject.cancel,
                    click: function() {
                        pwddialog.dialog("close");
                    }
                }
            ],
            open: function() {
                jQuery('.ui-dialog-buttonpane').find('button:contains("Submit")').addClass('SubmitButtonClass');
            },
            close: function() {
                pwddialog.dialog("close");
            }
        });
    }
    jQuery('body #ualpPwdSettings').on('keyup', 'input[name=password], input[name=repeatPassword]', function() {
        var size = jQuery(this).val();
        size = size.length;
        if (size > 0) {
            ualpChkPwdStrength(
                jQuery('input[name=password]'),
                jQuery('input[name=repeatPassword]'),
                jQuery('#pass-strength-result'),
                jQuery('input[name=btnSetPassword]'), [adminPwd] // blacklisted words which should not be a part of the password
            );
        }
        if (size < 1) {
            jQuery('#pass-strength-result').hide();
        } else {
            jQuery('#pass-strength-result').show();
        }
    });
    jQuery('body #ualpPwdSettings').on('keyup', 'input[name=newPassword], input[name=repeatNewPassword]', function() {
        var size = jQuery(this).val();
        size = size.length;
        if (size > 0) {
            ualpChkPwdStrength(
                jQuery('input[name=newPassword]'),
                jQuery('input[name=repeatNewPassword]'),
                jQuery('#change-pass-strength-result'),
                jQuery('input[name=btnSetPassword]'), [adminPwd, currentpwd] // blacklisted words which should not be a part of the password
            );
        }
        if (size < 1) {
            jQuery('#change-pass-strength-result').hide();
        } else {
            jQuery('#change-pass-strength-result').show();
        }
    });
});
function ualpViewAjaxCall() {
    var logID = jQuery(this).attr('data-view');
    jQuery.ajax({
        url: ajaxurl,
        data: {
            action: 'viewDetailLog',
            logID: logID,
        },
        method: 'POST',
        beforeSend: function() {
            jQuery(".ualp_view_details_ajax").dialog('open');
        },
        success: function(data) {
            jQuery('.ualp_view_details_ajax').html(data);
        }
    });
}
function ualpChkPwdStrength($pwd, $confirmPwd, $strengthStatus, $submitBtn, blacklistedWords) {
    var pwd = $pwd.val();
    var confirmPwd = $confirmPwd.val();
    blacklistedWords = blacklistedWords.concat(wp.passwordStrength.userInputBlacklist())
    $strengthStatus.removeClass('short bad good strong');
    var pwdStrength = wp.passwordStrength.meter(pwd, blacklistedWords, confirmPwd);
    switch (pwdStrength) {
        case 2:
            $submitBtn.attr('disabled', 'disabled');
            $strengthStatus.addClass('bad').html('bad');
            break;
        case 3:
            $submitBtn.attr('disabled', 'disabled');
            $strengthStatus.addClass('good').html('good');
            break;
        case 4:
            $submitBtn.removeAttr('disabled');
            $strengthStatus.addClass('strong').html('strong');
            break;
        case 5:
            $submitBtn.attr('disabled', 'disabled');
            $strengthStatus.addClass('short').html('mismatch');
            break;
        default:
            $submitBtn.attr('disabled', 'disabled');
            $strengthStatus.addClass('short').html('not valid');
    }
}
function ualpHookGroup(hook) {
    var taxonomyCount = 0;
    var taxonomyCheck = 0;
    var taxonomyUncheck = 0;
    jQuery('#ualpHookSettings tr.' + hook + ' input[type="checkbox"]').each(function() {
        taxonomyCount++;
        if (jQuery(this).is(':checked')) {
            taxonomyCheck++;
        } else {
            taxonomyUncheck++;
        }
    });
    if (taxonomyCount == taxonomyCheck) {
        jQuery('#ualpHookSettings input[data-hookGroup="' + hook + '"]').prop('checked', true);
    } else {
        jQuery('#ualpHookSettings input[data-hookGroup="' + hook + '"]').prop('checked', false);
    }
}