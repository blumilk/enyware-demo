<?php
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

if (!class_exists('WP_List_Table'))
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

class UALP_Admin_Ui {

    /**
     * @var UALP_Activity_Log_List_Table
     */
    protected $_list_table = null;
    protected $_screens = array();

    public function create_admin_menu() {
        
        $ualpRoleAccessList = get_option('ualpRoleAccessList');
        $currentUser = wp_get_current_user();
        $currentUserEmail = $currentUser->roles[0];
        if(sizeof($ualpRoleAccessList) > 1){
            foreach ($ualpRoleAccessList as $ualpRole){
                if($currentUserEmail == $ualpRole){
                    $this->_screens['main'] = add_menu_page(__('User Activity Log', 'user_activity_log_pro'), __('User Activity Log', 'user_activity_log_pro'), $ualpRole, 'ual_pro', array(&$this, 'ualp_log_page_func'), UALP_PLUGIN_URL.'/assets/images/menu-icon.png');
                    add_submenu_page('ual_pro', __('Settings | User Activity Log', 'user_activity_log_pro'), __('Settings', 'user_activity_log_pro'), $ualpRole, 'ualp_settings', 'ualpSettingsPanel');        
                    add_action('load-' . $this->_screens['main'], array(&$this, 'get_list_table'));
                }
            }
        }else{
            if($currentUserEmail == 'administrator'){
                $this->_screens['main'] = add_menu_page(__('User Activity Log', 'user_activity_log_pro'), __('User Activity Log', 'user_activity_log_pro'), 'manage_options', 'ual_pro', array(&$this, 'ualp_log_page_func'), UALP_PLUGIN_URL.'/assets/images/menu-icon.png');
                add_submenu_page('ual_pro', __('Settings | User Activity Log', 'user_activity_log_pro'), __('Settings', 'user_activity_log_pro'), 'manage_options', 'ualp_settings', 'ualpSettingsPanel');        
                add_action('load-' . $this->_screens['main'], array(&$this, 'get_list_table'));
            }
        }
    }

    public function ualp_log_page_func() {
        $this->get_list_table()->prepare_items();
        ?>
        <div class="wrap">
            <h1><?php _e('User Activity Log', 'user_activity_log_pro'); ?></h1>
            <form id="activity-filter" method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>">
                <input type="hidden" name="page" value="<?php echo esc_attr($_REQUEST['page']); ?>" />
                <?php $this->get_list_table()->display(); ?>
            </form>
        </div>
        <?php
    }

    public function __construct() {
        add_action('admin_menu', array(&$this, 'create_admin_menu'), 20);
    }
    
    /**
     * @return UALP_Activity_Log_List_Table
     */
    public function get_list_table() {
        if (is_null($this->_list_table))
            $this->_list_table = new UALP_Activity_Log_List_Table(array('screen' => $this->_screens['main']));
        return $this->_list_table;
    }

}

class UALP_Activity_Log_List_Table extends WP_List_Table {

    protected function _get_action_label($action) {
        return ucwords(str_replace('_', ' ', __($action, 'user_activity_log_pro')));
    }

    public function __construct($args = array()) {
        parent::__construct(
                array(
                    'singular' => 'activity',
                    'ajax' => true,
                    'screen' => isset($args['screen']) ? $args['screen'] : null,
                )
        );
        add_screen_option(
                'per_page', array(
                    'default' => 20,
                    'label' => __('Number of logs per page:', 'user_activity_log_pro'),
                    'option' => 'ualp_logs_per_page',
                )
        );
        add_filter('set-screen-option', array(&$this, 'ualp_set_screen_option'), 10, 3);
        set_screen_options();
    }

    public function get_columns() {
        $columns = array(
            'check' => '<input type="checkbox" id="cb-select-all-ual">',
            'modified_date' => __('Date', 'user_activity_log_pro'),
            'user_id' => __('Author', 'user_activity_log_pro'),
            'ip_address' => __('IP Address', 'user_activity_log_pro'),
            'hook' => __('Hook', 'user_activity_log_pro'),
            'object_type' => __('Type', 'user_activity_log_pro'),
            'title' => __('Description', 'user_activity_log_pro'),
            'action' => __('Action', 'user_activity_log_pro'),
        );
        return $columns;
    }

    public function get_sortable_columns() {
        return array('ip_address' => array('ip_address', false),
            'modified_date' => array('modified_date', true),
            'object_type' => array('object_type', false),
            'user_id' => array('user_id', false),
            'hook' => array('hook', false),
            );
    }

    public function column_default($item, $column_name) {
        global $wpdb;
        $return = '';
        $i = $paged = 1;
        switch ($column_name) {
            case 'check' :
                $return = "<input type='checkbox' name='chkual[]' id='cb-select-$item->uactid' value='$item->uactid'>";
                break;
            case 'modified_date' :
                $modified_date = strtotime($item->modified_date);
                $date_format = get_option('date_format');
                $time_format = get_option('time_format');
                $date = date($date_format, $modified_date);
                $time = date($time_format, $modified_date);
                $return = sprintf(__('%s', 'user_activity_log_pro'), $date);
                $return .= " ";
                $return .= sprintf(__('%s', 'user_activity_log_pro'), $time);
                break;
            case 'user_id' :
                global $wp_roles;
                if (!empty($item->user_id) && 0 !== (int) $item->user_id) {
                    $user = get_user_by('id', $item->user_id);
                    if ($user instanceof WP_User && 0 !== $user->ID) {
                        $return = sprintf(
                                '<a href="%s">%s <span>%s</span></a><br /><small>%s</small>', get_edit_user_link($user->ID), get_avatar($user->ID, 40), ucfirst($user->display_name), isset($user->roles[0]) && isset($wp_roles->role_names[$user->roles[0]]) ? $wp_roles->role_names[$user->roles[0]] : __('Unknown', 'user_activity_log_pro')
                        );
                    }
                $return .= "<br/>";
                $return .= sprintf(__('%s', 'user_activity_log_pro'), $item->user_email);
                }else {
                    $return = __('Guest', 'user_activity_log_pro');
                }
                break;
            case 'object_type' :
                $return = ucfirst($item->object_type);
                break;
            case 'title' :
                if (isset($item->description) && $item->description != "") {
                    $return .= sprintf(__('%s', 'user_activity_log_pro'), ucfirst($item->description));
                } else {
                    $return .= sprintf(__('%s : %s', 'user_activity_log_pro'), $item->action, $item->post_title);
                }
                break;
            case 'action' :
                $return = "";
                if ($item->favorite == '1') {
                    $favTitle = __('Favorite','user_activity_log_pro');
                    $selected = "selected";
                } else {
                    $favTitle = __('Unfavorite','user_activity_log_pro');
                    $selected = "";
                }
                $viewTitle = __('View','user_activity_log_pro');
                $deleteTitle = __('Delete','user_activity_log_pro');
                if (!get_option('ualp_delete_log_pwd') || get_option('ualp_delete_log_pwd') == "") {
                    $class = "setDeletePassword";
                } else {
                    $class = "deleteSingleLog";
                }
                $viewCount = $wpdb->get_var('SELECT COUNT(uactd_id) FROM '.UALP_DETAIL_TABLENAME.' WHERE uactid = '.$item->uactid);
                if($viewCount > 0){
                    $return .= '<a data-view="'.$item->uactid.'" title="'.$viewTitle.'" href="#" class="ualp_view"><span class="dashicons dashicons-visibility"></span></a>';
                }else {
                    $return .= '<span style="float:left;margin-right: 5px;margin-top: 4px;" class="dashicons dashicons-visibility ualp-not-allowed"></span>';
                }
                $return .= '<a title="'.$favTitle.'" class="ualpFavorite '.$selected.'" id="'.$item->uactid.'"></a>';

                if ($item->favorite == '1') {
                    $return .= '<span class="padding_left_right_5 padding_top_bottom_3 ualp-not-allowed dashicons dashicons-trash"></span>';
                } else {
                    $return .= '<a title="'.$deleteTitle.'" class="column-delete deleteLog '.$class.'" data-id="'.$item->uactid.'" href="'.wp_nonce_url(admin_url("admin.php?page=ual_pro&paged=$paged&action=delete&id=$item->uactid"), "delete-action$item->uactid", "delete-nonce").'" ><span class="dashicons dashicons-trash"></span></a>';
                }
                break;
            default :
                if (isset($item->$column_name))
                    $return = $item->$column_name;
        }
        $return = apply_filters('ualp_table_list_column_default', $return, $item, $column_name);
        return $return;
    }

    public function column_author($item) {
        global $wp_roles;
        if (!empty($item->user_id) && 0 !== (int) $item->user_id) {
            $user = get_user_by('id', $item->user_id);
            if ($user instanceof WP_User && 0 !== $user->ID) {
                $userLink = sprintf(__('%s','user_activity_log_pro'),get_edit_user_link($user->ID));
                $userAvatar = sprintf(__('%s','user_activity_log_pro'),get_avatar($user->ID, 40));
                $userDisplayName = sprintf(__('%s','user_activity_log_pro'),$user->display_name);
                $userRole = sprintf(__('%s','user_activity_log_pro'),isset($user->roles[0]) && isset($wp_roles->role_names[$user->roles[0]]) ? $wp_roles->role_names[$user->roles[0]] : 'Unknown');
                return '<a href="'.$userLink.'">'
                            .$userAvatar.' <span>'.$userDisplayName.'</span>'
                        . '</a><br />'
                        . '<small>'.$userRole.'</small>';
            }
        }
        return '<span>'.__('Guest', 'user_activity_log_pro').'</span>';
    }

    public function column_type($item) {
        $return = $item->object_type;
        $return = apply_filters('ualp_table_list_column_type', $return, $item);
        return $return;
    }

    public function column_label($item) {
        $return = '';
        if (!empty($item->object_subtype)) {
            $pt = get_post_type_object($item->object_subtype);
            $return = !empty($pt->label) ? $pt->label : $item->object_subtype;
        }
        $return = apply_filters('ualp_table_list_column_label', $return, $item);
        return $return;
    }

    public function display_tablenav($which) {
        ?>
        <div class="tablenav <?php echo esc_attr($which); ?>"><?php
            global $wpdb;
            $itemList = $wpdb->get_var('SELECT count(uactid) FROM '.UALP_TABLENAME);
            if($itemList){
                if ('top' == $which) {
                    wp_nonce_field('bulk-' . $this->_args['plural']); ?>
                    <div class="wp-filter">
                        <div class="filter-items">
                            <?php
                            $this->extra_tablenav($which); ?>
                                <div class="top-cover">
                                    <div class="alignleft bulkactions">
                                        <?php
                                        if (!get_option('ualp_delete_log_pwd') || get_option('ualp_delete_log_pwd') == "") {
                                            $class = "setBulkDeletePassword";
                                        } else {
                                            $class = "deleteBulkLog";
                                        }
                                        ?>
                                        <label class="screen-reader-text" for="bulk-action-selector-top">
                                            <?php _e('Select bulk action', 'user_activity_log_pro'); ?>
                                        </label>
                                        <select class="<?php echo $class; ?>" id="bulk-action-selector-top" name="bulk_action_selector">
                                            <option value="0"><?php _e('Bulk Actions', 'user_activity_log_pro'); ?></option>
                                            <option value="delete"><?php _e('Delete Permanently', 'user_activity_log_pro'); ?></option>
                                            <option value="favorite"><?php _e('Favorite', 'user_activity_log_pro'); ?></option>
                                            <option value="unfavorite"><?php _e('Unfavorite', 'user_activity_log_pro'); ?></option>
                                        </select>
                                        <input type="submit" value="<?php _e('Apply', 'user_activity_log_pro'); ?>" name="bulk_action" class="button action" id="doaction">
                                    </div>
                                    <?php
                                    $items_array = $this->items;
                                    if(is_array($items_array) && count($items_array) > 0) {
                                        if (!isset($_REQUEST['capshow']))
                                            $_REQUEST['capshow'] = '';
                                        if (!isset($_REQUEST['usershow']))
                                            $_REQUEST['usershow'] = '';
                                        if (!isset($_REQUEST['typeshow']))
                                            $_REQUEST['typeshow'] = '';
                                        if (!isset($_REQUEST['showip']))
                                            $_REQUEST['showip'] = '';
                                        if (!isset($_REQUEST['dateshow']))
                                            $_REQUEST['dateshow'] = '';
                                        if (!isset($_REQUEST['showhook']))
                                            $_REQUEST['showhook'] = '';
                                        if (!isset($_REQUEST['showfavorite']))
                                            $_REQUEST['showfavorite'] = '';
                                        if (!isset($_REQUEST['s']))
                                            $_REQUEST['s'] = '';
                                    ?>
                                        <a href="<?php echo wp_nonce_url(admin_url("admin.php?page=ual_pro&export=user_logs&userrole=".$_REQUEST['capshow']."&username=".$_REQUEST['usershow']."&type=".$_REQUEST['typeshow']."&ip=".$_REQUEST['showip']."&time=".$_REQUEST['dateshow']."&hook=".$_REQUEST['showhook']."&favorite=".$_REQUEST['showfavorite']."&txtsearch=".$_REQUEST['s']), "export-action", "export-nonce"); ?>" class="button-primary action">
                                            <?php _e('Export Logs (CSV)', 'user_activity_log_pro'); ?>
                                        </a><?php
                                    } ?>
                                </div><?php
                            $this->search_box(__('Search', 'user_activity_log_pro'), 'ualp-search'); ?>
                        </div>
                    </div><?php
                }
                $this->pagination($which);
            }?>
            <br class="clear" />
        </div><?php
    }

    public function extra_tablenav($which) {
        global $wpdb;
        if ('top' !== $which)
            return;
        echo '<div class="alignleft actions">';
        $users = $wpdb->get_results($wpdb->prepare(
                        'SELECT DISTINCT %1$s FROM `%2$s`
				WHERE 1 = 1 AND %1$s <> ""
				GROUP BY `%1$s`
				ORDER BY `%1$s`
			;', 'user_name', UALP_TABLENAME
        ));
        $types = $wpdb->get_results($wpdb->prepare(
                        'SELECT DISTINCT %1$s FROM `%2$s`
				WHERE 1 = 1
				GROUP BY `%1$s`
				ORDER BY `%1$s`
			;', 'object_type', UALP_TABLENAME
        ));
        // Make sure we get items for filter.
        if ($users || $types) {
            if (!isset($_REQUEST['dateshow']))
                $_REQUEST['dateshow'] = '';
            $date_options = array(
                '' => __('All Time', 'user_activity_log_pro'),
                'today' => __('Today', 'user_activity_log_pro'),
                'yesterday' => __('Yesterday', 'user_activity_log_pro'),
                'week' => __('Week', 'user_activity_log_pro'),
                'month' => __('Month', 'user_activity_log_pro'),
            );
            echo '<div class="filter-cover"><select name="dateshow" id="ualp-filter-date">';
            foreach ($date_options as $key => $value)
                printf('<option value="%1$s"%2$s>%3$s</option>', $key, selected($_REQUEST['dateshow'], $key, false), $value);
            echo '</select>';
        }
        $roles = $wpdb->get_results($wpdb->prepare(
            'SELECT DISTINCT %1$s FROM `%2$s`
                    WHERE 1 = 1 AND %1$s <> ""
                    GROUP BY `%1$s`
                    ORDER BY `%1$s`
            ;', 'user_role', UALP_TABLENAME
        ));
        if ($roles) {
            if (!isset($_REQUEST['capshow']))
                $_REQUEST['capshow'] = '';
            $output = array();
            foreach ($roles as $role) {
                $output[] = sprintf('<option value="%1$s"%2$s>%3$s</option>', $role->user_role, selected($_REQUEST['capshow'], $role->user_role, false), ucfirst($role->user_role));
            }
            echo '<select name="capshow" id="ualp-filter-capshow">';
            printf('<option value="">'.__('All Roles', 'user_activity_log_pro').'</option>');
            echo implode('', $output);
            echo '</select>';
        }
        if ($users) {
            if (!isset($_REQUEST['usershow']))
                $_REQUEST['usershow'] = '';
            $output = array();
            foreach ($users as $_user) {
                $output[] = sprintf('<option value="%s"%s>%s</option>', $_user->user_name, selected($_REQUEST['usershow'], $_user->user_name, false), $this->_get_action_label($_user->user_name));   
            }
            echo '<select name="usershow" id="ualp-filter-usershow">';
            printf('<option value="">'.__('All Users', 'user_activity_log_pro').'</option>');
            echo implode('', $output);
            echo '</select>';
        }
        $ips = $wpdb->get_results($wpdb->prepare(
            'SELECT DISTINCT %1$s FROM `%2$s`
                    WHERE 1 = 1
                    GROUP BY `%1$s`
                    ORDER BY `%1$s`
            ;', 'ip_address', UALP_TABLENAME
        ));
        if ($ips) {
            if (!isset($_REQUEST['showip']))
                $_REQUEST['showip'] = '';
            $output = array();
            foreach ($ips as $ip)
                $output[] = sprintf('<option value="%s"%s>%s</option>', $ip->ip_address, selected($_REQUEST['showip'], $ip->ip_address, false), $this->_get_action_label($ip->ip_address));
            echo '<select name="showip" id="ualp-filter-showip">';
            printf('<option value="">'.__('All IP', 'user_activity_log_pro').'</option>');
            echo implode('', $output);
            echo '</select>';
        }
        if ($types) {
            if (!isset($_REQUEST['typeshow']))
                $_REQUEST['typeshow'] = '';
            $output = array();
            foreach ($types as $type) {
                $output[] = sprintf('<option value="%1$s"%2$s>%3$s</option>', $type->object_type, selected($_REQUEST['typeshow'], $type->object_type, false), ucfirst($type->object_type));
            }
            echo '<select name="typeshow" id="ualp-filter-typeshow">';
            printf('<option value="">'.__('All Types', 'user_activity_log_pro').'</option>' );
            echo implode('', $output);
            echo '</select>';
        }
        $hooks = $wpdb->get_results($wpdb->prepare(
            'SELECT DISTINCT %1$s FROM `%2$s`
                    WHERE 1 = 1
                    GROUP BY `%1$s`
                    ORDER BY `%1$s`
            ;', 'hook', UALP_TABLENAME
        ));
        if ($hooks) {
            if (!isset($_REQUEST['showhook']))
                $_REQUEST['showhook'] = '';
            $output = array();

            foreach ($hooks as $hook) {
                if ($hook->hook != "") {
                    $output[] = sprintf('<option value="%s"%s>%s</option>', $hook->hook, selected($_REQUEST['showhook'], $hook->hook, false), $hook->hook);
                }
            }
            echo '<select name="showhook" id="ualp-filter-showhook">';
            printf('<option value="">'.__('All Hook', 'user_activity_log_pro').'</option>');
            echo implode('', $output);
            echo '</select>';
        }
        $favorites = $wpdb->get_results($wpdb->prepare(
            'SELECT DISTINCT %1$s FROM `%2$s`
                    WHERE 1 = 1
                    GROUP BY `%1$s`
                    ORDER BY `%1$s`
            ;', 'favorite', UALP_TABLENAME
        ));
        if ($favorites) {
            if (!isset($_REQUEST['showfavorite']))
                $_REQUEST['showfavorite'] = '';
            $output = array();
            $output = sprintf('<option value="1"%s>%s</option>', selected($_REQUEST['showfavorite'], '1', false), __('Favorite', 'user_activity_log_pro'));
            $output .= sprintf('<option value="0"%s>%s</option>', selected($_REQUEST['showfavorite'], '0', false), __('Unfavorite', 'user_activity_log_pro'));
            echo '<select name="showfavorite" id="ualp-filter-showfavorite">';
            printf('<option value="">'.__('All Favorite/Unfavorite', 'user_activity_log_pro').'</option>');
            echo $output;
            echo '</select></div>';
            ?>
            <div class="filter-btn-cover">
                <?php submit_button(__('Filter', 'user_activity_log_pro'), 'button', false, false, array('uactid' => 'activity-query-submit'));?>
            </div><?php
        }
        echo '</div>';
    }

    public function prepare_items() {
        global $wpdb;
        $_SERVER['REQUEST_URI'] = remove_query_arg('_wp_http_referer', $_SERVER['REQUEST_URI']);
        $items_per_page = $this->get_items_per_page('ualp_logs_per_page');
        $this->_column_headers = array($this->get_columns(), get_hidden_columns($this->screen), $this->get_sortable_columns());
        $where = ' WHERE 1=1';
        if (!empty($_REQUEST['bulk_action_selector']) && $_REQUEST['bulk_action_selector'] == 'favorite') {
            if (isset($_REQUEST['chkual']) && !empty($_REQUEST['chkual'])) {
                $logs = $_REQUEST['chkual'];
                foreach ($logs as $log) {
                    $result = $wpdb->update( 
                        UALP_TABLENAME, 
                        array( 'favorite' => '1' ), 
                        array( 'uactid' => $log ), 
                        array( '%s' ), 
                        array( '%d' ) 
                    );
                }
                wp_redirect(admin_url() . "admin.php?page=ual_pro");
            }
        }
        if (!empty($_REQUEST['bulk_action_selector']) && $_REQUEST['bulk_action_selector'] == 'unfavorite') {
            if (isset($_REQUEST['chkual']) && !empty($_REQUEST['chkual'])) {
                $logs = $_REQUEST['chkual'];
                foreach ($logs as $log) {
                    $result = $wpdb->update( 
                        UALP_TABLENAME, 
                        array( 'favorite' => '0' ), 
                        array( 'uactid' => $log ), 
                        array( '%s' ), 
                        array( '%d' ) 
                    );
                }
                wp_redirect(admin_url() . "admin.php?page=ual_pro");
            }
        }
        if (!isset($_REQUEST['order']) || !in_array($_REQUEST['order'], array('desc', 'asc'))) {
            $_REQUEST['order'] = 'desc';
        }
        if (!isset($_REQUEST['orderby']) || !in_array($_REQUEST['orderby'], array('modified_date', 'user_id', 'user_email', 'object_type', 'hook'))) {
            $_REQUEST['orderby'] = 'modified_date';
        }
        if (!empty($_REQUEST['typeshow'])) {
            $where .= $wpdb->prepare(' AND `object_type` = \'%s\'', $_REQUEST['typeshow']);
        }
        if (isset($_REQUEST['showip']) && '' !== $_REQUEST['showip']) {
            $where .= $wpdb->prepare(' AND `ip_address` = \'%s\'', $_REQUEST['showip']);
        }
        if (isset($_REQUEST['showhook']) && '' !== $_REQUEST['showhook']) {
            $where .= $wpdb->prepare(' AND `hook` = \'%s\'', $_REQUEST['showhook']);
        }
        if (isset($_REQUEST['usershow']) && '' !== $_REQUEST['usershow']) {
            $where .= $wpdb->prepare(' AND `user_name` = %s', $_REQUEST['usershow']);
        }
        if (isset($_REQUEST['showfavorite']) && '' !== $_REQUEST['showfavorite']) {
            $where .= $wpdb->prepare(' AND `favorite` = %s', $_REQUEST['showfavorite']);
        }
        if (isset($_REQUEST['capshow']) && '' !== $_REQUEST['capshow']) {
            $where .= $wpdb->prepare(' AND `user_role` = \'%s\'', strtolower($_REQUEST['capshow']));
        }
        if (isset($_REQUEST['dateshow']) && in_array($_REQUEST['dateshow'], array('today', 'yesterday', 'week', 'month'))) {
            $get_time = $_REQUEST['dateshow'];
            $current_time = current_time('timestamp');
            $start_time = mktime(0, 0, 0, date('m', $current_time), date('d', $current_time), date('Y', $current_time));
            $end_time = mktime(23, 59, 59, date('m', $current_time), date('d', $current_time), date('Y', $current_time));
            if ($get_time == 'today') {
                $start_time = $current_time;
                $end_time = strtotime('+1 day', $current_time);
            } elseif ($get_time === 'yesterday') {
                $start_time = strtotime('yesterday', $start_time);
                $end_time = $current_time;
            } elseif ($get_time === 'week') {
                $start_time = strtotime('-1 week', $start_time);
                $end_time = strtotime('+1 day', $current_time);
            } elseif ($get_time === 'month') {
                $start_time = strtotime('-1 month', $start_time);
                $end_time = strtotime('+1 day', $current_time);
            }
            $start_time = date('Y-m-d', $start_time);
            $end_time = date('Y-m-d', $end_time);
            $where .= $wpdb->prepare(' AND modified_date > "%1$s" AND modified_date < "%2$s"', $start_time, $end_time);
        }
        if (isset($_REQUEST['s']) && $_REQUEST['s'] != '') {
            // Search only searches 'description' fields.
            $where .= ' AND (user_name LIKE \'%%'.$wpdb->esc_like($_REQUEST['s']).'%%\' OR user_role LIKE \'%%'.$wpdb->esc_like($_REQUEST['s']).'%%\' OR object_type LIKE \'%%'.$wpdb->esc_like($_REQUEST['s']).'%%\')';
        }
        $offset = ( $this->get_pagenum() - 1 ) * $items_per_page;
        $total_items = $wpdb->get_var($wpdb->prepare(
            'SELECT COUNT(`uactid`) FROM `%1$s`
                ' . $where, UALP_TABLENAME, $offset, $items_per_page
        ));
        $this->items = $wpdb->get_results($wpdb->prepare(
            'SELECT * FROM `%1$s`
                ' . $where . '
                        ORDER BY `%2$s` %3$s
                        LIMIT %4$d, %5$d;', UALP_TABLENAME, $_REQUEST['orderby'], $_REQUEST['order'], $offset, $items_per_page
        ));
        $this->set_pagination_args(array(
            'total_items' => $total_items,
            'per_page' => $items_per_page,
            'total_pages' => ceil($total_items / $items_per_page)
        ));
    }

    public function ualp_set_screen_option( $status, $option, $value ) {        
        if ( 'ualp_logs_per_page' === $option )
            return $value;
    }
    
    public function no_items() {
            _e( 'No Activity found.','user_activity_log_pro' );
    }

    public function search_box($text, $input_id) {
        $search_data = isset($_REQUEST['s']) ? sanitize_text_field($_REQUEST['s']) : '';
        $input_id = $input_id . '-search-input';
        ?>
        <p class="search-box">
            <label class="screen-reader-text" for="<?php echo $input_id ?>"><?php echo $text; ?>:</label>
            <input type="search" id="<?php echo $input_id ?>" name="s" placeholder="<?php _e('User, Role, Type', 'user_activity_log_pro'); ?>" value="<?php echo esc_attr($search_data); ?>" />
            <?php submit_button($text, 'button', false, false, array('uactid' => 'search-submit')); ?>
        </p>
        <?php
    }

}

$ualpAdminUi = new UALP_Admin_Ui();