<?php

/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}

/*
 * Insert record into wp_user_activity table
 *
 * @param int $post_id Post ID.
 * @param string $post_title Post Title.
 * @param string $obj_type Object Type (Plugin, Post, User etc.).
 * @param int $current_user_id current user id.
 * @param string $current_user current user name.
 * @param string $user_role current user Role.
 * @param string $user_mail current user Email address.
 * @param datetime $modified_date current user's modified time.
 * @param string $ip current user's IP address.
 * @param string $action current user's activity name.
 *
 */
if (!function_exists('ualpInsertLog')) {

    function ualpInsertLog($post_id, $post_title, $obj_type, $current_user_name, $action, $hook, $description) {
        global $wpdb;
        $post_title = addslashes($post_title);
        $modified_date = current_time('mysql');
        $ip = $_SERVER['REMOTE_ADDR'];
        $username = get_user_by('login', $current_user_name);
        if(isset($username) && !empty($username)){
            $current_user_name = $username->data->display_name;
            $user_mail = $username->data->user_email;
            $current_user_id = $username->data->ID;
            if (!empty($username->roles) && is_array($username->roles)) {
                $user_role = $username->roles[0];
            }
        }else {
            $current_user_name = "Guest";
            $user_mail = "";
            $current_user_id = "";
            $user_role = "";
        }
        ualpCreateTable();
        /**
         * ualpBeforeInsertLog hook.
         */
        do_action('ualpBeforeInsetLog');
        $insert_query = $wpdb->insert(
	UALP_TABLENAME,
            array(
                'post_id' => $post_id,
                'favorite' => '0',
                'post_title' => $post_title,
                'user_id' => $current_user_id,
                'user_name' => $current_user_name,
                'user_role' => $user_role,
                'user_email' => $user_mail,
                'ip_address' => $ip,
                'modified_date' => $modified_date,
                'object_type' => $obj_type,
                'action' => $action,
                'hook' => $hook,
                'description' => $description
            )
        );
        /**
         * ualpAfterInsertLog hook.
         */
        do_action('ualpAfterInsertLog');
        return $wpdb->insert_id;
    }

}

/*
 * get activity
 *
 * @param string $action current user's activity name.
 * @param string $obj_type Object Type (Plugin, Post, User etc.).
 * @param int $post_id Post ID.
 * @param string $post_title Post Title.
 *
 */
if (!function_exists('ualpGetActivityLog')) {

    function ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description) {
        $current_user_id = get_current_user_id();
        $current_user = wp_get_current_user();
        $current_user_name = $current_user->user_login;
        $ualpInsertLog = ualpInsertLog($post_id, $post_title, $obj_type, $current_user_name, $action, $hook, $description);
        return $ualpInsertLog;
    }

}

/*
 * Add activity for the current user when login
 *
 * @param string $user_login current user's login name.
 *
 */
if (!function_exists('ualpHookWpLogin')){

    function ualpHookWpLogin($current_user_name,$user) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_login',$logHookList)) {
            $action = "logged in";
            $obj_type = "User";
            $post_id = "";
            $username = get_user_by('login', $current_user_name);
            $post_title = $username->display_name;
            $hook = "wp_login";
            $description = "$obj_type $action : $post_title";
            ualpInsertLog($post_id, $post_title, $obj_type, $current_user_name, $action, $hook, $description);
        }
    }

}
add_action('wp_login', 'ualpHookWpLogin', 9, 2);

/*
 * Get activity for the user - Login fail
 *
 * @param string $user username
 */
if (!function_exists('ualpHookWpLoginFailed')){

    function ualpHookWpLoginFailed($current_user_name) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_login_failed',$logHookList)) {
            $action = "login failed";
            $obj_type = "User";
            $post_id = "";
            $username = get_user_by('login', $current_user_name);
            if(isset($username) && !empty($username)){
                $post_title = $username->display_name;
            }else {
                $post_title = "Guest";
            }
            $hook = "wp_login_failed";
            $description = "$obj_type $action : $post_title";
            ualpInsertLog($post_id, $post_title, $obj_type, $current_user_name, $action, $hook, $description);
        }
    }
}
add_action('wp_login_failed', 'ualpHookWpLoginFailed', 10, 1);


/*
 * Get activity for the current user when logout
 */
if (!function_exists('ualpHookWpLogout')){

    function ualpHookWpLogout() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_logout',$logHookList)) {
            $action = "logged out";
            $obj_type = "User";
            $post_id = get_current_user_id();
            if(isset($post_id) && !empty($post_id)){
                $user_nm = wp_get_current_user();
                $current_user_name = $user_nm->data->user_login;
                $post_title = $user_nm->data->display_name;
            }else{
                $current_user_name = "";
                $post_title = "Guest";
            }
            $hook = "wp_logout";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_logout', 'ualpHookWpLogout', 10);

/*
 * Get activity for the delete user
 *
 * @param int $user Post ID
 *
 */
if (!function_exists('ualpHookDeleteUser')){

    function ualpHookDeleteUser($user) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('delete_user',$logHookList)) {
            $action = "deleted";
            $obj_type = "User";
            $post_id = $user;
            $user_nm = get_user_by('id', $post_id);
            $current_user_name = $user_nm->user_login;
            $post_title = $user_nm->display_name;
            $hook = 'delete_user';
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('delete_user', 'ualpHookDeleteUser', 10, 1);

/*
 * Get activity for the registered user
 *
 * @param int $user Post ID
 *
 */
if (!function_exists('ualpHookUserRegister')){

    function ualpHookUserRegister($user) {        
        $logHookList = get_option('enableLogHookList');
        if (in_array('user_register',$logHookList)) {
            $action = "registered";
            $obj_type = "User";
            $post_id = $user;
            $user_nm = get_user_by('id', $post_id);
            $current_user_name = $user_nm->user_login;
            $post_title = $user_nm->display_name;
            $hook = "user_register";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('user_register', 'ualpHookUserRegister', 10, 1);

/*
 * Get activity for the update user
 *
 * @param type $user_meta
 * @param type $user
 * @param type $update
 * @return type
 */
if (!function_exists('ualpHookUserUpdate')){

    function ualpHookUserUpdate($user_meta,$user,$update) {
        
        if($update == "")
            return $user_meta;

        $logHookList = get_option('enableLogHookList');
        if (in_array('insert_user_meta',$logHookList)) {
            $action = "profile updated";
            $obj_type = "User";
            $post_id = $user->data->ID;
            $current_user_name = $user->data->user_login;
            $post_title = $user->data->display_name;
            $hook = "insert_user_meta";
            $description = "$obj_type $action : $post_title";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetailUserUpdate($uactid,$user_meta,$user,$update,$hook);
        }        
        return $user_meta;
    }
}

add_filter('insert_user_meta', 'ualpHookUserUpdate',10,3);

/**
 * Get activity for the password reset
 * 
 * @param type $user
 * @param type $new_pass
 */
if(!function_exists('ualpHookPasswordReset')){

    function ualpHookPasswordReset( $user, $new_pass ) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('password_reset',$logHookList)) {
            $action = "Password Reset";
            $obj_type = "User";
            $post_id = "";
            $post_title = $obj_type;
            $hook = "password_reset";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action( 'password_reset', 'ualpHookPasswordReset', 10, 2 );

/*
 * Get activity for the user - add attach media file
 *
 * @param int $attach Post ID
 *
 */
if (!function_exists('ualpHookAddAttachment')){

    function ualpHookAddAttachment($attach) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('attachment',$logHookList)) {
            $action = "added";
            $obj_type = "Attachment";
            $post_id = $attach;
            $post_title = get_the_title($post_id);
            $hook = "add_attachment";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('add_attachment', 'ualpHookAddAttachment');

/*
 * Get activity for the user - edit attach media file
 *
 * @param int $attach Post ID
 *
 */
if (!function_exists('ualpHookEditAttachment')){

    function ualpHookEditAttachment($attach) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('attachment',$logHookList)) {
            $post_id = $attach;
            $post_title = get_the_title($post_id);
            $action = "edit";
            $obj_type = "Attachment";
            $hook = "edit_attachment";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('edit_attachment', 'ualpHookEditAttachment');

/*
 * Get activity for the user - delete attach media file
 *
 * @param int $attach Post ID
 *
 */
if (!function_exists('ualpHookDeleteAttachment')){

    function ualpHookDeleteAttachment($attach) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('attachment',$logHookList)) {
            $post_id = $attach;
            $post_title = get_the_title($post_id);
            $action = "deleted";
            $obj_type = "Attachment";
            $hook = "delete_attachment";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('delete_attachment', 'ualpHookDeleteAttachment');

/*
 * Get activity for the user - Insert Comment
 *
 * @param int $comment Comment ID
 *
 */
if (!function_exists('ualpHookWpInsertComment')){

    function ualpHookWpInsertComment($comment) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_insert_comment',$logHookList)) {
            $action = "inserted";
            $obj_type = "Comment";
            $comment_id = $comment;
            $com = get_comment($comment_id);
            $post_id = $com->comment_post_ID;
            $comment_detail_ary = array();
            $comment_detail_ary['ualp_comment'] = $com->comment_content;
            $comment_detail_ary['ualp_comment_id'] = $comment_id;
            $post_title = get_the_title($post_id);
            $post_link = get_edit_post_link($post_id);
            $hook = "wp_insert_comment";
            $description = "$obj_type $action in <a target='blank' href='$post_link'>$post_title</a>";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetail($uactid,$comment_detail_ary);
        }
    }
}

add_action('wp_insert_comment', 'ualpHookWpInsertComment');

/*
 * Get activity for the user - Edit Comment
 *
 * @param int $comment Comment ID
 *
 */
if (!function_exists('ualpHookEditComment')){

    function ualpHookEditComment($comment) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('edit_comment',$logHookList)) {
            $action = "edited";
            $obj_type = "Comment";
            $comment_id = $comment;
            $com = get_comment($comment_id);
            $post_id = $com->comment_post_ID;
            $comment_detail_ary = array();
            $comment_detail_ary['ualp_comment'] = $com->comment_content;
            $comment_detail_ary['ualp_comment_id'] = $comment_id;
            $post_title = get_the_title($post_id);
            $post_link = get_edit_post_link($post_id);
            $hook = "edit_comment";
            $description = "$obj_type $action in <a target='blank' href='$post_link'>$post_title</a>";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetail($uactid,$comment_detail_ary);
        }
    }
}

add_action('edit_comment', 'ualpHookEditComment');

/*
 * Get activity for the user - Delete Comment
 *
 * @param int $comment Comment ID
 *
 */
if (!function_exists('ualpHookDeleteComment')){

    function ualpHookDeleteComment($comment) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('delete_comment',$logHookList)) {
            $action = "deleted";
            $obj_type = "Comment";
            $com = get_comment($comment);
            $post_id = $com->comment_post_ID;
            $comment_detail_ary = array();
            $comment_detail_ary['ualp_comment_id'] = $comment;
            $comment_detail_ary['ualp_comment'] = $com->comment_content;
            $post_title = get_the_title($post_id);
            $post_link = get_edit_post_link($post_id);
            $hook = "delete_comment";
            $description = "$obj_type $action from <a target='blank' href='$post_link'>$post_title</a>";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetail($uactid,$comment_detail_ary);
        }
    }
}

add_action('delete_comment', 'ualpHookDeleteComment',10,1);

/*
 * Get activity for the user - Comment status change
 *
 * @param int $comment Comment ID
 *
 */
if (!function_exists('ualpHookStatusComment')){

    function ualpHookStatusComment($new_status, $old_status, $comment_id) {
        $logHookList = get_option('enableLogHookList');
        $obj_type = "Comment";
        $com = get_comment($comment_id);
        $post_id = $com->comment_post_ID;
        $comment_detail_ary = array();
        $comment_detail_ary['ualp_comment'] = $com->comment_content;
        $post_title = get_the_title($post_id);
        $post_link = get_edit_post_link($post_id);
        if($new_status == 'approved') {
            if($old_status == 'trash' || $old_status == 'spam') {
                $action = 'restored';
                $description = "$obj_type $action from $old_status in <a target='blank' href='$post_link'>$post_title</a>";
                $hook = "restore_comment";
                if (in_array('restore_comment',$logHookList)) {
                    $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                    ualpAddActivityLogDetail($uactid,$comment_detail_ary);
                }
            }
            else {
                $action = 'approved';
                $description = "$obj_type $action in <a target='blank' href='$post_link'>$post_title</a>";
                $hook = "comment_approve";
                if (in_array('comment_approve',$logHookList)) {
                    $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                    ualpAddActivityLogDetail($uactid,$comment_detail_ary);
                }
            }
        }
        else if($new_status == 'unapproved') {
            if($old_status == 'trash' || $old_status == 'spam') {
                $action = 'restored';
                $description = "$obj_type $action from $old_status in <a target='blank' href='$post_link'>$post_title</a>";
                $hook = "restore_comment";
                if (in_array('restore_comment',$logHookList)) {
                    $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                    ualpAddActivityLogDetail($uactid,$comment_detail_ary);
                }
            }
            else {
                $action = "unapproved";
                $description = "$obj_type $action in <a target='blank' href='$post_link'>$post_title</a>";
                $hook = "comment_unapprove";
                if (in_array('comment_unapprove',$logHookList)) {
                    $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                    ualpAddActivityLogDetail($uactid,$comment_detail_ary);
                }
            }
        }
        else if($new_status == 'spam') {
            $action = "spam";
            $description = "$obj_type marked as $action in <a target='blank' href='$post_link'>$post_title</a>";
            $hook = "spam_comment";
            if (in_array('spam_comment',$logHookList)) {
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                ualpAddActivityLogDetail($uactid,$comment_detail_ary);
            }
        }
        else if($new_status == 'trash') {
            $action = "trash";
            $description = "$obj_type moved to $action in <a target='blank' href='$post_link'>$post_title</a>";
            $hook = "trash_comment";
            if (in_array('trash_comment',$logHookList)) {
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                ualpAddActivityLogDetail($uactid,$comment_detail_ary);
            }
        }
    }
}

add_action('transition_comment_status', 'ualpHookStatusComment',10,3); // new

/*
 * Get activity for the user - Update navigation menu
 *
 * @param int $menu Post ID
 *
 */
if (!function_exists('ualpHookWpUpdateMenu')){

    function ualpHookWpUpdateMenu() {
        $logHookList = get_option('enableLogHookList');
        if(!isset($_REQUEST["menu"]) || !isset($_REQUEST["action"])) {
            return;
        }
        if($_REQUEST["action"]!= "delete" && $_REQUEST["action"] != "update") {
            return;
        }
        $menu_id = $_REQUEST["menu"];
        if(!is_nav_menu($menu_id)) {
            return;
        }
        $menu_object = wp_get_nav_menu_object($menu_id);
        $obj_type = "Menu";
        $post_id = $menu_id;
        $post_title = $menu_object->name;
        if("delete" == $_REQUEST["action"]) {
            if (in_array('delete_nav_menu',$logHookList)) {
                $action = "deleted";
                $hook = "delete_nav_menu";
                $description = "$post_title $obj_type $action";
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
        else {
            if (in_array('wp_update_nav_menu',$logHookList)) {
                $action = "updated";
                $hook = "wp_update_nav_menu";
                $description = "$post_title $obj_type $action";
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }
}
add_action("load-nav-menus.php", 'ualpHookWpUpdateMenu');

/*
 * Get activity for the user - Create navigation menu
 *
 * @param int $menu Post ID
 *
 */
if (!function_exists('ualpHookWpCreateMenu')){

    function ualpHookWpCreateMenu($menu) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_create_nav_menu',$logHookList)) {
            $action = "created";
            $obj_type = "Menu";
            $post_id = $menu;
            $menu_object = wp_get_nav_menu_object($post_id);
            $post_title = $menu_object->name;
            $hook = "wp_create_nav_menu";
            $description = "$post_title $obj_type $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_create_nav_menu', 'ualpHookWpCreateMenu');

/*
 * Get activity for the user - Create Terms
 *
 * @param int $term Post ID
 * @param string $taxonomy taxonomy name
 *
 */
if (!function_exists('ualpHookCreatedTerm')){

    function ualpHookCreatedTerm($term, $taxonomy) {
        if ('nav_menu' === $taxonomy)
            return $term;

        $logHookList = get_option('enableLogHookList');
        if (in_array($taxonomy,$logHookList)) {
            $termsDetails = get_term_by( 'name', $term, $taxonomy );
            $taxonomy_details = get_taxonomy( $taxonomy );
            $action = "created";
            $obj_type = "Term";
            $post_id = "";
            $post_title = $taxonomy." - ".$term;
            $hook = "pre_insert_term";
            $description = "$obj_type '$term' $action in taxonomy '$taxonomy_details->label'";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
        return $term;
    }
}
add_filter('pre_insert_term', 'ualpHookCreatedTerm', 10, 2);

/*
 * Get activity for the user - Delete Terms
 *
 * @param int $term_id Post ID
 * @param string $taxonomy taxonomy name
 * @param string $deleted_term = null
 *
 */
if (!function_exists('ualpHookDeleteTerm')){

    function ualpHookDeleteTerm($term, $taxonomy) {
        if ('nav_menu' === $taxonomy)
            return $term;
        if ($taxonomy && !is_wp_error($taxonomy)) {
            $logHookList = get_option('enableLogHookList');
            if (in_array($taxonomy,$logHookList)) {
                $action = 'deleted';
                $obj_type = 'Term';
                $post_id = $term;
                $post_title = "";
                $hook = "pre_delete_term";
                $termName = get_term_by("id", $term, $taxonomy);
                $termLink = get_edit_term_link($term, $taxonomy);
                $taxonomy_details = get_taxonomy( $taxonomy );
                if($taxonomy == "product_shipping_class"){
                    $description = "$obj_type '$termName->name' $action from taxonomy '$taxonomy_details->label'";
                }else{
                    $description = "$obj_type <a target='blank' href='$termLink'>'$termName->name'</a> $action from taxonomy '$taxonomy_details->label'";
                }
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }
}
add_action('pre_delete_term', 'ualpHookDeleteTerm', 10, 2);

/*
 * Get activity for the user - Edit Terms
 *
 * @param int $term Post ID
 * @param string $taxonomy taxonomy name
 *
 */
function ualpHookEditedTerm( $parent = null, $term_id = null, $taxonomy = null, $parsed_args = null, $args = null ) {

    if ('nav_menu' === $args["taxonomy"])
            return;

    $logHookList = get_option('enableLogHookList');
    if (in_array($args["taxonomy"],$logHookList)) {
        $action = "edited";
        $obj_type = "Term";
        $hook = "wp_update_term_parent";
        $old_term = get_term_by("id", $term_id, $taxonomy);
        if (! $args || empty( $old_term)) {
            return $parent;
        }
        $post_id = $term_id;
        $ulap_old_term_name = $old_term->name;
        $ulap_old_term_slug = $old_term->slug;
        $ulap_old_term_description = $old_term->description;
        $ulap_old_term_parent = $old_term->parent;
        $ulap_new_term_name = $args["name"];
        $ulap_new_term_taxonomy = $args["taxonomy"];
        $ulap_new_term_slug = $args["slug"];
        $ulap_new_term_description = $args["description"];
        $ulap_new_term_parent = $args["parent"];

        $post_title = $ulap_new_term_taxonomy." - ". $ulap_new_term_name;
        if($taxonomy == "product_shipping_class"){
            $description = "$obj_type '$ulap_new_term_name' $action in taxonomy '$ulap_new_term_taxonomy'";
        }else{
            $description = "$obj_type <a target='blank' href='".  get_edit_term_link( $term_id, $ulap_new_term_taxonomy )."'>'$ulap_new_term_name'</a> $action in taxonomy '$ulap_new_term_taxonomy'";
        }        
        $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        $user_detail_ary = array(
            'ulap_old_term_name' => $ulap_old_term_name,
            'ulap_old_term_slug' => $ulap_old_term_slug,
            'ulap_old_term_description' => $ulap_old_term_description,
            'ulap_old_term_parent' => $ulap_old_term_parent,
            'ulap_new_term_name' => $ulap_new_term_name,
            'ulap_new_term_slug' => $ulap_new_term_slug,
            'ulap_new_term_description' => $ulap_new_term_description,
            'ulap_new_term_parent' => $ulap_new_term_parent
        );
        ualpAddActivityLogDetail($uactid,$user_detail_ary);
    }
    return $parent;
}
add_filter( 'wp_update_term_parent', 'ualpHookEditedTerm', 10, 5 );

/*
 * Get activity for the user - Switch Theme
 *
 * @param string $theme Post Title
 *
 */

if (!function_exists('ualpHookSwitchTheme')) {

    function ualpHookSwitchTheme($themename,$theme) {
        global $old_theme_data;
        $logHookList = get_option('enableLogHookList');
        if (in_array('switch_theme',$logHookList)) {
            $action = "switched";
            $obj_type = "Theme";
            $post_id = "";
            $post_title = $themename;
            $hook = "switch_theme";
            $description = "$obj_type $action : $post_title";
            $new_theme = array(
                'name' => $theme->name,
                'version' => $theme->version
            );
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetailSwitchTheme($uactid, $old_theme_data, $new_theme);
        }
    }
}
add_action('switch_theme', 'ualpHookSwitchTheme',2,2);

/*
 * Get activity for the user - Load Theme
 *
 */
if(!function_exists('ualpHookLoadTheme')) {
    function ualpHookLoadTheme() {
        global $old_theme_data;
        if (!isset($_GET["action"]) || $_GET["action"] != "activate") {
            return;
        }
        $current_theme = wp_get_theme();
        if (!is_a($current_theme, "WP_Theme")) {
            return;
        }
        $old_theme_data = array(
            'name' => $current_theme->name,
            'version' => $current_theme->version
        );
    }
}
add_action('load-themes.php', 'ualpHookLoadTheme');

/*
 * Get activity for the user - Customize Theme
 *
 */
if (!function_exists('ualpHookCustomizeSave')){

    function ualpHookCustomizeSave($instance) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('customize_save',$logHookList)) {
            $action = "customize updated";
            $obj_type = "Theme";
            $post_id = "";
            $post_title = "Theme Customizer";
            $hook = "customize_save";
            $description = "$obj_type $action : $post_title";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpHookCustomizeSaveDetail($uactid,$instance);
        }
    }
}

add_action('customize_save', 'ualpHookCustomizeSave', 10, 1); // new

/**
 * @param Theme_Upgrader $upgrader
 * @param array $extra
 */
if (!function_exists('ualpHookThemeInstallUpdate')) {

    function ualpHookThemeInstallUpdate($upgrader, $extra) {
        if (!isset($extra['type']) || 'theme' !== $extra['type'])
            return;
        global $old_theme_data;
        $obj_type = "Theme";
        $post_id = "";
        $logHookList = get_option('enableLogHookList');
        if ('install' === $extra['action']) {
            $slug = $upgrader->theme_info();
            if (!$slug)
                return;

            wp_clean_themes_cache();
            $theme = wp_get_theme($slug);
            $name = $theme->name;
            $version = $theme->version;
            $action = 'installed';
            $hook = "theme_installed";
            $new_theme = array(
                'name' => $theme->name,
                'version' => $theme->version
            );
            $post_title = $name . ' ' . $version;
            $description = "Theme $action : $post_title";
            if (in_array('theme_installed',$logHookList)) {
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                ualpAddActivityLogDetailUpgradeTheme($uactid, $new_theme);
            }
        }
        if ('update' === $extra['action']) {
            if (isset($extra['bulk']) && true == $extra['bulk'])
                $slugs = $extra['themes'];
            else
                $slugs = array($upgrader->skin->theme);

            foreach ($slugs as $slug) {
                $theme = wp_get_theme($slug);
                $stylesheet = $theme['Stylesheet Dir'] . '/style.css';
                $theme_data = get_file_data($stylesheet, array('Version' => 'Version'));
                $name = $theme['Name'];
                $version = $theme_data['Version'];
                $new_theme = array(
                    'name' => $name,
                    'version' => $version
                );
                $action = 'updated';
                $hook = "theme_updated";
            }
            $post_title = $name . ' ' . $version;
            $description = "Theme $action : $post_title";
            if (in_array('theme_updated',$logHookList)) {
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                ualpAddActivityLogDetailUpgradeTheme($uactid, $new_theme);
            }
        }
    }

}
add_action('upgrader_process_complete', 'ualpHookThemeInstallUpdate', 10, 2);

/*
 * Get activity for the user - Delelte theme
 *
 */
if (!function_exists('ualpHookThemeDeleted')){

    function ualpHookThemeDeleted() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('delete_site_transient_update_themes',$logHookList)) {
            $backtrace_history = debug_backtrace();
            $delete_theme_call = null;
            foreach ($backtrace_history as $call) {
                if (isset($call['function']) && 'delete_theme' === $call['function']) {
                    $delete_theme_call = $call;
                    break;
                }
            }
            if (empty($delete_theme_call))
                return;
            $name = $delete_theme_call['args'][0];
            $action = 'deleted';
            $obj_type = 'Theme';
            $post_title = $name;
            $post_id = "";
            $hook = "delete_themes";
            $description = "Theme $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('delete_site_transient_update_themes', 'ualpHookThemeDeleted');


if (!function_exists('ualpHookThemeModify')) {

    function ualpHookThemeModify($location, $status) {

        if (strpos($location, 'theme-editor.php?file=') !== false) {

            if (!empty($_POST) && $_POST['action'] === 'update') {
                $logHookList = get_option('enableLogHookList');
                if (in_array('theme_file_modify',$logHookList)) {
                    $action = "File Updated";
                    $post_title = $_POST['theme'] . " - " . $_POST['file'];
                    $post_id = "";
                    $obj_type = "Theme";
                    $hook = "wp_redirect";
                    $description = "$action : $post_title";
                    ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                }
            }
        }
        // We are need return the instance, for complete the filter.
        return $location;
    }

}

add_filter('wp_redirect', 'ualpHookThemeModify', 10, 2); // new

/*
 * Get activity for the user - Activate Plugin
 *
 * @param string $plugin Post Title
 *
 */
if (!function_exists('ualpHookActivatedPlugin')){

    function ualpHookActivatedPlugin($plugin) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('activated_plugin',$logHookList)) {
            $action = "activated";
            $obj_type = "Plugin";
            $post_id = "";
            $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin, true, false);
            $post_title = $plugin_data['Name'];
            $hook = "activated_plugin";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('activated_plugin', 'ualpHookActivatedPlugin');

/*
 * Get activity for the user - Deactivate Plugin
 *
 * @param string $plugin Post Title
 *
 */
if (!function_exists('ualpHookDeactivatedPlugin')){

    function ualpHookDeactivatedPlugin($plugin) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('deactivated_plugin',$logHookList)) {
            $action = "deactivated";
            $obj_type = "Plugin";
            $post_id = "";
            $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin, true, false);
            $post_title = $plugin_data['Name'];
            $hook = "deactivated_plugin";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('deactivated_plugin', 'ualpHookDeactivatedPlugin');

/**
 * Get activity for the user - Delete Plugin
 * @param type $plugin_file
 */
if (!function_exists('ualpHookDeletePlugin')){

    function ualpHookDeletePlugin($plugin_file) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('delete_plugin',$logHookList)) {
            $action = "deleted";
            $obj_type = "Plugin";
            $post_id = "";
            $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/' . $plugin_file, true, false);
            $post_title = $plugin_data['Name'];
            $hook = "delete_plugin";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('delete_plugin', 'ualpHookDeletePlugin', 10, 1); // new

/**
 * Get activity for the user - Modify Plugin
 * @param type $location
 * @param type $status
 * @return type
 */
if (!function_exists('ualpHookPluginModify')) {

    function ualpHookPluginModify($location, $status) {
        if (strpos($location, 'plugin-editor.php') !== false) {
            if ((!empty($_POST) && 'update' === $_REQUEST['action']) && !empty($_REQUEST['file'])) {
                
                $plugin_explode = explode( '/', $_REQUEST['file'] );
                $pluginData = get_plugins( '/' . $plugin_explode[0] );
                foreach($pluginData as $plugin){
                    $pluginName = $plugin['Name'];
                }                
                $logHookList = get_option('enableLogHookList');
                if (in_array('plugin_modify',$logHookList)) {
                    $action = "File Changed";
                    $obj_type = "Plugin";
                    $post_id = "";
                    $post_title = $pluginName ." - ". $_REQUEST['file'];
                    $hook = "wp_redirect";
                    $description = "$obj_type $action : $post_title";
                    ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                }
            }
        }
        return $location;
    }

}
add_filter('wp_redirect', 'ualpHookPluginModify', 10, 2); // new

/*
 * Get activity for the user - Upgrader process complete
 *
 */
if (!function_exists('ualpHooksPluginInstallUpdate')) {

    function ualpHooksPluginInstallUpdate($upgrader, $extra) {
        if (!isset($extra['type']) || $extra['type'] !== 'plugin')
            return;

        $obj_type = "Plugin";
        $post_id = "";
        $logHookList = get_option('enableLogHookList');
        if ($extra['action'] === 'install') {
            $path = $upgrader->plugin_info();
            if (!$path)
                return;

            $data = get_plugin_data($upgrader->skin->result['local_destination'] . '/' . $path, true, false);
            $action = "Installed";
            $post_title = $data['Name'];
            $hook = "plugin_installed";
            $description = "$obj_type $action : $post_title";
            if (in_array('plugin_installed',$logHookList)) {
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
        if ($extra['action'] === 'update') {
            if (isset($extra['bulk']) && $extra['bulk'] == true) {
                $slugs = $extra['plugins'];
            } else {
                if (!isset($upgrader->skin->plugin))
                    return;
                $slugs = array($upgrader->skin->plugin);
            }
            foreach ($slugs as $slug) {
                $data = get_plugin_data(WP_PLUGIN_DIR . '/' . $slug, true, false);
                $action = "Updated";
            }
            $post_title = $data['Name'];
            $hook = "plugin_updated";
            $description = "$obj_type $action : $post_title";
            if (in_array('plugin_updated',$logHookList)) {
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }

}
add_action('upgrader_process_complete', 'ualpHooksPluginInstallUpdate', 10, 2); // new

/*
 * Get activity for the user - Export wordpress data
 *
 */
if (!function_exists('ualpHookExportWp')){

    function ualpHookExportWp($args) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('export_wp',$logHookList)) {
            $action = "Downloaded";
            $obj_type = "Export";
            $post_id = "";
            $post_title = isset( $args['content'] ) ? $args['content'] : 'all';
            $hook = "export_wp";
            $description = "$obj_type file $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('export_wp', 'ualpHookExportWp');

/*
 * Get activity for the user - Import wordpress data
 *
 */
if (!function_exists('ualpHookImportWp')){

    function ualpHookImportWp() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('import_end',$logHookList)) {
            $action = "Wordpress";
            $obj_type = "Import";
            $post_id = "";
            $post_title = $obj_type;
            $hook = "import_end";
            $description = "$obj_type $action data";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('import_end', 'ualpHookImportWp'); // new

/*
 * Get activity for the user - Activate Plugin
 *
 * @param string $new_status new posts status
 * @param string $old_status old posts status
 * @param object $post posts
 *
 */
if (!function_exists('ualpHookTransitionPostStatus')){

    function ualpHookTransitionPostStatus($new_status, $old_status, $post) {

        global $old_post_data;
        $action = '';
        $obj_type = $post->post_type;
        $logHookList = get_option('enableLogHookList');
        if (in_array($obj_type,$logHookList)) {
            if ("nav_menu_item" == get_post_type($post) || wp_is_post_revision($post)) {
                return;
            }
            $post_id = $post->ID;
            $post_title = $post->post_title;
            $action = "";

            if ( $new_status == 'auto-draft' || ( $old_status === 'new' &&  $new_status === 'inherit' ))
            return;

            if ($old_status === 'auto-draft' && ($new_status != 'auto-draft' && $new_status != 'inherit') ) {
                $action = 'created';
            } elseif ($new_status === 'trash') {
                $action = 'trash';
            } elseif ($new_status === 'pending') {
                $action = 'pending';
            } elseif ($new_status === 'draft') {
                $action = 'draft';
            } elseif ($new_status === 'publish' && $old_status == 'trash' ) {
                $action = 'restore';
            }else {
                $action = 'updated';
            }
            $hook = "transition_post_status";
            $description = "$obj_type $action : <a target='blank' href='". get_edit_post_link($post_id)."'>$post_title</a>";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            $new_post_data = array(
                "post_data" => $post,
                "post_meta" => get_post_custom($post->ID)
            );

            ualpAddActivityLogDetailPostUpdate($uactid,$old_post_data,$new_post_data);
        }
    }
}

add_action('transition_post_status', 'ualpHookTransitionPostStatus', 10, 3);

/**
 * Get activity for the user - Edit Post
 * 
 * @global type $old_post_data
 * @return type
 */
if(!function_exists('ualpHookEditPost')) {

    function ualpHookEditPost() {
        global $old_post_data;
        $post_ID = isset( $_POST["post_ID"] ) ? (int) $_POST["post_ID"] : 0;
        if (!$post_ID) {
            return;
        }
        if (!current_user_can('edit_post',$post_ID)) {
            return;
        };
        $prev_post_data = get_post($post_ID);
        $old_post_data = array(
            "post_data" => $prev_post_data,
            "post_meta" => get_post_custom($post_ID)
        );
    }
}
add_action("admin_action_editpost","ualpHookEditPost");

/**
 * Get activity for the user - Delete Post
 * 
 * @global type $post_type
 * @global type $wpdb
 * @param type $pid
 */
if(!function_exists('ualpHookDeletePost')){

    function ualpHookDeletePost( $pid ) {
        $logHookList = get_option('enableLogHookList');
        global $post_type,$wpdb;
        if (in_array($post_type,$logHookList)) {
            if(did_action( 'before_delete_post' ) == 1){
                $action = "deleted";
                $obj_type = $post_type;
                $post_id = $pid;
                $post_title = get_the_title($pid);
                $hook = "before_delete_post";
                $description = "$obj_type $action permanently: $post_title";
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }
}
add_action( 'before_delete_post', 'ualpHookDeletePost', 10 , 1 );

/*
 * Get activity for the user - Widget update
 *
 * @param string $widget widget data
 */
if (!function_exists('ualpHookWidgetUpdate')) {
    function ualpHookWidgetUpdate($instance, $new_instance, $old_instance, $widget_instance) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('widget_update_callback',$logHookList)) {
            if (empty($old_instance)) {
                return $instance;
            }
            $action = "updated";
            $obj_type = "Widget";
            $post_id = "";
            $post_title = $widget_instance->name;
            $hook = "widget_update_callback";
            $sidebar_id = $_POST["sidebar"];
            $sidebar = false;
            $sidebars = isset($GLOBALS['wp_registered_sidebars']) ? $GLOBALS['wp_registered_sidebars'] : false;
            $sidebar_name = '';
            if ($sidebars) {
                if (isset($sidebars[$sidebar_id])) {
                    $sidebar = $sidebars[$sidebar_id];
                }
            }
            if ($sidebar) {
                $sidebar_name = $sidebar["name"];
            }
            $description = "$post_title $obj_type $action in $sidebar_name";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpHookWidgetUpdateDetail($uactid,$instance, $new_instance, $old_instance, $widget_instance);
        }
        return $instance;
    }
}
add_filter('widget_update_callback', 'ualpHookWidgetUpdate', 8, 4);

/*
 * Get activity for the user - Widget Add Delete
 */
if(!function_exists('ualpHookWidgetAddedDeleted')) {
    function ualpHookWidgetAddedDeleted() {
        if ((isset($_POST["add_new"]) && !empty($_POST["add_new"]) && isset($_POST["sidebar"]) && isset($_POST["id_base"])) || isset($_POST["delete_widget"])) {
            $obj_type = "Widget";
            $post_id = "";
            $hook = "sidebar_admin_setup";
            $widget_id_base = $_POST["id_base"];
            $widget = false;
            $widget_detail_ary = array();
            $widget_factory = isset($GLOBALS["wp_widget_factory"]) ? $GLOBALS["wp_widget_factory"] : false;
            if ($widget_factory) {
                foreach ($widget_factory->widgets as $one_widget) {
                    if ($one_widget->id_base == $widget_id_base) {
                        $widget = $one_widget;
                    }
                }
            }
            if ($widget) {
                $post_title = $widget->name;
                $widget_detail_ary["ualp_widget_name_translated"] = $widget->name;
                $widget_detail_ary["ualp_widget_id_base"] = $widget_id_base;
            }
            $sidebar_id = $_POST["sidebar"];
            $sidebar = false;
            $sidebars = isset($GLOBALS['wp_registered_sidebars']) ? $GLOBALS['wp_registered_sidebars'] : false;
            if ($sidebars) {
                if (isset($sidebars[$sidebar_id])) {
                    $sidebar = $sidebars[$sidebar_id];
                }
            }
            if ($sidebar) {
                $sidebar_name = $sidebar["name"];
                $widget_detail_ary["ualp_sidebar_name_translated"] = $sidebar["name"];
                $widget_detail_ary["ualp_sidebar_id"] = $sidebar_id;
            }
            if(isset($_POST["delete_widget"])) {
                $action = "removed";
                $description = "$obj_type $post_title '$action' from '$sidebar_name'";
            }
            else {
                $action = "added";
                $description = "$obj_type $post_title '$action' in '$sidebar_name'";
            }
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetail($uactid, $widget_detail_ary);
        }
    }
}
add_action("sidebar_admin_setup", 'ualpHookWidgetAddedDeleted');

/*
 * Get activity for the user - Core file updated successfully
 *
 */
if (!function_exists('ualpHookCoreUpdated')){

    function ualpHookCoreUpdated($wp_version) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('_core_updated_successfully',$logHookList)) {
            $action = "update";
            $obj_type = "Core";
            $post_id = "";
            $post_title = $wp_version;
            $hook = "_core_updated_successfully";
            $description = "Wordpress $obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('_core_updated_successfully', 'ualpHookCoreUpdated');

/**
 * Get activity for Admin View
 * @global type $admin_title
 */
if (!function_exists('ualpHookAdminView')){

    function ualpHookAdminView() {
        global $admin_title;
        $currentUrl = ualpGetCurrentAdminPageUrl();
        $logHookList = get_option('enableLogHookList');
        if (in_array('admin_head',$logHookList)) {
            $action = "Viewed";
            $obj_type = "Admin Page";
            $post_id = "";
            $post_title = $admin_title;
            $hook = "admin_head";
            $description = "$action $obj_type : <a target='blank' href='$currentUrl'>$post_title</a>";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('admin_head', 'ualpHookAdminView'); // new

/**
 * Get activity for Front View
 * 
 * @global type $wp
 */
if (!function_exists('ualpHookFrontView')){

    function ualpHookFrontView() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wp_head',$logHookList)) {
            global $wp;
            $action = "Viewed";
            $obj_type = "Page";
            $current_url = "";
            $post_title = "";
            $post_id = "";
            if ( is_single() ){
                $post_id = get_the_ID();
                $post_title = get_the_title();
                $current_url = get_edit_post_link($post_id);
            }else {
                $current_url = ualpGetCurrentAdminPageUrl();
                $post_title = $current_url;
            }
            $hook = "wp_head";
            $description = "$action $obj_type : <a target='blank' href='$current_url'>$current_url</a>";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_head', 'ualpHookFrontView'); // new

/**
 * Get activity for wpcf7 Create
 * 
 * @param type $cf7
 */
if (!function_exists('ualpHookwpcf7Create')) {

    function ualpHookwpcf7Create($cf7) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wpcf7_after_create',$logHookList)) {
            $action = "Created";
            $obj_type = "Contact Form 7";
            $post_id = "";
            $cf7 = current((Array) $cf7);
            $cf7 = get_the_title($cf7);
            $post_title = $cf7;
            $hook = "wpcf7_after_create";
            $description = "$obj_type $action: $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}

add_action('wpcf7_after_create', 'ualpHookwpcf7Create', 10, 1); // new

/**
 * Get activity for wpcf7 Update
 *
 * @param type $cf7
 */
if (!function_exists('ualpHookwpcf7Update')) {

    function ualpHookwpcf7Update($cf7) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wpcf7_after_update',$logHookList)) {
            $action = "Updated";
            $obj_type = "Contact Form 7";
            $post_id = "";
            $cf7 = current((Array) $cf7);
            $cf7 = get_the_title($cf7);
            $post_title = $cf7;
            $hook = "wpcf7_after_update";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wpcf7_after_update', 'ualpHookwpcf7Update', 10, 1); // new

/**
 * Get activity for wpcf7 Delete
 * 
 * @param type $cf7
 * @return type
 */
if (!function_exists('ualpHookwpcf7Delete')) {

    function ualpHookwpcf7Delete($cf7) {
        if (empty($_REQUEST['message'])) {
            return;
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('wpcf7_admin_notices',$logHookList)) {
            if ('deleted' == $_REQUEST['message']) {
                $action = "Deleted";
                $obj_type = "Contact Form 7";
                $post_id = "";
                $post_title = $obj_type;
                $hook = "wpcf7_admin_notices";
                $description = "$obj_type $action : $post_title";
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }

}
add_action('wpcf7_admin_notices', 'ualpHookwpcf7Delete'); // new

/*
 * function to add custom actions
 */
if(!function_exists('ualpCustomEventHook')) {
    function ualpCustomEventHook() {
        $custom_event_data = get_option('custom_event_data');
        if(is_array($custom_event_data) && !empty($custom_event_data)) {
            foreach($custom_event_data as $custom_event_single) {
                add_action($custom_event_single['custom_event_name'],'ualpCustomEventAction');
            }
        }
    }
}
ualpCustomEventHook();

/*
 * callback function to add custom actions
 */
function ualpCustomEventAction() {
    $custom_event_data = get_option('custom_event_data');
    if(is_array($custom_event_data) && !empty($custom_event_data)) {
        foreach($custom_event_data as $custom_event_single) {
            $action = $custom_event_single['custom_event_label'];
            $hook = $custom_event_single['custom_event_name'];
            $obj_type = $custom_event_single['custom_event_type'];
            $description = $custom_event_single['custom_event_description'];
            if(current_action() == $custom_event_single['custom_event_name']) {
                ualpGetActivityLog($action, $obj_type, "", "", $hook,$description );
            }
        }
    }
}

/*
 * Add log for export user activities log
 */
if (!function_exists('ualpHookUalpExportLog')){

    function ualpHookUalpExportLog() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_export_activity_log',$logHookList)) {
            $action = "Export";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Export Activity Log";
            $hook = "ualp_export_activity_log";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_export_activity_log', 'ualpHookUalpExportLog'); // new

/*
 * Add log for general settings
 */
if (!function_exists('ualpHookUalpGeneralSettings')){

    function ualpHookUalpGeneralSettings() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_general_settings_updated',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "General Settings";
            $hook = "ualp_general_settings_updated";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('ualp_general_settings_updated', 'ualpHookUalpGeneralSettings'); // new

/*
 * Add log for hook settings
 */
if (!function_exists('ualpHookUalpLogHookSettings')){

    function ualpHookUalpLogHookSettings() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_hook_log_settings_updated',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Log Hook Settings";
            $hook = "ualp_hook_log_settings_updated";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('ualp_hook_log_settings_updated', 'ualpHookUalpLogHookSettings'); // new

/*
 * Add log for hook settings
 */
if (!function_exists('ualpHookUalpRoleSettings')){

    function ualpHookUalpRoleSettings() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_manage_role',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Role settings updated";
            $hook = "ualp_manage_role";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('ualp_manage_role', 'ualpHookUalpRoleSettings'); // new

/*
 * Add log for set password
 */
if (!function_exists('ualpHookUalpPasswordSet')){

    function ualpHookUalpPasswordSet() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_set_password',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Password Set";
            $hook = "ualp_set_password";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_set_password', 'ualpHookUalpPasswordSet'); // new

/*
 * Add log for change password
 */
if (!function_exists('ualpHookUalpPasswordChange')){

    function ualpHookUalpPasswordChange() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_change_password',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Password Updated";
            $hook = "ualp_change_password";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_change_password', 'ualpHookUalpPasswordChange'); // new

/*
 * Add log for email settings update
 */
if (!function_exists('ualpHookEmailSettingsUpdated')){

    function ualpHookEmailSettingsUpdated() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_email_settings_updated',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Email Settings Updated";
            $hook = "ualp_email_settings_updated";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_email_settings_updated', 'ualpHookEmailSettingsUpdated'); // new

/*
 * Add log for disable email notification
 */
if (!function_exists('ualpHookDisableEmail')){

    function ualpHookDisableEmail() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_disable_email_notification',$logHookList)) {
            $action = "Settings";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Disable Email Notification";
            $hook = "ualp_disable_email_notification";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_disable_email_notification', 'ualpHookDisableEmail'); // new

/*
 * Add log for update custom event
 */
if (!function_exists('ualpHookUalpCustomEvent')){

    function ualpHookUalpCustomEvent() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('ualp_custom_event_log_updated',$logHookList)) {
            $action = "Settings Updated";
            $obj_type = "User Activity Log";
            $post_id = "";
            $post_title = "Custom Event Updated";
            $hook = "ualp_custom_event_log_updated";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('ualp_custom_event_log_updated', 'ualpHookUalpCustomEvent'); // new

if(!function_exists('ualpHookUpdatedOption')){
    function ualpHookUpdatedOption($option, $oldvalue, $_newvalue) {
        $whitelist_options = array(
            // General
            'blogname',
            'blogdescription',
            'siteurl',
            'home',
            'admin_email',
            'users_can_register',
            'default_role',
            'timezone_string',
            'date_format',
            'time_format',
            'start_of_week',
            // Writing
            'use_smilies',
            'use_balanceTags',
            'default_category',
            'default_post_format',
            'mailserver_url',
            'mailserver_login',
            'mailserver_pass',
            'default_email_category',
            'ping_sites',
            // Reading
            'show_on_front',
            'page_on_front',
            'page_for_posts',
            'posts_per_page',
            'posts_per_rss',
            'rss_use_excerpt',
            'blog_public',
            // Discussion
            'default_pingback_flag',
            'default_ping_status',
            'default_comment_status',
            'require_name_email',
            'comment_registration',
            'close_comments_for_old_posts',
            'close_comments_days_old',
            'thread_comments',
            'thread_comments_depth',
            'page_comments',
            'comments_per_page',
            'default_comments_page',
            'comment_order',
            'comments_notify',
            'moderation_notify',
            'comment_moderation',
            'comment_whitelist',
            'comment_max_links',
            'moderation_keys',
            'blacklist_keys',
            'show_avatars',
            'avatar_rating',
            'avatar_default',
            // Media
            'thumbnail_size_w',
            'thumbnail_size_h',
            'thumbnail_crop',
            'medium_size_w',
            'medium_size_h',
            'large_size_w',
            'large_size_h',
            'uploads_use_yearmonth_folders',
            // Permalinks
            'permalink_structure',
            'category_base',
            'tag_base'
        ); 

        if (!in_array($option, $whitelist_options))
            return;

        $logHookList = get_option('enableLogHookList');
        if (in_array('updated_option',$logHookList)) {
            $action = "updated";
            $obj_type = "Settings";
            $post_id = "";
            $post_title = $option;
            $hook = "updated_option";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('updated_option', 'ualpHookUpdatedOption', 10, 3);