<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}

/**
 * General settings
 */
if (!function_exists('ualpSettingsPanel')) {

    function ualpSettingsPanel() {
        ?>
        <div class="wrap">
            <h1><?php _e('Settings', 'user_activity_log_pro'); ?></h1>
            <?php
            if (isset($_SESSION['success_msg'])) { ?>
                <div class="success_msg"><?php
                    ualpAdminNoticeMessage("updated", $_SESSION['success_msg']);
                    unset($_SESSION['success_msg']);
                    ?>
                </div><?php
            }?>
            <div class="tab_parent_parent ualpParentTabs">
                <h2 class="nav-tab-wrapper nav-tab-wrapper">
                    <a class="nav-tab nav-tab-active ualpGeneralSettings" data-href="ualpGeneralSettings" href="javascript:void(0)" >
                        <?php _e('General Settings', 'user_activity_log_pro'); ?>
                    </a>
                    <a class="nav-tab ualpHookSettings" data-href="ualpHookSettings" href="javascript:void(0)">
                        <?php _e('Hook Settings', 'user_activity_log_pro'); ?>
                    </a>
                    <a class="nav-tab ualpPwdSettings" data-href="ualpPwdSettings" href="javascript:void(0)">
                        <?php _e('Password Settings', 'user_activity_log_pro'); ?>
                    </a>
                    <a class="nav-tab ualpEmailSettings" data-href="ualpEmailSettings" href="javascript:void(0)">
                        <?php _e('Email Notification', 'user_activity_log_pro'); ?>
                    </a>
                    <a class="nav-tab ualpUsersSettings" data-href="ualpUsersSettings" href="javascript:void(0)">
                        <?php _e('Role Manager', 'user_activity_log_pro'); ?>
                    </a>
                    <a class="nav-tab ualpCustomEventSettings" data-href="ualpCustomEventSettings" href="javascript:void(0)">
                        <?php _e('Custom Event Settings', 'user_activity_log_pro'); ?>
                    </a>
                </h2>
            </div>
            <div class="ualpTabContentWrap">
                <div id="ualpGeneralSettings" style="display: none" class="ualpContentDiv"><?php ualpGeneralSettings(); ?></div>
                <div id="ualpHookSettings" style="display: none" class="ualpContentDiv"><?php ualpLogSettings(); ?></div>
                <div id="ualpEmailSettings" style="display: none" class="ualpContentDiv"><?php ualpEmailSettings(); ?></div>
                <div id="ualpUsersSettings" style="display: none" class="ualpContentDiv"><?php ualpRoleSetting(); ?></div>
                <div id="ualpPwdSettings" style="display: none" class="ualpContentDiv"><?php ualpPwdSettings(); ?></div>
                <div id="ualpCustomEventSettings" style="display: none" class="ualpContentDiv"><?php ualpCustomEventSettings(); ?></div>
            </div>
        </div>
        <?php
    }

}