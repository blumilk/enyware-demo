<?php
/**
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/**
 * Fires once the admin request has been validated or not.
 *
 * @param string    $action The nonce action.
 * @param false|int $result False if the nonce is invalid,
 *                  1 if the nonce is valid and generated between 0-12 hours ago,
 *                  2 if the nonce is valid and generated between 12-24 hours ago.
 */
if (!function_exists('ualpHookGformsSaveEntry')){

    function ualpHookGformsSaveEntry($action,$result) {
        if( $action == "gforms_save_entry" || $action == "gf_export_forms" || $action == "gf_import_forms" ||
            $action == "gforms_save_notification" || $action == "gforms_update_settings" )  {
            $logHookList = get_option('enableLogHookList');
            if (in_array('gravityforms',$logHookList)) {
                if($action == "gforms_save_entry"){
                    $post_title = "Entry saved successfully";
                }
                if($action == "gf_export_forms"){
                    $post_title = "Form exported successfully";
                }
                if($action == "gf_import_forms"){
                    $post_title = "Form imported successfully";
                }
                if($action == "gforms_save_notification"){
                    $post_title = "Email Notifcation saved successfully";
                }
                if($action == "gforms_update_settings"){
                    $post_title = "Settings Updated Successfully";
                }
                $obj_type = "Gravity Form";
                $post_id = "";
                $hook = $action;
                $description = "$obj_type : $post_title";
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }
}

add_action('check_admin_referer', 'ualpHookGformsSaveEntry', 10, 2);

if (!function_exists('ualpHookGformsDuplicateNotify')){

    function ualpHookGformsDuplicateNotify($action,$result) {
        if($action == "gform_notification_list_action" )  {
            $logHookList = get_option('enableLogHookList');
            if (in_array('gravityforms',$logHookList)) {
                $actions = rgpost( 'action' );
                if($actions == "duplicate"){
                    $post_title = "Notification duplicated successfully";
                    $obj_type = "Gravity Form";
                    $post_id = "";
                    $hook = "notification_duplicated";
                    $description = "$obj_type : $post_title";
                    ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                }
            }
        }
    }
}

add_action('check_admin_referer', 'ualpHookGformsDuplicateNotify', 10, 2);

/**
 * Fires when input type changed
 */
if (!function_exists('ualpHook_wp_ajax_rg_change_input_type')){

    function ualpHook_wp_ajax_rg_change_input_type() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Change input type";
            $hook = "wp_ajax_rg_change_input_type";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_change_input_type', 'ualpHook_wp_ajax_rg_change_input_type');
/**
 * Fires when make any duplicate field
 */
if (!function_exists('ualpHook_wp_ajax_rg_duplicate_field')){

    function ualpHook_wp_ajax_rg_duplicate_field() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Field duplicated successfully";
            $hook = "wp_ajax_rg_duplicate_field";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_duplicate_field', 'ualpHook_wp_ajax_rg_duplicate_field');
/**
 * Fires when delete any field
 */
if (!function_exists('ualpHook_wp_ajax_rg_delete_field')){

    function ualpHook_wp_ajax_rg_delete_field() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Field deleted successfully";
            $hook = "wp_ajax_rg_delete_field";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_delete_field', 'ualpHook_wp_ajax_rg_delete_field');
/**
 * Fires when delete any file
 */
if (!function_exists('ualpHook_wp_ajax_rg_delete_file')){

    function ualpHook_wp_ajax_rg_delete_file() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "File deleted successfully";
            $hook = "wp_ajax_rg_delete_file";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_delete_file', 'ualpHook_wp_ajax_rg_delete_file');
/**
 * Fires when select form for export
 */
if (!function_exists('ualpHook_wp_ajax_rg_select_export_form')){

    function ualpHook_wp_ajax_rg_select_export_form() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Select form for export entries";
            $hook = "rg_select_export_form";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_select_export_form', 'ualpHook_wp_ajax_rg_select_export_form');
/**
 * Fires when custom choice is deleted
 */
if (!function_exists('ualpHook_wp_ajax_gf_delete_custom_choice')){

    function ualpHook_wp_ajax_gf_delete_custom_choice() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Custom choice deleted";
            $hook = "wp_ajax_gf_delete_custom_choice";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_gf_delete_custom_choice', 'ualpHook_wp_ajax_gf_delete_custom_choice');

/**
 * Fires when custom choice is save
 */
if (!function_exists('ualpHook_wp_ajax_gf_save_custom_choice')){

    function ualpHook_wp_ajax_gf_save_custom_choice() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Save custom choice";
            $hook = "wp_ajax_gf_save_custom_choice";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_gf_save_custom_choice', 'ualpHook_wp_ajax_gf_save_custom_choice');

/**
 * Fires when form saved successfuly
 */
if (!function_exists('ualpHook_wp_ajax_rg_save_form')){

    function ualpHook_wp_ajax_rg_save_form() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Saved Successfuly";
            $hook = "rg_save_form";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_save_form', 'ualpHook_wp_ajax_rg_save_form');

/**
 * Fires when new field added
 */
if (!function_exists('ualpHook_wp_ajax_rg_add_field')){

    function ualpHook_wp_ajax_rg_add_field() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Field added successfully";
            $hook = "rg_add_field";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_rg_add_field', 'ualpHook_wp_ajax_rg_add_field');

/**
 * Fired after an entry is created
 *
 * @param array $lead The Entry object
 * @param array $form The Form object
 */
if (!function_exists('ualpHook_gform_entry_created')){

    function ualpHook_gform_entry_created($lead, $form) {        
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $form_title = '';
            if(isset($form['title']) && !empty($form['title'])){
                $form_title = $form['title'];
            }
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "New entry created";
            $hook = "gform_entry_created";
            $description = "$obj_type : $post_title in '".$form_title."'";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('gform_entry_created', 'ualpHook_gform_entry_created', 10, 2);

/**
 * Fires when resend email notifications
 */
if (!function_exists('ualpHook_wp_ajax_gf_resend_notifications')){

    function ualpHook_wp_ajax_gf_resend_notifications() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Resend notifications";
            $hook = "gf_resend_notifications";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_gf_resend_notifications', 'ualpHook_wp_ajax_gf_resend_notifications');

/**
 * Fires before a note is deleted
 *
 * @param int $note_id The current note ID
 * @param int $lead_id The current lead ID
 */
if (!function_exists('ualpHookgform_pre_note_deleted')){
    function ualpHookgform_pre_note_deleted($note_id, $lead_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_lead_notes';
        $note_name = $wpdb->get_results( "SELECT value FROM $table_name WHERE note_type = 'note' AND id = $note_id AND lead_id = $lead_id" , ARRAY_A);
        $notename = $note_name[0]['value'];
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Note deleted successfully";
            $hook = "gform_pre_note_deleted";
            $description = "$obj_type : '$notename' $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_pre_note_deleted', 'ualpHookgform_pre_note_deleted',10,2);

/**
 * Fires when settings updated
 *
 * @param string $option
 * @param string $oldvalue
 * @param string $_newvalue
 */
if(!function_exists('ualpHookGarvityFormUpdatedOption')){
    function ualpHookGarvityFormUpdatedOption($option, $oldvalue, $_newvalue) {
        $whitelist_options = array(
            'gform_email_count',
            'gform_enable_noconflict',
            'rg_gforms_currency',
            'gform_enable_toolbar_menu',
            'rg_gforms_key',
            'gform_enable_background_updates',
            'rg_gforms_enable_html5',
            'rg_gforms_captcha_public_key',
            'rg_gforms_captcha_private_key',
        );

        if (!in_array($option, $whitelist_options))
            return;

        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "updated";
            $obj_type = "Settings";
            $post_id = "";
            $post_title = $option;
            $hook = "updated_option";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('updated_option', 'ualpHookGarvityFormUpdatedOption', 20, 3);

/**
 * Fires after exporting all the entries in form
 *
 * @param array  $form       The Form object to get the entries from
 * @param string $start_date The start date for when the export of entries should take place
 * @param string $end_date   The end date for when the export of entries should stop
 * @param array  $fields     The specified fields where the entries should be exported from
 */
if (!function_exists('ualpHookgform_post_export_entries')){
    function ualpHookgform_post_export_entries($form, $start_date, $end_date, $fields) {
        $form_title = "";
        if(isset($form['title']) && !empty($form['title'])){
            $form_title = $form['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Post entries exported";
            $hook = "gform_post_export_entries";
            $description = "$obj_type : $post_title from '$form_title'";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_export_entries', 'ualpHookgform_post_export_entries',10,4);

/**
 * Fires after a note has been added to an entry
 *
 * @param int    $note_id         The row ID of this note in the database
 * @param int    $lead_id         The ID of the entry that the note was added to
 * @param int    $user_id         The ID of the current user adding the note
 * @param string $user_name       The user name of the current user
 * @param string $note            The content of the note being added
 * @param string $note_type       The type of note being added.  Defaults to 'note'
 */
if (!function_exists('ualpHookgform_post_note_added')){
    function ualpHookgform_post_note_added($note_id, $lead_id, $user_id, $user_name, $note, $note_type) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_lead_notes';
        $note_name = $wpdb->get_results( "SELECT value FROM $table_name WHERE note_type = 'note' AND id = $note_id AND lead_id = $lead_id" , ARRAY_A);
        $notename = $note_name[0]['value'];
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Post note added";
            $hook = "gform_post_note_added";
            $description = "$obj_type : '$notename' $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_note_added', 'ualpHookgform_post_note_added', 10, 6);

/**
 * Fires when you delete entries for a specific form
 *
 * @param int    $form_id The form ID to specify from which form to delete entries
 * @param string $status  Allows you to set the form entries to a deleted status
 */
if (!function_exists('ualpHookgform_delete_entries')){
    function ualpHookgform_delete_entries($form_id, $status) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Entry deleted successfully";
            $hook = "gform_delete_entries";
            $description = "$obj_type : $post_title from '$form_title'";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_delete_entries', 'ualpHookgform_delete_entries', 10, 2);

/**
 * Fires after form views are deleted
 *
 * @param int $form_id The ID of the form that views were deleted from
 */
if (!function_exists('ualpHookgform_post_form_views_deleted')){
    function ualpHookgform_post_form_views_deleted($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Views deleted";
            $hook = "gform_post_form_views_deleted";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_views_deleted', 'ualpHookgform_post_form_views_deleted', 10, 1);

/**
 * Fires after an inactive form gets marked as active
 *
 * @param int $form_id The Form ID used to specify which form to activate
 */
if (!function_exists('ualpHookgform_post_form_activated')){
    function ualpHookgform_post_form_activated($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form activated";
            $hook = "gform_post_form_activated";
            $description = "$obj_type : '$formtitle' $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_activated', 'ualpHookgform_post_form_activated', 10, 1);

/**
 * Fires after an active form gets marked as inactive
 *
 * @param int $form_id The Form ID used to specify which form to activate
 */
if (!function_exists('ualpHookgform_post_form_deactivated')){
    function ualpHookgform_post_form_deactivated($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form deactivated";
            $hook = "gform_post_form_deactivated";
            $description = "$obj_type : '$formtitle' $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_deactivated', 'ualpHookgform_post_form_deactivated', 10, 1);

/**
 * Fires before a form is deleted
 *
 * @param int $form_id The ID of the form being deleted
 */
if (!function_exists('ualpHookgform_before_delete_form')){
    function ualpHookgform_before_delete_form($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form deleted successfully";
            $hook = "gform_before_delete_form";
            $description = "$obj_type : '$formtitle' $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_before_delete_form', 'ualpHookgform_before_delete_form', 10, 1);

/**
 * Fires after a form is trashed
 *
 * @param int $form_id The ID of the form that was trashed
 */
if (!function_exists('ualpHookgform_post_form_trashed')){
    function ualpHookgform_post_form_trashed($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form trashed successfully";
            $hook = "gform_post_form_trashed";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_trashed', 'ualpHookgform_post_form_trashed', 10, 1);

/**
 * Fires after a form is restored from trash
 *
 * @param int $form_id The ID of the form that was restored
 */
if (!function_exists('ualpHookgform_post_form_restored')){
    function ualpHookgform_post_form_restored($form_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form restored successfully";
            $hook = "gform_post_form_restored";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_restored', 'ualpHookgform_post_form_restored', 10, 1);

/**
 * Fires after a form is duplicated
 *
 * @param int $form_id The original form's ID
 * @param int $new_id  The ID of the new, duplicated form
 */
if (!function_exists('ualpHookgform_post_form_duplicated')){
    function ualpHookgform_post_form_duplicated($form_id,$new_id) {
        global $wpdb;
        $table_name = $wpdb->prefix.'rg_form';
        $form_title = $wpdb->get_row("SELECT title from $table_name WHERE id = $form_id", ARRAY_A);
        if(isset($form_title)){
            $formtitle = $form_title['title'];
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form duplicated successfully";
            $hook = "gform_post_form_duplicated";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_form_duplicated', 'ualpHookgform_post_form_duplicated', 10, 2);

/**
 * Fires after an email is sent
 *
 * @param bool   $is_success     True is successfully sent.  False if failed
 * @param string $to             Recipient address
 * @param string $subject        Subject line
 * @param string $message        Message body
 * @param string $headers        Email headers
 * @param string $attachments    Email attachments
 * @param string $message_format Format of the email.  Ex: text, html
 * @param string $from           Address of the sender
 * @param string $from_name      Displayed name of the sender
 * @param string $bcc            BCC recipients
 * @param string $reply_to       Reply-to address
 * @param array  $entry          Entry object associated with the sent email
 *
 */
if (!function_exists('ualpHookgform_after_email')){
    function ualpHookgform_after_email($is_success, $to, $subject, $message, $headers, $attachments, $message_format, $from, $from_name, $bcc, $reply_to, $entry ) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Email send successfully";
            $hook = "gform_after_email";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_after_email', 'ualpHookgform_after_email',10,12);

/**
 * Fires when an email from Gravity Forms has failed to send
 *
 * @param string $error   The Error message returned after the email fails to send
 * @param array  $details The details of the message that failed
 * @param array  $entry   The Entry object
 *
 */
if (!function_exists('ualpHookgform_send_email_failed')){
    function ualpHookgform_send_email_failed($error,$details,$entry) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Failed to send email";
            $hook = "gform_send_email_failed";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_send_email_failed', 'ualpHookgform_send_email_failed', 10, 3);

/**
 * Fires after a form is saved
 *
 * Used to run additional actions after the form is saved
 *
 * @param array $form_meta The form meta
 * @param bool  true       Returns true if this is a new form.
 */
if (!function_exists('ualpHookgform_after_save_form')){
    function ualpHookgform_after_save_form($form_meta,$flag) {
        if($flag == 'false'){
            $post_title = "Form created successfully ";
        }else {
            $form_title = $form_meta['title'];
            $post_title = "$form_title" . " Form updated successfully ";
        }
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $hook = "gform_after_save_form";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_after_save_form', 'ualpHookgform_after_save_form', 10, 2);

/**
 * Fires after forms have been imported.
 *
 * Used to perform additional actions after import
 *
 * @param array $forms An array imported form objects.
 *
 */
if (!function_exists('ualpHookgform_forms_post_import')){
    function ualpHookgform_forms_post_import($forms) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Post imported successfully";
            $hook = "gform_forms_post_import";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_forms_post_import', 'ualpHookgform_forms_post_import', 10, 1);

/**
 * Fires after submission, if the confirmation page includes a redirect
 *
 * Used to perform additional actions after submission
 *
 * @param array $lead The Entry object
 * @param array $form The Form object
 */
if (!function_exists('ualpHookgform_post_submission')){
    function ualpHookgform_post_submission( $lead, $form ) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form submitted successfully";
            $hook = "gform_post_submission";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_post_submission', 'ualpHookgform_post_submission', 10, 2);

/**
 * Fires when viewing entries of a certain form
 *
 * @since Unknown
 *
 * @param string $view    The current view/entry type
 * @param string $form_id The current form ID
 * @param string $lead_id The current entry ID
 */
if (!function_exists('ualpHookgform_entries_view')){
    function ualpHookgform_entries_view($view, $form_id, $lead_id) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Entries viewed";
            $hook = "gform_entries_view";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_entries_view', 'ualpHookgform_entries_view', 10, 3);
/**
 * Fires before a notification is deleted.
 *
 * @since Unknown
 *
 * @param array $notification_id    The notification being deleted.
 * @param array $form               The Form Object that the notification is being deleted from.
 */
if (!function_exists('ualpHookgform_pre_notification_deleted')){
    function ualpHookgform_pre_notification_deleted($notification_id, $form ) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Notification deleted successfully";
            $hook = "gform_pre_notification_deleted";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_pre_notification_deleted', 'ualpHookgform_pre_notification_deleted', 10, 2);

/**
 * Fires after the Entry is updated from the entry detail page.
 *
 * @param array   $form             The form object for the entry.
 * @param integer $lead_id          The entry ID.
 * @param array   $original_entry   The entry object before being updated.
 */
if (!function_exists('ualpHookgform_after_update_entry')){
    function ualpHookgform_after_update_entry($form, $lead_id, $original_entry) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Entry updated";
            $hook = "gform_after_update_entry";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_after_update_entry', 'ualpHookgform_after_update_entry',10,3);

/**
 * Fires right before a confirmation is deleted.
 *
 * @param int   $confirmation_id The ID of the confirmation being deleted.
 * @param array $form                                    The Form object.
 */
if (!function_exists('ualpHookgform_pre_confirmation_deleted')){
    function ualpHookgform_pre_confirmation_deleted($confirmation_id,$form) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Confirmation deleted";
            $hook = "gform_pre_confirmation_deleted";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('gform_pre_confirmation_deleted', 'ualpHookgform_pre_confirmation_deleted', 10, 2);

/**
 * Fires when form title updated
 */
if (!function_exists('ualpHookwp_ajax_gf_save_title')){
    function ualpHookwp_ajax_gf_save_title() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Form title updated successfully";
            $hook = "wp_ajax_gf_save_title";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_gf_save_title', 'ualpHookwp_ajax_gf_save_title');

/**
 * Fires when entry status change
 */
if (!function_exists('ualpHookwp_ajax_delete_gf_entry')){
    function ualpHookwp_ajax_delete_gf_entry() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $status  = rgpost( 'status' );
            if($status == 'unspam'){
                $post_title = "Entry unspammed successfully";
            }
            elseif($status == 'delete'){
                $post_title = "Entry deleted successfully";
            }
            elseif($status == 'active'){
                $post_title = "Entry restored successfully";
            }
            else {
                $post_title = "Entry ".$status . " successfully";
            }
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $hook = "wp_ajax_delete-gf_entry";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_delete-gf_entry', 'ualpHookwp_ajax_delete_gf_entry');

/**
 * Fires when entry property updated
 */
if (!function_exists('ualpHookwp_ajax_rg_update_lead_property')){
    function ualpHookwp_ajax_rg_update_lead_property() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Entry property updated";
            $hook = "wp_ajax_rg_update_lead_property";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_rg_update_lead_property', 'ualpHookwp_ajax_rg_update_lead_property');

/**
 * Fires when update notification saved
 */
if (!function_exists('ualpHookwp_ajax_rg_update_notification_active')){
    function ualpHookwp_ajax_rg_update_notification_active() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Notification activated";
            $hook = "wp_ajax_rg_update_notification_active";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_rg_update_notification_active', 'ualpHookwp_ajax_rg_update_notification_active');

/**
 * Fires when delete confirmation
 */
if (!function_exists('ualpHookwp_ajax_gf_delete_confirmation')){
    function ualpHookwp_ajax_gf_delete_confirmation() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Confirmation deleted";
            $hook = "wp_ajax_gf_delete_confirmation";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_gf_delete_confirmation', 'ualpHookwp_ajax_gf_delete_confirmation');

/**
 * Fires when confirmation activated
 */
if (!function_exists('ualpHookwp_ajax_rg_update_confirmation_active')){
    function ualpHookwp_ajax_rg_update_confirmation_active() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Confirmation activated";
            $hook = "wp_ajax_rg_update_confirmation_active";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('wp_ajax_rg_update_confirmation_active', 'ualpHookwp_ajax_rg_update_confirmation_active');

if(!function_exists('ualpHookgform_confirmation_actions')){
    function ualpHookgform_confirmation_actions($form_id){
        $duplicated_cid = sanitize_key( rgget( 'duplicatedcid' ) );
        $is_duplicate   = empty( $_POST ) && ! empty( $duplicated_cid );
        if($is_duplicate){
            $logHookList = get_option('enableLogHookList');
            if (in_array('gravityforms',$logHookList)) {
                $action = "";
                $obj_type = "Gravity Form";
                $post_id = "";
                $post_title = "Confirmation settings duplicated";
                $hook = "gform_admin_pre_render";
                $description = "$obj_type : $post_title";
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
        return $form_id;
    }
}
add_filter('gform_admin_pre_render', 'ualpHookgform_confirmation_actions', 10, 1);

if(!function_exists('ualpHookgform_confirmation_save')){
    function ualpHookgform_confirmation_save( $confirmation ){
        $logHookList = get_option('enableLogHookList');
        if (in_array('gravityforms',$logHookList)) {
            $action = "";
            $obj_type = "Gravity Form";
            $post_id = "";
            $post_title = "Confirmation settings saved";
            $hook = "gform_pre_confirmation_save";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
        return $confirmation;
    }
}
add_filter('gform_pre_confirmation_save', 'ualpHookgform_confirmation_save', 10, 1);
