<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Enqueue admin scripts
 */
if (!function_exists('ualpAdminScripts')) {

    function ualpAdminScripts() { ?>
        <script>
            var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
        </script>
        <?php
        $screen = get_current_screen();
        $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/user-activity-log-pro/user_activity_log_pro.php', $markup = true, $translate = true);
        $current_version = $plugin_data['Version'];
        $old_version = get_option('ual_version');
        if ($old_version != $current_version) {
            update_option('is_user_subscribed_cancled', '');
            update_option('ual_version', $current_version);
        }

        if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes') {
            wp_enqueue_script('thickbox');
            wp_enqueue_style('thickbox');
        }
        if($screen->id == 'dashboard' || $screen->id == 'user-activity-log_page_ualp_list' || $screen->id == 'toplevel_page_ual_pro' || $screen->id == 'user-activity-log_page_ualp_settings' || $screen->id == 'user-activity-log_page_ualp_license_key'){
            wp_enqueue_script('jquery');
            wp_enqueue_script('password-strength-meter', array('jquery'));
            wp_enqueue_script('jquery-ui-dialog', array('jquery'));
            wp_enqueue_script('ualp-script', UALP_PLUGIN_URL.'assets/js/ualp_script.js');
            wp_localize_script('ualp-script', 'ualpJSObject', array(
                'reset_confirm' => __('Are you sure reset password?', 'user_activity_log_pro'),
                'note_for_uncheck' => __('If you uncheck this option, it will not keep log for', 'user_activity_log_pro'),
                'select_option_delete_log' => __('Select any one option to delete log.', 'user_activity_log_pro'),
                'file_save_success' => __('File Saved successfully in uploads folder and log deleted successfully.', 'user_activity_log_pro'),
                'email_send_error' => __('Error in sending email.', 'user_activity_log_pro'),
                'file_save_and_error_email' => __('File Saved successfully in uploads folder, But error in sending email.', 'user_activity_log_pro'),
                'email_send_and_log_delete' => __('Email sent successfully and log deleted successfully.', 'user_activity_log_pro'),
                'file_save_and_send_email' => __('File Saved successfully in uploads folder, Email sent successfully and log deleted successfully', 'user_activity_log_pro'),
                'enter_valid_pwd' => __('Enter valid password.', 'user_activity_log_pro'),
                'select_option_for_delete_log' => __('Select any one option to delete log', 'user_activity_log_pro'),
                'file_save_and_log_delete_success' => __('File Saved successfully in uploads folder and log deleted successfully', 'user_activity_log_pro'),
                'email_send_and_delete_success' => __('Email sent successfully and log deleted successfully', 'user_activity_log_pro'),
                'file_save_email_send_and_success_delete_log' => __('File Saved successfully in uploads folder, Email sent successfully and log deleted successfully', 'user_activity_log_pro'),
                'submit' => __('Submit', 'user_activity_log_pro'),
                'cancel' => __('Cancel', 'user_activity_log_pro')
            ));
        }
    }

}
/**
 * Enqueue admin styles
 */
if (!function_exists('ualpAdminStyles')) {

    function ualpAdminStyles() {
        $screen = get_current_screen();
        if($screen->id == 'dashboard' || $screen->id == 'user-activity-log_page_ualp_list' || $screen->id == 'toplevel_page_ual_pro' || $screen->id == 'user-activity-log_page_ualp_settings' || $screen->id == 'user-activity-log_page_ualp_license_key'){
            wp_enqueue_style('jquery-ui', UALP_PLUGIN_URL.'assets/css/jquery-ui.css');
            wp_enqueue_style('ualp-style', UALP_PLUGIN_URL.'assets/css/ualp_style.css');
        }
    }

}

/**
 * start session if not
 */
if (!function_exists('ualpSessionStart')) {

    function ualpSessionStart() {
        if (session_id() == '') {
            session_start();
        }
    }

}

/**
 * Add Admin Dashboard Widget - News from Solwin Infotech
 */
if (!function_exists('latest_news_solwin_feed')) {

    function latest_news_solwin_feed() {
        // Register the new dashboard widget with the 'wp_dashboard_setup' action
        add_action('wp_dashboard_setup', 'solwin_latest_news_with_product_details');
        if (!function_exists('solwin_latest_news_with_product_details')) {

            function solwin_latest_news_with_product_details() {
                add_screen_option('layout_columns', array('max' => 3, 'default' => 2));
                add_meta_box('wp_ualp_dashboard_widget', __('News From Solwin Infotech', 'user_activity_log_pro'), 'solwin_dashboard_widget_news', 'dashboard', 'normal', 'high');
            }

        }
        if (!function_exists('solwin_dashboard_widget_news')) {

            function solwin_dashboard_widget_news() {
                echo '<div class="rss-widget">'
                . '<div class="solwin-news"><p><strong>' . __('Solwin Infotech News', 'user_activity_log_pro') . '</strong></p>';
                wp_widget_rss_output(array(
                    'url' => 'https://www.solwininfotech.com/feed/',
                    'title' => __('News From Solwin Infotech', 'user_activity_log_pro'),
                    'items' => 5,
                    'show_summary' => 0,
                    'show_author' => 0,
                    'show_date' => 1
                ));
                echo '</div>';
                $title = $link = $thumbnail = "";
                //get Latest product detail from xml file

                $file = 'https://www.solwininfotech.com/documents/assets/latest_product.xml';
                define('LATEST_PRODUCT_FILE', $file);
                echo '<div class="display-product">'
                . '<div class="product-detail"><p><strong>' . __('Latest Product', 'user_activity_log_pro') . '</strong></p>';
                $response = wp_remote_post(LATEST_PRODUCT_FILE);
                if (is_wp_error($response)) {
                    $error_message = $response->get_error_message();
                    echo "<p>" . __('Something went wrong', 'user_activity_log_pro') . " : $error_message" . "</p>";
                } else {
                    $body = wp_remote_retrieve_body($response);
                    $xml = simplexml_load_string($body);
                    $title = $xml->item->name;
                    $thumbnail = $xml->item->img;
                    $link = $xml->item->link;

                    $allProducttext = $xml->item->viewalltext;
                    $allProductlink = $xml->item->viewalllink;
                    $moretext = $xml->item->moretext;
                    $needsupporttext = $xml->item->needsupporttext;
                    $needsupportlink = $xml->item->needsupportlink;
                    $customservicetext = $xml->item->customservicetext;
                    $customservicelink = $xml->item->customservicelink;
                    $joinproductclubtext = $xml->item->joinproductclubtext;
                    $joinproductclublink = $xml->item->joinproductclublink;


                    echo '<div class="product-name"><a href="' . $link . '" target="_blank">'
                    . '<img alt="' . $title . '" src="' . $thumbnail . '"> </a>'
                    . '<a href="' . $link . '" target="_blank">' . $title . '</a>'
                    . '<p><a href="' . $allProductlink . '" target="_blank" class="button button-default">' . $allProducttext . ' &RightArrow;</a></p>'
                    . '<hr>'
                    . '<p><strong>' . $moretext . '</strong></p>'
                    . '<ul>'
                    . '<li><a href="' . $needsupportlink . '" target="_blank">' . $needsupporttext . '</a></li>'
                    . '<li><a href="' . $customservicelink . '" target="_blank">' . $customservicetext . '</a></li>'
                    . '<li><a href="' . $joinproductclublink . '" target="_blank">' . $joinproductclubtext . '</a></li>'
                    . '</ul>'
                    . '</div>';
                }
                echo '</div></div><div class="clear"></div>'
                . '</div>';
            }

        }
    }

}

/**
 * Add Footer link
 */
if (!function_exists('ualpFooterAdv')) {

    function ualpFooterAdv() {
        $screen = get_current_screen();
        if ($screen->id == "toplevel_page_ual_pro" || $screen->id == "user-activity-log_page_ualp_settings" || $screen->id == "user-activity-log_page_ualp_license_key") {
            add_filter('admin_footer_text', 'ualpFooterAdminAdv'); //change admin footer text
        }
    }

}

/**
 * Add rating html at footer of admin
 * @return html rating
 */
if (!function_exists('ualpFooterAdminAdv')) {

    function ualpFooterAdminAdv() {
        ob_start();
        ?>
        <p id="footer-left" class="alignleft">
            <?php _e('If you like ', 'user_activity_log_pro'); ?>
            <a href="http://www.solwininfotech.com/product/wordpress-plugins/user-activity-log-pro" target="_blank">
                <strong><?php _e('User Activity Log Pro Plugin', 'user_activity_log_pro'); ?></strong>
            </a>
            <?php _e('please leave us a', 'user_activity_log_pro'); ?>
            <a class="bdp-rating-link" data-rated="Thanks :)" target="_blank" href="http://www.solwininfotech.com/product/wordpress-plugins/user-activity-log-pro">&#x2605;&#x2605;&#x2605;&#x2605;&#x2605;</a>
            <?php _e('rating. A heartly thank you from Solwin Infotech in advance!', 'user_activity_log_pro'); ?>
        </p>
        <?php
        return ob_get_clean();
    }

}

/**
 * subscribe email form
 */
if (!function_exists('ualpSubscribeMail')) {

    function ualpSubscribeMail() {
        $customer_email = get_option('admin_email');
        $current_user = wp_get_current_user();
        $f_name = $current_user->user_firstname;
        $l_name = $current_user->user_lastname;
        if (isset($_POST['sbtEmail'])) {
            $_SESSION['success_msg'] = __('Thank you for your subscription.', 'user_activity_log_pro');
            //Email To Admin
            update_option('is_user_subscribed', 'yes');
            $customer_email = trim($_POST['txtEmail']);
            $customer_name = trim($_POST['txtName']);
            $to = 'plugins@solwininfotech.com';
            $from = get_option('admin_email');
            $headers = "MIME-Version: 1.0;\r\n";
            $headers .= "From: " . strip_tags($from) . "\r\n";
            $headers .= "Content-Type: text/html; charset: utf-8;\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
            $subject = __('New user subscribed from Plugin - User Activity Log Pro', 'user_activity_log_pro');
            $body = '';
            ob_start(); ?>
            <div style="background: #F5F5F5; border-width: 1px; border-style: solid; padding-bottom: 20px; margin: 0px auto; width: 750px; height: auto; border-radius: 3px 3px 3px 3px; border-color: #5C5C5C;">
                <div style="border: #FFF 1px solid; background-color: #ffffff !important; margin: 20px 20px 0;
                     height: auto; -moz-border-radius: 3px; padding-top: 15px;">
                    <div style="padding: 20px 20px 20px 20px; font-family: Arial, Helvetica, sans-serif;
                         height: auto; color: #333333; font-size: 13px;">
                        <div style="width: 100%;">
                            <strong>Dear Admin (User Activity Log Pro plugin developer)</strong>,
                            <br />
                            <br />
                            Thank you for developing useful plugin.
                            <br />
                            <br />
                            I <?php echo $customer_name; ?> want to notify you that I have installed plugin on my <a href="<?php echo home_url(); ?>">website</a>. Also I want to subscribe to your newsletter, and I do allow you to enroll me to your free newsletter subscription to get update with new products, news, offers and updates.
                            <br />
                            <br />
                            I hope this will motivate you to develop more good plugins and expecting good support form your side.
                            <br />
                            <br />
                            Following is details for newsletter subscription.
                            <br />
                            <br />
                            <div>
                                <table border='0' cellpadding='5' cellspacing='0' style="font-family: Arial, Helvetica, sans-serif; font-size: 13px;color: #333333;width: 100%;">
                                    <?php if ($customer_name != '') {
                                        ?>
                                        <tr style="border-bottom: 1px solid #eee;">
                                            <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                                Name<span style="float:right">:</span>
                                            </th>
                                            <td style="padding: 8px 5px;">
                                                <?php echo $customer_name; ?>
                                            </td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <tr style="border-bottom: 1px solid #eee;">
                                            <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                                Name<span style="float:right">:</span>
                                            </th>
                                            <td style="padding: 8px 5px;">
                                                <?php echo home_url(); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                            Email<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo $customer_email; ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left;width: 120px;">
                                            Website<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo home_url(); ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left; width: 120px;">
                                            Date<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php echo date('d-M-Y  h:i  A'); ?>
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid #eee;">
                                        <th style="padding: 8px 5px; text-align: left; width: 120px;">
                                            Plugin<span style="float:right">:</span>
                                        </th>
                                        <td style="padding: 8px 5px;">
                                            <?php _e('User Activity Log Pro', 'user_activity_log_pro'); ?>
                                        </td>
                                    </tr>
                                </table>
                                <br /><br />
                                Again Thanks you
                                <br />
                                <br />
                                Regards
                                <br />
                                <?php echo $customer_name; ?>
                                <br />
                                <?php echo home_url(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $body = ob_get_clean();
            wp_mail($to, $subject, $body, $headers);
        }
        if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes') { ?>
            <div id="subscribe_widget_ual" style="display:none;">
                <div class="subscribe_widget">
                    <h3><?php _e('Notify to plugin developer and subscribe.', 'user_activity_log_pro'); ?></h3>
                    <form class='sub_form' name="frmSubscribe" method="post" action="<?php echo admin_url() . 'admin.php?page=ualp_settings'; ?>">
                        <div class="sub_row">
                            <label><?php _e('Your Name: ', 'user_activity_log_pro');?></label>
                            <input placeholder="<?php esc_attr_e('Your Name', 'user_activity_log_pro'); ?>" name="txtName" type="text" value="<?php echo $f_name . ' ' . $l_name; ?>" />
                        </div>
                        <div class="sub_row">
                            <label><?php _e('Email Address: ', 'user_activity_log_pro');?></label>
                            <input placeholder="<?php esc_attr_e('Email Address', 'user_activity_log_pro'); ?>" required name="txtEmail" type="email" value="<?php echo $customer_email; ?>" />
                        </div>
                        <input class="button button-primary" type="submit" name="sbtEmail" value="<?php _e('Notify & Subscribe','user_activity_log_pro');?>" />
                    </form>
                </div>
            </div>
            <?php
        }
        if (isset($_GET['page'])) {
            if (get_option('is_user_subscribed') != 'yes' && get_option('is_user_subscribed_cancled') != 'yes' && ($_GET['page'] == 'ualp_settings' || $_GET['page'] == 'ual_pro' )) {
                ?>
                <a style="display:none" href="#TB_inline?width=400&height=210&inlineId=subscribe_widget_ual" class="thickbox" id="subscribe_thickbox"></a>
                <?php
            }
        }
    }

}

/**
 * user cancel subscribe
 */
if (!function_exists('ualpWpAjaxCloseTab')) {

    function ualpWpAjaxCloseTab() {
        update_option('is_user_subscribed_cancled', 'yes');
        exit();
    }

}

if (!function_exists('ualpAdminNoticeMessage')) {

    /**
     * Display success or error message
     *
     * @param string $class
     * @param string $message
     * @return string message with HTML
     */
    function ualpAdminNoticeMessage($class, $message) {
        ?>
        <div class="<?php echo $class; ?> is-dismissible ualpAdminNotice notice settings-error">
            <p><?php echo $message; ?></p>
        </div>
        <?php
    }

}

/*
 * Create table when activate plugin
 */
if (!function_exists('ualpCreateTable')) {

    function ualpCreateTable() {
        global $wpdb;
        $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/user-activity-log-pro/user_activity_log_pro.php', $markup = true, $translate = true);
        $current_version = $plugin_data['Version'];
        $old_table_name = $wpdb->prefix . "user_activity";
        //table is not created. you may create the table here.
        if ($wpdb->get_var("SHOW TABLES LIKE '".UALP_TABLENAME."'") != UALP_TABLENAME) {
            $old_table_exist = ualpCheckifTableRename();
            if(!$old_table_exist) {
                $create_table_query = "CREATE TABLE ".UALP_TABLENAME." ("
                    . "uactid bigint(20) unsigned NOT NULL auto_increment,"
                    . "post_id int(20) unsigned,"
                    . "favorite int(1) default '0',"
                    . "post_title varchar(250),"
                    . "user_id bigint(20) unsigned default '0',"
                    . "user_name varchar(250),"
                    . "user_role varchar(250),"
                    . "user_email varchar(250),"
                    . "ip_address varchar(50),"
                    . "modified_date datetime default '0000-00-00 00:00:00',"
                    . "object_type varchar(50) default 'post',"
                    . "action varchar(50),"
                    . "hook varchar(100),"
                    . "description varchar(255),"
                    . "PRIMARY KEY (uactid))";
                require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
                dbDelta($create_table_query);
            }
            else {
                $wpdb->query( "RENAME TABLE " . $old_table_name . " TO " . UALP_TABLENAME );
            }
        }
        update_option('ualp_version',$current_version);
    }

}

/*
 * Rename table "user_activity" to "ualp"
 */
if(!function_exists('ualpCheckifTableRename')) {
    function ualpCheckifTableRename() {
        global $wpdb;
        $old_table_name = $wpdb->prefix . "user_activity";
        if ($wpdb->get_var("SHOW TABLES LIKE '$old_table_name'") == $old_table_name) {
            if($wpdb->get_var("SHOW COLUMNS FROM ".$old_table_name." LIKE 'uactid'") == 'uactid') {
                return true;
            }
            return false;
        }
        return false;
    }
}

/*
 * Run the database updater
 */
if(!function_exists('ualpDatabaseUpgrade')) {
    function ualpDatabaseUpgrade() {
        global $wpdb;
        $old_table_exist = ualpCheckifTableRename();
        $old_table_name = $wpdb->prefix . "user_activity";
        $plugin_data = get_plugin_data(WP_PLUGIN_DIR . '/user-activity-log-pro/user_activity_log_pro.php', $markup = true, $translate = true);
        $current_version = $plugin_data['Version'];
        if($old_table_exist) {
            ?>
            <div class="updated">
                <p>
                    <strong>
                        <?php _e( 'User Activity Log Data Update', 'user_activity_log_pro' ); ?>
                    </strong> &#8211; <?php _e( 'We need to update your database to the latest version.', 'user_activity_log_pro' ); ?>
                </p>
                <p class="submit">
                    <a href="<?php echo esc_url( add_query_arg( 'do_update_ualp', 'do', admin_url( 'admin.php?page=ualp_settings' ) ) ); ?>" class="ualp-update-now button-primary">
                        <?php _e( 'Run the updater', 'user_activity_log_pro' ); ?>
                    </a>
                </p>
            </div>
            <?php
            if(isset($_GET['do_update_ualp']) && $_GET['do_update_ualp'] == 'do') {
                global $wpdb;
                $wpdb->query( "RENAME TABLE " . $old_table_name . " TO " . UALP_TABLENAME );
                update_option('ualp_version',$current_version);
            }
            ?>
            <script type="text/javascript">
                jQuery( '.ualp-update-now' ).click( 'click', function() {
                    return window.confirm( '<?php echo esc_js( __( 'It is strongly recommended that you backup your database before proceeding. Are you sure you wish to run the updater now?', 'user_activity_log_pro' ) ); ?>' );
                });
            </script>
            <?php
        }
    }
}
add_action( 'admin_notices', 'ualpDatabaseUpgrade' );

/*
 * Create log detail table when activate plugin
 */
if (!function_exists('ualpCreateDetailTable')) {

    function ualpCreateDetailTable() {
        global $wpdb;
        //table is not created. you may create the table here.
        if ($wpdb->get_var("SHOW TABLES LIKE '".UALP_DETAIL_TABLENAME."'") != UALP_DETAIL_TABLENAME) {
            $create_table_query = "CREATE TABLE ".UALP_DETAIL_TABLENAME." ("
                . "uactd_id int(20) NOT NULL auto_increment,"
                . "uactid int(20),"
                . "uactd_key varchar(200),"
                . "uactd_value text,"
                . "PRIMARY KEY (uactd_id))";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta($create_table_query);
        }
    }

}

/**
 *
 * @param $links for take a action for redirection setting
 * @return action for setting link
 */
if (!function_exists('ualpAddSettingsLink')) {

    function ualpAddSettingsLink($links) {
        $settings_link = '<a href="' . menu_page_url( 'ualp_settings', false ) . '">' . __( 'Settings' , 'user_activity_log_pro') . '</a>';
        array_push( $links, $settings_link );
        return $links;
    }

}

/**
 * function for set the value in header
 */
if (!function_exists('ualpFilterLogs')):

    function ualpFilterLogs() {

        $paged = 1;
        $userrole = $username = $objecttype = $ipaddress = $time = $txtSearch = $hook = $favorite = "";
        if (isset($_GET['userrole']) && $_GET['userrole'] != "") {
            $userrole = $_GET['userrole'];
        }
        if (isset($_GET['username']) && $_GET['username'] != "") {
            $username = $_GET['username'];
        }
        if (isset($_GET['type']) && $_GET['type'] != "") {
            $objecttype = $_GET['type'];
        }
        if (isset($_GET['ip']) && $_GET['ip'] != "") {
            $ipaddress = $_GET['ip'];
        }
        if (isset($_GET['time']) && $_GET['time'] != "" && $_GET['time'] != "0") {
            $time = $_GET['time'];
        }
        if (isset($_GET['hook']) && $_GET['hook'] != "" && $_GET['hook'] != "0") {
            $hook = $_GET['hook'];
        }
        if (isset($_GET['favorite']) && $_GET['favorite'] != "" && $_GET['favorite'] != "0") {
            $favorite = $_GET['favorite'];
        }
        if (isset($_GET['txtsearch']) && $_GET['txtsearch'] != "") {
            $txtSearch = $_GET['txtsearch'];
        }

        // For filtering data
        if (isset($_POST['btn_filter'])) {
            if (isset($_POST['role']) && $_POST['role'] != '0') {
                $userrole = $_POST['role'];
            }
            if (isset($_POST['user']) && $_POST['user'] != '0') {
                $username = $_POST['user'];
            }
            if (isset($_POST['post_type']) && $_POST['post_type'] != '0') {
                $objecttype = $_POST['post_type'];
            }
            if (isset($_POST['ip_address']) && $_POST['ip_address'] != '0') {
                $ipaddress = $_POST['ip_address'];
            }
            if (isset($_POST['time']) && $_POST['time'] != "") {
                $time = $_POST['time'];
            }
            if (isset($_POST['hook']) && $_POST['hook'] != '0') {
                $hook = $_POST['hook'];
            }
            if (isset($_POST['favorite']) && $_POST['favorite'] != '') {
                $favorite = $_POST['favorite'];
            }
            if (isset($_POST['txtSearchinput']) && $_POST['txtSearchinput'] != "") {
                $txtSearch = $_POST['txtSearchinput'];
            }
            wp_redirect(admin_url() . "admin.php?page=ual_pro&paged=$paged&userrole=$userrole&username=$username&type=$objecttype&hook=$hook&favorite=$favorite&ip=$ipaddress&time=$time&txtsearch=$txtSearch");
            exit();
        }
        if (isset($_POST['btnSearch']) && $_POST['btnSearch']) {
            $txtSearch = $_POST['txtSearchinput'];
            wp_redirect(admin_url() . "admin.php?page=ual_pro&paged=$paged&userrole=$userrole&username=$username&type=$objecttype&hook=$hook&favorite=$favorite&ip=$ipaddress&time=$time&txtsearch=$txtSearch");
            exit();
        }
    }

endif;

/**
 * Filter user Roles
 *
 */
if (!function_exists('ualpFilterUserRole')):

    function ualpFilterUserRole() {
        $paged = 1;
        $admin_url = get_admin_url();
        $display = $search = '';
        if (isset($_POST['user_role'])) {
            $display = $_POST['user_role'];
        }
        if (isset($_POST['btn_filter_user_role'])) {
            $display = $_POST['user_role'];
            wp_redirect(admin_url()."admin.php?page=ualp_settings&paged=$paged&display=$display&txtsearch=$search");
            exit();
        }
        if (isset($_POST['btnSearch_user_role'])) {
            $search = $_POST['txtSearchinput'];
            wp_redirect(admin_url()."admin.php?page=ualp_settings&paged=$paged&display=$display&txtsearch=$search");
            exit();
        }
    }

endif;

/*
 * Compare password to delete logs
 */
if (!function_exists('ualpCompareLogPassword')) {

    function ualpCompareLogPassword() {
        $ualpDeleteLogPwd = get_option('ualp_delete_log_pwd');
        if (isset($_POST['logPwd'])) {
            $ualpGetDeleteLogPwd = $_POST['logPwd'];
            $ualpGetDeleteLogPwd = md5($ualpGetDeleteLogPwd);
            if ($ualpDeleteLogPwd == $ualpGetDeleteLogPwd) {
                echo 'valid';
            } else {
                echo 'in_valid';
            }
        }
        exit();
    }

}

/*
 * Reset user activity database 
 */
if(!function_exists('ualpDeleteUalpTable')){
    function ualpDeleteUalpTable() {
        global $wpdb;
        if(isset($_POST['downloadLogs']) && $_POST['downloadLogs'] == 'true') {
            ualpGenerateCsvAttachment();
            echo "downloadLogs";
            $wpdb->query('TRUNCATE ' . UALP_TABLENAME);
            $wpdb->query('TRUNCATE ' . UALP_DETAIL_TABLENAME);
        }
        if(isset($_POST['sendLog']) && $_POST['sendLog'] == 'true') {
            $sent = ualpSendCsvInEmail();
            if($sent) {
                echo "sendLog";
                $wpdb->query('TRUNCATE ' . UALP_TABLENAME);
                $wpdb->query('TRUNCATE ' . UALP_DETAIL_TABLENAME);
            }
            else {
                echo "mailnotsent";
            }
        }
        exit();
    }
}
/*
 * Delete single logs
 */
if(!function_exists('ualpDeleteSingleLog')){
    function ualpDeleteSingleLog() {
        global $wpdb;
        $delete_id = $_POST['logid'];
        if(isset($_POST['downloadLogs']) && $_POST['downloadLogs'] == 'true') {
            ualpGenerateCsvAttachment();
            $uactdIdList = $wpdb->get_results("SELECT uactd_id FROM ".UALP_TABLENAME." left join ".UALP_DETAIL_TABLENAME." on ".UALP_DETAIL_TABLENAME.".uactid = ".UALP_TABLENAME.".uactid WHERE ".UALP_TABLENAME.".favorite = 0 AND ".UALP_DETAIL_TABLENAME.".uactid = $delete_id",ARRAY_A);
            foreach($uactdIdList as $uactdId){
                $wpdb->query($wpdb->prepare("DELETE FROM ".UALP_DETAIL_TABLENAME." WHERE uactid = %d AND uactd_id = %d",$delete_id,$uactdId['uactd_id']));
            }
            $wpdb->query( $wpdb->prepare( "DELETE FROM ".UALP_TABLENAME." WHERE uactid = %d AND favorite = %d",$delete_id, '0' ));
            echo "downloadLogs";
        }
        if(isset($_POST['sendLog']) && $_POST['sendLog'] == 'true') {
            $sent = ualpSendCsvInEmail();
            if($sent) {
                $wpdb->query( $wpdb->prepare( "DELETE FROM ".UALP_TABLENAME." WHERE uactid = %d AND favorite = %d",$delete_id, '0' ));
                echo "sendLog";
            }
            else {
                echo "mailnotsent";
            }
        }
        exit();
    }
}
/*
 * Delete bulk logs
 */
if(!function_exists('ualpDeleteBulkLog')){
    function ualpDeleteBulkLog() {
        global $wpdb;
        $delete_ids = $_POST['logid'];
        if(isset($_POST['downloadLogs']) && $_POST['downloadLogs'] == 'true') {
            ualpGenerateCsvAttachment();
            foreach($delete_ids as $delete_id){
                $uactdIdList = $wpdb->get_results("SELECT uactd_id FROM ".UALP_TABLENAME." left join ".UALP_DETAIL_TABLENAME." on ".UALP_DETAIL_TABLENAME.".uactid = ".UALP_TABLENAME.".uactid WHERE ".UALP_TABLENAME.".favorite = 0 AND ".UALP_DETAIL_TABLENAME.".uactid = $delete_id",ARRAY_A);
                foreach($uactdIdList as $uactdId){
                    $wpdb->query($wpdb->prepare("DELETE FROM ".UALP_DETAIL_TABLENAME." WHERE uactid = %d AND uactd_id = %d",$delete_id,$uactdId['uactd_id']));
                }
                $wpdb->query( $wpdb->prepare( "DELETE FROM ".UALP_TABLENAME." WHERE uactid = %d AND favorite = %d",$delete_id, '0' ));
            }
            echo "downloadLogs";
        }
        if(isset($_POST['sendLog']) && $_POST['sendLog'] == 'true') {
            $sent = ualpSendCsvInEmail();
            if($sent) {
                foreach($delete_ids as $delete_id){
                    $wpdb->query( $wpdb->prepare( "DELETE FROM ".UALP_TABLENAME." WHERE uactid = %d AND favorite = %d",$delete_id, '0' ));
                }
                echo "sendLog";
            }
            else {
                echo "mailnotsent";
            }
        }
        exit();
    }
}
/*
 * Display dialog box
 */
if(!function_exists('ualpDialogboxContent')){
    function ualpDialogboxContent() {
        global $screen;
        $screen = get_current_screen();
        if ($screen->id == "toplevel_page_ual_pro" || $screen->id == "user-activity-log_page_ualp_list" || $screen->id == "user-activity-log_page_ualp_settings") { ?>
            <div class="ualp_view_details_ajax" title="<?php esc_attr_e('Log Details', 'user_activity_log_pro'); ?>"></div>
            <div id="dialogPassword" title="<?php esc_attr_e('Are you sure to delete activity logs?','user_activity_log_pro'); ?>" style="display:none">
                <div id="dialog-form" title="<?php esc_attr_e('Delete activity logs','user_activity_log_pro'); ?>">
                    <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>">
                        <fieldset>
                            <br/>
                            <label for="deleteLogPassword"><?php _e('Enter Password','user_activity_log_pro'); ?></label>
                            <input type="password" name="deleteLogPassword" id="deleteLogPassword" value="" class="text ui-widget-content ui-corner-all">
                            <p class="pwdErrorMsg red"></p>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div id="dialogSinglePassword" title="<?php esc_attr_e('Are you sure to delete activity log?','user_activity_log_pro'); ?>" style="display:none">
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>">
                    <fieldset>
                        <br/>
                        <label for="deleteSingleLogPassword"><?php _e('Enter Password','user_activity_log_pro'); ?></label>
                        <input type="password" name="deleteLogPassword" id="deleteSingleLogPassword" value="" class="text ui-widget-content ui-corner-all">
                        <p class="pwdErrorMsg red"></p>
                    </fieldset>
                </form>
            </div>
            <div id="dialogDownload" title="<?php esc_attr_e('Download Activity Logs','user_activity_log_pro'); ?>" style="display:none">
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>">
                    <fieldset>
                        <label for="downloadLog">
                            <input checked="" type="checkbox" name="downloadLogs" value="downloadLog" id="downloadLog">
                            <?php _e('Download Log file','user_activity_log_pro'); ?>
                        </label>
                    </fieldset>
                    <fieldset>
                        <label for="sendLog">
                            <input type="checkbox" name="sendLog" value="sendLog" id="sendLog">
                            <?php _e('Send me log file in email','user_activity_log_pro'); ?>
                        </label>
                    </fieldset>
                    <p class="pwdErrorMsg red"></p>
                </form>
            </div>
            <div id="setDeletePassword" title="<?php esc_attr_e('Set Password for delete log','user_activity_log_pro'); ?>" style="display:none">
                <p>
                    <?php
                    _e("To delete the user activity log, you need to set the password. You can set the password using ",'user_activity_log_pro');
                    ?>
                    <a href='<?php echo admin_url("admin.php?page=ualp_settings"); ?>'>
                        <b><?php _e('password settings','user_activity_log_pro'); ?></b>
                    </a>
                </p>
            </div><?php
        }
    }
}

/**
 * Get term without knowing it's taxonomy.
 *
 * @uses type $wpdb
 * @uses get_term()
 * @param int|object $term
 * @param string $output
 * @param string $filter
 */
if(!function_exists('ualpGetTermById')){
    function ualpGetTermById($term, $output = ARRAY_A, $filter = 'raw') {
        global $wpdb;
        $null = null;
        if ( empty($term) ) {
            $error = new WP_Error('invalid_term', __('Empty Term'));
            return $error;
        }
        $_tax = $wpdb->get_row( $wpdb->prepare( "SELECT t.* FROM $wpdb->term_taxonomy AS t WHERE t.term_id = %s LIMIT 1", $term) );
        $taxonomy = $_tax->taxonomy;
        return get_term($term, $taxonomy, $output, $filter);
    }
}

/*
 * Update database and deactivate user activity plugin
 */
if(!function_exists('ualpDeactivateUal')){
    function ualpDeactivateUal(){
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        global $wpdb;
        $hookResult = $wpdb->query("SHOW COLUMNS FROM ". UALP_TABLENAME ." LIKE 'hook'");
        if($hookResult == 0){
            $alter_table_query = $wpdb->query("ALTER TABLE " . UALP_TABLENAME
                . " ADD COLUMN hook varchar(100)"
                . " AFTER action");
        }
        $favoriteResult = $wpdb->query("SHOW COLUMNS FROM ". UALP_TABLENAME ." LIKE 'favorite'");
        if($favoriteResult == 0){
            $alter_table_query = $wpdb->query("ALTER TABLE " . UALP_TABLENAME
                . " ADD COLUMN favorite int(1) DEFAULT '0'"
                . " AFTER post_id");
        }
        $descriptionResult = $wpdb->query("SHOW COLUMNS FROM ". UALP_TABLENAME ." LIKE 'description'");
        if($descriptionResult == 0){
            $alter_table_query = $wpdb->query("ALTER TABLE " . UALP_TABLENAME
                . " ADD COLUMN description varchar(255)"
                . " AFTER hook");
        }
        if (is_plugin_active('user-activity-log/user_activity_log.php')) {
            deactivate_plugins('user-activity-log/user_activity_log.php');
        }

    }
}
/*
 * Redirect to settings page on plugin activation
 */
if(!function_exists('ualpRedirect')) {
    function ualpRedirect($plugin, $network_activation) {
        if($plugin == 'user-activity-log-pro/user_activity_log_pro.php'){
            wp_redirect( admin_url( 'admin.php?page=ualp_settings' ) );
            exit();
        }
    }
}

/*
 * Get current admin password
 */
if(!function_exists('ulapGetAdminPwd')) {
    function ulapGetAdminPwd() {
        $current_user = wp_get_current_user();
        $admin_pass = $current_user->user_pass;
        ?>
        <script type="text/javascript">
            var adminPwd = '<?php echo $admin_pass; ?>';
            var currentpwd = '<?php echo get_option('ualp_delete_log_pwd'); ?>';
            var displayDateColumn = '<?php echo get_option('ualpDisplaycolumn-date'); ?>';
            var displayAuthorColumn = '<?php echo get_option('ualpDisplaycolumn-author'); ?>';
            var displayIpColumn = '<?php echo get_option('ualpDisplaycolumn-ip'); ?>';
            var displayHookColumn = '<?php echo get_option('ualpDisplaycolumn-hook'); ?>';
            var displayTypeColumn = '<?php echo get_option('ualpDisplaycolumn-type'); ?>';
            var displayDeleteColumn = '<?php echo get_option('ualpDisplaycolumn-delete'); ?>';
        </script>
        <?php
    }
}

/**
 * Send email when selected user login
 *
 * @param string $login current username when login
 */
if (!function_exists('ualpSendEmailNotification')) {

    function ualpSendEmailNotification($login) {
        if(get_option('ualpEnableEmailNotify') == 1){
            $current_user1 = get_user_by('login', $login);
            $current_user = !empty($current_user1->user_login) ? $current_user1->user_login : "-";
            $enable_unm = get_option('enable_user_list');
            for ($i = 0; $i < count($enable_unm); $i++) {
                if ($enable_unm[$i] == $current_user) {
                    $to_email = get_option('ualpToEmail');
                    $from_email = get_option('ualpFromEmail');
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $firstname = ucfirst($current_user1->user_firstname);
                    $lastname = ucfirst($current_user1->user_lastname);
                    $user_firstnm = !empty($firstname) ? ucfirst($firstname) : "-";
                    $user_lastnm = !empty($lastname) ? $lastname : "-";
                    $user_email = !empty($current_user1->user_email) ? $current_user1->user_email : "-";
                    $user_reg = !empty($current_user1->user_registered) ? $current_user1->user_registered : "-";
                    $current_user = ucfirst($current_user);
                    $user_details = "<table cellspacing='0' border='1px solid #ccc' style='margin-top:30px'>
                                    <tr>
                                        <td style='padding:5px 10px;'>" . __('Username', 'user_activity_log_pro') . "</td>
                                        <td style='padding:5px 10px;'>" . __('Firstname', 'user_activity_log_pro') . "</td>
                                        <td style='padding:5px 10px;'>" . __('Lastname', 'user_activity_log_pro') . "</td>
                                        <td style='padding:5px 10px;'>" . __('Email', 'user_activity_log_pro') . "</td>
                                        <td style='padding:5px 10px;'>" . __('Date Time', 'user_activity_log_pro') . "</td>
                                        <td style='padding:5px 10px;'>" . __('IP address', 'user_activity_log_pro') . "</td>
                                    </tr>
                                    <tr>
                                        <td style='padding:5px 10px;'>$current_user</td>
                                        <td style='padding:5px 10px;'>$user_firstnm</td>
                                        <td style='padding:5px 10px;'>$user_lastnm</td>
                                        <td style='padding:5px 10px;'>$user_email</td>
                                        <td style='padding:5px 10px;'>$user_reg</td>
                                        <td style='padding:5px 10px;'>$ip</td>
                                    </tr>
                                </table>";
                    $mail_msg = __('Hi ', 'user_activity_log_pro');
                    $mail_msg .= sprintf(__('%s,', 'user_activity_log_pro'),$current_user1->display_name);
                    $mail_msg .= "\n\n ".__("Following user is logged in your site", 'user_activity_log_pro') . " \n$user_details";
                    $mail_msg .= "\n\n".__("Thanks, \n", 'user_activity_log_pro');
                    $mail_msg .= home_url();
                    if ($to_email != "" && $mail_msg != "" && $from_email != "") {
                        $headers = "From: " . strip_tags($from_email) . "\r\n";
                        $headers .= "Reply-To: " . strip_tags($from_email) . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                        wp_mail($to_email, __('User Login Notification', 'user_activity_log_pro'), $mail_msg, $headers);
                    }
                }
            }
        }
    }

}

/**
 * Enable user notification of email at login
 *
 * @param int $user_id user ID
 */
if (!function_exists('ualpEnableUserForNotification')) {

    function ualpEnableUserForNotification($user_id) {
        $user_info = get_userdata($user_id);
        $user_role = $user_info->roles[0];
        $user_role_enable = get_option('enable_role_list');
        $user_enabled = get_option('enable_user_list');
        for ($i = 0; $i < count($user_role_enable); $i++) {
            if ($user_role_enable[$i] == $user_role) {
                array_push($user_enabled, $user_info->user_login);
                update_option('enable_user_list', $user_enabled);
            }
        }
    }

}

/*
 * Set default role manager
 */
if(!function_exists('ualpSetRoleManager')){
    function ualpSetRoleManager(){
        $ualpRoleAccessList = get_option('ualpRoleAccessList');
        $ualpRoleAccessList1 = array(0 => "administrator");
        if(empty($ualpRoleAccessList)){
                $ualpRoleAccessList = $ualpRoleAccessList1;
        }
        if(!in_array('administrator',$ualpRoleAccessList)){
            $ualpRoleAccessList = array_merge($ualpRoleAccessList,$ualpRoleAccessList1);
        }
        update_option('ualpRoleAccessList',$ualpRoleAccessList);
    }
}

/*
 * Set default log hook
 */
if(!function_exists('ualpSetDefaultLogHook')){
    function ualpSetDefaultLogHook(){
        global $wpdb;
        $post_types = get_post_types(array('public' => true), 'objects', 'and');
        $taxonomies = get_taxonomies(array('public' => true), 'objects', 'and');
        if( !get_option( 'enableLogHookList' ) ) {
            $logHook = array();
            foreach ($post_types as $post_type) {
                $postTypeHook[] = $post_type->name;
            }
            $logHook = array_merge($logHook,$postTypeHook);
            foreach ($taxonomies as $taxonomy) {
                $taxonomyHook[] = $taxonomy->name;
            }
            $logHook = array_merge($logHook,$taxonomyHook);
            $pluginHook = array('activated_plugin','deactivated_plugin','delete_plugin','plugin_modify','plugin_updated','plugin_installed');
            $logHook = array_merge($pluginHook,$logHook);
            $contactHook = array('wpcf7_after_create','wpcf7_after_update','wpcf7_admin_notices');
            $logHook = array_merge($contactHook,$logHook);
            $ualpHook = array('ualp_export_activity_log','ualp_general_settings_updated','ualp_hook_log_settings_updated','ualp_manage_role','ualp_set_password','ualp_change_password','ualp_email_settings_updated','ualp_disable_email_notification');
            $logHook = array_merge($ualpHook,$logHook);
            $userHook = array('wp_login','wp_login_failed','wp_logout','delete_user','user_register','insert_user_meta');
            $logHook = array_merge($userHook,$logHook);
            $themeHook = array('theme_file_modify','switch_theme','customize_save','theme_updated','theme_installed','delete_site_transient_update_themes');
            $logHook = array_merge($themeHook,$logHook);
            $commentHook = array('wp_insert_comment','edit_comment','trash_comment','spam_comment','restore_comment','delete_comment','comment_unapprove','comment_approve');
            $logHook = array_merge($commentHook,$logHook);
            $menuviewHook = array('wp_create_nav_menu','wp_update_nav_menu','delete_nav_menu');
            $logHook = array_merge($menuviewHook,$logHook);
            $otherHook = array('_core_updated_successfully','import_end','export_wp','widget_update_callback','updated_option','woocommerce_updated_option');
            $logHook = array_merge($otherHook,$logHook);
            update_option('enableLogHookList', $logHook);
        }
    }
}

add_action('wp_ajax_ualpFavoriteLog', 'ualpAjaxFavoriteLog');
if (!function_exists('ualpAjaxFavoriteLog')) {

    /**
     * Function to handle AJAX request.
     *
     * @since    1.0.0
     */
    function ualpAjaxFavoriteLog() {
        global $wpdb;
        $featured = $_POST['featured'];
        $id = (int) $_POST['post'];
        if (!empty($id) && $featured !== NULL) {
            if ($featured == 'no') {
                $result = $wpdb->update(
                    UALP_TABLENAME,
                    array( 'favorite' => '0' ),
                    array( 'uactid' => $id ),
                    array( '%s' ),
                    array( '%d' )
                );
            } else {
                $result = $wpdb->update(
                    UALP_TABLENAME,
                    array( 'favorite' => '1' ),
                    array( 'uactid' => $id ),
                    array( '%s' ),
                    array( '%d' )
                );
            }
        }
        exit;
    }

}

/*
 * View detailed log
 */
if(!function_exists('ualpViewUpdateDetail')){
    function ualpViewUpdateDetail(){
        global $wpdb;
        $uactid = $_POST['logID'];
        $uactLogActivity = $wpdb->get_results( 'SELECT * FROM '.UALP_TABLENAME.' WHERE uactid = '.$uactid );
        $uactDetails = $wpdb->get_results( 'SELECT uactd_key,uactd_value FROM '.UALP_DETAIL_TABLENAME.' WHERE uactid = '.$uactid );

        foreach($uactLogActivity as $k => $v){
            $objectType = $v->object_type;
            $action = $v->action;
            $userId = $v->user_id;
            $date = $v->modified_date;
            $username = $v->user_name;
            $userrole = $v->user_role;
            $useremail = $v->user_email;
            $ipaddress = $v->ip_address;
            $detail = $v->description;
        }
        $get_dafault_user_key_array = _get_additional_user_keys($userId);
        $user_full_key_array = array_merge($get_dafault_user_key_array, array('user_email', 'user_url', 'display_name', 'role', 'user_nicename'));

        $get_dafault_post_key_array = array("post_title","post_name","post_content","post_status","menu_order","post_date","post_date_gmt","post_excerpt","comment_status","ping_status","post_parent","post_author");
        $post_full_key_array = apply_filters('ualp_post_primary_array',$get_dafault_post_key_array);

        $get_term_key_array = array("term_name","term_slug","term_description","term_parent");

        $customizerActivities = array();
        $customizerActivities = $wpdb->get_results( 'SELECT uactd_key,uactd_value FROM '.UALP_DETAIL_TABLENAME.' WHERE uactid = '.$uactid , ARRAY_A);

        $post_types = get_post_types(array('public' => true), 'objects', 'and'); ?>
        <div class="ualp_view_details_wrap" title="<?php esc_attr_e('Log Details', 'user_activity_log_pro'); ?>">
            <div class="ualp_view_user_details">
                <table class="striped widefat">
                    <tr>
                        <td colspan="2" class="ulap_view_title">
                            <?php _e('Activity Detail','user_activity_log_pro'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Action ID','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$uactid); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Message','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$detail); ?>
                        </td>
                    </tr>
                    <?php
                    if($objectType == 'Theme' && $action == 'switched'){ ?>
                        <tr>
                            <td><?php printf(__('%s %s','user_activity_log_pro'), $objectType, $action); ?></td>
                            <td>
                                <span>
                                    <del><?php printf(__('%s','user_activity_log_pro'), ualpGetActivityLogDetail('ualp_old_theme_name',$uactid)); echo "(". sprintf(__('%s','user_activity_log_pro'), ualpGetActivityLogDetail('ualp_old_theme_version',$uactid)).")";?></del>
                                    <?php printf(__('%s','user_activity_log_pro'), ualpGetActivityLogDetail('ualp_new_theme_name',$uactid)); echo "(". sprintf(__('%s','user_activity_log_pro'), ualpGetActivityLogDetail('ualp_new_theme_version',$uactid)).")";?>
                                </span>
                            </td>
                        </tr><?php
                    }

                    if($objectType == 'Theme' && $action == 'updated'){ ?>
                        <tr>
                            <td><?php _e('Theme Name','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),ualpGetActivityLogDetail('ualp_theme_name',$uactid));?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Theme Version','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),ualpGetActivityLogDetail('ualp_theme_version',$uactid));?>
                                </span>
                            </td>
                        </tr><?php
                    }

                    if($objectType == 'Theme' && $action == 'customize updated'){
                        foreach($customizerActivities as $customizerActivity){
                            $customizerOldValue = ualpGetActivityLogDetail($customizerActivity['uactd_value']."_setting_old_value",$uactid);
                            $customizerNewValue = ualpGetActivityLogDetail($customizerActivity['uactd_value']."_setting_new_value",$uactid);
                            if($customizerActivity['uactd_value'] == 'main_text_color'){
                                $costomizeLabel = __('Main text color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'secondary_text_color'){
                                $costomizeLabel = __('Secondary text color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'background_image'){
                                $costomizeLabel = __('Background image','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'background_repeat'){
                                $costomizeLabel = __('Background repeat','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'background_position_x'){
                                $costomizeLabel = __('Background position','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'background_attachment'){
                                $costomizeLabel = __('Background attachment','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'nav_menu_locations[primary]'){
                                $costomizeLabel = __('Primary menu locations','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'nav_menu_locations[social]'){
                                $costomizeLabel = __('Social menu locations','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'page_background_color'){
                                $costomizeLabel = __('Page background color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'link_color'){
                                $costomizeLabel = __('Link color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'custom_logo'){
                                $costomizeLabel = __('Custom logo','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'header_textcolor'){
                                $costomizeLabel = __('Header text color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_value'] == 'background_color'){
                                $costomizeLabel = __('Background color','user_activity_log_pro');
                            }
                            if($customizerActivity['uactd_key'] == 'main_text_colorcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'secondary_text_colorcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'background_repeatcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'background_position_xcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'background_attachmentcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'page_background_colorcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'link_colorcontrol_id' ||
                                $customizerActivity['uactd_key'] == 'header_textcolorcontrol_id' ||

                                    $customizerActivity['uactd_key'] == 'nav_menu_locations[primary]control_id' ||
                                    $customizerActivity['uactd_key'] == 'nav_menu_locations[social]control_id' ||

                                $customizerActivity['uactd_key'] == 'background_colorcontrol_id'
                            ){ ?>
                                <tr>
                                    <td><?php echo $costomizeLabel; ?></td>
                                    <td>
                                        <del>
                                            <?php printf(__('%s','user_activity_log_pro'),$customizerOldValue);?>
                                        </del>
                                        <span>
                                            <?php printf(__('%s','user_activity_log_pro'),$customizerNewValue);?>
                                        </span>
                                    </td>
                                </tr><?php
                            }
                            if($customizerActivity['uactd_key'] == 'background_imagecontrol_id'){ ?>
                                <tr>
                                    <td><?php echo $costomizeLabel; ?></td>
                                    <td><?php
                                        if($customizerOldValue != ""){?>
                                            <img width="150" height="150" src="<?php echo $customizerOldValue; ?>" alt="<?php esc_attr_e('Old Background image', 'user_activity_log_pro'); ?>"><?php
                                        }?>
                                        <span><?php
                                            if($customizerNewValue != ""){?>
                                                <img width="150" height="150" src="<?php echo $customizerNewValue; ?>" alt="<?php esc_attr_e('New Background image', 'user_activity_log_pro'); ?>"><?php
                                            }?>
                                        </span>
                                    </td>
                                </tr><?php
                            }
                            if($customizerActivity['uactd_key'] == 'custom_logocontrol_id'){ ?>
                                <tr>
                                    <td><?php echo $costomizeLabel; ?></td>
                                    <td><?php
                                        if($customizerOldValue != ""){?>
                                            <img width="150" height="150" src="<?php echo wp_get_attachment_url( $customizerOldValue ); ?>" alt="<?php esc_attr_e('Old custom logo', 'user_activity_log_pro'); ?>"><?php
                                        }?>
                                        <span><?php
                                            if($customizerNewValue != ""){?>
                                                <img width="150" height="150" src="<?php echo wp_get_attachment_url( $customizerNewValue ); ?>" alt="<?php esc_attr_e('New custom logo', 'user_activity_log_pro'); ?>"><?php
                                            }?>
                                        </span>
                                    </td>
                                </tr><?php
                            }
                        }
                    }

                    if($objectType == 'Comment' && ($action == 'edited' || $action == 'inserted')){
                        $commentID = ualpGetActivityLogDetail('ualp_comment_id',$uactid);
                        $comment = get_comment( $commentID );
                        ?>
                        <tr>
                            <td><?php _e('Comment ID','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php echo $commentID;?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment Author','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_author);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment author email','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_author_email);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment author url','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_author_url);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment content','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_content);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment content','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_date);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment approved','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),$comment->comment_approved);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('Comment post title','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php printf(__('%s','user_activity_log_pro'),get_the_title($comment->comment_post_ID));?>
                                </span>
                            </td>
                        </tr><?php
                    }

                    if($objectType == 'Widget' && $action == 'updated'){?>
                        <tr>
                            <td><?php _e('Old instance','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php echo ualpGetActivityLogDetail('ualp_old_instance',$uactid);?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td><?php _e('New instance','user_activity_log_pro'); ?></td>
                            <td>
                                <span>
                                    <?php echo ualpGetActivityLogDetail('ualp_new_instance',$uactid);?>
                                </span>
                            </td>
                        </tr><?php
                    }

                    if($objectType == 'User' && $action == 'profile updated'){
                        foreach($user_full_key_array as $user_full_key_single) {
                            $old_user_meta_key = "ualp_user_old_$user_full_key_single";
                            $new_user_meta_key = "ualp_user_new_$user_full_key_single";
                            foreach ($uactDetails as $k => $v){
                                if($old_user_meta_key == $v->uactd_key){
                                    $oldValue = ualpGetActivityLogDetail($old_user_meta_key,$uactid);
                                    $newValue = ualpGetActivityLogDetail($new_user_meta_key,$uactid);
                                    ?><tr>
                                        <td>
                                            <label><?php
                                                if($user_full_key_single == 'user_email'){
                                                    _e("Email", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'user_url'){
                                                    _e("Website", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'display_name'){
                                                    _e("Display name publicly", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'role'){
                                                    _e("Role", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'first_name'){
                                                    _e("First Name", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'last_name'){
                                                    _e("Last Name", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'nickname'){
                                                    _e("Nickname", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'description'){
                                                    _e("Biographical Info", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'rich_editing'){
                                                    _e("Visual Editor", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'comment_shortcuts'){
                                                    _e("Keyboard Shortcuts", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'admin_color'){
                                                    _e("Admin Color Scheme", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'use_ssl'){
                                                    _e("Use ssl", 'user_activity_log_pro');
                                                }
                                                if($user_full_key_single == 'show_admin_bar_front'){
                                                    _e("Show Toolbar", 'user_activity_log_pro');
                                                }
                                                ?> :
                                            </label>
                                        </td>
                                        <td class="user_updated">
                                            <?php
                                                $diff_table = wp_text_diff( $oldValue,$newValue);
                                                echo $diff_table;
                                            ?>
                                    </tr><?php
                                }
                            }
                        }
                    }

                    if($objectType == 'Term' && $action == 'edited') {
                        foreach($get_term_key_array as $term_key_single) {
                            $old_term_meta_key = "ulap_old_$term_key_single";
                            $new_term_meta_key = "ulap_new_$term_key_single";
                            foreach ($uactDetails as $k => $v) {
                                if ($old_term_meta_key == $v->uactd_key) {
                                    $oldValue = ualpGetActivityLogDetail($old_term_meta_key, $uactid);
                                    $newValue = ualpGetActivityLogDetail($new_term_meta_key, $uactid);
                                    if($oldValue != "" && $newValue != ""){
                                        if($term_key_single == "term_name"){
                                            $term_key = __("Term name", "user_activity_log_pro");
                                        }
                                        if($term_key_single == "term_slug"){
                                            $term_key = __("Term slug", "user_activity_log_pro");
                                        }
                                        if($term_key_single == "term_description"){
                                            $term_key = __("Term description", "user_activity_log_pro");
                                        }
                                        if($term_key_single == "term_parent"){
                                            $term_key = __("Term parent", "user_activity_log_pro");
                                        }
                                        ?>
                                        <tr>
                                            <td><?php printf(__('%s','user_activity_log_pro'),$term_key); ?></td>
                                            <td>
                                                <?php if($term_key_single == 'term_parent'){

                                                    if($oldValue == 0){
                                                        $oldValue = __("None", "user_activity_log_pro");
                                                    }else{
                                                        $oldValue = ualpGetTermById($oldValue);
                                                        $oldValue = $oldValue['name'];
                                                    }
                                                    if($newValue == 0){
                                                        $newValue = __("None", "user_activity_log_pro");
                                                    }else{
                                                        $newValue = ualpGetTermById($newValue);
                                                        $newValue = $newValue['name'];
                                                    }
                                                } ?>
                                                <del><?php printf(__('%s','user_activity_log_pro'),$oldValue); ?></del>
                                                <span>
                                                    <?php printf(__('%s','user_activity_log_pro'),$newValue); ?>
                                                </span>
                                            </td>
                                        </tr><?php
                                    }
                                }
                            }
                        }
                    }
                    foreach ($post_types as $post_type) {
                        if($objectType == $post_type->name) {
                            foreach($post_full_key_array as $post_full_key_single) {
                                    $old_user_meta_key = "ualp_post_old_$post_full_key_single";
                                    $new_user_meta_key = "ualp_post_new_$post_full_key_single";
                                    foreach ($uactDetails as $k=>$v){
                                        if($old_user_meta_key == $v->uactd_key){
                                            $oldValue = ualpGetActivityLogDetail($old_user_meta_key,$uactid);
                                            $newValue = ualpGetActivityLogDetail($new_user_meta_key,$uactid);
                                            ?>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <?php
                                                        if($post_full_key_single == 'post_title') {
                                                            _e("Post Title", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_content') {
                                                            _e("Post Content", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_status') {
                                                            _e("Post Status", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_name') {
                                                            _e("Post Slug", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_excerpt') {
                                                            _e("Post Excerpt", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_parent') {
                                                            _e("Post Parent", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_author') {
                                                            _e("Post Author", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'menu_order') {
                                                            _e("Menu Order", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_date') {
                                                            _e("Post Date", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'post_date_gmt') {
                                                            _e("Post Date GMT", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'comment_status') {
                                                            _e("Comment Status", "user_activity_log_pro");
                                                        }
                                                        if($post_full_key_single == 'ping_status') {
                                                            _e("Ping Status", "user_activity_log_pro");
                                                        }
                                                        ?>
                                                    </label>
                                                </td>
                                                <td>
                                                    <?php if($post_full_key_single == 'post_content') {
                                                        $diff_table = wp_text_diff( $oldValue,$newValue);
                                                        echo $diff_table;
                                                    } else { ?>
                                                        <span>
                                                            <?php
                                                            if($post_full_key_single == 'post_parent'){
                                                                $oldValue = get_the_title($oldValue);
                                                                $newValue = get_the_title($newValue);
                                                            }
                                                            if($post_full_key_single == 'post_author'){
                                                                $oldUser = get_user_by('id',$oldValue);
                                                                $newUser = get_user_by('id',$newValue);
                                                                if(isset($newUser) && !empty($newUser)){
                                                                    $oldValue = $oldUser->display_name;
                                                                    $newValue = $newUser->display_name;
                                                                }else {
                                                                    $oldValue = "";
                                                                    $newValue = "";
                                                                }
                                                            }
                                                            if($oldValue != '') { ?>
                                                            <del><?php echo $oldValue; ?></del>
                                                            <?php } ?>
                                                            <?php echo $newValue; ?>
                                                        </span>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                                <?php
                                        }
                                    }
                                }
                        }
                    }
                    ?>
                    <tr>
                        <td class="ulap_view_title" colspan="2">
                            <?php _e('User Detail','user_activity_log_pro'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Date','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$date); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Username','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$username); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Role','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$userrole); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Email Address','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$useremail); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php _e('Ip Address','user_activity_log_pro'); ?></td>
                        <td>
                            <?php printf(__('%s','user_activity_log_pro'),$ipaddress); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div><?php
        exit;
    }
}

/*
 * Get current admin page url
 */
if (!function_exists('ualpGetCurrentAdminPageUrl')) {

    function ualpGetCurrentAdminPageUrl($SERVER = null) {
        if ($SERVER === null)
            $SERVER = $_SERVER;
        if (!isset($SERVER['SERVER_PORT']))
            return '';
        $ssl = false;
        if (isset($SERVER['HTTPS'])) {
            $value = strtolower($SERVER['HTTPS']);
            $ssl = (( $value != '' ) && ( $value != 'off' ));
        }

        $port = $SERVER['SERVER_PORT'];
        if ($ssl && $port == 443)
            $port = '';
        if (!$ssl && $port == 80)
            $port = '';
        if ($port != '')
            $port = ':' . $port;

        $url = $SERVER['REQUEST_URI'];
        return sprintf('%s://%s%s%s', $ssl ? 'https' : 'http', $SERVER['HTTP_HOST'], $port, $url);
    }

}

/*
 * Get detail activity log 
 */
if(!function_exists('ualpGetActivityLogDetail')){
    function ualpGetActivityLogDetail($optionName,$activityId){
        global $wpdb;
        $row = $wpdb->get_row("SELECT uactd_value FROM ".UALP_DETAIL_TABLENAME." WHERE uactd_key = '$optionName' AND uactid = '$activityId'", ARRAY_A );
        return $row['uactd_value'];
    }
}

/*
 * Delete activity log as per selected days
 */
if(!function_exists('ualpDeleteLog')){
    function ualpDeleteLog(){
        global $wpdb;
        $getLogSpan = "";
        $getLogSpan = get_option('ualpKeepLogsDay');
        $time = strtotime('-' . $getLogSpan . ' days');
        $date = date('Y-m-d', $time);
        if (isset($getLogSpan) && !empty($getLogSpan)) {
            $wpdb->query("DELETE FROM " . UALP_TABLENAME . " WHERE modified_date < '$date'");
        }
    }
}