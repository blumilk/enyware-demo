<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/**
 * Fires when create product variation
 * 
 * @param int $variation_id
 */
if (!function_exists('ualpHookWCCreateProductVariation')) {

    function ualpHookWCCreateProductVariation($variation_id) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('woocommerce_variation',$logHookList)) {
            $variation = wc_get_product($variation_id);
            $variationName = $variation->get_formatted_name();
            $vt = explode(" ",$variationName);
            $variation_title = $vt[5].' '.$vt[6];
            $action = "product variation created";
            $obj_type = "Woocommerce";
            $post_id = $variation->id;
            $post_title = get_the_title($post_id);
            $hook = "woocommerce_create_product_variation";
            $description = "$action in <a href='". get_edit_post_link($post_id) ."'>'$post_title'</a>";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('woocommerce_create_product_variation', 'ualpHookWCCreateProductVariation', 15, 1); 

/**
 * Fires when delete product variation
 */
if (!function_exists('ualpHookDeleteProductVariation')) {

    function ualpHookDeleteProductVariation() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('woocommerce_variation',$logHookList)) {
            $variation_ids = $_POST['variation_ids'];
            foreach ($variation_ids as $variation_id){
                $variation = wc_get_product($variation_id);
                $variationName = $variation->get_formatted_name();
                $product = new WC_Product($variation->id);
                $variation_data = $variation->variation_data;
                $rprice = get_post_meta($variation_id, '_regular_price', true);
                $sprice = get_post_meta($variation_id, '_sale_price', true);
                $variation_detail_ary = array();
                $variation_detail_ary['ualp_product_name'] = get_the_title($variation->id);
                $variation_detail_ary['ualp_variation_data'] = serialize($variation_data);
                $action = "product variation deleted";
                $obj_type = "Woocommerce";
                $post_id = $variation->id;
                $post_title = get_the_title($post_id);
                $hook = "wp_ajax_woocommerce_remove_variations";
                $description = "$action from <a target='blank' href='".get_edit_post_link($post_id)."'>'$post_title'</a>";
                $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
                ualpAddActivityLogDetail($uactid,$variation_detail_ary);
            }
        }
    }

}
add_action('wp_ajax_woocommerce_remove_variations','ualpHookDeleteProductVariation');

/**
 * Fires when save product variation
 * 
 * @param int $variation_id, int $i
 */
if (!function_exists('ualpHookWCSaveProductVariation')) {

    function ualpHookWCSaveProductVariation($variation_id, $i) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('woocommerce_variation',$logHookList)) {
            $variation = $variation_data = $rprice = $sprice = $SKU = $virtual = $downloadable = $weight = $length = $manage_stock = $backorders = $tax_class = $download_limit = $download_expiry = $downloadable_files = $variation_description = $default_attributes = '';
            
            $variation = wc_get_product($variation_id);
            $variation_data = $variation->variation_data;
            
            $rprice = get_post_meta($variation_id, '_regular_price', true);
            $sprice = get_post_meta($variation_id, '_sale_price', true);
            
            $SKU = get_post_meta( $variation_id, '_sku' );
            if(isset($SKU) && !empty($SKU)){
                $SKU = $SKU[0];
            }
            $virtual = get_post_meta( $variation_id, '_virtual' );
            if(isset($virtual) && !empty($virtual)){
                $virtual = $virtual[0];
            }
            $downloadable = get_post_meta( $variation_id, '_downloadable' );
            if(isset($downloadable) && !empty($downloadable)){
                $downloadable = $downloadable[0];
            }
            $weight = get_post_meta( $variation_id, '_weight' );
            if(isset($weight) && !empty($weight)){
                $weight = $weight[0];
            }
            $length = get_post_meta( $variation_id, '_length' );
            if(isset($length) && !empty($length)){
                $length = $length[0];
            }
            $manage_stock = get_post_meta( $variation_id, '_manage_stock' );
            if(isset($manage_stock) && !empty($manage_stock)){
                $manage_stock = $manage_stock[0];
            }
            $backorders = get_post_meta( $variation_id, '_backorders' );
            if(isset($backorders) && !empty($backorders)){
                $backorders = $backorders[0];
            }
            $tax_class = get_post_meta( $variation_id, '_tax_class' );
            if(isset($tax_class) && !empty($tax_class)){
                $tax_class = $tax_class[0];
            }
            $download_limit = get_post_meta( $variation_id, '_download_limit' );
            if(isset($download_limit) && !empty($download_limit)){
                $download_limit = $download_limit[0];
            }
            $download_expiry = get_post_meta( $variation_id, '_download_expiry' );
            if(isset($download_expiry) && !empty($download_expiry)){
                $download_expiry = $download_expiry[0];
            }
            $variation_description = get_post_meta( $variation_id, '_variation_description' );
            if(isset($variation_description) && !empty($variation_description)){
                $variation_description = $variation_description[0];
            }
            $variation_detail_ary = array();
            $variation_detail_ary['ualp_variation_sku'] = $SKU;
            $variation_detail_ary['ualp_product_name'] = get_the_title($variation->id);
            $variation_detail_ary['ualp_variation_regular_price'] = $rprice;
            $variation_detail_ary['ualp_variation_sale_price'] = $sprice;
            $variation_detail_ary['ualp_virtual'] = $virtual;
            $variation_detail_ary['ualp_downloadable'] = $downloadable;
            $variation_detail_ary['ualp_weight'] = $weight;
            $variation_detail_ary['ualp_length'] = $length;
            $variation_detail_ary['ualp_manage_stock'] = $manage_stock;
            $variation_detail_ary['ualp_backorders'] = $backorders;
            $variation_detail_ary['ualp_tax_class'] = $tax_class;
            $variation_detail_ary['ualp_download_limit'] = $download_limit;
            $variation_detail_ary['ualp_download_expiry'] = $download_expiry;
//            $variation_detail_ary['ualp_downloadable_file'] = $downloadable_files;
            $variation_detail_ary['ualp_variation_description'] = $variation_description;
            $variation_detail_ary['ualp_variation_data'] = serialize($variation_data);
            $action = "product variation saved";
            $obj_type = "Woocommerce";
            $post_id = $variation->id;
            $post_title = get_the_title($post_id);
            $hook = "woocommerce_save_product_variation";
            $description = "$action in <a href='". get_edit_post_link($post_id) ."' target='blank'>'$post_title'<a>";
            $uactid = ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            ualpAddActivityLogDetail($uactid,$variation_detail_ary);
        }
    }

}
add_action('woocommerce_save_product_variation', 'ualpHookWCSaveProductVariation', 15, 2); 

/**
 * 
 * Fires when update woocommerce options
 * 
 * @param string $options 
 * @param string $oldvalue 
 * @param string $newvalue 
 */
if(!function_exists('ualpHookWoocomerceUpdatedOption')){
    
    function ualpHookWoocomerceUpdatedOption($option, $oldvalue, $_newvalue) {
        $whitelist_options = apply_filters('ualpWhitelistOptions', array(
            'woocommerce_shop_page_id',
            'woocommerce_shop_page_display',
            'woocommerce_category_archive_display',
            'woocommerce_default_catalog_orderby',
            'woocommerce_cart_redirect_after_add',
            'woocommerce_enable_ajax_add_to_cart',
            'shop_catalog_image_size',
            'shop_single_image_size',
            'shop_thumbnail_image_size',
            'woocommerce_enable_lightbox',
            'woocommerce_manage_stock',
            'woocommerce_hold_stock_minutes',
            'woocommerce_notify_low_stock',
            'woocommerce_notify_no_stock',
            'woocommerce_stock_email_recipient',
            'woocommerce_notify_low_stock_amount',
            'woocommerce_notify_no_stock_amount',
            'woocommerce_hide_out_of_stock_items',
            'woocommerce_stock_format',
            'woocommerce_file_download_method',
            'woocommerce_downloads_require_login',
            'woocommerce_downloads_grant_access_after_paymen',
            'woocommerce_bacs_settings',
            'woocommerce_bacs_accounts',
            'woocommerce_cheque_settings',
            'woocommerce_cod_settings'
        )); 

        if (!in_array($option, $whitelist_options))
            return;

        $logHookList = get_option('enableLogHookList');
        if (in_array('woocommerce_updated_option',$logHookList)) {
            $action = "updated";
            $obj_type = "Settings";
            $post_id = "";
            $post_title = $option;
            $hook = "updated_option";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('updated_option', 'ualpHookWoocomerceUpdatedOption', 15, 3);

class UALP_Integration_WooCommerce {

    private $_wc_options = array();

    public function init() {
        if (!class_exists('Woocommerce'))
            return;

        $logHookList = get_option('enableLogHookList');
        if (in_array('woocommerce_updated_option',$logHookList)) {
            add_filter('ualpWhitelistOptions', array(&$this, 'ualpWcWhitelistOptions'));
            add_filter('woocommerce_get_settings_pages', array(&$this, 'ualpWcGetSettingsPages'), 9999);
        }
    }

    /**
     * @param WC_Settings_Page[] $settings
     *
     * @return WC_Settings_Page[]
     */
    public function ualpWcGetSettingsPages($settings) {
        
        if (empty($this->_wc_options)) {
            $wc_exclude_types = array(
                'title',
                'sectionend',
            );
            $this->_wc_options = array();

            foreach ($settings as $setting) {
                foreach ($setting->get_settings() as $option) {
                    if (isset($option['id']) && (!isset($option['type']) || !in_array($option['type'], $wc_exclude_types) ))
                        $this->_wc_options[] = $option['id'];
                }
            }
        }
        return $settings;
    }

    public function ualpWcWhitelistOptions($ualpWcWhitelistOptions) {
        
        if (!empty($this->_wc_options)) {
            $ualpWcWhitelistOptions = array_unique(array_merge($ualpWcWhitelistOptions, $this->_wc_options));
        }
        return $ualpWcWhitelistOptions;
    }

    public function __construct() {
        add_action('init', array(&$this, 'init'));
    }

}

new UALP_Integration_WooCommerce();