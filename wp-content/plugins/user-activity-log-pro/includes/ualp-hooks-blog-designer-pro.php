<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}


/**
 * Get setting from database from shortcode id
 * @param int $shortcode_id
 * @global object $wpdb
 * @return string Shortcode name
 */
if (!function_exists('ualp_get_shortcode_settings')) {

    function ualp_get_shortcode_settings($shortcode_id) {
        global $wpdb;
        $tableName = $wpdb->prefix . 'blog_designer_pro_shortcodes';
        $get_settings_query = "SELECT * FROM $tableName WHERE bdid = " . $shortcode_id;
        $settings_val = $wpdb->get_results($get_settings_query, ARRAY_A);
        if (!$settings_val) {
            return;
        }
        foreach ($settings_val as $settings){
            return $settings['shortcode_name'];
        }
        return false;
    }

}

/**
 * Get Archive layout name from database from shortcode id
 * @param int $shortcode_id
 * @global object $wpdb
 * @return string Archive name
 */
if (!function_exists('ualp_get_archive_settings')) {

    function ualp_get_archive_settings($shortcode_id) {
        global $wpdb;
        $tableName = $wpdb->prefix . 'bdp_archives';
        $get_settings_query = "SELECT archive_name FROM $tableName WHERE id = " . $shortcode_id;
        $settings_val = $wpdb->get_results($get_settings_query, ARRAY_A);
        if (!$settings_val) {
            return;
        }
        foreach ($settings_val as $settings){
            return $settings['archive_name'];
        }
        return false;
    }

}

/**
 * Get activity for blog layout shortcode update
 *
 * @param int $shortcode_ID
 */
if (!function_exists('ualpHookBDPUpdateShortcode')){

    function ualpHookBDPUpdateShortcode($shortcode_ID) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {        
            $action = "layout updated";
            $obj_type = "Blog Designer Pro";
            $post_id = $shortcode_ID;
            $post_title = ualp_get_shortcode_settings($shortcode_ID);            
            $hook = "bdp_update_shortcode";
            $description = "$obj_type : '$post_title' $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_update_shortcode', 'ualpHookBDPUpdateShortcode',10,1);

/**
 * Get activity for add blog shortcode
 *
 * @param int $shortcode_ID
 */
if (!function_exists('ualpHookBDPAddShortcode')){

    function ualpHookBDPAddShortcode($shortcode_ID) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {   
            $action = "layout added";
            $obj_type = "Blog Designer Pro";
            $post_id = $shortcode_ID;
            $post_title = ualp_get_shortcode_settings($shortcode_ID);      
            $hook = "bdp_add_shortcode";
            $description = "$obj_type : '$post_title' $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_add_shortcode', 'ualpHookBDPAddShortcode');

/**
 * Get activity for Single file update
 *
 */
if (!function_exists('ualpHookBDPUpdateSingleFile')){

    function ualpHookBDPUpdateSingleFile() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {   
            $action = "updated in theme";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Single template file";
            $hook = "bdp_update_single_file";
            $description = "$obj_type : $post_title $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_update_single_file', 'ualpHookBDPUpdateSingleFile');

/**
 * Get activity for add archive layout
 *
 * @param int $shortcode_ID
 */
if (!function_exists('ualpHookBDPAddArchiveLayout')){

    function ualpHookBDPAddArchiveLayout($shortcode_ID) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {   
            $action = "Archive layout created";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = ualp_get_archive_settings($shortcode_ID);      
            $hook = "bdp_add_archive_layout";
            $description = "$obj_type :  $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_add_archive_layout', 'ualpHookBDPAddArchiveLayout');

/**
 * Get activity for
 *
 * @param int $shortcode_ID 
 */
if (!function_exists('ualpHookBDPDeleteShortcode')){

    function ualpHookBDPDeleteShortcode($shortcode_ID) {        
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {   
            $action = "Layout deleted";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "";  
            $hook = "bdp_delete_shortcode";
            $description = "$obj_type : $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_delete_shortcode', 'ualpHookBDPDeleteShortcode');

/**
 * Get activity for delete single template
 *
 */
if (!function_exists('ualpHookBDPDeleteSingleTemplate')){

    function ualpHookBDPDeleteSingleTemplate() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "delete single template";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Delete single file from theme";
            $hook = "bdp_delete_single_template";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_delete_single_template', 'ualpHookBDPDeleteSingleTemplate');

/**
 * Get activity for copy single template
 *
 */
if (!function_exists('ualpHookBDPCopySingleTemplate')){

    function ualpHookBDPCopySingleTemplate() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Copy template";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Copy single template in theme";
            $hook = "bdp_copy_single_template";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_copy_single_template', 'ualpHookBDPCopySingleTemplate');

/**
 * Get activity for duplicate layout
 *
 * @param int $shortcode_ID
 */
if (!function_exists('ualpHookBDPDuplicateLayout')){

    function ualpHookBDPDuplicateLayout($shortcode_ID) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Blog layout duplicated";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "";  
            $hook = "bdp_duplicate_layout_settings";
            $description = "$obj_type : $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_duplicate_layout_settings', 'ualpHookBDPDuplicateLayout');

/**
 * Get activity for import blog layout
 *
 */
if (!function_exists('ualpHookBDPImportBlogLayout')){

    function ualpHookBDPImportBlogLayout() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Import";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Import blog layout";
            $hook = "bdp_import_blog_layout_settings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_import_blog_layout_settings', 'ualpHookBDPImportBlogLayout');

/**
 * Get activity for import archive layout
 *
 */
if (!function_exists('ualpHookBDPImportArchiveLayout')){

    function ualpHookBDPImportArchiveLayout() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Import";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Import archive layout";
            $hook = "bdp_import_archive_layout_settings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_import_archive_layout_settings', 'ualpHookBDPImportArchiveLayout');

/**
 * Get activity for export archive layout
 *
 */
if (!function_exists('ualpHookBDPExportArchiveLayout')){

    function ualpHookBDPExportArchiveLayout() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Export";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Export archive layout";
            $hook = "bdp_export_archive_layout_settings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_export_archive_layout_settings', 'ualpHookBDPExportArchiveLayout');

/**
 * Get activity for export blog layout
 *
 */
if (!function_exists('ualpHookBDPExportBlogLayout')){

    function ualpHookBDPExportBlogLayout() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Export";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Export blog layout";
            $hook = "bdp_export_blog_layout_settings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_export_blog_layout_settings', 'ualpHookBDPExportBlogLayout');

/**
 * Get activity for update archive layout
 *
 * @param int $shortcode_ID
 */
if (!function_exists('ualpHookBDPUpdateArchivePage')){

    function ualpHookBDPUpdateArchivePage($shortcode_ID) {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Update Archive Layout";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = ualp_get_archive_settings($shortcode_ID);
            $hook = "bdp_update_archive_page";
            $description = "$obj_type $action : '$post_title'";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_update_archive_page', 'ualpHookBDPUpdateArchivePage');

/**
 * Get activity for update single page design
 *
 */
if (!function_exists('ualpHookBDPUpdateSinglePost')){

    function ualpHookBDPUpdateSinglePost() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('blog_designer',$logHookList)) {
            $action = "Update";
            $obj_type = "Blog Designer Pro";
            $post_id = "";
            $post_title = "Single post updated";
            $hook = "bdp_update_single_post";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('bdp_update_single_post', 'ualpHookBDPUpdateSinglePost');
