<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/**
 * User activity general Settings
 */

if(!function_exists('ualpGeneralSettings')){
    function ualpGeneralSettings(){
        global $wpdb;
        if(isset($_POST['btnResetGeneralData'])){
            // reset settings
            update_option('ualpKeepLogsDay','');
            $message = __('General Settings reset successfully.', 'user_activity_log_pro');
            ualpAdminNoticeMessage('updated', $message);
        }
        if(isset($_POST['btnSubmitGeneralData']) && isset($_POST['_wp_general_log_nonce']) && wp_verify_nonce($_POST['_wp_general_log_nonce'], '_wp_general_log_action')){
            // Log days settings
            update_option('ualpKeepLogsDay', $_POST['logdel']);
            $message = __('General Settings updated successfully.', 'user_activity_log_pro');
            ualpAdminNoticeMessage('updated', $message);
            do_action('ualp_general_settings_updated');
        }
        $log_day = get_option('ualpKeepLogsDay'); ?>
        <form class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>" method="POST">
            <div class="padding_left_right_15">
                <p class="margin_bottom_30"></p>
                <table>
                    <tr>
                        <th><?php _e('Keep logs for', 'user_activity_log_pro'); ?></th>
                        <td>
                            <input type="number" step="1" min="1" placeholder="30" value="<?php echo $log_day; ?>" name="logdel">&nbsp;<?php _e('Days','user_activity_log_pro'); ?>
                            <p><?php _e('Maximum number of days to keep activity log. Leave blank to keep activity log forever (not recommended).', 'user_activity_log_pro'); ?></p>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Delete Log Activities', 'user_activity_log_pro'); ?></th>
                        <td>
                            <?php $nonce = wp_create_nonce('my-nonce');
                            if(!get_option('ualp_delete_log_pwd') || get_option('ualp_delete_log_pwd') == ""){
                                $class = "setDeleteDataPassword";
                            }else {
                                $class = "deleteLogData";
                            }?>
                            <a id="<?php echo $class; ?>" href="?page=ualp_settings&_wpnonce=<?php echo $nonce; ?>">
                                <?php _e('Reset Database', 'user_activity_log_pro'); ?>
                            </a>
                            <p><?php
                                echo '<b class="red">'.__('Warning', 'user_activity_log_pro').' : </b>';
                                _e('Clicking this will delete all activities from the database.', 'user_activity_log_pro');
                                ?>
                            </p>
                        </td>
                    </tr>
                </table>
                <?php
                wp_nonce_field('_wp_general_log_action', '_wp_general_log_nonce');
                ?>
                <p class="submit">
                    <input type="submit" name="btnSubmitGeneralData" class="button button-primary" id="submit" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>" >
                    <input type="submit" name="btnResetGeneralData" class="button-secondary" value="<?php _e('Reset Changes','user_activity_log_pro'); ?>"  onclick="return confirm('<?php esc_js(_e('Are you sure to reset data?', 'user_activity_log_pro')); ?>')" >
                </p>
            </div>
        </form><?php
    }
}
/**
 * User activity log Settings
 */

if (!function_exists('ualpLogSettings')) {

    function ualpLogSettings() {
        global $wpdb;
        $post_types = get_post_types(array('public' => true), 'objects', 'and');
        $taxonomies = get_taxonomies(array('public' => true), 'objects', 'and');
        $taxCount = sizeof($taxonomies) - 1 ;
        if (isset($_POST['btnSubmitLog']) && isset($_POST['_wp_log_hook_nonce']) && wp_verify_nonce($_POST['_wp_log_hook_nonce'], '_wp_log_hook_action')) {
            $logHookList = "";
            $logHookList = $_POST['hook'];
            if($logHookList == ""){
                $logHookList = array();
            }
            update_option('enableLogHookList', $logHookList);
            $message = __('Log Hook Settings updated successfully.', 'user_activity_log_pro');
            ualpAdminNoticeMessage('updated', $message);
            do_action('ualp_hook_log_settings_updated');
        }        
        // Set default enable log for action type
        if(isset($_POST['btnResetLogData'])){
            $logHook = array();
            foreach ($post_types as $post_type) {
                $postTypeHook[] = $post_type->name;
            }
            $logHook = array_merge($logHook,$postTypeHook);
            foreach ($taxonomies as $taxonomy) {
                $taxonomyHook[] = $taxonomy->name;
            }
            $logHook = array_merge($logHook,$taxonomyHook);
            $pluginHook = array('activated_plugin','deactivated_plugin','delete_plugin','plugin_modify','plugin_updated','plugin_installed');
            $logHook = array_merge($pluginHook,$logHook);
            $contactHook = array('wpcf7_after_create','wpcf7_after_update','wpcf7_admin_notices');
            $logHook = array_merge($contactHook,$logHook);
            $ualpHook = array('ualp_export_activity_log','ualp_general_settings_updated','ualp_hook_log_settings_updated','ualp_manage_role','ualp_set_password','ualp_change_password','ualp_email_settings_updated','ualp_disable_email_notification');
            $logHook = array_merge($ualpHook,$logHook);
            $userHook = array('wp_login','wp_login_failed','wp_logout','delete_user','user_register','insert_user_meta');
            $logHook = array_merge($userHook,$logHook);
            $themeHook = array('theme_file_modify','switch_theme','customize_save','theme_updated','theme_installed','delete_site_transient_update_themes');
            $logHook = array_merge($themeHook,$logHook);
            $commentHook = array('wp_insert_comment','edit_comment','trash_comment','spam_comment','restore_comment','delete_comment','comment_unapprove','comment_approve');
            $logHook = array_merge($commentHook,$logHook);
            $menuviewHook = array('wp_create_nav_menu','wp_update_nav_menu','delete_nav_menu');
            $logHook = array_merge($menuviewHook,$logHook);
            $otherHook = array('_core_updated_successfully','import_end','export_wp','widget_update_callback','updated_option','woocommerce_updated_option');
            $logHook = array_merge($otherHook,$logHook);
            $supportedPluginHook = array('blog_designer','gravityforms','woocommerce_variation','wordfence','wordpress-seo');
            $logHook = array_merge($supportedPluginHook,$logHook);
            update_option('enableLogHookList', $logHook);
            $message = __('Log hook settings reset successfully.', 'user_activity_log_pro');
            ualpAdminNoticeMessage('updated', $message);
        }
        $logHookList = get_option('enableLogHookList');
        ?>
        <form class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>" method="POST">
            <div class="padding_left_right_15">
                <h3 class="ualpHeaderTitle"><?php _e('Display Option', 'user_activity_log_pro'); ?></h3>
                <table class="widefat post">
                    <tbody>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="post_type">
                            </th>
                            <td colspan="2"><?php _e("Post Types", "user_activity_log_pro")."(".sizeof($post_types).")"; ?></td>
                        </tr><?php
                        foreach ($post_types as $post_type) { ?>
                            <tr class="post_type">
                                <th class="check-column" scope="row">
                                    <input value="<?php echo $post_type->name; ?>" <?php checked(in_array($post_type->name,$logHookList),1); ?> data-type="<?php echo $post_type->label; ?>" name="hook[]" type="checkbox">
                                </th>
                                <td><?php printf(__('%s', 'user_activity_log_pro'), $post_type->label); ?></td>
                                <td><?php printf(__('%s : Create, update, delete', 'user_activity_log_pro'), $post_type->label); ?></td>
                            </tr><?php
                        }?>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="taxonomy">
                            </th>
                            <td colspan="2"><?php _e("Taxonomies", "user_activity_log_pro")."(".$taxCount.")"; ?></td>
                        </tr><?php
                        foreach ($taxonomies as $taxonomy) {
                            if ($taxonomy->name != 'post_format') {
                                ?>
                                <tr class="taxonomy">
                                    <th class="check-column" scope="row">
                                        <input value="<?php echo $taxonomy->name; ?>" <?php checked(in_array($taxonomy->name,$logHookList),1); ?> data-type="<?php echo $taxonomy->label; ?>" name="hook[]" type="checkbox" >
                                    </th>
                                    <td><?php printf(__('%s', 'user_activity_log_pro'), $taxonomy->label); ?></td>
                                    <td><?php printf(__('%s: Create, update, delete', 'user_activity_log_pro'), $taxonomy->label); ?></td>
                                </tr><?php
                            }
                        }?>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="plugin">
                            </th>
                            <td colspan="2"><?php _e('Plugin(6)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="activated_plugin" <?php checked(in_array('activated_plugin',$logHookList),1);?> data-type="plugin activated" name="hook[]" type="checkbox">
                            </th>
                            <td><?php echo 'activated_plugin'; ?></td>
                            <td><?php _e('Plugin activated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="deactivated_plugin" <?php checked(in_array('deactivated_plugin',$logHookList),1); ?> data-type="plugin deactivated" name="hook[]" type="checkbox">
                            </th>
                            <td><?php echo 'deactivated_plugin'; ?></td>
                            <td><?php _e('Plugin deactivated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="delete_plugin" <?php checked(in_array('delete_plugin',$logHookList),1); ?> data-type="plugin deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'delete_plugin'; ?></td>
                            <td><?php _e('Plugin deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="plugin_modify" <?php checked(in_array('plugin_modify',$logHookList),1); ?> data-type="Plugin file modified" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_redirect'; ?></td>
                            <td><?php _e('Plugin file modified', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="plugin_installed" <?php checked(in_array('plugin_installed',$logHookList),1); ?> data-type="Plugin Installed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'plugin_installed'; ?></td>
                            <td><?php _e('Plugin installed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="plugin">
                            <th class="check-column" scope="row">
                                <input value="plugin_updated" <?php checked(in_array('plugin_updated',$logHookList),1); ?> data-type="Plugin updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'plugin_updated'; ?></td>
                            <td><?php _e('Plugin updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <?php if (is_plugin_active('contact-form-7/wp-contact-form-7.php')) { ?>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="contactForm">
                            </th>
                            <td colspan="2"><?php _e('Contact Form 7(3)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="contactForm">
                            <th class="check-column" scope="row">
                                <input value="wpcf7_after_create" <?php checked(in_array('wpcf7_after_create',$logHookList),1); ?> data-type="Contact Form created" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wpcf7_after_create'; ?></td>
                            <td><?php _e('Contact Form created', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="contactForm">
                            <th class="check-column" scope="row">
                                <input value="wpcf7_after_update" <?php checked(in_array('wpcf7_after_update',$logHookList),1); ?> data-type="Contact Form updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wpcf7_after_update'; ?></td>
                            <td><?php _e('Contact Form updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="contactForm">
                            <th class="check-column" scope="row">
                                <input value="wpcf7_admin_notices" <?php checked(in_array('wpcf7_admin_notices',$logHookList),1); ?> data-type="Contact Form deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wpcf7_admin_notices'; ?></td>
                            <td><?php _e('Contact Form deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <?php } ?>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="ualp">
                            </th>
                            <td colspan="2"><?php _e('User Activity Log Settings(9)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_export_activity_log" <?php checked(in_array('ualp_export_activity_log',$logHookList),1); ?> data-type="User activity log exported" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_export_activity_log'; ?></td>
                            <td><?php _e('User activity log exported', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_general_settings_updated" <?php checked(in_array('ualp_general_settings_updated',$logHookList),1); ?> data-type="General settings updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_general_settings_updated'; ?></td>
                            <td><?php _e('General settings updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_hook_log_settings_updated" <?php checked(in_array('ualp_hook_log_settings_updated',$logHookList),1); ?> data-type="Log Hook settings updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_hook_log_settings_updated'; ?></td>
                            <td><?php _e('Log Hook settings updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_manage_role" <?php checked(in_array('ualp_manage_role',$logHookList),1); ?> data-type="Manage Role settings" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_manage_role'; ?></td>
                            <td><?php _e('Manage Role settings', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_set_password" <?php checked(in_array('ualp_set_password',$logHookList),1); ?> data-type="Set password" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_set_password'; ?></td>
                            <td><?php _e('Set password', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_change_password" <?php checked(in_array('ualp_change_password',$logHookList),1); ?> data-type="Change password" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_change_password'; ?></td>
                            <td><?php _e('Change password', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_email_settings_updated" <?php checked(in_array('ualp_email_settings_updated',$logHookList),1); ?> data-type="Email settings updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_email_settings_updated'; ?></td>
                            <td><?php _e('Email settings updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_disable_email_notification" <?php checked(in_array('ualp_disable_email_notification',$logHookList),1); ?> data-type="Disable email notification" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_disable_email_notification'; ?></td>
                            <td><?php _e('Disable email notification', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="ualp">
                            <th class="check-column" scope="row">
                                <input value="ualp_custom_event_log_updated" <?php checked(in_array('ualp_custom_event_log_updated',$logHookList),1); ?> data-type="Custom event log updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'ualp_custom_event_log_updated'; ?></td>
                            <td><?php _e('Custom event log updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="users">
                            </th>
                            <td colspan="2"><?php _e('User(6)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="wp_login" <?php checked(in_array('wp_login',$logHookList),1); ?> data-type="User login" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_login'; ?></td>
                            <td><?php _e('User login', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="wp_login_failed" <?php checked(in_array('wp_login_failed',$logHookList),1); ?> data-type="User login failed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_login_failed'; ?></td>
                            <td><?php _e('User login failed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="wp_logout" <?php checked(in_array('wp_logout',$logHookList),1); ?> data-type="User logout" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_logout'; ?></td>
                            <td><?php _e('User logout', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="delete_user" <?php checked(in_array('delete_user',$logHookList),1); ?> data-type="User deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'delete_user'; ?></td>
                            <td><?php _e('User deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="user_register" <?php checked(in_array('user_register',$logHookList),1); ?> data-type="User registerd" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'user_register'; ?></td>
                            <td><?php _e('User registerd', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="users">
                            <th class="check-column" scope="row">
                                <input value="insert_user_meta" <?php checked(in_array('insert_user_meta',$logHookList),1); ?> data-type="User profile updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'insert_user_meta'; ?></td>
                            <td><?php _e('User profile updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="theme">
                            </th>
                            <td colspan="2"><?php _e('Theme(6)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="theme_file_modify" <?php checked(in_array('theme_file_modify',$logHookList),1); ?> data-type="Theme file updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_redirect'; ?></td>
                            <td><?php _e('Theme file updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="switch_theme" <?php checked(in_array('switch_theme',$logHookList),1); ?> data-type="Theme Switched" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'switch_theme'; ?></td>
                            <td><?php _e('Theme Switched', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="customize_save" <?php checked(in_array('customize_save',$logHookList),1); ?> data-type="Theme customizer updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'customize_save'; ?></td>
                            <td><?php _e('Theme customizer updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="theme_updated" <?php checked(in_array('theme_updated',$logHookList),1); ?> data-type="Theme updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'theme_updated'; ?></td>
                            <td><?php _e('Theme updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="theme_installed" <?php checked(in_array('theme_installed',$logHookList),1); ?> data-type="Theme installed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'theme_installed'; ?></td>
                            <td><?php _e('Theme installed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="theme">
                            <th class="check-column" scope="row">
                                <input value="delete_site_transient_update_themes" <?php checked(in_array('delete_site_transient_update_themes',$logHookList),1); ?> data-type="Theme deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'delete_themes'; ?></td>
                            <td><?php _e('Theme deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="comment">
                            </th>
                            <td colspan="2"><?php _e('Comments(8)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="wp_insert_comment" <?php checked(in_array('wp_insert_comment',$logHookList),1); ?> data-type="Comment inserted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_insert_comment'; ?></td>
                            <td><?php _e('Comment inserted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="edit_comment" <?php checked(in_array('edit_comment',$logHookList),1); ?> data-type="Comment updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'edit_comment'; ?></td>
                            <td><?php _e('Comment updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="delete_comment" <?php checked(in_array('delete_comment',$logHookList),1); ?> data-type="Comment deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'delete_comment'; ?></td>
                            <td><?php _e('Comment deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="trash_comment" <?php checked(in_array('trash_comment',$logHookList),1); ?> data-type="Comment trashed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'trash_comment'; ?></td>
                            <td><?php _e('Comment trashed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="spam_comment" <?php checked(in_array('spam_comment',$logHookList),1); ?> data-type="Comment spammed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'spam_comment'; ?></td>
                            <td><?php _e('Comment spammed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="restore_comment" <?php checked(in_array('restore_comment',$logHookList),1); ?> data-type="Comment restore" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'restore_comment'; ?></td>
                            <td><?php _e('Comment restore', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="comment_approve" <?php checked(in_array('comment_approve',$logHookList),1); ?> data-type="Comment approved" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'comment_approve'; ?></td>
                            <td><?php _e('Comment approved', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="comment">
                            <th class="check-column" scope="row">
                                <input value="comment_unapprove" <?php checked(in_array('comment_unapprove',$logHookList),1); ?> data-type="Comment unapproved" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'comment_unapprove'; ?></td>
                            <td><?php _e('Comment unapproved', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="menu">
                            </th>
                            <td colspan="2"><?php _e('Menu(3)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="menu">
                            <th class="check-column" scope="row">
                                <input value="wp_create_nav_menu" <?php checked(in_array('wp_create_nav_menu',$logHookList),1); ?> data-type="Menu created" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_create_nav_menu'; ?></td>
                            <td><?php _e('Menu created', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="menu">
                            <th class="check-column" scope="row">
                                <input value="wp_update_nav_menu" <?php checked(in_array('wp_update_nav_menu',$logHookList),1); ?> data-type="Menu updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_update_nav_menu'; ?></td>
                            <td><?php _e('Menu updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="menu">
                            <th class="check-column" scope="row">
                                <input value="delete_nav_menu" <?php checked(in_array('delete_nav_menu',$logHookList),1); ?> data-type="Menu deleted" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'delete_nav_menu'; ?></td>
                            <td><?php _e('Menu deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="view">
                            </th>
                            <td colspan="2"><?php _e('Page Views(2)', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="view">
                            <th class="check-column" scope="row">
                                <input value="admin_head" <?php checked(in_array('admin_head',$logHookList),1); ?> data-type="Admin page viewed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'admin_head'; ?></td>
                            <td><?php _e('Admin page viewed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="view">
                            <th class="check-column" scope="row">
                                <input value="wp_head" <?php checked(in_array('wp_head',$logHookList),1); ?> data-type="Front page viewed" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'wp_head'; ?></td>
                            <td><?php _e('Front page viewed', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="groupHeader">
                            <th class="check-column" scope="row">
                                <input type="checkbox" name="" data-hookGroup="other">
                            </th>
                            <?php
                            $c = 5;
                            if(class_exists('Woocommerce')) {
                                $c = $c + 2;
                            }
                            if (is_plugin_active('gravityforms/gravityforms.php')) {
                                $c++;
                            }
                            if (is_plugin_active('wordfence/wordfence.php')) {
                                $c++;
                            }
                            if (is_plugin_active('wordpress-seo/wp-seo.php')) {
                                $c++;
                            }
                            if (is_plugin_active('blog-designer-pro/blog-designer-pro.php')) {
                                $c++;
                            }
                            ?>
                            <td colspan="2"><?php _e('Other', 'user_activity_log_pro'); echo "($c)"; ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="_core_updated_successfully" <?php checked(in_array('_core_updated_successfully',$logHookList),1); ?> data-type="Core Update" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo '_core_updated_successfully'; ?></td>
                            <td><?php _e('Core Update', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="import_end" <?php checked(in_array('import_end',$logHookList),1); ?> data-type="Import data" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'import_end'; ?></td>
                            <td><?php _e('Import wordpress data', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="export_wp" <?php checked(in_array('export_wp',$logHookList),1); ?> data-type="Export" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'export_wp'; ?></td>
                            <td><?php _e('Export wordpress data', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="widget_update_callback" <?php checked(in_array('widget_update_callback',$logHookList),1); ?> data-type="Widget updated" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'widget_update_callback'; ?></td>
                            <td><?php _e('Widget updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="updated_option" <?php checked(in_array('updated_option',$logHookList),1); ?> data-type="Settings Option" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'updated_option'; ?></td>
                            <td><?php _e('Settings option updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <?php if (class_exists('Woocommerce')) { ?>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="woocommerce_updated_option" <?php checked(in_array('woocommerce_updated_option',$logHookList),1); ?> data-type="Woocommerce Settings" name="hook[]" type="checkbox" >
                            </th>
                            <td class="woocommerce"><?php echo 'updated_option'; ?></td>
                            <td><?php _e('Woocommerce settings updated', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <tr class="other">
                            <th class="check-column" scope="row">
                                <input value="woocommerce_variation" <?php checked(in_array('woocommerce_variation',$logHookList),1); ?> data-type="Woocommerce variation" name="hook[]" type="checkbox" >
                            </th>
                            <td><?php echo 'Woocommerce product variation'; ?></td>
                            <td><?php _e('Woocommerce variation created, updated, deleted', 'user_activity_log_pro'); ?></td>
                        </tr>
                        <?php }
                        if (is_plugin_active('gravityforms/gravityforms.php')) {?>
                            <tr class="other">
                                <th class="check-column" scope="row">
                                    <input value="gravityforms" <?php checked(in_array('gravityforms',$logHookList),1); ?> data-type="gravity forms" name="hook[]" type="checkbox" >
                                </th>
                                <td><?php echo 'Gravity forms'; ?></td>
                                <td><?php _e('Gravity forms settings updated', 'user_activity_log_pro'); ?></td>
                            </tr><?php
                        }
                        if (is_plugin_active('wordfence/wordfence.php')) {?>
                            <tr class="other">
                                <th class="check-column" scope="row">
                                    <input value="wordfence" <?php checked(in_array('wordfence',$logHookList),1); ?> data-type="wordfence" name="hook[]" type="checkbox" >
                                </th>
                                <td><?php echo 'Wordfence'; ?></td>
                                <td><?php _e('Wordfence settings updated', 'user_activity_log_pro'); ?></td>
                            </tr><?php
                        }
                        if (is_plugin_active('wordpress-seo/wp-seo.php')) {?>
                            <tr class="other">
                                <th class="check-column" scope="row">
                                    <input value="wordpress-seo" <?php checked(in_array('wordpress-seo',$logHookList),1); ?> data-type="Yoast SEO" name="hook[]" type="checkbox" >
                                </th>
                                <td><?php echo 'Yoast SEO'; ?></td>
                                <td><?php _e('Yoast SEO settings updated', 'user_activity_log_pro'); ?></td>
                            </tr><?php
                        }
                        if (is_plugin_active('blog-designer-pro/blog-designer-pro.php')) {?>
                            <tr class="other">
                                <th class="check-column" scope="row">
                                    <input value="blog_designer" <?php checked(in_array('blog_designer',$logHookList),1); ?> data-type="Blog Designer Pro" name="hook[]" type="checkbox" >
                                </th>
                                <td><?php echo 'Blog Designer Pro'; ?></td>
                                <td><?php _e('Blog Designer Pro layout created, updated, deleted. Import, export, duplicate layouts', 'user_activity_log_pro'); ?></td>
                            </tr><?php
                        }
                        ?>
                    </tbody>
                    <thead>
                        <tr>
                            <th class="column-cb check-column">
                                <input type="checkbox" id="cb_select" title="<?php _e('Select All','user_activity_log_pro'); ?>">
                            </th>
                            <td scope="col"><?php _e('Hook','user_activity_log_pro'); ?></td>
                            <td scope="col"><?php _e('Description','user_activity_log_pro'); ?></td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="column-cb check-column">
                                <input type="checkbox" id="cb_select" title="<?php _e('Select All','user_activity_log_pro'); ?>">
                            </th>
                            <td scope="col"><?php _e('Hook','user_activity_log_pro'); ?></td>
                            <td scope="col"><?php _e('Description','user_activity_log_pro'); ?></td>
                        </tr>
                    </tfoot>
                </table>
                <?php
                wp_nonce_field('_wp_log_hook_action', '_wp_log_hook_nonce');
                ?>
                <p class="submit">
                    <input type="submit" name="btnSubmitLog" class="button button-primary" id="submit" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>" >
                    <input type="submit" name="btnResetLogData" class="button-secondary" value="<?php _e('Reset Changes','user_activity_log_pro'); ?>"  onclick="return confirm('<?php esc_js(_e('Are you sure to reset data?', 'user_activity_log_pro')); ?>')" >
                </p>
            </div>
        </form><?php
    }

}
/**
 * User activity email Settings
 */
if (!function_exists('ualpEmailSettings')){

    function ualpEmailSettings() {
        global $wpdb, $current_user;
        $search = $get_user_data = $get_role_data = "";
        $paged = $total_pages = 1;
        $srno = 0;
        $recordperpage = 10;
        $where = "where 1=1";
        $display = "roles";
        $user_details = "[user_details]";
        wp_get_current_user();
        if (isset($_POST['saveLogin']) && isset($_POST['_wp_email_notify_nonce']) && wp_verify_nonce($_POST['_wp_email_notify_nonce'], '_wp_email_notify_action')) {
            $emailEnable = $_POST['rdoualpEnableEmail'];
            $to_email = $_POST['txtualpToEmail'];
            $from_email = $_POST['txtualpFromEmail'];
            $mail_msg = $_POST['txtualpEmailMsg'];
            if ($_POST['rdoualpEnableEmail'] == '1') {
                $display = isset($_REQUEST['display']) ? $_REQUEST['display'] : "roles";
                if ($display == "users") {
                    $enableuser = isset($_POST['usersID']) ? $_POST['usersID'] : "";
                    update_option('enable_user_list', $enableuser);
                }
                if ($display == "roles") {
                    $enablerole = isset($_POST['rolesID']) ? $_POST['rolesID'] : array(0 => "administrator");
                    $ualpRole = array(0 => "administrator");
                    if(!in_array('administrator',$enablerole)){
                        $enablerole = array_merge($enablerole,$ualpRole);
                    }
                    $enable_user_login = array();
                    for ($i = 0; $i < sizeof($enablerole); $i++) {
                        $condition = "um.meta_key='" . $wpdb->prefix . "capabilities' and um.meta_value like '%" . $enablerole[$i] . "%' and u.ID = um.user_id";
                        $enable_list_user = "SELECT * FROM " . $wpdb->prefix . "usermeta as um, " . $wpdb->prefix . "users as u WHERE $condition";
                        $get_user = $wpdb->get_results($enable_list_user);
                        foreach ($get_user as $k => $v) {
                            $enable_user_login[] = $v->user_login;
                        }
                    }
                    update_option('enable_role_list', $enablerole);
                    update_option('enable_user_list', $enable_user_login);
                }
                if ($mail_msg == "") {
                    $message = __("Please enter message", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } else if ($to_email == "" || $from_email == "") {
                    $message = __("Please enter the email address.", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } else if (!filter_var($to_email, FILTER_VALIDATE_EMAIL) || !filter_var($from_email, FILTER_VALIDATE_EMAIL) || !is_email($to_email) || !is_email($from_email)) {
                    $message = __("Please enter valid email address.", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } else {
                    update_option('ualpToEmail', $to_email);
                    update_option('ualpFromEmail', $from_email);
                    update_option('ualpEmailMessage', $mail_msg);
                    $message = __("Email Settings saved successfully.", "user_activity_log_pro");
                    ualpAdminNoticeMessage("updated", $message);
                    do_action('ualp_email_settings_updated');
                }
            } else {
                $message = __("Email notification disable successfully.", "user_activity_log_pro");
                ualpAdminNoticeMessage("updated", $message);
                do_action('ualp_disable_email_notification');
            }
            update_option('ualpEnableEmailNotify', $emailEnable);
        }
        if(isset($_POST['btnResetEmailData'])){
            $mail_msg = __('Hi ', 'user_activity_log_pro');
            $mail_msg .= sprintf(__('%s,', 'user_activity_log_pro'),$current_user->display_name);
            $mail_msg .= "\n\n".__("Following user is logged in your site", 'user_activity_log_pro') . " \n$user_details";
            $mail_msg .= "\n\n".__("Thanks, \n", 'user_activity_log_pro');
            $mail_msg .= home_url();
            update_option('ualpEnableEmailNotify', 1);
            update_option('ualpToEmail', $current_user->user_email);
            update_option('ualpFromEmail', get_option('admin_email'));
            update_option('ualpEmailMessage', $mail_msg);
            $message = __("Email settings reset successfully.", "user_activity_log_pro");
            ualpAdminNoticeMessage("updated", $message);
        }
        if (isset($_GET['paged']))
            $paged = $_GET['paged'];
        $offset = ($paged - 1) * $recordperpage;
        if (isset($_GET['display'])) {
            $display = $_GET['display'];
        }
        if (isset($_GET['txtsearch']) && $_GET['txtsearch'] != "" && $display == "users") {
            $search = $_GET['txtsearch'] ? sanitize_text_field($_GET['txtsearch']) : "" ;
            $where.=" and user_login like '%$search%' or user_email like '%$search%' or display_name like '%$search%'";
        }        
        // query for display all the users data start
        if ($display == "users") {
            $table_name = $wpdb->prefix . "users";
            $select_query = "SELECT * from $table_name $where LIMIT $offset,$recordperpage";
            $get_user_data = $wpdb->get_results($select_query);
            $total_items_query = "SELECT count(*) FROM $table_name $where";
            $total_items = $wpdb->get_var($total_items_query, 0, 0);
        } else {
            $table_name = $wpdb->prefix . "usermeta as um";
            $where.=" and um.meta_key='" . $wpdb->prefix . "capabilities'";
            $select_query = "SELECT distinct um.meta_value from $table_name $where LIMIT $offset,$recordperpage";
            $get_role_data = $wpdb->get_results($select_query);
            $allRolesList = array();
            foreach ($get_role_data as $getRole) {
                $final_roles = unserialize($getRole->meta_value);
                if(sizeof($final_roles)>1){
                    foreach($final_roles as $k=>$v){
                        $allRolesList[] = $k;
                    }                                
                }else{
                    $allRolesList[] = key($final_roles);
                }
            }
            $allRolesList = array_unique($allRolesList);
            $total_items_query = "SELECT count(distinct um.meta_value) FROM $table_name $where";
            $total_items = $wpdb->get_var($total_items_query, 0, 0);
        }
        // query for pagination
        $total_pages = ceil($total_items / $recordperpage);
        $next_page = (int) $paged + 1;
        if ($next_page > $total_pages)
            $next_page = $total_pages;
        $prev_page = (int) $paged - 1;
        if ($prev_page < 1)
            $prev_page = 1;
        
        $ualpToEmail = get_option('ualpToEmail');
        $ualpFromEmail = get_option('ualpFromEmail');
        $ualpEmailEnable = get_option('ualpEnableEmailNotify');
        $ualpEmailMsg = get_option('ualpEmailMessage');
        
        $mail_message = __('Hi ', 'user_activity_log_pro');
        $mail_message .= sprintf(__('%s,', 'user_activity_log_pro'),$current_user->display_name);
        $mail_message .= "\n\n".__("Following user is logged in your site", 'user_activity_log_pro') . " \n$user_details";
        $mail_message .= "\n\n".__("Thanks, \n", 'user_activity_log_pro');
        $mail_message .= home_url();
        
        $emailEnable = (isset($ualpEmailEnable) && $ualpEmailEnable != "") ? $ualpEmailEnable : 1;
        $to_email = (isset($ualpToEmail) && $ualpToEmail != "") ? $ualpToEmail : $current_user->user_email;
        $from_email = (isset($ualpFromEmail) && $ualpFromEmail != "") ? $ualpFromEmail : get_option('admin_email');
        $mail_msg = (isset($ualpEmailMsg) && $ualpEmailMsg != "") ? $ualpEmailMsg : $mail_message;
        ?>
        <form method="POST" class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>">
            <div class="padding_left_right_15">
                <div class="emailNotificationCover">
                    <h3 class="ualpHeaderTitle"><?php _e('Email Settings', 'user_activity_log_pro'); ?></h3>
                    <table class="widefat" cellspacing="0">
                        <tr>
                            <th><?php _e('Enable Email Notification?', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="radio" <?php checked($emailEnable, 1); ?> value="1" id="enableEmail" name="rdoualpEnableEmail" class="ui-helper-hidden-accessible">
                                <label class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left" for="enableEmail" role="button">
                                    <span class="ui-button-text"><?php _e('Yes', 'user_activity_log_pro'); ?></span>
                                </label>
                                <input type="radio" <?php checked($emailEnable, 0); ?> value="0" id="disableEmail" name="rdoualpEnableEmail" class="ui-helper-hidden-accessible">                                    
                                <label class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-right"for="disableEmail" role="button">
                                    <span class="ui-button-text"><?php _e('No', 'user_activity_log_pro'); ?></span>
                                </label>
                            </td>
                        </tr>
                        <tr class="fromEmailTr">
                            <th><?php _e('From Email', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="email" name="txtualpFromEmail" value="<?php echo $from_email; ?>">
                                <p class="description"><?php _e('The source Email address', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr>
                        <tr class="toEmailTr">
                            <th><?php _e('To Email', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="email" name="txtualpToEmail" value="<?php echo $to_email; ?>">
                                <p class="description"><?php _e('The Email address notifications will be sent to', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr>
                        <tr class="messageTr">
                            <th><?php _e('Message', 'user_activity_log_pro'); ?></th>
                            <td>
                                <?php wp_editor($mail_msg, 'txtualpEmailMsg', array('editor_height' => 250)); ?>
                                <p class="description"><?php _e('Customize the message as per your requirement', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="selectUserForEmail">
                    <h3 class="ualpHeaderTitle"><?php _e('Select Users/Roles', 'user_activity_log_pro'); ?></h3>
                    <p><?php _e('Email will be sent upon login of these selected users/roles.', 'user_activity_log_pro'); ?></p>
                    <!-- Search Box start --><?php if ($display == 'users') { ?>
                        <p class="search-form">
                            <label class="screen-reader-text" for="search-input"><?php _e('Search', 'user_activity_log_pro'); ?> :</label>
                            <input type="search" title="<?php _e('Search user by username,email,firstname and lastname','user_activity_log_pro'); ?>" placeholder="<?php _e('Username, Email, Firstname, Lastname','user_activity_log_pro'); ?>" value="<?php echo $search; ?>" name="txtSearchinput">
                            <input id="search-submit" class="button" type="submit" value="<?php esc_attr_e('Search', 'user_activity_log_pro'); ?>" name="btnSearch_user_role">
                        </p><?php }
                                    ?>
                    <!-- Search Box end -->
                    <div class="tablenav top <?php if ($display == 'roles') echo 'ualpDisplayRoles'; ?>">
                        <!-- Drop down menu for user and Role Start -->
                        <div class="actions pull-left">
                            <select name="user_role">
                                <option selected value="roles"><?php _e('Role', 'user_activity_log_pro'); ?></option>
                                <option <?php selected($display, 'users'); ?> value="users"><?php _e('User', 'user_activity_log_pro'); ?></option>
                            </select>
                            <input class="button-secondary action sol-filter-btn" type="submit" value="<?php _e('Filter', 'user_activity_log_pro'); ?>" name="btn_filter_user_role">
                        </div>
                        <!-- Drop down menu for user and Role end -->
                        <!-- top pagination start -->
                        <div class="tablenav-pages" <?php
                        if ((int) $total_pages <= 1) {
                            echo 'style="display:none;"';
                        }
                        ?>>
                                 <?php $items = sprintf(_n('%s item', '%s items', $total_items, 'user_activity_log_pro'), $total_items); ?>
                            <span class="displaying-num"><?php echo $items; ?></span>
                            <span class="pagination-links">
                                <?php if ($paged == '1') { ?>
                                    <span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>
                                    <span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>
                                <?php } else { ?>
                                    <a class="first-page <?php if ($paged == '1') echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=1&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the first page', 'user_activity_log_pro'); ?>">&laquo;</a>
                                    <a class="prev-page <?php if ($paged == '1') echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $prev_page . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the previous page', 'user_activity_log_pro'); ?>">&lsaquo;</a>
                                <?php } ?>
                                <span class="paging-input">
                                    <input class="current-page" type="text" size="1" value="<?php echo $paged; ?>" name="paged" title="<?php _e('Current page', 'user_activity_log_pro'); ?>"> <?php _e('of', 'user_activity_log_pro'); ?>
                                    <span class="total-pages"><?php echo $total_pages; ?></span>
                                </span>
                                <a class="next-page <?php if ($paged == $total_pages) echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $next_page . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the next page', 'user_activity_log_pro'); ?>">&rsaquo;</a>
                                <a class="last-page <?php if ($paged == $total_pages) echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $total_pages . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the last page', 'user_activity_log_pro'); ?>">&raquo;</a>
                            </span>
                        </div>
                        <!-- top pagination end -->
                    </div>
                    <!-- display users details start -->
                    <?php
                    if ($display == "users") { ?>
                        <table class="widefat striped" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" class="check-column"><input type="checkbox" id="cb-select-all-user"/></th>
                                    <td scope="col"><?php _e('Author', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('First name', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('Last name', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('Email address', 'user_activity_log_pro'); ?></td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th scope="col" class="check-column"><input type="checkbox" id="cb-select-all-user1"/></th>
                                    <td scope="col"><?php _e('Author', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('First name', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('Last name', 'user_activity_log_pro'); ?></td>
                                    <td scope="col"><?php _e('Email address', 'user_activity_log_pro'); ?></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                if ($get_user_data) {
                                    $user_enable = get_option('enable_user_list');
                                    $checked = '';
                                    foreach ($get_user_data as $data) {
                                        $u_d = get_userdata($data->ID);
                                        ?>
                                        <tr>
                                            <th class="check-column" scope="row">
                                                <input type="checkbox" name="usersID[]" value="<?php echo $data->user_login; ?>" <?php 
                                                    if ($user_enable != "" && in_array($data->user_login, $user_enable)) {
                                                       echo $checked = "checked=checked";
                                                    } ?> />
                                            </th>
                                            <td class="author column-author">
                                                <?php
                                                global $wp_roles;
                                                if (!empty($data->ID) && 0 !== (int) $data->ID) {
                                                    $user = get_user_by('id', $data->ID);
                                                    if ($user instanceof WP_User && 0 !== $user->ID) {
                                                        ?>
                                                            <a href="<?php printf(__('%s','user_activity_log_pro'),get_edit_user_link($user->ID)); ?>">
                                                                <?php echo get_avatar($user->ID, 40);?> 
                                                                <span><?php printf(__('%s','user_activity_log_pro'),ucfirst($user->display_name)); ?></span>
                                                            </a>
                                                            <br />
                                                            <small><?php printf(__('%s','user_activity_log_pro'),isset($user->roles[0]) && isset($wp_roles->role_names[$user->roles[0]]) ? $wp_roles->role_names[$user->roles[0]] : 'Unknown'); ?></small>
                                                            <?php
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                $first_name = (isset($u_d->user_firstname) && !empty($u_d->user_firstname)) ? $u_d->user_firstname : ' - ';
                                                printf(__('%s', 'user_activity_log_pro'), ucfirst($first_name));
                                                ?>
                                            </td>
                                            <td><?php
                                                $last_name = isset($u_d->user_lastname) ? $u_d->user_lastname : '-';
                                                printf(__('%s', 'user_activity_log_pro'), ucfirst($last_name));
                                                ?>
                                            </td>
                                            <td><?php printf(__('%s', 'user_activity_log_pro'), $data->user_email); ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    echo '<tr class="no-items">';
                                    echo '<td class="colspanchange" colspan="5">' . __('No log found.', 'user_activity_log_pro') . '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                        </table><?php
                    }
                    // Display users details end
                    if ($display == "roles") {
                        $editable_roles = get_editable_roles();
                        foreach ($editable_roles as $role => $details) {
                            $sub = esc_attr($role);
                            $roles[] = $sub;
                        }
                        // Display roles details start?>
                        <table class="widefat ualpDisplayRoles striped" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" class="column-cb check-column"><input type="checkbox" id="cb-select-all-role"/></th>
                                    <td scope="col"><?php _e('Role', 'user_activity_log_pro'); ?></td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th scope="col" class="column-cb check-column"><input type="checkbox" id="cb-select-all-role"/></th>
                                    <td scope="col"><?php _e('Role', 'user_activity_log_pro'); ?></td>
                                </tr>
                            </tfoot>
                            <tbody><?php
                                if ($allRolesList) {
                                    $role_enable = get_option('enable_role_list');
                                    foreach ($allRolesList as $data) {
                                        if(in_array( $data,$roles)){
                                            ?>
                                            <tr>
                                                <th class="<?php if($data != 'administrator'){ echo 'check-column';}else{ echo 'margin_0 ualp-not-allowed';} ?>">
                                                    <input name="rolesID[]" value="<?php echo $data; ?>" type="checkbox" <?php 
                                                        if($data == 'administrator'){ 
                                                            echo "disabled=''"; 
                                                            echo "checked=checked";
                                                        } 
                                                        if($data != 'administrator'){ 
                                                            if ($role_enable != "" && in_array($data, $role_enable)) {
                                                                echo $checked = "checked=checked";
                                                            }
                                                        }?> >
                                                </th>
                                                <td><?php echo ucfirst($data); ?></td>
                                            </tr><?php
                                        }
                                    }
                                } else {
                                    echo '<tr class="no-items">';
                                    echo '<td class="colspanchange" colspan="1">' . __('No log found.', 'user_activity_log_pro') . '</td>';
                                    echo '</tr>';
                                } ?>
                            </tbody>
                        </table>
                        <!-- display roles details end --><?php
                    } ?>
                    <!-- bottom pagination start -->
                    <div class="tablenav bottom <?php if ($display == 'roles') echo 'ualpDisplayRoles'; ?>" <?php if ((int) $total_pages <= 1) echo 'style="display:none;"';?>>
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php echo $items; ?></span>
                            <span class="pagination-links">
                                <?php if ($paged == '1') { ?>
                                    <span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>
                                    <span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>
                                <?php } else { ?>
                                    <a class="first-page <?php if ($paged == '1') echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=1&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the first page', 'user_activity_log_pro'); ?>">&laquo;</a>
                                    <a class="prev-page <?php if ($paged == '1') echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $prev_page . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the previous page', 'user_activity_log_pro'); ?>">&lsaquo;</a>
                                <?php } ?>
                                <span class="paging-input">
                                    <span class="current-page" title="<?php _e('Current page', 'user_activity_log_pro'); ?>"><?php
                                        printf(__('%s', 'user_activity_log_pro'), $paged);
                                        _e(' of', 'user_activity_log_pro');
                                        ?> </span>
                                    <span class="total-pages"><?php echo $total_pages; ?></span>
                                </span>
                                <a class="next-page <?php if ($paged == $total_pages) echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $next_page . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the next page', 'user_activity_log_pro'); ?>">&rsaquo;</a>
                                <a class="last-page <?php if ($paged == $total_pages) echo 'disabled'; ?>" href="<?php echo '?page=ualp_settings&paged=' . $total_pages . '&display=' . $display . '&txtsearch=' . $search; ?>" title="<?php _e('Go to the last page', 'user_activity_log_pro'); ?>">&raquo;</a>
                            </span>
                        </div>
                    </div>
                    <!-- bottom pagination end -->
                </div>
                <?php
                wp_nonce_field('_wp_email_notify_action', '_wp_email_notify_nonce');
                ?>
                <p class="submit">
                    <input id="submit" class="button button-primary" type="submit" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>" name="saveLogin">
                    <input class="button button-secondary" type="submit" value="<?php esc_attr_e('Reset Changes', 'user_activity_log_pro'); ?>" name="btnResetEmailData" onclick="return confirm('<?php esc_js(_e('Are you sure to reset data?', 'user_activity_log_pro')); ?>')">
                </p>
            </div>
        </form><?php
    }

}

/**
 * User activity Role Manager settings
 */
if (!function_exists('ualpRoleSetting')){

    function ualpRoleSetting() {
        global $wpdb;
        if(isset($_POST['btnRoleManager']) && isset($_POST['_wp_role_manage_nonce']) && wp_verify_nonce($_POST['_wp_role_manage_nonce'], '_wp_role_manage_action')){
            if(isset($_POST['rolesManage']) && $_POST['rolesManage'] != ""){
                update_option('ualpRoleAccessList',$_POST['rolesManage']);   
            }else{
                update_option('ualpRoleAccessList',"");   
            }
            do_action('ualp_manage_role');
            $message = __("Users access save successfully.", "user_activity_log_pro");
            ualpAdminNoticeMessage("updated", $message);
            
            $ualpAdminUi = new UALP_Admin_Ui();
            $ualpAdminUi->create_admin_menu();

        }
        if(isset($_POST['btnResetRoleData'])){
            update_option('ualpRoleAccessList','');
            $message = __("Users access reset successfully.", "user_activity_log_pro");
            ualpAdminNoticeMessage("updated", $message);
        }
        $table_name = $wpdb->prefix . "usermeta as um";
        $rolesQuery = "SELECT distinct um.meta_value from $table_name where um.meta_key='" . $wpdb->prefix . "capabilities'";
        $getRoles = $wpdb->get_results($rolesQuery);
        $ualpRoleAccessList = get_option('ualpRoleAccessList');
        $ualpRoleAccessList1 = array(0 => "administrator");
        if(empty($ualpRoleAccessList)){
            $ualpRoleAccessList = $ualpRoleAccessList1;
        }
        if(!in_array('administrator',$ualpRoleAccessList)){
            $ualpRoleAccessList = array_merge($ualpRoleAccessList,$ualpRoleAccessList1);
        }
        $editable_roles = get_editable_roles();
        foreach ($editable_roles as $role => $details) {
            $sub = esc_attr($role);
            $roles[] = $sub;
        }
        update_option('ualpRoleAccessList',$ualpRoleAccessList);
        ?>
        <form class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>" method="POST">
            <div class="padding_left_right_15">
                <h3 class="ualpHeaderTitle"><?php _e('Select Role','user_activity_log_pro'); ?></h3>
                <p class="margin_bottom_30"><?php _e('Who can access the User Activity Log Pro panel? There is one "Role Manager" tab option available under settings page and you can assign that stuff to Contributor, Editor or one of your custom role.','user_activity_log_pro'); ?></p>
                <table class="widefat striped ualpDisplayRoles" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="column-cb check-column"><input type="checkbox" id="cb-select-all-roles"/></th>
                            <td scope="col"><?php _e('Role', 'user_activity_log_pro'); ?></td>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="column-cb check-column"><input type="checkbox" id="cb-select-all-roles"/></th>
                            <td scope="col"><?php _e('Role', 'user_activity_log_pro'); ?></td>
                        </tr>
                    </tfoot>
                    <tbody><?php
                        $inArray = 0;
                        $allRolesList = array();
                        foreach ($getRoles as $getRole) {
                            $final_roles = unserialize($getRole->meta_value);
                            if(sizeof($final_roles)>1){
                                foreach($final_roles as $k=>$v){
                                    $allRolesList[] = $k;
                                }                                
                            }else{
                                $allRolesList[] = key($final_roles);
                            }
                        }
                        $allRolesList = array_unique($allRolesList);
                        $ualpRoleAccessList = get_option('ualpRoleAccessList');
                        foreach($allRolesList as $allRoleList){
                            if(in_array($allRoleList,$roles)){
                                $inArray = in_array($allRoleList,$ualpRoleAccessList); ?>
                                <tr>
                                    <th class="<?php if($allRoleList != 'administrator'){ echo 'check-column';}else{ echo 'margin_0 ualp-not-allowed';} ?>">
                                        <input type="checkbox" <?php if($allRoleList == 'administrator'){ echo "disabled=''"; echo " checked=''";} ?>  name="rolesManage[]" <?php echo checked($inArray,'1'); ?> value="<?php echo $allRoleList; ?>">
                                    </th>
                                    <td>
                                        <?php printf(__('%s','user_activity_log_pro'),ucfirst($allRoleList)); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <?php
                wp_nonce_field('_wp_role_manage_action', '_wp_role_manage_nonce');
                ?>
                <p class="submit">
                    <input id="roleManager" class="button button-primary" type="submit" name="btnRoleManager" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>">
                    <input class="button button-secondary" type="submit" name="btnResetRoleData" value="<?php _e('Reset Changes','user_activity_log_pro'); ?>" onclick="return confirm('<?php esc_js(_e('Are you sure to reset data?', 'user_activity_log_pro')); ?>')">
                </p>
            </div>
        </form><?php
    }

}

/**
 * User activity Set password for delete log
 */
if (!function_exists('ualpPwdSettings')) {

    function ualpPwdSettings() {
        $admin_pass = $password = $repeatPassword = $oldPassword = $newPassword = $repeatNewPassword = $oldPasswordMD5 = $passwordMD5 = $newPasswordMD5 = $matchadminpwd = $matchadminnewpwd = "";
        $current_user = wp_get_current_user();
        $admin_pass = $current_user->user_pass;
        $currentPassword = get_option('ualp_delete_log_pwd');
        if (isset($_POST['btnSetPassword']) && isset($_POST['_wp_set_password_nonce']) && wp_verify_nonce($_POST['_wp_set_password_nonce'], '_wp_set_password_action')) {
            if (isset($_POST['password']) && $_POST['password'] != "") {
                $password = $_POST['password'];
                $passwordMD5 = md5($password);
                $matchadminpwd = wp_check_password($password, $admin_pass);
            }
            if (isset($_POST['repeatPassword']) && $_POST['repeatPassword'] != "") {
                $repeatPassword = $_POST['repeatPassword'];
            }
            if (isset($_POST['oldPassword']) && $_POST['oldPassword'] != "") {
                $oldPassword = $_POST['oldPassword'];
                $oldPasswordMD5 = md5($oldPassword);
            }
            if (isset($_POST['newPassword']) && $_POST['newPassword'] != "") {
                $newPassword = $_POST['newPassword'];
                $newPasswordMD5 = md5($newPassword);
                $matchadminnewpwd = wp_check_password($newPassword, $admin_pass);
            }
            if (isset($_POST['repeatNewPassword']) && $_POST['repeatNewPassword'] != "") {
                $repeatNewPassword = $_POST['repeatNewPassword'];
            }
            if (isset($currentPassword) && $currentPassword != "") {
                if ($oldPassword == "") {
                    $message = __("Please Enter Old password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($newPassword == "") {
                    $message = __("Please Enter New password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($repeatNewPassword == "") {
                    $message = __("Please Enter Repeat password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($oldPasswordMD5 != $currentPassword) {
                    $message = __("Old password not match", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($newPassword != "" && $repeatNewPassword != "" && ($newPassword != $repeatNewPassword)) {
                    $message = __("Password and repeat password not same", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($newPassword != "" && $newPasswordMD5 == $currentPassword) {
                    $message = __("This Password is not valid. You current password and new password is same", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($matchadminnewpwd == 1) {
                    $message = __("This password is not acceptable. This is same as admin password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($newPassword != "" && ($repeatNewPassword == $newPassword) && ($newPassword != $currentPassword) && ($oldPasswordMD5 == $currentPassword) && $matchadminnewpwd != 1) {
                    update_option('ualp_delete_log_pwd', $newPasswordMD5);
                    $message = __("Password updated successfully", "user_activity_log_pro");
                    ualpAdminNoticeMessage("updated", $message);
                    do_action('ualp_change_password');
                }
            } else {
                if ($password == "") {
                    $message = __("Please Enter password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($repeatPassword == "") {
                    $message = __("Please Enter Repeat password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($password != "" && $repeatPassword != "" && ($password != $repeatPassword)) {
                    $message = __("Password and repeat password not same", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($matchadminpwd == 1) {
                    $message = __("This password is not acceptable. This is same as admin password", "user_activity_log_pro");
                    ualpAdminNoticeMessage("error", $message);
                } elseif ($password != "" && $password == $repeatPassword && $matchadminpwd != 1) {
                    update_option('ualp_delete_log_pwd', $passwordMD5);
                    $message = __("Password saved successfully", "user_activity_log_pro");
                    ualpAdminNoticeMessage("updated", $message);
                    do_action('ualp_set_password');
                }
            }
            $currentPassword = get_option('ualp_delete_log_pwd');
        }
        if (isset($_POST['btnResetPassword']) && isset($_POST['_wp_set_password_nonce']) && wp_verify_nonce($_POST['_wp_set_password_nonce'], '_wp_set_password_action')) {
            global $user;
            $user = wp_get_current_user();
            $old_delete_log_password = get_option('ualp_delete_log_pwd');
            $random_passwd = wp_generate_password();
            $random_hash_passwd = md5($random_passwd);
            $from = get_option('ualpFromEmail');
            $to = get_option('ualpToEmail');
            $headers = "MIME-Version: 1.0;\r\n";
            $headers .= "From: " . strip_tags($from) . "\r\n";
            $headers .= "Content-Type: text/html; charset: utf-8;\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
            $subject = __('Reset Password - User Activity Log Pro', 'user_activity_log_pro');
            $body = '';
            ob_start();
            ?>
            <div style="background: #F5F5F5; border-width: 1px; border-style: solid; padding-bottom: 20px; margin: 0px auto; width: 750px; height: auto; border-radius: 3px 3px 3px 3px; border-color: #5C5C5C;">
                <div style="border: #FFF 1px solid; background-color: #ffffff !important; margin: 20px 20px 0;height: auto; -moz-border-radius: 3px; padding-top: 15px;">
                    <div style="padding: 20px 20px 20px 20px; font-family: Arial, Helvetica, sans-serif;height: auto; color: #333333; font-size: 13px;">
                        <div style="width: 100%;">
                            <strong><?php 
                            _e('Dear','user_activity_log_pro'); 
                            printf(__('%s','user_activity_log_pro'),$user->display_name);
                            ?></strong>,<br /><br />
                            <?php _e('Here is your New password :-','user_activity_log_pro'); ?>  <strong><?php echo $random_passwd; ?></strong><br /><br />
                            <?php _e('Thank you','user_activity_log_pro'); ?> ,<br /><br />
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $body = ob_get_clean();            
            $mail = wp_mail($to, $subject, $body, $headers);
            if($mail) {
                update_option('ualp_delete_log_pwd', $random_hash_passwd);
                $message = __('Password Reset Successfully', 'user_activity_log_pro');
                ualpAdminNoticeMessage('updated', $message);
            }
            else {
                $message = __('Error in Password Reset', 'user_activity_log_pro');
                ualpAdminNoticeMessage('error', $message);
            }
        }
        if (isset($currentPassword) && $currentPassword != "") {
            $subtitle = __('Change Password','user_activity_log_pro');
        }else{
            $subtitle = __('Add Password','user_activity_log_pro');
        }
        ?>
        <form class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>" method="POST" >
            <div class="padding_left_right_15">
                <h3 class="ualpHeaderTitle"><?php echo $subtitle; ?></h3>
                <table class="widefat" cellspacing="0">
                    <?php if (isset($currentPassword) && $currentPassword != "") { ?>
                        <tr>
                            <th><?php _e('Old Password', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="Password" name="oldPassword" value="<?php echo $oldPassword; ?>">
                                <p class="description"><?php _e('Enter Old Password', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr>
                        <tr>
                            <th><?php _e('New Password', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="Password" name="newPassword" value="<?php echo $newPassword; ?>">
                                <p class="description"><?php _e('Enter New Password', 'user_activity_log_pro'); ?></p>
                                <div aria-live="polite" id="change-pass-strength-result" style="" class=""></div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php _e('Repeat New Password', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="Password" name="repeatNewPassword" value="<?php echo $repeatNewPassword; ?>">
                                <p class="description"><?php _e('Repeat New Password', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr><?php } else {
                        ?>
                        <tr>
                            <th><?php _e('Password', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="Password" name="password" value="<?php echo $password; ?>">
                                <p class="description"><?php _e('Enter Password for delete logs', 'user_activity_log_pro'); ?></p>
                                <div aria-live="polite" id="pass-strength-result" style="" class=""></div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php _e('Repeat Password', 'user_activity_log_pro'); ?></th>
                            <td>
                                <input type="Password" name="repeatPassword" value="<?php echo $repeatPassword; ?>">
                                <p class="description"><?php _e('Repeat Password', 'user_activity_log_pro'); ?></p>
                            </td>
                        </tr><?php }
                    ?>
                </table>
                <input type="hidden" id="currentpwd" name="currentpwd" value="<?php echo $currentPassword; ?>">
                <input type="hidden" id="adminpwd" name="adminpwd" value="<?php echo $admin_pass; ?>">
                <?php
                wp_nonce_field('_wp_set_password_action', '_wp_set_password_nonce');
                ?>
                <p class="submit">
                    <input class="button button-primary" type="submit" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>" name="btnSetPassword">
                    <?php if(current_user_can('administrator') && isset($currentPassword) && $currentPassword != "") { ?>
                        <input class="button button-default reset_delete_log_passwd" type="submit" value="<?php esc_attr_e('Reset Password', 'user_activity_log_pro'); ?>" name="btnResetPassword">
                    <?php } ?>
                </p>
            </div>
        </form><?php
    }

}
/**
 * User activity custom event settings
 */
if (!function_exists('ualpCustomEventSettings')) {

    function ualpCustomEventSettings() {
        if (isset($_POST['btnAddCustomEvent']) && isset($_POST['_wp_custom_event_nonce']) && wp_verify_nonce($_POST['_wp_custom_event_nonce'], '_wp_custom_event_action')) {
            $custom_event_name = $custom_event_data = "";
            $i = $emptyCount = 0;
            if((isset($_POST['custom_event_name']) && $_POST['custom_event_name'] != "") ||
                (isset($_POST['custom_event_label']) && $_POST['custom_event_label'] != "") || 
                (isset($_POST['custom_event_type']) && $_POST['custom_event_type'] != "") || 
                (isset($_POST['custom_event_description']) && $_POST['custom_event_description'] != "")){
                $custom_event_name = $_POST['custom_event_name'];
                $custom_event_label = $_POST['custom_event_label'];
                $custom_event_type = $_POST['custom_event_type'];
                $custom_event_description = $_POST['custom_event_description'];
            }
            if(sizeof($custom_event_name) > 0 && $custom_event_name != ""){
                foreach ($custom_event_name as $custom_event_name_single) {
                    if ($custom_event_name_single != '') {
                        $custom_event_data[] = array(
                            'custom_event_name' => $custom_event_name_single,
                            'custom_event_label' => $custom_event_label[$i],
                            'custom_event_type' => $custom_event_type[$i],
                            'custom_event_description' => $custom_event_description[$i]
                        );
                        $i++;
                    }
                }
            }
            if(sizeof($custom_event_data) >= 1 && $custom_event_data != ""){
                foreach($custom_event_data as $custom_event_single_data){
                    $eventName = $custom_event_single_data['custom_event_name'];
                    $eventLabel = $custom_event_single_data['custom_event_label'];
                    $eventType = $custom_event_single_data['custom_event_type'];
                    $eventDesc = $custom_event_single_data['custom_event_description'];
                    if($eventName == "" || $eventLabel == "" || $eventType == "" || $eventDesc == "" ){
                        $emptyCount++;
                    }
                }
            }
            if($emptyCount == 0 && $custom_event_data != "" ){
                update_option('custom_event_data', $custom_event_data);
                $class = "updated";
                $message = __("Custom event saved successfully.", "user_activity_log_pro");
                do_action('ualp_custom_event_log_updated');
            } elseif ($emptyCount == 0 && sizeof($custom_event_name) > 0) {
                update_option('custom_event_data', $custom_event_data);
                $class = "updated";
                $message = __("Custom event deleted successfully.", "user_activity_log_pro");
                do_action('ualp_custom_event_log_updated');
            } else {
                update_option('custom_event_data', $custom_event_data);
                $class = "error";
                $message = __("Custom event not saved. Fill the empty field.", "user_activity_log_pro");
            }
            ualpAdminNoticeMessage($class, $message);
        }
        $custom_event_data = get_option('custom_event_data');
        ?>
        <script>
            /*add new and remove custom event functionality*/
            jQuery(document).ready(function () {
                jQuery('.ualp_add_new_event_btn').click(function () {
                    var new_row = '<tr>';
                    new_row += '<td><span><?php _e('Event Name(Hook Name): ', 'user_activity_log_pro'); ?></span><br/><input class="ualp_event_medium" type="text" name="custom_event_name[]" placeholder="<?php _e('Ex: wp_login', 'user_activity_log_pro'); ?>" /></td>';
                    new_row += '<td><span><?php _e('Event Label: ', 'user_activity_log_pro'); ?></span><br/><input class="ualp_event_medium" type="text" name="custom_event_label[]" placeholder="<?php _e('Ex: Login', 'user_activity_log_pro'); ?>" /></td>';
                    new_row += '<td><span><?php _e('Event Type: ', 'user_activity_log_pro'); ?></span><br/><input class="ualp_event_medium" type="text" name="custom_event_type[]" placeholder="<?php _e('Ex: User', 'user_activity_log_pro'); ?>" /></td>';
                    new_row += '<td><span><?php _e('Event Description: ', 'user_activity_log_pro'); ?></span><br/><input class="ualp_event_large" type="text" name="custom_event_description[]" placeholder="<?php _e('Ex: User Login from custom event', 'user_activity_log_pro'); ?>" /></td>';
                    new_row += '<td class="ulap_last_column"><span title="<?php _e('Remove', 'user_activity_log_pro'); ?>" class="ualp_remove_event_btn dashicons dashicons-trash"></span></td>';
                    new_row += '</tr>';
                    jQuery('.custom_event_table').append(new_row);
                });
                jQuery('.ualp_remove_event_btn').live('click', function () {
                    jQuery(this).closest('tr').remove();
                });
            });
        </script>
        <form class="ualpSettingsForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?" . $_SERVER['QUERY_STRING']); ?>" method="POST" >
            <div class="padding_left_right_15">
                <h3 class="ualpHeaderTitle"><?php _e('Add Custom Event', 'user_activity_log_pro'); ?></h3>
                <table class="widefat custom_event_table" cellspacing="0">
                    <?php
                    $j = 1;
                    if (is_array($custom_event_data) && !empty($custom_event_data)) {
                        foreach ($custom_event_data as $custom_event_data_single) {
                            ?>
                            <tr>
                                <td>
                                    <span><?php _e('Event Name(Hook Name): ', 'user_activity_log_pro'); ?></span><br/>
                                    <input class="ualp_event_medium" type="text" name="custom_event_name[]" placeholder="<?php _e('Ex: wp_login', 'user_activity_log_pro'); ?>" value="<?php echo $custom_event_data_single['custom_event_name']; ?>" />
                                </td>
                                <td>
                                    <span><?php _e('Event Label: ', 'user_activity_log_pro'); ?></span><br/>
                                    <input class="ualp_event_medium" type="text" name="custom_event_label[]" placeholder="<?php _e('Ex: Login', 'user_activity_log_pro'); ?>" value="<?php echo $custom_event_data_single['custom_event_label']; ?>" />
                                </td>
                                <td>
                                    <span><?php _e('Event Type: ', 'user_activity_log_pro'); ?></span><br/>
                                    <input class="ualp_event_medium" type="text" name="custom_event_type[]" placeholder="<?php _e('Ex: User', 'user_activity_log_pro'); ?>" value="<?php echo $custom_event_data_single['custom_event_type']; ?>" />
                                </td>
                                <td>
                                    <span><?php _e('Event Description: ', 'user_activity_log_pro'); ?></span><br/>
                                    <input class="ualp_event_large" type="text" name="custom_event_description[]" placeholder="<?php _e('Ex: User Login from custom event', 'user_activity_log_pro'); ?>" value="<?php echo $custom_event_data_single['custom_event_description']; ?>" />
                                </td>
                                <td class="ulap_last_column">
                                    <span title="<?php _e('Remove','user_activity_log_pro'); ?>" class="ualp_remove_event_btn dashicons dashicons-trash"></span>
                                </td>
                            </tr>
                            <?php
                            $j++;
                        }
                    } else {
                        ?>
                        <tr>
                            <td>
                                <span><?php _e('Event Name(Hook Name): ', 'user_activity_log_pro'); ?></span><br/>
                                <input class="ualp_event_medium" type="text" name="custom_event_name[]" placeholder="<?php _e('Ex: wp_login', 'user_activity_log_pro'); ?>" />
                            </td>
                            <td>
                                <span><?php _e('Event Label: ', 'user_activity_log_pro'); ?></span><br/>
                                <input class="ualp_event_medium" type="text" name="custom_event_label[]" placeholder="<?php _e('Ex: Login', 'user_activity_log_pro'); ?>" />
                            </td>
                            <td>
                                <span><?php _e('Event Type: ', 'user_activity_log_pro'); ?></span><br/>
                                <input class="ualp_event_medium" type="text" name="custom_event_type[]" placeholder="<?php _e('Ex: User', 'user_activity_log_pro'); ?>" />
                            </td>
                            <td>
                                <span><?php _e('Event Description: ', 'user_activity_log_pro'); ?></span><br/>
                                <input class="ualp_event_large" type="text" name="custom_event_description[]" placeholder="<?php _e('Ex: User Login from custom event', 'user_activity_log_pro'); ?>" />
                            </td>
                            <td class="ulap_last_column">
                                <span title="<?php _e('Remove','user_activity_log_pro'); ?>" class="ualp_remove_event_btn dashicons dashicons-trash"></span>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
                <div title="<?php _e('Add New','user_activity_log_pro'); ?>" class="ualp_add_new_event_btn dashicons dashicons-plus-alt"></div>
                <?php
                wp_nonce_field('_wp_custom_event_action', '_wp_custom_event_nonce');
                ?>
                <p class="submit">
                    <input class="button button-primary" type="submit" value="<?php esc_attr_e('Save Changes', 'user_activity_log_pro'); ?>" name="btnAddCustomEvent">
                </p>
            </div>
        </form><?php
    }

}