<?php
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

/*
 * contain all functions for detail log entry
 */

// add log in detail
if(!function_exists('ualpAddActivityLogDetail')) {
    function ualpAddActivityLogDetail($uactid,$update_info) {
        global $wpdb;
        ualpCreateDetailTable();
        if(is_array($update_info) && !empty($update_info)) {
            foreach($update_info as $update_info_key => $update_info_value) {
                $wpdb->insert(
                    UALP_DETAIL_TABLENAME,
                    array(
                        'uactid' => $uactid,
                        'uactd_key' => $update_info_key,
                        'uactd_value' => $update_info_value
                    )
                );
            }
        }
    }
}

// for user update
if(!function_exists('ualpAddActivityLogDetailUserUpdate')) {
    function ualpAddActivityLogDetailUserUpdate($uactid,$user_meta,$user,$update,$hook) {
        if($hook == 'insert_user_meta') {
            if(!$update)
                return $user_meta;
            if(empty($user) || !is_object($user)) {
                return $user_meta;
            }
            if(!function_exists("_get_additional_user_keys")) {
                return $user_meta;
            }
            $user_new_data = $_POST;
            $get_dafault_key_array = _get_additional_user_keys($user);
            $user_full_key_array = array_merge($get_dafault_key_array, array("user_email", "user_url", "display_name"));
            $user_new_data["user_url"] = isset( $user_new_data["url"] ) ? $user_new_data["url"] : null;
            $user_new_data["show_admin_bar_front"] = isset( $user_new_data["admin_bar_front"] ) ? true : null;
            $user_new_data["user_email"] = isset( $user_new_data["email"] ) ? $user_new_data["email"] : null;
            $user_new_data['comment_shortcuts'] = isset( $user_new_data['comment_shortcuts'] ) ? "true" : "false";
            $user_new_data['rich_editing'] = isset( $user_new_data['rich_editing'] ) ? "false" : "true";
            $user_new_data['show_admin_bar_front'] = isset( $user_new_data['admin_bar_front'] ) ? "true" : "false";
            $user_data_diff = array();
            $user_detail_ary = array();
            foreach($user_full_key_array as $user_full_key_single) {
                $old_user_meta = $user->$user_full_key_single;
                $new_user_meta = isset($user_new_data[$user_full_key_single]) ? $user_new_data[$user_full_key_single] : null;
                if(!isset($new_user_meta)) {
                    continue;
                }
                if ( $old_user_meta != $new_user_meta ) {
                    $user_data_diff[$user_full_key_single] = array(
                        "old" => $old_user_meta,
                        "new" => $new_user_meta
                    );
                }
            }
            //user meta
            if(is_array($user_data_diff) && !empty($user_data_diff)) {
                foreach ( $user_data_diff as $user_data_diff_key => $user_data_diff_value ) {
                    $user_detail_ary["ualp_user_old_{$user_data_diff_key}"] = $user_data_diff_value["old"];
                    $user_detail_ary["ualp_user_new_{$user_data_diff_key}"] = $user_data_diff_value["new"];
                }
            }
            //password
            if (!empty( $user_new_data['pass1'] ) && !empty( $user_new_data['pass2'] ) && $user_new_data['pass1'] == $user_new_data['pass2']) {
                $user_detail_ary["ualp_user_password_changed"] = "1";
            }
            //role
            $new_role = isset( $user_new_data["role"] ) ? $user_new_data["role"] : null;
            if ( $new_role ) {
                $user_roles = array_intersect( array_values( $user->roles ), array_keys( get_editable_roles() ) );
                $old_role  = reset( $user_roles );
                if($new_role != $old_role) {
                    $user_detail_ary["ualp_user_old_role"] = $old_role;
                    $user_detail_ary["ualp_user_new_role"] = $new_role;
                }
            }
            ualpAddActivityLogDetail($uactid,$user_detail_ary);
            return $user_meta;
        }
    }
}

//for post update
if(!function_exists('ualpAddActivityLogDetailPostUpdate')) {
    function ualpAddActivityLogDetailPostUpdate($uactid,$old_post_data,$new_post_data) {
        $get_dafault_key_array = array("post_title","post_name","post_content","post_status","menu_order","post_date","post_date_gmt","post_excerpt","comment_status","ping_status","post_parent","post_author");
        $post_full_key_array = apply_filters('ualp_post_primary_array',$get_dafault_key_array);
        $post_data_diff = array();
        $post_detail_ary = array();
        $old_post_content = '';
        foreach($post_full_key_array as $post_full_key_single) {
            if(is_array($old_post_data) && !empty($old_post_data)) {
                $old_post_content = $old_post_data['post_data']->$post_full_key_single;
            }
            $new_post_content = isset($new_post_data['post_data']->$post_full_key_single) ? $new_post_data['post_data']->$post_full_key_single : null;
            if(!isset($new_post_data)) {
                continue;
            }
            if ( $old_post_content != $new_post_content ) {
                $post_data_diff[$post_full_key_single] = array(
                    "old" => $old_post_content,
                    "new" => $new_post_content
                );
            }
        }
        //post deta
        if(is_array($post_data_diff) && !empty($post_data_diff)) {
            foreach ( $post_data_diff as $post_data_diff_key => $post_data_diff_value ) {
                $post_detail_ary["ualp_post_old_{$post_data_diff_key}"] = $post_data_diff_value["old"];
                $post_detail_ary["ualp_post_new_{$post_data_diff_key}"] = $post_data_diff_value["new"];
            }
        }
        ualpAddActivityLogDetail($uactid,$post_detail_ary);
    }
}

//for switch theme
if(!function_exists('ualpAddActivityLogDetailSwitchTheme')) {
    function ualpAddActivityLogDetailSwitchTheme($uactid,$old_theme,$new_theme) {
        $post_detail_ary['ualp_old_theme_name'] = $old_theme['name'];
        $post_detail_ary['ualp_old_theme_version'] = $old_theme['version'];
        $post_detail_ary['ualp_new_theme_name'] = $new_theme['name'];
        $post_detail_ary['ualp_new_theme_version'] = $new_theme['version'];
        ualpAddActivityLogDetail($uactid,$post_detail_ary);
    }
}

// for upgrade theme
if(!function_exists('ualpAddActivityLogDetailUpgradeTheme')) {
    function ualpAddActivityLogDetailUpgradeTheme($uactid,$new_theme) {
        $post_detail_ary['ualp_theme_name'] = $new_theme['name'];
        $post_detail_ary['ualp_theme_version'] = $new_theme['version'];
        ualpAddActivityLogDetail($uactid,$post_detail_ary);
    }
}

//for customize save
if(!function_exists('ualpHookCustomizeSaveDetail')) {
    function ualpHookCustomizeSaveDetail($uactid,$customizer) {
        $customizer->prepare_controls();
        $settings = $customizer->settings();
        $sections = $customizer->sections();
        $controls = $customizer->controls();
        $customized = json_decode(wp_unslash($_REQUEST["customized"]));
        $customize_detail_ary = array();
        foreach ($customized as $setting_id => $posted_values) {
            foreach ($settings as $one_setting) {
                if ($one_setting->id == $setting_id) {
                    $old_value = $one_setting->value();
                    $new_value = $one_setting->post_value();
                    if ($old_value != $new_value) {
                        $customize_detail_ary = array(
                            $one_setting->id."_setting_id" => $one_setting->id,
                            $one_setting->id."_setting_old_value" => $old_value,
                            $one_setting->id."_setting_new_value" => $new_value,
                        );
                        foreach ($controls as $one_control) {
                            foreach ($one_control->settings as $section_control_setting) {
                                if ($section_control_setting->id == $setting_id) {
                                    $customize_detail_ary[$one_control->id."control_id"] = $one_control->id;
                                    $customize_detail_ary[$one_setting->id."control_label"] = $one_control->label;
                                    $customize_detail_ary[$one_setting->id."control_type"] = $one_control->type;
                                }
                            }
                        }
                        ualpAddActivityLogDetail($uactid,$customize_detail_ary);
                    }
                }
            }
        }
    }
}

//for widget update
if(!function_exists('ualpHookWidgetUpdateDetail')) {
    function ualpHookWidgetUpdateDetail($uactid,$instance, $new_instance, $old_instance, $widget_instance){
        $widget_id_base = $widget_instance->id_base;
        $widget_detail_ary = array();
        $widget_detail_ary["ualp_widget_id_base"] = $widget_id_base;
        $widget = false;
        $widget_factory = isset($GLOBALS["wp_widget_factory"]) ? $GLOBALS["wp_widget_factory"] : false;
        if ($widget_factory) {
            foreach ($widget_factory->widgets as $one_widget) {
                if ($one_widget->id_base == $widget_id_base) {
                    $widget = $one_widget;
                }
            }
        }
        if ($widget) {
            $widget_detail_ary["ualp_widget_name_translated"] = $widget->name;
        }
        $sidebar_id = $_POST["sidebar"];
        $widget_detail_ary["ualp_sidebar_id"] = $sidebar_id;
        $sidebar = false;
        $sidebars = isset($GLOBALS['wp_registered_sidebars']) ? $GLOBALS['wp_registered_sidebars'] : false;
        if ($sidebars) {
            if (isset($sidebars[$sidebar_id])) {
                $sidebar = $sidebars[$sidebar_id];
            }
        }
        if ($sidebar) {
            $widget_detail_ary["ualp_sidebar_name_translated"] = $sidebar["name"];
        }
        $widget_detail_ary["ualp_old_instance"] = json_encode($old_instance);
        $widget_detail_ary["ualp_new_instance"] = json_encode($new_instance);
        ualpAddActivityLogDetail($uactid,$widget_detail_ary);
    }
}