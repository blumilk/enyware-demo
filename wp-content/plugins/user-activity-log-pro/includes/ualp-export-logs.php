<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/*
 * Export logs in csv format
 */
if(!function_exists('ualpExportUserLogCSV')){
    function ualpExportUserLogCSV($userrole,$username,$type,$ip,$time,$hook,$favorite,$search) {
        global $wpdb, $query;
        $Header = array();
        $where = " where 1 = 1";
        if(isset($userrole) && $userrole != ''){
            $where.=" AND user_role like '$userrole'";
        }
        if(isset($username) && $username != ''){
            $where.=" AND user_name like '$username'";
        }
        if(isset($type) && $type != ''){
            $where.=" AND object_type like '$type'";
        }
        if(isset($ip) && $ip != ''){
            $where.=" AND ip_address like '$ip'";
        }
        if(isset($hook) && $hook != ''){
            $where.=" AND hook like '$hook'";
        }
        if(isset($favorite) && $favorite != ''){
            $where.=" AND favorite like '$favorite'";
        }
        if(isset($time) && $time != ''){
            $current_time = current_time('timestamp');

            $start_time = mktime(0, 0, 0, date('m', $current_time), date('d', $current_time), date('Y', $current_time));
            $end_time = mktime(23, 59, 59, date('m', $current_time), date('d', $current_time), date('Y', $current_time));

            if($get_time == 'today'){
                $start_time = $current_time;
                $end_time = strtotime('+1 day', $current_time);
            }elseif ($time === 'yesterday') {
                $start_time = strtotime('yesterday', $start_time);
                $end_time = $current_time;
            } elseif ($time === 'week') {
                $start_time = strtotime('-1 week', $start_time);
                $end_time = strtotime('+1 day', $current_time);
            } elseif ($time === 'month') {
                $start_time = strtotime('-1 month', $start_time);
                $end_time = strtotime('+1 day', $current_time);
            }
            $start_time = date('Y-m-d',$start_time);
            $end_time = date('Y-m-d',$end_time);
            $where .= " AND modified_date > '$start_time' AND modified_date < '$end_time'";
        }
        if (isset($search) && $search != "") {
            $where.=" AND user_name like '$search' or user_role like '$search' or object_type like '$search' or action like '$search'";
        }

        $getLogQuery = "SELECT uactid,favorite,user_name,user_role,user_email,ip_address,modified_date,object_type,hook,description FROM ".UALP_TABLENAME." $where";
        $resultLogQuery = $wpdb->get_results($getLogQuery);
        $result = $wpdb->get_col($getLogQuery);
        $fieldsCount = count($result);

        if (!empty($resultLogQuery)) {
            foreach ($resultLogQuery as $resultLog) {
                foreach ($resultLog as $resultLogKey => $resultLogVal) {
                    if (!in_array($resultLogKey, $Header)) {
                        $Header[] = $resultLogKey;
                    }
                }
            }
        }
        $c = 0;
        $CSVContent = array();
        $date_format = get_option('date_format');
        $time_format = get_option('time_format');
        foreach ($Header as $header_key) {
            if (is_array($resultLogQuery)) {
                foreach ($resultLogQuery as $key => $val) {
                    $c++;
                    if (array_key_exists($header_key, $val)) {
                        if ($header_key == 'uactid') {
                            $CSVContent[$key][$header_key] = $c;
                        } elseif ($header_key == 'favorite') {
                            if($resultLogQuery[$key]->$header_key == 1){
                                $CSVContent[$key][$header_key] = "Yes";
                            }
                            if($resultLogQuery[$key]->$header_key == 0){
                                $CSVContent[$key][$header_key] = "No";
                            }
                        } elseif ($header_key == 'modified_date') {
                            $get_date = strtotime($resultLogQuery[$key]->$header_key);
                            $date = date($date_format, $get_date);
                            $time = date($time_format, $get_date);
                            $CSVContent[$key][$header_key] = $date . " " . $time;
                        }
                        else {
                            $CSVContent[$key][$header_key] = html_entity_decode($resultLogQuery[$key]->$header_key);
                        }
                    } else {
                        $CSVContent[$key][$header_key] = null;
                    }
                }
            }
        }
        for($h=0;$h<sizeof($Header);$h++){
            if($Header[$h] == 'uactid'){
                $Header[$h] = __('Sr No.', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_name'){
                $Header[$h] = __('Author name', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_role'){
                $Header[$h] = __('Author role', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_email'){
                $Header[$h] = __('Author email', 'user_activity_log_pro');
            }
            if($Header[$h] == 'ip_address'){
                $Header[$h] = __('IP address', 'user_activity_log_pro');
            }
            if($Header[$h] == 'modified_date'){
                $Header[$h] = __('Date', 'user_activity_log_pro');
            }
            if($Header[$h] == 'object_type'){
                $Header[$h] = __('Activity type', 'user_activity_log_pro');
            }
            if($Header[$h] == 'hook'){
                $Header[$h] = __('Activity Hook', 'user_activity_log_pro');
            }
            if($Header[$h] == 'description'){
                $Header[$h] = __('Description', 'user_activity_log_pro');
            }
            if($Header[$h] == 'favorite'){
                $Header[$h] = __('Favorite ?', 'user_activity_log_pro');
            }
        }
        $export_delimiter = ',';
        $random_id = rand();
        $csv_file_name = __('export_user_logs', 'user_activity_log_pro') . "_" . date("Y-m-d_H-i-s", time()) . ".csv";
        ualpCSVOutput($csv_file_name, $CSVContent, $Header, $export_delimiter);
        do_action('ualp_export_activity_log');
    }
}

if(!function_exists('ualpCSVOutput')){
    function ualpCSVOutput($filename = null, $data = array(), $fields = array(), $delimiter = null) {
        if ($delimiter === null) {
            $delimiter = ',';
        }
        $random_id = rand();
        $filename = __('export_user_logs', 'user_activity_log_pro') . "_" . date("Y-m-d_H-i-s", time()) . ".csv";
        if ($filename) {
            $data = ualpUnparse($data, $fields, null, null, $delimiter);
            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            echo $data;
        }
        return $data;
    }
}

if(!function_exists('ualpUnparse')){
    function ualpUnparse($data = array(), $fields = array(), $append = false, $is_php = false, $delimiter = null) {
        $linefeed = "\r\n";
        $string = ($is_php) ? "<?php header('Status: 403'); die(' '); ?>" . $linefeed : '';
        $entry = array();
        // create heading
        if (!empty($fields)) {
            foreach ($fields as $key => $value) {
                $entry[] = ualpEncloseValue($value);
            }
            $string .= implode($delimiter, $entry) . $linefeed;
            $entry = array();
        }
        // create data
        foreach ($data as $key => $row) {
            foreach ($row as $field => $value) {
                $entry[] = ualpEncloseValue($value);
            }
            $string .= implode($delimiter, $entry) . $linefeed;
            $entry = array();
        }
        return $string;
    }
}

if(!function_exists('ualpEncloseValue')){
    function ualpEncloseValue($value = null) {
        $this_delimiter = ',';
        $this_enclosure = '"';
        if ($value !== null && $value != '') {
            $delimiter = preg_quote($this_delimiter, '/');
            $enclosure = preg_quote($this_enclosure, '/');
            if ($value[0] == '=')
                $value = "'" . $value;
            if (preg_match("/" . $delimiter . "|" . $enclosure . "|\n|\r/i", $value) || ($value{0} == ' ' || substr($value, -1) == ' ')) {
                $value = str_replace($this_enclosure, $this_enclosure . $this_enclosure, $value);
                $value = $this_enclosure . $value . $this_enclosure;
            }
        }
        return $value;
    }
}

if (is_admin()) {
    if (isset($_GET['export']) && $_GET['export'] == 'user_logs' && isset($_GET['export-nonce']) && $_GET['export-nonce'] != "") {
        $userrole = $username = $type = $ip = $time = $search = "";
        if(isset($_GET['userrole'])){
            $userrole = $_GET['userrole'];
        }
        if(isset($_GET['username'])){
            $username = $_GET['username'];
        }
        if(isset($_GET['type'])){
            $type = $_GET['type'];
        }
        if(isset($_GET['ip'])){
            $ip = $_GET['ip'];
        }
        if(isset($_GET['time'])){
            $time = $_GET['time'];
        }
        if(isset($_GET['hook'])){
            $hook = $_GET['hook'];
        }
        if(isset($_GET['favorite'])){
            $favorite = $_GET['favorite'];
        }
        if(isset($_GET['txtsearch'])){
            $search = $_GET['txtsearch'];
        }
        ualpExportUserLogCSV($userrole,$username,$type,$ip,$time,$hook,$favorite,$search);
        exit();
    }
}


/*
 * Send CSv file in email
 */
if(!function_exists('ualpSendCsvInEmail')){
    function ualpSendCsvInEmail(){
        $current_user = wp_get_current_user();
        $f_name = $current_user->user_firstname;
        $l_name = $current_user->user_lastname;
        $to = get_option('ualpToEmail');
        $from = get_option('ualpFromEmail');
        $headers = "MIME-Version: 1.0;\r\n";
        $headers .= "From: " . strip_tags($from) . "\r\n";
        $headers .= "Content-Type: text/html; charset: utf-8;\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
        $subject = __('Users Activity Log file - User Activity Log Pro', 'user_activity_log_pro');
        $attachment = ualpGenerateCsvAttachment();
        $body = '';
        ob_start();
        ?>
        <div style="background: #F5F5F5; border-width: 1px; border-style: solid; padding-bottom: 20px; margin: 0px auto; width: 750px; height: auto; border-radius: 3px 3px 3px 3px; border-color: #5C5C5C;">
            <div style="border: #FFF 1px solid; background-color: #ffffff !important; margin: 20px 20px 0;height: auto; -moz-border-radius: 3px; padding-top: 15px;">
                <div style="padding: 20px 20px 20px 20px; font-family: Arial, Helvetica, sans-serif;height: auto; color: #333333; font-size: 13px;">
                    <div style="width: 100%;">
                        <strong><?php _e('Dear', 'user_activity_log_pro'); ?> <?php echo $f_name; ?> </strong>,<br /><br />
                        <?php _e('Here is the attachment of users activity log file of your', 'user_activity_log_pro'); ?> <a href="<?php echo home_url(); ?>"><?php _e('website', 'user_activity_log_pro'); ?></a>.<br /><br />
                    </div>
                </div>
            </div>
        </div>
        <?php
        $body = ob_get_clean();
        $sent = wp_mail($to, $subject, $body, $headers, $attachment);
        return $sent;
    }
}

if(!function_exists('ualpGenerateCsvAttachment')){
    function ualpGenerateCsvAttachment() {
        global $wpdb, $query;
        $c = 0;
        $CSVContent = array();
        $Header = array();
        $date_format = get_option('date_format');
        $time_format = get_option('time_format');

        $csvFilePath = WP_CONTENT_DIR . __('/uploads/export_user_logs', 'user_activity_log_pro') . "_" . date("Y-m-d_H-i-s", time()) . ".csv";
        $ualpFileHandler = fopen($csvFilePath, 'w');
        $getLogQuery = "SELECT uactid,favorite,user_name,user_role,user_email,ip_address,modified_date,object_type,hook,description FROM ".UALP_TABLENAME;
        $resultLogQuery = $wpdb->get_results($getLogQuery);
        $result = $wpdb->get_col($getLogQuery);

        if ($ualpFileHandler === FALSE) {
            wp_die( __( 'Failed to open export file', 'user_activity_log_pro' ) );
        }
        if (!empty($resultLogQuery)) {
            foreach ($resultLogQuery as $resultLog) {
                foreach ($resultLog as $resultLogKey => $resultLogVal) {
                    if (!in_array($resultLogKey, $Header)) {
                        $Header[] = $resultLogKey;
                    }
                }
            }
        }
        foreach ($Header as $header_key) {
            if (is_array($resultLogQuery)) {
                foreach ($resultLogQuery as $key => $val) {
                    $c++;
                    if (array_key_exists($header_key, $val)) {
                        if ($header_key == 'uactid') {
                            $CSVContent[$key][$header_key] = $c;
                        } elseif ($header_key == 'favorite') {
                            if($resultLogQuery[$key]->$header_key == 1){
                                $CSVContent[$key][$header_key] = "Yes";
                            }
                            if($resultLogQuery[$key]->$header_key == 0){
                                $CSVContent[$key][$header_key] = "No";
                            }
                        } elseif ($header_key == 'modified_date') {
                            $get_date = strtotime($resultLogQuery[$key]->$header_key);
                            $date = date($date_format, $get_date);
                            $time = date($time_format, $get_date);
                            $CSVContent[$key][$header_key] = $date . " " . $time;
                        } else {
                            $CSVContent[$key][$header_key] = html_entity_decode($resultLogQuery[$key]->$header_key);
                        }
                    } else {
                        $CSVContent[$key][$header_key] = null;
                    }
                }
            }
        }
        for($h=0;$h<sizeof($Header);$h++){
            if($Header[$h] == 'uactid'){
                $Header[$h] = __('Sr No.', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_name'){
                $Header[$h] = __('Author name', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_role'){
                $Header[$h] = __('Author role', 'user_activity_log_pro');
            }
            if($Header[$h] == 'user_email'){
                $Header[$h] = __('Author email', 'user_activity_log_pro');
            }
            if($Header[$h] == 'ip_address'){
                $Header[$h] = __('IP address', 'user_activity_log_pro');
            }
            if($Header[$h] == 'modified_date'){
                $Header[$h] = __('Date', 'user_activity_log_pro');
            }
            if($Header[$h] == 'object_type'){
                $Header[$h] = __('Activity type', 'user_activity_log_pro');
            }
            if($Header[$h] == 'hook'){
                $Header[$h] = __('Activity Hook', 'user_activity_log_pro');
            }
            if($Header[$h] == 'description'){
                $Header[$h] = __('Description', 'user_activity_log_pro');
            }
            if($Header[$h] == 'favorite'){
                $Header[$h] = __('Favorite ?', 'user_activity_log_pro');
            }
        }
        fputcsv($ualpFileHandler, $Header);
        foreach ($CSVContent as $record) {
            fputcsv($ualpFileHandler, $record);
        }
        rewind($ualpFileHandler);
        fclose($ualpFileHandler);
        return $csvFilePath;
    }
}