<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/*
 * Fires when import data from other SEO plugins
 */
if (!function_exists('ualpHook_wpseo_handle_import')){

    function ualpHook_wpseo_handle_import() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Import data from other SEO plugins";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Import from other SEO plugins";
            $hook = "wpseo_handle_import";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wpseo_handle_import', 'ualpHook_wpseo_handle_import'); 

/*
 * Fires when Opengraph output added
 */
if (!function_exists('ualpHook_wpseo_opengraph')){

    function ualpHook_wpseo_opengraph() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Opengraph";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Opengraph output added";
            $hook = "wpseo_opengraph";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wpseo_opengraph', 'ualpHook_wpseo_opengraph'); 

/*
 *  Fires when Twitter output added
 */
if (!function_exists('ualpHook_wpseo_twitter')){

    function ualpHook_wpseo_twitter() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Twitter";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Twitter output added";
            $hook = "wpseo_twitter";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wpseo_twitter', 'ualpHook_wpseo_twitter'); 

/*
 * Fires when save meta boxes
 */
if (!function_exists('ualpHookwpseo_save_metaboxes')){

    function ualpHookwpseo_save_metaboxes() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Save meta boxes";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save meta boxes";
            $hook = "wpseo_save_metaboxes";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wpseo_save_metaboxes', 'ualpHookwpseo_save_metaboxes'); 

/*
 * Fires when save title
 */
if (!function_exists('ualpHook_wp_ajax_wpseo_save_title')){

    function ualpHook_wp_ajax_wpseo_save_title() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Save title";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save title";
            $hook = "wpseo_save_title";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_wpseo_save_title', 'ualpHook_wp_ajax_wpseo_save_title'); 

/*
 * Fires when save meta description
 */
if (!function_exists('ualpHook_wp_ajax_wpseo_save_metadesc')){

    function ualpHook_wp_ajax_wpseo_save_metadesc() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Save meta description";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save meta description";
            $hook = "wpseo_save_metadesc";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_wpseo_save_metadesc', 'ualpHook_wp_ajax_wpseo_save_metadesc'); 

/*
 * Fires when save all titles
 */
if (!function_exists('ualpHook_wp_ajax_wpseo_save_all_titles')){

    function ualpHook_wp_ajax_wpseo_save_all_titles() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Save all titles";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save all titles";
            $hook = "wpseo_save_all_titles";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_wpseo_save_all_titles', 'ualpHook_wp_ajax_wpseo_save_all_titles'); 

/*
 * Fires when save All descriptions
 */
if (!function_exists('ualpHook_wp_ajax_wpseo_save_all_descriptions')){

    function ualpHook_wp_ajax_wpseo_save_all_descriptions() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "Save All descriptions";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save All descriptions";
            $hook = "wpseo_save_all_descriptions";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_wpseo_save_all_descriptions', 'ualpHook_wp_ajax_wpseo_save_all_descriptions'); 

/*
 * Fires when save authentication code
 */
if (!function_exists('ualpHook_wpseo_save_auth_code')){

    function ualpHook_wpseo_save_auth_code() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "save_auth_code";
            $obj_type = "Yoast SEO";
            $post_id = "";
            $post_title = "Save authentication code";
            $hook = "wpseo_save_auth_code";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}

add_action('wp_ajax_wpseo_save_auth_code', 'ualpHook_wpseo_save_auth_code'); 

/*
 * Fires when Robots.txt update, htaccess file update, Import file in wpseo
 */
if (!function_exists('ualpHookcheck_admin_referer')){

    function ualpHookcheck_admin_referer($action, $result) {

        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            if($action == 'wpseo-robotstxt' || $action == 'wpseo-htaccess' || $action == 'wpseo-import-file'  || $action == 'wpseo_export'){
                if($action == 'wpseo-robotstxt'){
                    $post_title = "Robots.txt file updated";
                }
                if($action == 'wpseo-htaccess'){
                    $post_title = ".htaccess file updated";
                }
                if($action == 'wpseo-import-file'){
                    $post_title = "Import wpseo settings";
                }
                if($action == 'wpseo_export'){
                    $post_title = "Export wpseo settings";
                }
                $obj_type = "Yoast SEO";
                $post_id = "";
                $hook = "check_admin_referer";
                $description = "$obj_type : $post_title";
                ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
        }
    }
}

add_action('check_admin_referer', 'ualpHookcheck_admin_referer', 10, 2); 

/*
 * Fires when wordpress seo settings updated
 */
if(!function_exists('ualpHookWpSEOUpdatedOption')){
    function ualpHookWpSEOUpdatedOption($option, $oldvalue, $_newvalue){
        $whitelist_options = array(
            'wpseo',
            'wpseo_permalinks',
            'wpseo_titles',
            'wpseo_social',
            'wpseo_rss',
            'wpseo_internallinks',
            'wpseo_xml'
        );

        if (!in_array($option, $whitelist_options))
                return;

        $logHookList = get_option('enableLogHookList');
        if (in_array('wordpress-seo',$logHookList)) {
            $action = "updated";
            $obj_type = "Settings";
            $post_id = "";
            $post_title = $option;
            $hook = "updated_option";
            $description = "$obj_type $action : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }
}
add_action('updated_option', 'ualpHookWpSEOUpdatedOption' , 10, 3);