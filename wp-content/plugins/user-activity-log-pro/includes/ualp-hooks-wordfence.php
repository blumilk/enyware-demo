<?php
/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/*
 * Fires when Issue Status Updated
 */
if (!function_exists('ualpHookUpdateIssueStatus')) {

    function ualpHookUpdateIssueStatus() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Issue Status";
            $hook = "wordfence_updateIssueStatus";
            $description = "$obj_type : $post_title $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_updateIssueStatus', 'ualpHookUpdateIssueStatus'); 
/*
 * Fires when WAF Config Updated
 */
if (!function_exists('ualpHookSaveWAFConfig')) {

    function ualpHookSaveWAFConfig() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "WAF Config";
            $hook = "wordfence_saveWAFConfig";
            $description = "$obj_type : $post_title $action";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveWAFConfig', 'ualpHookSaveWAFConfig'); 

/*
 * Fires when optimize WAF Firewall
 */
if (!function_exists('ualpHookOptimizeWAFFirewall')) {

    function ualpHookOptimizeWAFFirewall() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Optimize";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Firewall";
            $hook = "wordfence_wafConfigureAutoPrepend";
            $description = "$action : $obj_type $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_wafConfigureAutoPrepend', 'ualpHookOptimizeWAFFirewall'); 

/*
 * Fires when Bulk Enable whitelist
 */
if (!function_exists('ualpHookWhitelistBulkEnable')) {

    function ualpHookWhitelistBulkEnable() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "WordFence";
            $post_id = "";
            $post_title = "Bulk Enable whitelist";
            $hook = "wordfence_whitelistBulkEnable";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_whitelistBulkEnable', 'ualpHookWhitelistBulkEnable'); 
/*
 * Fires when Bulk Disable Whitelist
 */
if (!function_exists('ualpHookWhitelistBulkDisable')) {

    function ualpHookWhitelistBulkDisable() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Bulk Disable Whitelist";
            $hook = "wordfence_whitelistBulkDisable";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_whitelistBulkDisable', 'ualpHookWhitelistBulkDisable'); 
/*
 * Fires when Bulk Deleted Whitelist
 */
if (!function_exists('ualpHookWhitelistBulkDelete')) {

    function ualpHookWhitelistBulkDelete() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Bulk Deleted Whitelist";
            $hook = "wordfence_whitelistBulkDelete";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_whitelistBulkDelete', 'ualpHookWhitelistBulkDelete'); 
/*
 * Fires when Wordfence config updated
 */
if (!function_exists('ualpHookUpdateWordFenceConfig')) {

    function ualpHookUpdateWordFenceConfig() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "Updated";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Wordfence config updated";
            $hook = "wordfence_updateConfig";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_updateConfig', 'ualpHookUpdateWordFenceConfig'); 
/*
 * Fires when Load Live Traffic
 */
if (!function_exists('ualpHookLoadLiveTraffic')) {

    function ualpHookLoadLiveTraffic() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Load Live Traffic";
            $hook = "wordfence_loadLiveTraffic";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_loadLiveTraffic', 'ualpHookLoadLiveTraffic'); 
/*
 * Fires when Load Cache Exclusions
 */
if (!function_exists('ualpHookLoadCacheExclusions')) {

    function ualpHookLoadCacheExclusions() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Load Cache Exclusions";
            $hook = "wordfence_loadCacheExclusions";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_loadCacheExclusions', 'ualpHookLoadCacheExclusions'); 
/*
 * Fires when Save Cache Config
 */
if (!function_exists('ualpHookSaveCacheConfig')) {

    function ualpHookSaveCacheConfig() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Cache Config";
            $hook = "wordfence_saveCacheConfig";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveCacheConfig', 'ualpHookSaveCacheConfig'); 
/*
 * Fires when Clear cache
 */
if (!function_exists('ualpHookWordfenceCacheClear')) {

    function ualpHookWordfenceCacheClear() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Clear cache";
            $hook = "wordfence_cache_clear";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_cache_clear', 'ualpHookWordfenceCacheClear'); 
/*
 * Fires when Reverse Look up
 */
if (!function_exists('ualpHookReverseLookup')) {

    function ualpHookReverseLookup() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Reverse Look up";
            $hook = "wordfence_reverseLookup";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_reverseLookup', 'ualpHookReverseLookup'); 
/*
 * Fires when WHOIS Lookup
 */
if (!function_exists('ualpHookWhois')) {

    function ualpHookWhois() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "WHOIS Lookup";
            $hook = "wordfence_whois";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_whois', 'ualpHookWhois'); 
/*
 * Fires when Clear Page Cache
 */
if (!function_exists('ualpHookClearPageCache')) {

    function ualpHookClearPageCache() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Clear Page Cache";
            $hook = "wordfence_clearPageCache";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_clearPageCache', 'ualpHookClearPageCache'); 
/*
 * Fires when Save Configuration
 */
if (!function_exists('ualpHookSaveConfig')) {

    function ualpHookSaveConfig() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Configuration";
            $hook = "wordfence_saveConfig";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveConfig', 'ualpHookSaveConfig'); 
/*
 * Fires when add IP for Block
 */
if (!function_exists('ualpHookBlockIP')) {

    function ualpHookBlockIP() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "wordfence";
            $post_id = "";
            $post_title = "Block IP";
            $hook = "wordfence_blockIP";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_blockIP', 'ualpHookBlockIP'); 
/*
 * Fires when Enable Cellphone Sign in
 */
if (!function_exists('ualpHookAddTwoFactor')) {

    function ualpHookAddTwoFactor() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Add Two Factor - Enable Cellphone Sign in";
            $hook = "wordfence_addTwoFactor";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_addTwoFactor', 'ualpHookAddTwoFactor'); 
/*
 * Fires when Export Settings
 */
if (!function_exists('ualpHookExportSettings')) {

    function ualpHookExportSettings() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Export Settings";
            $hook = "wordfence_exportSettings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_exportSettings', 'ualpHookExportSettings'); 
/*
 * Fires when Clear All Blocked IP
 */
if (!function_exists('ualpHookClearAllBlocked')) {

    function ualpHookClearAllBlocked() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Clear All Blocked";
            $hook = "wordfence_clearAllBlocked";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_clearAllBlocked', 'ualpHookClearAllBlocked'); 
/*
 * Fires when set Block IP UA Range
 */
if (!function_exists('ualpHookBlockIPUARange')) {

    function ualpHookBlockIPUARange() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Block IP UA Range";
            $hook = "wordfence_blockIPUARange";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_blockIPUARange', 'ualpHookBlockIPUARange'); 
/*
 * Fires when Unblock IP Range
 */
if (!function_exists('ualpHookUnblockRange')) {

    function ualpHookUnblockRange() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Unblock Range";
            $hook = "wordfence_unblockRange";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_unblockRange', 'ualpHookUnblockRange'); 
/*
 * Fires when save country blocking
 */
if (!function_exists('ualpHookSaveCountryBlocking')) {

    function ualpHookSaveCountryBlocking() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Country Blocking";
            $hook = "wordfence_saveCountryBlocking";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveCountryBlocking', 'ualpHookSaveCountryBlocking'); 
/*
 * Fires when Save Debugging Config
 */
if (!function_exists('ualpHookSaveDebuggingConfig')) {

    function ualpHookSaveDebuggingConfig() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Debugging Config";
            $hook = "wordfence_saveDebuggingConfig";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveDebuggingConfig', 'ualpHookSaveDebuggingConfig'); 
/*
 * Fires when Start Password Audit
 */
if (!function_exists('ualpHookStartPasswdAudit')) {

    function ualpHookStartPasswdAudit() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Start Password Audit";
            $hook = "wordfence_startPasswdAudit";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_startPasswdAudit', 'ualpHookStartPasswdAudit'); 
/*
 * Fires when save scan schedule
 */
if (!function_exists('ualpHookSaveScanSchedule')) {

    function ualpHookSaveScanSchedule() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Scan Schedule";
            $hook = "wordfence_saveScanSchedule";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveScanSchedule', 'ualpHookSaveScanSchedule'); 
/*
 * Fires when WAF Rules update
 */
if (!function_exists('ualpHookUpdateWAFRules')) {

    function ualpHookUpdateWAFRules() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "update WAF Rules";
            $hook = "wordfence_updateWAFRules";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_updateWAFRules', 'ualpHookUpdateWAFRules'); 
/*
 * Fires when Send Test Email
 */
if (!function_exists('ualpHookSendTestEmail')) {

    function ualpHookSendTestEmail() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Send Test Email";
            $hook = "wordfence_sendTestEmail";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_sendTestEmail', 'ualpHookSendTestEmail'); 
/*
 * Fires when Send Diagnostic
 */
if (!function_exists('ualpHookSendDiagnostic')) {

    function ualpHookSendDiagnostic() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Send Diagnostic";
            $hook = "wordfence_sendDiagnostic";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
            }
    }

}
add_action('wp_ajax_wordfence_sendDiagnostic', 'ualpHookSendDiagnostic'); 
/*
 * Fires when Update All Issues
 */
if (!function_exists('ualpHookUpdateAllIssues')) {

    function ualpHookUpdateAllIssues() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Update All Issues";
            $hook = "wordfence_updateAllIssues";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_updateAllIssues', 'ualpHookUpdateAllIssues'); 
/*
 * Fires when Send Activity Log
 */
if (!function_exists('ualpHookSendActivityLog')) {

    function ualpHookSendActivityLog() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Send Activity Log";
            $hook = "wordfence_sendActivityLog";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_sendActivityLog', 'ualpHookSendActivityLog'); 
/*
 * Fires when Add Cache Exclusion
 */
if (!function_exists('ualpHookAddCacheExclusion')) {

    function ualpHookAddCacheExclusion() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Add Cache Exclusion";
            $hook = "wordfence_addCacheExclusion";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_addCacheExclusion', 'ualpHookAddCacheExclusion'); 
/*
 * Fires when Daily Auto Update
 */
if (!function_exists('ualpHookDailyAutoUpdate')) {

    function ualpHookDailyAutoUpdate() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Daily Auto Update";
            $hook = "wordfence_daily_autoUpdate";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_daily_autoUpdate', 'ualpHookDailyAutoUpdate'); 
/*
 * Fires when Update blocked IPs
 */
if (!function_exists('ualpHookUpdateBlockedIPs')) {

    function ualpHookUpdateBlockedIPs() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Update blocked IPs";
            $hook = "wordfence_update_blocked_IPs";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_update_blocked_IPs', 'ualpHookUpdateBlockedIPs'); 
/*
 * Fires when Start scheduled scan
 */
if (!function_exists('ualpHookStartScheduledScan')) {

    function ualpHookStartScheduledScan() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Start scheduled scan";
            $hook = "wordfence_start_scheduled_scan";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_start_scheduled_scan', 'ualpHookStartScheduledScan'); 
/*
 * Fires when Do Scan
 */
if (!function_exists('ualpHookWordfenceDoScan')) {

    function ualpHookWordfenceDoScan() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Do Scan";
            $hook = "wordfence_doScan";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_doScan', 'ualpHookWordfenceDoScan'); 
/*
 * Fires when do Daily cron
 */
if (!function_exists('ualpHookDailyCron')) {

    function ualpHookDailyCron() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Daily cron";
            $hook = "wordfence_daily_cron";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_daily_cron', 'ualpHookDailyCron'); 
/*
 * Fires when do Hourly cron
 */
if (!function_exists('ualpHookWordfenceHourlyCron')) {

    function ualpHookWordfenceHourlyCron() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Hourly cron";
            $hook = "wordfence_hourly_cron";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_hourly_cron', 'ualpHookWordfenceHourlyCron'); 
/*
 * Fires when Email activity report
 */
if (!function_exists('ualpHookEmailActivityReport')) {

    function ualpHookEmailActivityReport() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Email activity report";
            $hook = "wordfence_email_activity_report";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wordfence_email_activity_report', 'ualpHookEmailActivityReport'); 
/*
 * Fires when start Scan
 */
if (!function_exists('ualpHookWordfenceScan')) {

    function ualpHookWordfenceScan() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "wordfence";
            $post_id = "";
            $post_title = "Scan";
            $hook = "wordfence_scan";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_scan', 'ualpHookWordfenceScan'); 
/*
 * Fires when Update Alert Email
 */
if (!function_exists('ualpHookUpdateAlertEmail')) {

    function ualpHookUpdateAlertEmail() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Update Alert Email";
            $hook = "wordfence_updateAlertEmail";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_updateAlertEmail', 'ualpHookUpdateAlertEmail'); 
/*
 * Fires when Restore File
 */
if (!function_exists('ualpHookRestoreFile')) {

    function ualpHookRestoreFile() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Restore File";
            $hook = "wordfence_restoreFile";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_restoreFile', 'ualpHookRestoreFile'); 
/*
 * Fires when Delete Password Audit
 */
if (!function_exists('ualpHookDeletePasswdAudit')) {

    function ualpHookDeletePasswdAudit() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Delete Password Audit";
            $hook = "wordfence_deletePasswdAudit";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_deletePasswdAudit', 'ualpHookDeletePasswdAudit'); 
/*
 * Fires when Fix weak passwords
 */
if (!function_exists('ualpHookWeakPasswordsFix')) {

    function ualpHookWeakPasswordsFix() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "wordfence";
            $post_id = "";
            $post_title = "Fix weak passwords";
            $hook = "wordfence_weakPasswordsFix";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_weakPasswordsFix', 'ualpHookWeakPasswordsFix'); 
/*
 * Fires when Kill Password Audit
 */
if (!function_exists('ualpHookKillPasswdAudit')) {

    function ualpHookKillPasswdAudit() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Kill Password Audit";
            $hook = "wordfence_killPasswdAudit";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_killPasswdAudit', 'ualpHookKillPasswdAudit'); 
/*
 * Fires when Import Settings
 */
if (!function_exists('ualpHookImportSettings')) {

    function ualpHookImportSettings() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Import Settings";
            $hook = "wordfence_importSettings";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_importSettings', 'ualpHookImportSettings'); 
/*
 * Fires when Bulk Operation
 */
if (!function_exists('ualpHookBulkOperation')) {

    function ualpHookBulkOperation() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "wordfence";
            $post_id = "";
            $post_title = "Bulk Operation";
            $hook = "wordfence_bulkOperation";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_bulkOperation', 'ualpHookBulkOperation'); 
/*
 * Fires when Delete File
 */
if (!function_exists('ualpHookDeleteFile')) {

    function ualpHookDeleteFile() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Delete File";
            $hook = "wordfence_deleteFile";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_deleteFile', 'ualpHookDeleteFile'); 
/*
 * Fires when Delete Database Option
 */
if (!function_exists('ualpHookDeleteDatabaseOption')) {

    function ualpHookDeleteDatabaseOption() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "wordfence";
            $post_id = "";
            $post_title = "Delete Database Option";
            $hook = "wordfence_deleteDatabaseOption";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_deleteDatabaseOption', 'ualpHookDeleteDatabaseOption'); 
/*
 * Fires when Remove Exclusion
 */
if (!function_exists('ualpHookRemoveExclusion')) {

    function ualpHookRemoveExclusion() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Remove Exclusion";
            $hook = "wordfence_removeExclusion";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_removeExclusion', 'ualpHookRemoveExclusion'); 
/*
 * Fires when Delete Issue
 */
if (!function_exists('ualpHookDeleteIssue')) {

    function ualpHookDeleteIssue() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Delete Issue";
            $hook = "wordfence_deleteIssue";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_deleteIssue', 'ualpHookDeleteIssue'); 
/*
 * Fires when Unlock Out IP
 */
if (!function_exists('ualpHookUnlockOutIP')) {

    function ualpHookUnlockOutIP() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Unlock Out IP";
            $hook = "wordfence_unlockOutIP";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_unlockOutIP', 'ualpHookUnlockOutIP'); 
/*
 * Fires when Load Block Ranges
 */
if (!function_exists('ualpHookLoadBlockRanges')) {

    function ualpHookLoadBlockRanges() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Load Block Ranges";
            $hook = "wordfence_loadBlockRanges";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_loadBlockRanges', 'ualpHookLoadBlockRanges'); 
/*
 * Fires when Unblock IP
 */
if (!function_exists('ualpHookUnblockIP')) {

    function ualpHookUnblockIP() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Unblock IP";
            $hook = "wordfence_unblockIP";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_unblockIP', 'ualpHookUnblockIP'); 
/*
 * Fires when Permanently Block IP
 */
if (!function_exists('ualpHookPermBlockIP')) {

    function ualpHookPermBlockIP() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Permanently Block IP";
            $hook = "wordfence_permBlockIP";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_permBlockIP', 'ualpHookPermBlockIP'); 
/*
 * Fires when Download Htaccess file
 */
if (!function_exists('ualpHookDownloadHtaccess')) {

    function ualpHookDownloadHtaccess() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Download Htaccess";
            $hook = "wordfence_downloadHtaccess";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_downloadHtaccess', 'ualpHookDownloadHtaccess'); 
/*
 * Fires when Check Falcon Htaccess
 */
if (!function_exists('ualpHookCheckFalconHtaccess')) {

    function ualpHookCheckFalconHtaccess() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Check Falcon Htaccess";
            $hook = "wordfence_checkFalconHtaccess";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_checkFalconHtaccess', 'ualpHookCheckFalconHtaccess'); 
/*
 * Fires when Remove Cache 
 */
if (!function_exists('ualpHookRemoveFromCache')) {

    function ualpHookRemoveFromCache() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Remove From Cache";
            $hook = "wordfence_removeFromCache";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_removeFromCache', 'ualpHookRemoveFromCache'); 
/*
 * Fires when Enable auto update
 */
if (!function_exists('ualpHookAutoUpdateChoice')) {

    function ualpHookAutoUpdateChoice() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Enable auto update";
            $hook = "wordfence_autoUpdateChoice";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_autoUpdateChoice', 'ualpHookAutoUpdateChoice'); 
/*
 * Fires when Save Cache Options
 */
if (!function_exists('ualpHookSaveCacheOptions')) {

    function ualpHookSaveCacheOptions() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Save Cache Options";
            $hook = "wordfence_saveCacheOptions";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_saveCacheOptions', 'ualpHookSaveCacheOptions'); 
/*
 * Fires when Admin Email Choice
 */
if (!function_exists('ualpHookAdminEmailChoice')) {

    function ualpHookAdminEmailChoice() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Admin Email Choice";
            $hook = "wordfence_adminEmailChoice";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_adminEmailChoice', 'ualpHookAdminEmailChoice'); 
/*
 * Fires when Get Cache Stats
 */
if (!function_exists('ualpHookGetCacheStats')) {

    function ualpHookGetCacheStats() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Get Cache Stats";
            $hook = "wordfence_getCacheStats";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_getCacheStats', 'ualpHookGetCacheStats'); 
/*
 * Fires when Kill Scan
 */
if (!function_exists('ualpHookKillScan')) {

    function ualpHookKillScan() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Kill Scan";
            $hook = "wordfence_killScan";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_killScan', 'ualpHookKillScan'); 
/*
 * Fires when plugin Tour Closed
 */
if (!function_exists('ualpHookTourClosed')) {

    function ualpHookTourClosed() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Tour Closed";
            $hook = "wordfence_tourClosed";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_tourClosed', 'ualpHookTourClosed'); 

/*
 * Fires when Remove Cache Exclusion
 */
if (!function_exists('ualpHookRemoveCacheExclusion')) {

    function ualpHookRemoveCacheExclusion() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Remove Cache Exclusion";
            $hook = "wordfence_removeCacheExclusion";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_removeCacheExclusion', 'ualpHookRemoveCacheExclusion'); 
/*
 * Fires when Unblock Network
 */
if (!function_exists('ualpHookUnblockNetwork')) {

    function ualpHookUnblockNetwork() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Unblock Network";
            $hook = "wordfence_unblockNetwork";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_unblockNetwork', 'ualpHookUnblockNetwork'); 
/*
 * Fires when Permanently Block All IPs
 */
if (!function_exists('ualpHookPermanentlyBlockAllIPs')) {

    function ualpHookPermanentlyBlockAllIPs() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Permanently Block All IPs";
            $hook = "wordfence_permanentlyBlockAllIPs";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_permanentlyBlockAllIPs', 'ualpHookPermanentlyBlockAllIPs'); 
/*
 * Fires when Delete Admin User
 */
if (!function_exists('ualpHookDeleteAdminUser')) {

    function ualpHookDeleteAdminUser() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Delete Admin User";
            $hook = "wordfence_deleteAdminUser";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_deleteAdminUser', 'ualpHookDeleteAdminUser'); 
/*
 * Fires when Revoke Admin User
 */
if (!function_exists('ualpHookRevokeAdminUser')) {

    function ualpHookRevokeAdminUser() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "Wordfence";
            $post_id = "";
            $post_title = "Revoke Admin User";
            $hook = "wordfence_revokeAdminUser";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_revokeAdminUser', 'ualpHookRevokeAdminUser'); 
/*
 * Fires when Hide htaccess file
 */
if (!function_exists('ualpHookHideFileHtaccess')) {

    function ualpHookHideFileHtaccess() {
        $logHookList = get_option('enableLogHookList');
        if (in_array('wordfence',$logHookList)) {
            $action = "";
            $obj_type = "";
            $post_id = "";
            $post_title = "Hide htaccess file";
            $hook = "wordfence_hideFileHtaccess";
            $description = "$obj_type : $post_title";
            ualpGetActivityLog($action, $obj_type, $post_id, $post_title, $hook, $description);
        }
    }

}
add_action('wp_ajax_wordfence_hideFileHtaccess', 'ualpHookHideFileHtaccess'); 