=== User Activity Log PRO ===
Contributors: solwininfotech
Support link: http://support.solwininfotech.com/
Tags: admin user log, user log, user, log, user activity, activity log, log user, record user activity, log user action, user action log, wordpress admin security, wordpress security, security, user tracking, user activity monitor, watch user, track user, track user activity, track activity.
Requires at least: 4.1
Tested up to: 4.7
Stable tag: 1.1

Log all activity of users and get notified when user login to admin area.

== Description ==
Do your site have many users for various admin side activity? Do you stuck with issue to track user activity on your website admin side? do you want to secure your site by tracking log of all user activity ? do you want to get notified when particular user logged in ?
Just relax, Now with the help of "User Activity Log Pro" Plugin you can track all users activity in detail on your website.

As of this moment, the plugin logs data when anyone do following activity:

* WordPress - Core Updates
* Posts - Created, Updated, Deleted
* Pages - Created, Updated, Deleted
* Custom Post Type Posts- Created, Updated, Deleted
* Tags - Created, Edited, Deleted
* Categories - Created, Edited, Deleted
* Taxonomies terms - Created, Edited, Deleted
* Comments - Created, Approved, Unproved, Trashed, Untrashed, Spammed, Unspammed, Deleted
* Media - Uploaded, Edited, Deleted
* Users - Login, Logout, Login has failed, Update profile, Registered and Delete
* Plugins -Activated, Deactivated
* Themes - Installed, Updated, Deleted, Activated
* Widgets - Added to a sidebar / Deleted from a sidebar, Order widgets
* Menus - A menu is being Created, Updated, Deleted
* Export - User download export file from the site

Additional features for security:
* Admin will be notified via email when selected user logged in.
* Admin will be notified via email when selected role's any users logged in.

= Warning =
The purpose of this plugin is to keep track of all activity with in your WordPress area. We have performed testing with various cases to make sure plugins works very well, but you should make sure you have a backup of your database, before installing plugin.

== Installation ==

1. Go to Plugins > Add New.
<br />
2. Click on Upload Plugin and browse and select user-activity-log-pro.zip then click on Install Now.
<br />
3. To activate User Activity Log Pro, Click on Activate link.
<br />

== Changelog ==

= 1.1 =
Release date: January 3rd, 2017

* Enhancement: Added plugin support - Wordfence, Gravity form, Yoast SEO and Blog Designer PRO
* Bug Fix: Custom message not changing in Email Settings
* Bug Fix: Email getting "User Registration Date" instead of "Login Date"

= 1.0 =
Release Date: September 20th, 2016

* Initial release
