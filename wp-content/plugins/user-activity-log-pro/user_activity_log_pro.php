<?php
/*
 * Plugin Name: User Activity Log Pro
 * Plugin URI: https://www.solwininfotech.com/product/wordpress-plugins/user-activity-log-pro/
 * Description: Log the activity of users to monitor your site with actions
 * Author: Solwin Infotech
 * Author URI: https://www.solwininfotech.com/
 * Version: 1.1
 * Requires at least: 4.1
 * Tested up to: 4.7
 * Copyright: Solwin Infotech
 */

/*
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}
/*
 * Define variables
 */
global $wpdb;
define('UALP_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('UALP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('UALP_TABLENAME', $wpdb->prefix.'ualp_user_activity');
define('UALP_DETAIL_TABLENAME', $wpdb->prefix.'ualp_user_activity_detail');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
include_once( ABSPATH . 'wp-includes/pluggable.php' );

include(UALP_PLUGIN_DIR . 'includes/ualp-list-table.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-common-functions.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-hooks.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-log-detail.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-export-logs.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-settings-menu.php');
include(UALP_PLUGIN_DIR . 'includes/ualp-settings-tab.php');

if (is_plugin_active('blog-designer-pro/blog-designer-pro.php')) {
    include(UALP_PLUGIN_DIR . 'includes/ualp-hooks-blog-designer-pro.php');
}
if (is_plugin_active('woocommerce/woocommerce.php')) {
    include(UALP_PLUGIN_DIR . 'includes/ualp-hooks-woocommerce.php');
}
if (is_plugin_active('gravityforms/gravityforms.php')) {
    include(UALP_PLUGIN_DIR . 'includes/ualp-hooks-gravityform.php');
}
if (is_plugin_active('wordfence/wordfence.php')) {
    include(UALP_PLUGIN_DIR . 'includes/ualp-hooks-wordfence.php');
}
if (is_plugin_active('wordpress-seo/wp-seo.php')) {
    include(UALP_PLUGIN_DIR . 'includes/ualp-hooks-wordpress-seo.php');
}

register_activation_hook(__FILE__,'ualpDeactivateUal');

add_action('current_screen', 'ualpFooterAdv');
add_action('wp_login', 'ualpSendEmailNotification', 99);
add_action('user_register', 'ualpEnableUserForNotification');

$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'ualpAddSettingsLink' );

add_action('plugins_loaded', 'ualpLoadTextDomain');
add_action('plugins_loaded', 'latest_news_solwin_feed');

add_action('init', 'ualpSetRoleManager',6);
add_action('init', 'ualpFilterUserRole');
add_action('init', 'ualpSessionStart');
add_action('init', 'ualpDeleteLog');

add_action('activate_plugin', 'ualpCreateTable',2);
add_action('activate_plugin', 'ualpCreateDetailTable',4);
add_action('activate_plugin', 'ualpSetDefaultLogHook',8);
add_action('activated_plugin', 'ualpRedirect', 10, 2);

add_action('admin_notices', 'ualpDatabaseUpgrade');

add_action('admin_head', 'ualpDialogboxContent');
add_action('admin_head', 'ualpSubscribeMail', 11);
add_action('admin_head', 'ulapGetAdminPwd');

add_action('admin_enqueue_scripts', 'ualpAdminStyles');
add_action('admin_enqueue_scripts', 'ualpAdminScripts');

add_action('wp_ajax_close_tab', 'ualpWpAjaxCloseTab');
add_action('wp_ajax_compareLogPassword', 'ualpCompareLogPassword');
add_action('wp_ajax_downloadLogFile', 'ualpDeleteUalpTable');
add_action('wp_ajax_downloadSingleLog', 'ualpDeleteSingleLog');
add_action('wp_ajax_downloadBulkLog', 'ualpDeleteBulkLog');
add_action('wp_ajax_viewDetailLog', 'ualpViewUpdateDetail');
add_action('wp_ajax_ualpFavoriteLog', 'ualpAjaxFavoriteLog');

/**
 * Load plugin text domain (user_activity_log_pro)
 */
if (!function_exists('ualpLoadTextDomain')) {

    function ualpLoadTextDomain() {
        load_plugin_textdomain('user_activity_log_pro', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

}