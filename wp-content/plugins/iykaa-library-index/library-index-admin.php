<style>
	p span {
		font-weight: bold;
	}
	hr {
		border: none;
		height: 1px;
		margin: 10px 0;
		display: block;
		background-color: #666;
	}
	.wide {
		width: 27.5%;
	}
	.thin {
		width: 15%;
	}
	.wrap>h2:first-child {
		margin-bottom: 20px;
	}
	.warnings .tab {
		vertical-align: middle;
	}
	
	.warnings .tab span {
		background-color: #ed4a56;
		width: 10px;
		height: 10px;
		display: block;
		border-radius: 50%;
	}
	
	/* Style the tab */
	div.tab {
		overflow: hidden;
		border: 1px solid #ccc;
		background-color: #f1f1f1;
	}
	
	/* Style the buttons inside the tab */
	div.tab button {
		background-color: inherit;
		float: left;
		border: none;
		outline: none;
		cursor: pointer;
		padding: 14px 16px;
		transition: 0.3s;
	}
	
	/* Change background color of buttons on hover */
	div.tab button:hover {
		background-color: #ddd;
	}
	
	/* Create an active/current tablink class */
	div.tab button.active {
		background-color: #ccc;
	}
	
	/* Style the tab content */
	.tabcontent {
		display: none;
		padding: 6px 12px;	
		border: 1px solid #ccc;
		border-top: none;
	}
</style>

<div class="wrap">
	<?php
		echo '<h2>' . __('Library Index', 'iykaa_login_access') . '</h2>';
	?>
	<div class="tab">
		<button class="tablinks active" onclick="openCity(event, 'main')">Main Library</button>
		<button class="tablinks" onclick="openCity(event, 'custom')">Custom Library</button>
	</div>
	<div id="main" class="tabcontent">
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th class="wide">
					Name
				</th>
				<th class="wide">
					Linked Files
				</th>
				<th class="thin">
					Published
				</th>
				<th class="thin">
					Updated
				</th>
				<th class="thin">
					Author
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_library',
					'orderby' => 'name',
					'order' => 'DESC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish'
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<?php if(have_rows('download')) { ?>
				<tr>
					<td class="tab" width="10px"><span></span></td>
					<td class="wide">
						<p><a href="<?php the_permalink();?>" target="_blank"><?php the_title(); ?></a></p>
					</td>
					<td class="wide">
						<?php if(have_rows('download')) { ?>
							<?php while(have_rows('download')) { ?>
								<?php the_row(); ?>
									<p><span>Title:</span> <?php the_sub_field('file_name'); ?></p>
									<p><span>Location:</span> <a href="<?php the_sub_field('file'); ?>" target="_blank">File Link</a></p>
									<hr>
							<?php } ?>
						<?php } ?>
					</td>
					<td class="thin">
						<p><?php the_date("d/m/Y | H:i a");?></p>
					</td>
					<td class="thin">
						<p><?php the_modified_date("d/m/Y | H:i a");?></p>
					</td>
					<td class="thin">
						<?php $author = get_the_author(); ?>
						<p><?php echo $author;?></p>
					</td>
				</tr>
				<?php } else {?>
				<?php } ?>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Linked Files
				</th>
				<th>
					Published
				</th>
				<th>
					Updated
				</th>
				<th>
					Author
				</th>
			</tfooter>
		</table>
	</div>
	<div id="custom" class="tabcontent">
		<table width="100%" class="wp-list-table widefat fixed striped pages">
			<thead>
				<td width="10px"></td>
				<th class="wide">
					Name
				</th>
				<th class="wide">
					Linked Files
				</th>
				<th class="thin">
					Published
				</th>
				<th class="thin">
					Updated
				</th>
				<th class="thin">
					Author
				</th>
			</thead>
			<?php
				$args = array(
					'post_type' => 'iykaa_library_custom',
					'orderby' => 'name',
					'order' => 'DESC',
					'posts_per_page' => -1,
					'post_parent' => 0,
					'post_status' => 'publish'
				);
				
				$memberSearch = new WP_Query($args);
			?>
			
			<?php while($memberSearch->have_posts()) {
				$memberSearch->the_post(); ?>
				<?php if(have_rows('download')) { ?>
				<tr>
					<td class="tab" width="10px"><span></span></td>
					<td class="wide">
						<p><a href="<?php the_permalink();?>" target="_blank"><?php the_title(); ?></a></p>
					</td>
					<td class="wide">
						<?php if(have_rows('download')) { ?>
							<?php while(have_rows('download')) { ?>
								<?php the_row(); ?>
									<p><span>Title:</span> <?php the_sub_field('file_name'); ?></p>
									<p><span>Location:</span> <a href="<?php the_sub_field('file'); ?>" target="_blank">File Link</a></p>
									<hr>
							<?php } ?>
						<?php } ?>
					</td>
					<td class="thin">
						<p><?php the_date("d/m/Y | H:i a");?></p>
					</td>
					<td class="thin">
						<p><?php the_modified_date("d/m/Y | H:i a");?></p>
					</td>
					<td class="thin">
						<?php $author = get_the_author(); ?>
						<p><?php echo $author;?></p>
					</td>
				</tr>
				<?php } else {?>
				<?php } ?>
			<?php } ?>
			<tfooter>
				<td width="10px"></td>
				<th>
					Name
				</th>
				<th>
					Linked Files
				</th>
				<th>
					Published
				</th>
				<th>
					Updated
				</th>
				<th>
					Author
				</th>
			</tfooter>
		</table>
	</div>
</div>
<script>
	function openCity(evt, cityName) {
		// Declare all variables
		var i, tabcontent, tablinks;
		
		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		
		// Get all elements with class="tablinks" and remove the class "active"
		tablinks = document.getElementsByClassName("tablinks");
		
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		
		// Show the current tab, and add an "active" class to the button that opened the tab
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>