<?php
	/*
		Plugin Name: Iykaa Library Index
		Plugin URI: http://www.blumilk.com
		Description: Provides simple list of all of the content in the library and their linked files
		Version: 1.0
		Author: Blumilk
		Author URI: http://www.blumilk.com
	*/
	
	function library_index_admin_actions() {
		add_menu_page('Library Index', 'Library Index', 'manage_options', 'iykaa-library-index', 'library_index_admin', 'dashicons-list-view');
	}
	add_action('admin_menu', 'library_index_admin_actions');
	
	function library_index_admin() {
		include('library-index-admin.php');
	}
?>