<?php
	// Remove the admin bar for logged in users
	add_filter('show_admin_bar', '__return_false');

	// Adding CSS styles
	add_action('wp_enqueue_scripts', 'iykaa_styles');
	function iykaa_styles() {
		wp_register_style('screen', get_stylesheet_directory_uri() . '/css/style.css', '', '1.4.1', 'all');
		wp_enqueue_style('screen');
		wp_register_style('print', get_stylesheet_directory_uri() . '/css/print.css', '', '1.0', 'print');
		wp_enqueue_style('print');
	}
	
	//Add Google fonts properly and enqueue the script. To add another font, include a ' \ ' after the font name set e.g. 'PT+Sans:400,400italic,700,700italic\Open+Sans:400'
	add_action('wp_enqueue_scripts', 'google_fonts');
	function google_fonts() {
		$query_args = array(
			'family' => 'Open+Sans:300,400,700|Montserrat:300,400,600',
			'subset' => 'latin,latin-ext',
		);
		wp_register_style( 'google_fonts', add_query_arg( $query_args, "https://fonts.googleapis.com/css" ), array(), null );
		wp_enqueue_style('google_fonts');
	}
	
	// Load jQuery from Google CDN to help with load time but also have the local fallback	
	$url = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'; // the URL to check against
	$test_url = @fopen($url,'r');
	if($test_url !== false) { // test if the URL exists
		function load_external_jQuery() { // load external file
			wp_deregister_script( 'jquery' ); // deregisters the default WordPress jQuery
			wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'); // register the external file
			wp_enqueue_script('jquery'); // enqueue the external file
		}
		add_action('wp_enqueue_scripts', 'load_external_jQuery'); // initiate the function
	} else {
		function load_local_jQuery() {
			wp_enqueue_script('jquery'); // enqueue built in WordPress version
		}
	add_action('wp_enqueue_scripts', 'load_local_jQuery'); // initiate the function
	}
	
	// Adds a logged-out class if a user isn't logged in
	function my_class_names($classes) {
		if(!(is_user_logged_in())) {
			$classes[] = 'logged-out';
		}
		
		return $classes;
	}
	add_filter('body_class', 'my_class_names');
	
	// Custom imagery sizes
	if(function_exists('add_image_size')) { 
		add_image_size(
			'happening-tile',
			400,
			300,
			true
		);
		
		// 1:1 Square images
		add_image_size(
			'square-img-large',
			800,
			800,
			true
		);
		add_image_size(
			'square-img-medium',
			600,
			600,
			true
		);
		add_image_size(
			'square-img-small',
			400,
			400,
			true
		);
		add_image_size(
			'square-img-x-small',
			200,
			200,
			true
		);
	}
	
	// Removes the model login box when in the CMS
	remove_action('admin_enqueue_scripts', 'wp_auth_check_load');
	remove_filter('heartbeat_send',        'wp_auth_check');
	remove_filter('heartbeat_nopriv_send', 'wp_auth_check');
	
	// Add Defer to scripts that are loaded to improve page speed
	function add_defer_attribute($tag, $handle) {
	// add script handles to the array below
		$scripts_to_defer = array('custom-script', 'owl-carousel','slick', 'all-slides');
		
		foreach($scripts_to_defer as $defer_script) {
		  if ($defer_script === $handle) {
			 return str_replace(' src', ' defer src', $tag);
		  }
		}
		return $tag;
	}
	
	add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

	// Adding JavaScript scripts
	add_action('wp_enqueue_scripts', 'iykaa_scripts');
	function iykaa_scripts() {
		wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/min/site-min.js', '', '1.4.1', array('jquery'));
		wp_enqueue_script('owl-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', '0.1', array('jquery'));
		wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/min/slick-min.js', '0.1');
		wp_enqueue_script('all-slides', get_stylesheet_directory_uri() . '/js/min/owlslides-min.js', '0.1');
	}
	
	// Register the navigation menu
	add_action('init', 'iykaa_menu');
	function iykaa_menu() {
		register_nav_menu('iykaa-menu',__('Iykaa Menu'));
	}
	
	// Show Featured Image on posts
	add_theme_support('post-thumbnails');
	
	// Allows redirection, even if my theme starts to send output to the browser
	add_action('init', 'do_output_buffer');
	function do_output_buffer() {
		ob_start();
	}
	
	// Advanced Custom Fields Option Page
	if(function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title' => 'Admin Options',
			'menu_title' => 'Admin Options',
			'menu-slug' => 'admin-options',
			'icon_url' => 'dashicons-admin-generic'
		));
		
		acf_add_options_page(array(
			'page_title' => 'System Options',
			'menu_title' => 'System Options',
			'menu-slug' => 'system-options',
			'icon_url' => 'dashicons-admin-generic'
		));
	}
	
	// Excerpt length
	add_filter('excerpt_length', 'custom_excerpt_length', 999);
	function custom_excerpt_length($length) {
		return 22;
	}
	
	// Remove the [...] from the excerpt	
	function wpse_excerpt_more( $more ) {
	    return '...';
	}
	add_filter( 'excerpt_more', 'wpse_excerpt_more' );
	
	//Remove Wordpress version declaration (in head and in Rss) for security purposes
	remove_action('wp_head', 'wp_generator'); 
	
	function wpt_remove_version() {  
		return '';  
	}  
	add_filter('the_generator', 'wpt_remove_version');
	
	// Google API Key for maps
	function my_acf_init() {
		acf_update_setting('google_api_key', 'AIzaSyAWrXyk7pqnJhF_ywrIcb7Pw02eBwItLak');
	}
	add_action('acf/init', 'my_acf_init');
	
	// Removes squared brackets from the reset password link
	// Very useful when you require emails to be formatted to HTML
	function removeBracketsHTML($message, $key, $user_login, $user_data) {
		return preg_replace('#<(https?:\/\/.+?)>#i', '$1', $message);
	}
	add_filter('retrieve_password_message', 'removeBracketsHTML', 100, 4);
	
	// disable random password
	function itsg_disable_random_password( $password ) {
	    $action = isset( $_GET['action'] ) ? $_GET['action'] : '';
	    if ( 'wp-login.php' === $GLOBALS['pagenow'] && ( 'rp' == $action  || 'resetpass' == $action ) ) {
	        return '';
	    }
	    return $password;
	}
	add_filter( 'random_password', 'itsg_disable_random_password', 10, 2 );
	
	// Loads the login.css stylesheet for when a client is logging in
	function loginStylesIykaa() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css">';
	}
	add_action('login_head', 'loginStylesIykaa');
	
	function logoURLIykaa() {
		return 'http://www.blumilk.com';
	}
	add_filter('login_headerurl', 'logoURLIykaa');
	
	function adminStylesIykaa() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css">';
	}
	add_action('admin_head', 'adminStylesIykaa');
	
	// Custom footer in the CMS
	function dashboard_footer() {
		$currentHour = date('H');
		$currentHour++;
		
		if($currentHour >= '00' && $currentHour <= '12') {
			$timeOfDay = 'morning';
		} elseif($currentHour >= '12' && $currentHour <= '18') {
			$timeOfDay = 'afternoon';
		} elseif($currentHour >= '18') {
			$timeOfDay = 'evening';
		} else {
			$timeOfDay = 'day';
		}
		
		echo 'Good ' . $timeOfDay . ' from team Enyware.';
	}
	add_filter('admin_footer_text', 'dashboard_footer');
	
	// Iykaa version number
	function iykaa_version_footer() {
		$enywareTheme = wp_get_theme();
		echo $enywareTheme->get('Name') . ' version ' . $enywareTheme->get('Version');
	}
	add_filter('update_footer', 'iykaa_version_footer', 11);
	
	// Removes the default meta boxes from the dashboard
	function remove_dashboard_meta() {
		remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
		remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
		remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
		remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
		remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
		remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
		remove_meta_box('dashboard_activity', 'dashboard', 'normal');
		remove_meta_box('mandrill_widget', 'dashboard', 'normal');
		remove_meta_box('ual_dashboard_widget', 'dashboard', 'normal');
	}
	add_action('admin_head', 'remove_dashboard_meta');
	
	function remove_dashboard_meta_primary() {
		remove_meta_box('dashboard_primary', 'dashboard', 'normal');
	}
	add_action('admin_init', 'remove_dashboard_meta_primary');
	
	// Update Image Sizes - This will constrain images, not crop them
	update_option( 'thumbnail_size_w', 250 );
	update_option( 'thumbnail_size_h', 250 );
	update_option( 'thumbnail_crop', 0 );
	update_option( 'medium_size_w', 1024 );
	update_option( 'medium_size_h', 1024 );
	update_option( 'medium_crop', 0 );
	update_option( 'large_size_w', 1920 );
	update_option( 'large_size_h', 1920 );
	update_option( 'large_crop', 0 );
	
	// Custom meta boxes
	function iykaa_welcome_content() {
		echo 'Enyware is a cloud based internal communications platform that allows all members of staff within an organisation to receive and access key company and department specific communications and documentation.';
	}
	
	function iykaa_admin_tools_content() {
		include ('includes/admin/cms-links.php');
	}
	
	function iykaa_welcome_meta_box() {
		add_meta_box('ikyaa_welcome', 'Welcome to Enyware', 'iykaa_welcome_content', 'dashboard', 'normal', 'high', null);
		add_meta_box('ikyaa_admin_tools', 'Admin Tools', 'iykaa_admin_tools_content', 'dashboard', 'normal', 'high', null);
	}
	add_action('admin_init', 'iykaa_welcome_meta_box');
	
	function mytheme_remove_help_tabs($old_help, $screen_id, $screen) {
		$screen->remove_help_tabs();
		return $old_help;
	}
	add_filter('contextual_help', 'mytheme_remove_help_tabs', 999, 3);

	// Use 'if' statments based on custom post types bot inside and outside of the loop. if (is_single() && is_post_type('iykaa_notifications')){ }
	function is_post_type($type){
		global $wp_query;
		if($type == get_post_type($wp_query->post->ID)) return true;
		return false;
	}
	
	// Hide update nag from all users that aren't the site's main admin  
	function hide_update_notice_to_all_but_admin_users(){
		global $user_ID;
		wp_get_current_user();
		if ($user_ID !== 1) {
			remove_action( 'admin_notices', 'update_nag', 3 );
			remove_menu_page('plugins.php');						// Plugins
			remove_menu_page('edit-comments.php');					// Comments
			remove_menu_page('edit.php?post_type=acf-field-group');	// ACF
			// Enyware Spesific
			remove_menu_page('admin.php?page=onesignal-push');	// OpenSignal
//			remove_menu_page('edit.php?post_type=page');		// Pages
			remove_menu_page('admin.php?page=CF7DBPluginSubmissions'); // ContactForm DB
		} else {
			
		}
	}
	add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );
	
	// Custom post types
	function create_post_type() {
		register_post_type ('iykaa_members',
			array(
				'labels' => array(
					'name' => __('Members'),
					'singular_name' => __('Member'),
					'add_new_item' => __('Add New Member'),
					'add_new' => _x('Add New', 'Member'),
					'edit_item' => __('Edit Member'),
					'new_item' => __('New Member'),
					'all_items' => __('All Members'),
					'view_item' => __('View Member'),
					'search_items' => __('Search Members'),
					'not_found' =>  __('No Members Found'),
					'not_found_in_trash' => __('No Members Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Members',
					'add_new' => 'Add Member'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-id-alt',
				'menu_position' => 4,
				'hierarchical' => true,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'members',
				),
			)
		);
		
		register_post_type ('iykaa_notifications',
			array(
				'labels' => array(
					'name' => __('Notifications'),
					'singular_name' => __('Notifications'),
					'add_new_item' => __('Add New Notification'),
					'add_new' => _x('Add New', 'Notification'),
					'edit_item' => __('Edit Notification'),
					'new_item' => __('New Notification'),
					'all_items' => __('All Notifications'),
					'view_item' => __('View Notification'),
					'search_items' => __('Search Notifications'),
					'not_found' =>  __('No Notifications Found'),
					'not_found_in_trash' => __('No Notifications Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Notifications',
					'add_new' => 'Add Notification'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-megaphone',
				'menu_position' => 5,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'notifications',
				),
			)
		);
		
		register_post_type ('iykaa_library',
			array(
				'labels' => array(
					'name' => __('For Me'),
					'singular_name' => __('For Me'),
					'add_new_item' => __('Add New For Me Item'),
					'add_new' => _x('Add New', 'For Me Item'),
					'edit_item' => __('Edit For Me Item'),
					'new_item' => __('New For Me Item'),
					'all_items' => __('All For Me Items'),
					'view_item' => __('View For Me Item'),
					'search_items' => __('Search For Me'),
					'not_found' =>  __('No For Me Items Found'),
					'not_found_in_trash' => __('No For Me Items Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'For Me',
					'add_new' => 'Add For Me Item',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-book-alt',
				'menu_position' => 6,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'taxonomies' => array('library_tag'),
				'capability_type' => 'page',
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields',
					'page-attributes'
				),
				'rewrite' => array(
					'slug' => 'library',
				),
			)
		);
		
		register_post_type ('iykaa_locations',
			array(
				'labels' => array(
					'name' => __('Locations'),
					'singular_name' => __('Location'),
					'add_new_item' => __('Add New Location'),
					'add_new' => _x('Add New', 'Location'),
					'edit_item' => __('Edit Location'),
					'new_item' => __('New Location'),
					'all_items' => __('All Locations'),
					'view_item' => __('View Location'),
					'search_items' => __('Search Locations'),
					'not_found' =>  __('No Locations Found'),
					'not_found_in_trash' => __('No Locations Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Locations',
					'add_new' => 'Add Location'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-location',
				'menu_position' => 4,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'locations',
				),
			)
		);
		
		register_post_type ('iykaa_departments',
			array(
				'labels' => array(
					'name' => __('Departments'),
					'singular_name' => __('Department'),
					'add_new_item' => __('Add New Department'),
					'add_new' => _x('Add New', 'Department'),
					'edit_item' => __('Edit Department'),
					'new_item' => __('New Department'),
					'all_items' => __('All Departments'),
					'view_item' => __('View Department'),
					'search_items' => __('Search Departments'),
					'not_found' =>  __('No Departments Found'),
					'not_found_in_trash' => __('No Departments Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Departments',
					'add_new' => 'Add Department'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-exerpt-view',
				'menu_position' => 4,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'departments',
				),
			)
		);
		
/*		register_post_type ('iykaa_dms',
			array(
				'labels' => array(
					'name' => __('Document Management System'),
					'singular_name' => __('Document Management System'),
					'add_new_item' => __('Add New Document'),
					'add_new' => _x('Add New', 'Document'),
					'edit_item' => __('Edit Document'),
					'new_item' => __('New Document'),
					'all_items' => __('All Documents'),
					'view_item' => __('View Document'),
					'search_items' => __('Search Documents'),
					'not_found' =>  __('No Documents Found'),
					'not_found_in_trash' => __('No Documents Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Document Management System',
					'add_new' => 'Add Document',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-book-alt',
				'menu_position' => 3,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'revisions',
					'custom-fields',
				),
				'rewrite' => array(
					'slug' => 'document-management-system',
				),
				'capability_type' => array(
					'dms_document',
					'dms_documents',
				),
				'map_meta_cap' => true,
				'taxonomies' => array(
					'post_tag'
				),
			)
		); */
	}
	add_action('init', 'create_post_type');
	
	// Custom post type taxonomies
	function create_post_type_taxonomies() {
		register_taxonomy('location_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
				'iykaa_locations',
				'iykaa_departments',
			),
			array(
				'labels' => array(
					'name' => 'Location',
					'add_new_item' => 'Add New Location',
					'new_item_name' => 'New Location'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('position_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
				'iykaa_dms',
			),
			array(
				'labels' => array(
					'name' => 'Position',
					'add_new_item' => 'Add New Position',
					'new_item_name' => 'New Position'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('department_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
				'iykaa_dms',
				'iykaa_departments',
			),
			array(
				'labels' => array(
					'name' => 'Department',
					'add_new_item' => 'Add Department',
					'new_item_name' => 'New Department'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('status_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Status',
					'add_new_item' => 'Add Status',
					'new_item_name' => 'New Status'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('download_type_tax',
			array(
				'iykaa_library',
			),
			array(
				'labels' => array(
					'name' => 'Library Content Type',
					'add_new_item' => 'Add Library Content Type',
					'new_item_name' => 'New Library Content Type'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'library-type',
				),
			)
		);
		
		register_taxonomy('library_tag',
			array(
				'iykaa_library',
			),
			array(
				'labels' => array(
					'name' => 'Library Tags',
					'add_new_item' => 'Add Library Tag',
					'new_item_name' => 'New Library Tag'
				),
				'show_tagcloud' 		=> true,
				'hierarchical'          => false,
				'show_ui'               => true,
				'show_admin_column'     => true,
				'update_count_callback' => '_update_post_term_count',
				'query_var'             => true,		
				'rewrite' => array(
					'slug' => 'library-tag',
				),
			)
		);
		
		register_taxonomy('buy_sell_tax',
			array(
				'buy_sell',
			),
			array(
				'labels' => array(
					'name' => 'Item Category',
					'add_new_item' => 'Add Item Category',
					'new_item_name' => 'New Item Category'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('buy_sell_item',
			array(
				'buy_sell',
			),
			array(
				'labels' => array(
					'name' => 'Item Type',
					'add_new_item' => 'Add Item Type',
					'new_item_name' => 'New Item Type'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('buy_sell_status',
			array(
				'buy_sell',
			),
			array(
				'labels' => array(
					'name' => 'Item Status',
					'add_new_item' => 'Add Item Status',
					'new_item_name' => 'New Item Status'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
/*		register_taxonomy('menu_structure_tax',
			array(
				'iykaa_library',
			),
			array(
				'labels' => array(
					'name' => 'Menu Structure',
					'add_new_item' => 'Add Menu Structure',
					'new_item_name' => 'New Menu Structure'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'library-order',
					'hierarchical' => true,
				),
			)
		); */
		
		register_taxonomy('notification_priority_tax',
			array(
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Notification Priority',
					'add_new_item' => 'Add Notification Priority',
					'new_item_name' => 'New Notification Priority'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'notification-priority',
				),
			)
		);
	}
	add_action('init', 'create_post_type_taxonomies', 0);
	
	// Remove the default WordPress access levels so they can be recreated
	// remove_role('subscriber');
	// remove_role('contributor');
	// remove_role('author');
	// remove_role('editor');
	// remove_role('administrator');
	
	// remove_role('deity_iykaa');
	
	// Custom user roles
	$accessDeity = add_role(
		'deity_iykaa',
		'Deity',
		array(
			// Admin
			'activate_plugins' 			=> true,
			'create_users' 				=> true,
			'delete_plugins' 			=> true,
			'delete_themes' 			=> true,
			'delete_users' 				=> true,
			'edit_files' 				=> true,
			'edit_plugins' 				=> true,
			'edit_theme_options' 		=> true,
			'edit_themes' 				=> true,
			'edit_users' 				=> true,
			'export' 					=> true,
			'import' 					=> true,
			'install_plugins' 			=> true,
			'install_themes' 			=> true,
			'list_users' 				=> true,
			'manage_options' 			=> true,
			'promote_users' 			=> true,
			'remove_users' 				=> true,
			'switch_themes' 			=> true,
			'update_core' 				=> true,
			'update_plugins' 			=> true,
			'update_themes' 			=> true,
			'edit_dashboard' 			=> true,
			// Editor
			'moderate_comments' 		=> true,
			'manage_categories' 		=> true,
			'manage_links' 				=> true,
			'edit_others_posts' 		=> true,
			'edit_pages' 				=> true,
			'edit_others_pages' 		=> true,
			'edit_published_pages' 		=> true,
			'publish_pages' 			=> true,
			'delete_pages' 				=> true,
			'delete_others_pages' 		=> true,
			'delete_published_pages' 	=> true,
			'delete_others_posts' 		=> true,
			'delete_private_posts' 		=> true,
			'edit_private_posts' 		=> true,
			'read_private_posts' 		=> true,
			'delete_private_pages' 		=> true,
			'edit_private_pages' 		=> true,
			'read_private_pages' 		=> true,
			'unfiltered_html' 			=> true,
			// Author
			'edit_published_posts' 		=> true,
			'upload_files' 				=> true,
			'publish_posts' 			=> true,
			'delete_published_posts' 	=> true,
			// Contributor
			'edit_posts' 				=> true,
			'delete_posts' 				=> true,
			// Subscriber
			'read' 						=> true,
		)
	);
	
	// Use this to reset the admin rights. 	When you enable something new, enable this, upload and refresh the system then disable it again.
	// remove_role('admin_iykaa');
	
	$accessAdmin = add_role(
		'admin_iykaa',
		'Admin',
		array(
			// Admin
			'create_users' 				=> true,
			'delete_users' 				=> true,
			'edit_files' 				=> true,
			'edit_users' 				=> true,
			'list_users' 				=> true,
			'manage_options' 			=> true,
			'promote_users' 			=> true,
			'remove_users' 				=> true,
			'edit_dashboard' 			=> true,
			// Editor
			'manage_categories' 		=> true,
			'manage_links' 				=> true,
			'edit_others_posts' 		=> true,
			'edit_pages' 				=> true,
			'edit_others_pages' 		=> true,
			'edit_published_pages' 		=> true,
			'publish_pages' 			=> true,
			'delete_pages' 				=> true,
			'delete_others_pages' 		=> true,
			'delete_published_pages' 	=> true,
			'delete_others_posts' 		=> true,
			'delete_private_posts' 		=> true,
			'edit_private_posts' 		=> true,
			'read_private_posts' 		=> true,
			'delete_private_pages' 		=> true,
			'edit_private_pages' 		=> true,
			'read_private_pages' 		=> true,
			'unfiltered_html' 			=> true,
			// Author
			'edit_published_posts' 		=> true,
			'upload_files' 				=> true,
			'publish_posts' 			=> true,
			'delete_published_posts' 	=> true,
			// Contributor
			'edit_posts' 				=> true,
			'delete_posts' 				=> true,
			// Subscriber
			'read' 						=> true,
		)
	);
	
	// remove_role('member_iykaa');
	$accessMember = add_role(
		'member_iykaa',
		'Member',
		array(
			// By setting 'read' to false stops the member seeing anything in the CMS
			'read' 						=> false,
			'edit_posts' 				=> false,
			'delete_posts' 				=> false,
			'publish_posts' 			=> false,
			'upload_files' 				=> false,
			'level_1' 					=> true,
		)
	);
	
	// Helps to show users under the authors on member pages
	$memberRole = get_role('member_iykaa');
	$memberRole->add_cap('level_1');
	
	/*
	function psp_add_role_caps() {
		// Add the roles you'd like to administer the custom post types
		$roles = array(
			'deity_iykaa',
			'admin_iykaa'
		);
		
		// Loop through each role and assign capabilities
		foreach($roles as $the_role) { 
			
			$role = get_role($the_role);
			
			$role->add_cap('read');
			$role->add_cap('read_dms_document');
			$role->add_cap('read_private_dms_documents');
			$role->add_cap('edit_dms_document');
			$role->add_cap('edit_dms_documents');
			$role->add_cap('edit_others_dms_documents');
			$role->add_cap('edit_published_dms_documents');
			$role->add_cap('publish_dms_documents');
			$role->add_cap('delete_others_dms_documents');
			$role->add_cap('delete_private_dms_documents');
			$role->add_cap('delete_dms_documents');
			$role->add_cap('delete_post_dms_documents');
			$role->add_cap('delete_published_dms_documents');
			$role->add_cap('delete_draft_dms_documents');
			$role->add_cap('delete_others_posts_dms_documents');
			$role->add_cap('delete_others_posts_dms_document');
			$role->add_cap('delete_posts_dms_documents');
			$role->add_cap('delete_posts_dms_document');
		}
	}
	add_action('admin_init','psp_add_role_caps', 999);
	*/
	
	function blumilk_wp_die( $message = '', $title = '', $args = array() ) {
	
		if ( is_int( $args ) ) {
			$args = array( 'response' => $args );
		} elseif ( is_int( $title ) ) {
			$args  = array( 'response' => $title );
			$title = '';
		}
	
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			/**
			 * Filter callback for killing WordPress execution for AJAX requests.
			 *
			 * @since 3.4.0
			 *
			 * @param callable $function Callback function name.
			 */
			$function = apply_filters( 'blumilk_wp_die_ajax_handler', '_ajax_blumilk_wp_die_handler' );
		} elseif ( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST ) {
			/**
			 * Filter callback for killing WordPress execution for XML-RPC requests.
			 *
			 * @since 3.4.0
			 *
			 * @param callable $function Callback function name.
			 */
			$function = apply_filters( 'blumilk_wp_die_xmlrpc_handler', '_xmlrpc_blumilk_wp_die_handler' );
		} else {
			/**
			 * Filter callback for killing WordPress execution for all non-AJAX, non-XML-RPC requests.
			 *
			 * @since 3.0.0
			 *
			 * @param callable $function Callback function name.
			 */
			$function = apply_filters( 'blumilk_wp_die_handler', 'blumilk_default_blumilk_wp_die_handler' );
		}
	
		call_user_func( $function, $message, $title, $args );
	}
	
	function blumilk_default_blumilk_wp_die_handler( $message, $title = '', $args = array() ) {
		$defaults = array( 'response' => 500 );
		$r = wp_parse_args($args, $defaults);
	
		$have_gettext = function_exists('__');
	
		if ( function_exists( 'is_wp_error' ) && is_wp_error( $message ) ) {
			if ( empty( $title ) ) {
				$error_data = $message->get_error_data();
				if ( is_array( $error_data ) && isset( $error_data['title'] ) )
					$title = $error_data['title'];
			}
			$errors = $message->get_error_messages();
			switch ( count( $errors ) ) {
			case 0 :
				$message = '';
				break;
			case 1 :
				$message = "<p>{$errors[0]}</p>";
				break;
			default :
				$message = "<ul>\n\t\t<li>" . join( "</li>\n\t\t<li>", $errors ) . "</li>\n\t</ul>";
				break;
			}
		} elseif ( is_string( $message ) ) {
			$message = "<p>$message</p>";
		}
	
		if ( isset( $r['back_link'] ) && $r['back_link'] ) {
			$back_text = $have_gettext? __('&laquo; Back') : '&laquo; Back';
			$message .= "\n<p><a href='javascript:history.back()'>$back_text</a></p>";
		}
	
		if ( ! did_action( 'admin_head' ) ) :
			if ( !headers_sent() ) {
				status_header( $r['response'] );
				nocache_headers();
				header( 'Content-Type: text/html; charset=utf-8' );
			}
	
			if ( empty($title) )
				$title = $have_gettext ? __('WordPress &rsaquo; Error') : 'WordPress &rsaquo; Error';
	
			$text_direction = 'ltr';
			if ( isset($r['text_direction']) && 'rtl' == $r['text_direction'] )
				$text_direction = 'rtl';
			elseif ( function_exists( 'is_rtl' ) && is_rtl() )
				$text_direction = 'rtl';
	?>
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" <?php if ( function_exists( 'language_attributes' ) && function_exists( 'is_rtl' ) ) language_attributes(); else echo "dir='$text_direction'"; ?>>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="viewport" content="width=device-width">
			<link href="<?php echo get_template_directory_uri(); ?>/css/login.css" type="text/css" rel="stylesheet">
			<title><?php echo $title ?></title>
		</head>
		<body id="error-page">
		<?php endif; // ! did_action( 'admin_head' ) ?>
			<div class="error-box">
				<?php echo $message; ?>
				<a class="cta primary size-m white" href="<?php echo home_url(); ?>">
					Return to <?php echo bloginfo('name'); ?>
				</a>
			</div>
		</body>
	</html>
	<?php
		die();
	}
	
	// Allows for custom cookie login sessions to be set
	function my_expiration_filter($seconds, $user_id, $remember) {
		$setDate = get_field('login_cookies', 'options');
		
		if($remember) {
			if($setDate) {
				$expiration = $setDate*24*60*60;
			} else {
				$expiration = 7*24*60*60;
			}
		} else {
			if(get_field('shared_computer', 'options')) {
				$expiration = get_field('shared_computer', 'options');
			} else {
				$expiration = 600; // 10 minutes
			}
		}
		
		if(PHP_INT_MAX - time() < $expiration) {
			$expiration =  PHP_INT_MAX - time() - 5;
		}
		
		return $expiration;
	}
	add_filter('auth_cookie_expiration', 'my_expiration_filter', 99, 3);
	
	
	// Add a div wrapper to video embeds from the CMS
	add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;

	function custom_oembed_filter($html, $url, $attr, $post_ID) {
		$return = '<div class="video-container">'.$html.'</div>';
		return $return;
	}
	
	// Increase the number of items shown in search results without adjusting it in the CMS
	function change_wp_search_size($queryVars) {
		if ( isset($_REQUEST['s']) ) // Make sure it is a search page
			$queryVars['posts_per_page'] = -1; // Change a number to limit it or leave as -1 to have no limit.
		return $queryVars; // Return our modified query variables
	}
	add_filter('request', 'change_wp_search_size'); // Hook our custom function onto the request filter
	
	// Add Custom Fields to search - Only seems to work in the CMS. Need to investigate the Front End Quries
	// Join posts and postmeta tables
	function cf_search_join( $join ) {
		global $wpdb;
	
		if ( is_search() ) {	
			$join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
		}
		
		return $join;
	}
	add_filter('posts_join', 'cf_search_join' );
	
	// Modify the search query with posts_where
	function cf_search_where( $where ) {
		global $pagenow, $wpdb;
		
		if ( is_search() ) {
			$where = preg_replace(
				"/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
				"(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
		}
	
		return $where;
	}
	add_filter( 'posts_where', 'cf_search_where' );
	
	//Prevent duplicates
	function cf_search_distinct( $where ) {
		global $wpdb;
	
		if ( is_search() ) {
			return "DISTINCT";
		}
	
		return $where;
	}
	add_filter( 'posts_distinct', 'cf_search_distinct' );
	
	// Remove admin menus
	function remove_menus() {
		if(!current_user_can('deity_iykaa')) {
			remove_menu_page('edit.php?post_type=page'); // Pages
			remove_menu_page('edit-comments.php'); // Comments
			remove_menu_page('themes.php'); // Appearance
			remove_menu_page('plugins.php'); // Plugins
			remove_menu_page('tools.php'); // Tools
			remove_menu_page('options-general.php'); // Settings
			remove_menu_page('edit.php?post_type=acf-field-group'); // ACF
			remove_menu_page('admin.php?page=wpcf7'); // Contact Form 7
		}
	}
	add_action('admin_menu', 'remove_menus');
	
	// Remove Options Pages
	function remove_menus_options() {
		if(!current_user_can('deity_iykaa','admin_iykaa')) {
			remove_menu_page('admin.php?page=acf-options-system-options'); // System Options
			remove_menu_page('admin.php?page=acf-options-admin-options'); // Admin Options
		}
	}
	add_action('admin_menu', 'remove_menus_options');
	
	// Remove Dashboard Boxes
	function hideAdminMenus() {
		if(!current_user_can('deity_iykaa')) {
			echo '<style>
				#toplevel_page_acf-options-admin-options,
				#toplevel_page_acf-options-system-options,
				#toplevel_page_user_action_log,
				#toplevel_page_onesignal-push {
					display: none !important;
				}
			</style>';
		}
	}
	add_action('admin_head', 'hideAdminMenus');
	
	/*
		// Changes Posts to What's Happening
		function revcon_change_post_label() {
			global $menu;
			global $submenu;
			$menu[5][0] = 'What\'s Happening';
			$submenu['edit.php'][5][0] = 'What\'s Happening';
			$submenu['edit.php'][10][0] = 'Add Article';
			$submenu['edit.php'][16][0] = 'Article Tags';
		}
		add_action('admin_menu', 'revcon_change_post_label');
	*/
	
	function revcon_change_post_object() {
		global $wp_post_types;
		$labels = &$wp_post_types['post']->labels;
		$labels->name = 'What\'s Happening';
		$labels->singular_name = 'What\'s Happening';
		$labels->add_new = 'Add Article';
		$labels->add_new_item = 'Add Article';
		$labels->edit_item = 'Edit Article';
		$labels->new_item = 'What\'s Happening';
		$labels->view_item = 'View Articles';
		$labels->search_items = 'Search Articles';
		$labels->not_found = 'No Articles Found';
		$labels->not_found_in_trash = 'No articles Found in Bin';
		$labels->all_items = 'All Articles';
		$labels->menu_name = 'What\'s Happening';
		$labels->name_admin_bar = 'What\'s Happening';
	}
	add_action('init', 'revcon_change_post_object');

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );
	
	
	// Group Search Results By Post Types
	
	function group_by_post_type($orderby, $query) {
		global $wpdb;
		if ($query->is_search) {
			return $wpdb->posts . '.post_type DESC';
		}
		// provide a default fallback return if the above condition is not true
		return $orderby;
	}

	add_filter('posts_orderby', 'group_by_post_type', 10, 2);
		
	add_action( 'admin_bar_menu', function ( $wp_admin_bar ) {
	if ( ! is_admin() ) {
		return;
	}
	$wp_admin_bar->remove_node( 'view-site' );

		$wp_admin_bar->add_menu( array(
			'id'	=> 'view-site',
			'title' => __( 'Return to Enyware' ),
			'href'  => home_url( '/dashboard' ),
		) );
	}, 31 );
	
	
	/*
		Document Management System
		
		Emails members if a document they are assigned to has been edited/updated.
	*/
	function dms_file_edited($post_id, $post) {
		$postID = $post->ID;
		
		$emailAddresses = array();
		// Waits two minutes after a file has been modified
		$currentTime = strtotime('-10 minutes');
		
		// Access
		if(get_field('departmental_access', $postID)) {
			
		} else {
			if(get_field('member_access', $postID)) {
				foreach(get_field('member_access', $postID) as $details) {
					while(have_rows('email_addresses', $details->ID)) {
						the_row();
						array_push($emailAddresses, get_sub_field('email_address'));
					}
				}
				
				$subjectLine = 'A document you have access to has been updated.';
				$emailContent = 'A document by the name of <a href="' . get_permalink($postID) . '"><strong>' . get_the_title($postID) . '</strong></a> has been updated.';
				
				if($currentTime > get_the_modified_time('U')) { // Waits two minutes after a file has been modified
					wp_mail($emailAddresses, $subjectLine, $emailContent);
				}
			}
		}
		
		unset($emailAddresses);
		
		$emailAddresses = array();
		
		// Author
		if(get_field('author_access')) {
			foreach(get_field('author_access', $postID) as $details) {
				while(have_rows('email_addresses', $details->ID)) {
					the_row();
					array_push($emailAddresses, get_sub_field('email_address'));
				}
			}
			
			$subjectLine = 'A document you are a co-author of has been edited.';
			$emailContent = 'A document by the name of <a href="' . get_permalink($postID) . '"><strong>' . get_the_title($postID) . '</strong></a> has been edited.';
			
			if($currentTime > get_the_modified_time('U')) { // Waits two minutes after a file has been modified
				wp_mail($emailAddresses, $subjectLine, $emailContent);
			}
		}
	}
	add_action('save_post', 'dms_file_edited', 20, 2);
	
	function my_edit_movie_columns($columns) {
		$columns = array(
			'cb' 			=> '<input type="checkbox" />',
			'title' 		=> __('File Name'),
			'members' 		=> __('Member Access'),
			'authors' 		=> __('Authors'),
			'department' 	=> __('Department'),
			'position' 		=> __('Position'),
			'tags' 			=> __('Tags'),
			'date' 			=> __('Date')
		);
		
		return $columns;
	}
	add_filter('manage_edit-iykaa_dms_columns', 'my_edit_movie_columns');
	
	function my_manage_movie_columns($column, $post_id) {
		global $post;
		
		switch($column) {
			case 'members':
				$memberAccess = get_field('member_access', $post_id);
				
				if(!empty($memberAccess)) {
					$out = array();
					
					foreach($memberAccess as $member) {
						$out[] = $member->post_title;
					}
					
					echo join(', ', $out);
				} else {
					echo 'No members have been set.';
				}
				
				break;
			case 'authors':
				$authorAccess = get_field('author_access', $post_id);
				
				if(!empty($authorAccess)) {
					$out = array();
					
					foreach($authorAccess as $author) {
						$out[] = $author->post_title;
					}
					
					echo join(', ', $out);
				} else {
					echo 'No authors have been set.';
				}
				
				break;
			case 'department':
				$terms = get_the_terms($post_id, 'department_tax');
				
				if(!empty($terms)) {
					$out = array();
					
					foreach($terms as $term) {
						$out[] = sprintf('<a href="%s">%s</a>',
							esc_url(add_query_arg( array('post_type' => $post->post_type, 'department_tax' => $term->slug), 'edit.php')),
							esc_html(sanitize_term_field('name', $term->name, $term->term_id, 'department_tax', 'display'))
						);
					}
					
					echo join(', ', $out);
				} else {
					_e('No departments have been set.');
				}
				
				break;
			case 'position':
				$terms = get_the_terms($post_id, 'position_tax');
				
				if(!empty($terms)) {
					$out = array();
					
					foreach($terms as $term) {
						$out[] = sprintf('<a href="%s">%s</a>',
							esc_url(add_query_arg( array('post_type' => $post->post_type, 'position_tax' => $term->slug), 'edit.php')),
							esc_html(sanitize_term_field('name', $term->name, $term->term_id, 'position_tax', 'display'))
						);
					}
					
					echo join(', ', $out);
				} else {
					_e('No positions have been set.');
				}
				
				break;
			case 'tags':
				$terms = get_the_terms($post_id, 'post_tag');
				
				if(!empty($terms)) {
					$out = array();
					
					foreach($terms as $term) {
						$out[] = sprintf('<a href="%s">%s</a>',
							esc_url(add_query_arg( array('post_type' => $post->post_type, 'post_tag' => $term->slug), 'edit.php')),
							esc_html(sanitize_term_field('name', $term->name, $term->term_id, 'post_tag', 'display'))
						);
					}
					
					echo join(', ', $out);
				} else {
					_e('No positions have been set.');
				}
				
				break;
			default:
				break;
		}
	}
	add_action('manage_iykaa_dms_posts_custom_column', 'my_manage_movie_columns', 10, 2);
	
	
	function dmsMemberCSS() {
		$user = wp_get_current_user();
		$allowedRoles = array('member_iykaa');
		if(array_intersect($allowedRoles, $user->roles)) {
			echo '<style>
				#adminmenumain {
					display: none;
				}
				
				#wpcontent, #wpfooter {
					margin-left: 0;
					padding-left: 20px;
				}
				
				.post-type-iykaa_dms .page-title-action, #wp-admin-bar-new-content, #delete-action, #minor-publishing {
					display: none;
				}
				
				[data-key="field_59369c5d130c0"], [data-key="field_594917d6d3d6f"] {
					display: none !important;
				}
			</style>';
		}
	}
	add_action('admin_head', 'dmsMemberCSS');
	
	function dmsEditCSS() {
		$user = wp_get_current_user();
		$allowedRoles = array('member_iykaa');
		if(isset($_GET['display']) && $_GET['display'] == 'iframe') {
			echo '<style>
				html.wp-toolbar {
					padding-top: 0;
				}
				
				#wpcontent, #wpfooter {
					margin-left: 0;
					padding-left: 20px;
				}
				
				#adminmenumain {
					display: none;
				}
				
				#revisionsdiv, #wpadminbar, .update-nag, #screen-options-link-wrap {
					display: none;
				}
			</style>';
		}
	}
	add_action('admin_head', 'dmsEditCSS');
	
	function dmsMemberJS() {
		$user = wp_get_current_user();
		$allowedRoles = array('member_iykaa');
		if(array_intersect($allowedRoles, $user->roles)) {
			echo '<script>
				document.getElementById("title").disabled = true;
			</script>';
		}
	}
	add_action('admin_footer', 'dmsMemberJS');
	
	function dmsDefaultDepartment($post_id, $post) {
		if('iykaa_dms' === $post->post_type) {
			$defaults = array(
				'department_tax' => array(
					'all'
				),
				'position_tax' => array(
					'all'
				),
			);
			
			$taxonomies = get_object_taxonomies($post->post_type);
			
			foreach($taxonomies as $taxonomy) {
				$terms = wp_get_post_terms($post_id, $taxonomy);
				
				if(empty($terms) && array_key_exists($taxonomy, $defaults)) {
					wp_set_object_terms($post_id, $defaults[$taxonomy], $taxonomy);
				}
			}
			
			// Add the last files file size
			if(have_rows('file_settings')) {
				$searchTerms = '';
				include('includes/dms/functions/pdf-to-text.php');
				
				while(have_rows('file_settings')) {
					the_row();
					
					$fileSizes = array();
					$fileID = get_sub_field('select_file')['id'];
					$fileSize = filesize(get_attached_file($fileID));
					$fileType = pathinfo(get_attached_file($fileID), PATHINFO_EXTENSION);
					
					$a = new PDF2Text();
					$a->setFilename(get_attached_file($fileID));
					$a->decodePDF();
					$searchTerms .= $a->output();
					
					if($fileType == 'docx') {
						$filename = get_attached_file($fileID);
						$striped_content = '';
						$content = '';
						
						$zip = zip_open($filename);
						
						while ($zip_entry = zip_read($zip)) {
						
							if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
						
							if (zip_entry_name($zip_entry) != "word/document.xml") continue;
						
							$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
						
							zip_entry_close($zip_entry);
						}
						zip_close($zip);	  
						$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
						$content = str_replace('</w:r></w:p>', "\r\n", $content);
						$striped_content = strip_tags($content);
						$searchTerms .= $striped_content;
					}
					
					$searchTerms .= $fileType;
					$searchTerms .= get_sub_field('file_name_dms');
					$searchTerms .= get_sub_field('summary_notes');
				}
				update_post_meta($post_id, 'dms_file_size', $fileSize);
				
				// File type
				update_post_meta($post_id, 'dms_file_type', $fileType);
				
				// Revision count
				update_post_meta($post_id, 'dms_revision_count', count(get_field('file_settings')));
				
				update_post_meta($post_id, 'search_terms_raw', $searchTerms);
			}
		}
		
		if('iykaa_library' === $post->post_type) {
			$searchTerms = '';
			
			// Adds the title
			$searchTerms .= get_the_title($post_id);
			
			// Adds the content
			$content_post = get_post($post_id);
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			$searchTerms .= $content;
			
			// Gets the file names
			if(have_rows('download')) {
				while(have_rows('download')) {
					the_row();
					$searchTerms .= get_sub_field('file_name');
					$searchTerms .= get_sub_field('file');
				}
			}
			
			update_post_meta($post_id, 'search_terms_raw', $searchTerms);
		}
	}
	add_action('save_post', 'dmsDefaultDepartment', 20, 2);

	// Use this to update the meta information in the library - only run once then dissable. 	
	function libraryPostUpdater() {
		$args = array(
			'post_type' => 'iykaa_library',
			'posts_per_page' => -1
		);
		
		$libraryPosts = new WP_Query($args);
		while($libraryPosts->have_posts()) {
			$libraryPosts->the_post();
			
			$searchTerms = '';
			
			// Adds the title
			$searchTerms .= get_the_title();
			
			// Adds the content
			$content_post = get_post();
			$content = $content_post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			$searchTerms .= $content;
			
			// Gets the file names
			if(have_rows('download')) {
				while(have_rows('download')) {
					the_row();
					$searchTerms .= get_sub_field('file_name');
					$searchTerms .= get_sub_field('file');
				}
			}
			
			update_post_meta(get_the_ID(), 'search_terms_raw', $searchTerms);
		}
	}
	// Uncomment this to activate the updater for the library posts
	// Recomment it after a refresh
	// add_action('init', 'libraryPostUpdater');
	
	// This include is where deployment spesific functions should be added to keep them from the core installation.
	include('customisations/deployment-functions.php');
	
	// Amends the Username and Email Address to just Email Address in the lost password section
	function lostPasswordText($translated) {
		$translated = str_ireplace('Username or Email Address',  '<strong>Email Address</strong>',  $translated);
		return $translated;
	}
	add_filter('gettext',  'lostPasswordText');
	
	// Redirects from the default wp-login.php page
	function prevent_wp_login() {
		global $pagenow;
		
		$action = (isset($_GET['action'])) ? $_GET['action'] : '';
		
		if($pagenow == 'wp-login.php' && (! $action || ($action && ! in_array($action, array('logout', 'lostpassword', 'rp', 'resetpass'))))) {
			$page = home_url();
			wp_redirect($page);
			exit();
		}
	}
	add_action('init', 'prevent_wp_login');
	
	// Allows for the post_tag to show custom post types
	function wpse28145_add_custom_types($query) {
		if(is_tag() && $query->is_main_query()) {
			
			// this gets all post types:
			$post_types = get_post_types();
			
			// alternately, you can add just specific post types using this line instead of the above:
			// $post_types = array( 'post', 'your_custom_type' );
			
			$query->set('post_type', $post_types);
		}
	}
	add_filter('pre_get_posts', 'wpse28145_add_custom_types');
	
	// Remove admin menus
	function enyware_remove_menus() {
		$userID = get_current_user_id();
		
		if($userID != 1) {
			remove_menu_page('index.php');                  			// Dashboard
			remove_menu_page('upload.php');                 			// Media
			remove_menu_page('edit-comments.php');          			// Comments
			remove_menu_page('themes.php');                 			// Appearance
			remove_menu_page('plugins.php');                			// Plugins
			remove_menu_page('tools.php');                  			// Tools
			remove_menu_page('options-general.php');        			// Settings
			// remove_menu_page('users.php');        					// Users
			remove_menu_page('edit.php?post_type=acf-field-group'); 	// ACF
			remove_menu_page('wpcf7'); 									// CF7
			remove_menu_page('user_action_log'); 						// User Activity Log
			remove_menu_page('onesignal-push'); 						// OneSignal
		}
	}
	add_action('admin_init', 'enyware_remove_menus');
	
	// Remove the original update nag
	function enyware_remove_core_update_nag() {
		remove_action('admin_notices', 'update_nag', 3);
		remove_action('network_admin_notices', 'update_nag', 3);
	}
	add_action( 'admin_menu', 'enyware_remove_core_update_nag', 2 );
	
	// Custom update nag
	function enyware_custom_update_nag() {
		if(is_multisite() && !current_user_can('update_core'))
			return false;
		
		global $pagenow;
		
		if('update-core.php' == $pagenow)
			return;
		
		$cur = get_preferred_from_update_core();
		
		if(!isset($cur->response) || $cur->response != 'upgrade')
			return false;
		
		$enywareTheme = wp_get_theme();
		if(current_user_can('update_core')) {
			$msg = sprintf(__('Enyware ' . $enywareTheme->get('Version') . ' is available!  <a href="' . $enywareTheme->get('ThemeURI') . '">Please contact us now</a>.'), $cur->current, 'your_custom_url');
		} else {
			$msg = sprintf( __('<a href="http://codex.wordpress.org/Version_%1$s">WordPress %1$s</a> is available! Please notify the site administrator.'), $cur->current);
		}
		
		echo "<div class='update-nag'>$msg</div>";
	}
	add_action('admin_notices', 'enyware_custom_update_nag', 99);
	add_action('network_admin_notices', 'enyware_custom_update_nag', 99);
	
	// Custom columns
	function blu_edit_columns($columns) {
		$columns = array(
			'cb' 			=> '<input type="checkbox" />',
			'title' 		=> __('Member'),
			'last-login' 	=> __('Last Login Date'),
			'date' 			=> __('Registration Date')
		);
		
		return $columns;
	}
	add_filter('manage_edit-iykaa_members_columns', 'blu_edit_columns');
	
	function blu_manage_columns($column, $post_id) {
		global $post;
		
		switch($column) {
			case 'last-login':
				$lastLogin = get_field('member_last_login');
				$firstLogin = get_field('member_first_login_date');
				
				if($lastLogin) {
					echo date('Y/m/d', strtotime($lastLogin));
				} else {
					echo date('Y/m/d', strtotime($firstLogin));
				}
				
				break;
			default:
				break;
		}
	}
	add_action('manage_iykaa_members_posts_custom_column', 'blu_manage_columns', 10, 2);
	
	// Adds a registration date to the useres column
	function blu_modify_user_table($columns) {
		$columns['registration_date'] = 'Registration Date';
		
		return $columns;
	}
	add_filter('manage_users_columns', 'blu_modify_user_table');
	
	function blu_modify_user_table_row($row_output, $column_id_attr, $user) {
		$date_format = 'Y/m/d';
		
		switch($column_id_attr) {
			case 'registration_date' :
				return date($date_format, strtotime(get_the_author_meta('registered', $user)));
				break;
			default:
		}
		
		return $row_output;
	}
	add_filter('manage_users_custom_column', 'blu_modify_user_table_row', 10, 3);
	 
	function blu_make_registered_column_sortable($columns) {
		return wp_parse_args(array('registration_date' => 'registered'), $columns);
	}
	add_filter('manage_users_sortable_columns', 'blu_make_registered_column_sortable');
	
	function ipLoginChecker($redirect) {
		// Sets and gets the list of IP addresses
		$ipAddress = array();
		if(have_rows('ip_addresses', 'options')) {
			while(have_rows('ip_addresses', 'options')) {
				the_row();
				array_push($ipAddress, get_sub_field('ip_address'));
			}
		}
		
		if(in_array($_SERVER['REMOTE_ADDR'], $ipAddress)) {
			include(get_template_directory() . '/includes/ip-login.php');
			include($redirect);
		} else {
			wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
		}
	}
	
	// Shortcodes
	// Part of the user registration that will change the [cta] links
	function get_string_between($string, $start, $end) {
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		
		if($ini == 0) {
			return '';
		}
		
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		
		return substr($string, $ini, $len);
	}
	
add_action( 'pre_get_posts', function( $q )
{
    if( $title = $q->get( '_meta_or_title' ) )
    {
        add_filter( 'get_meta_sql', function( $sql ) use ( $title )
        {
            global $wpdb;

            // Only run once:
            static $nr = 0; 
            if( 0 != $nr++ ) return $sql;

            // Modified WHERE
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );

            return $sql;
        });
    }
});
?>