<!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title><?php wp_title(''); ?> | <?php bloginfo('name'); ?> | Enyware</title>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Manifest for PWA view -->
	<meta name="theme-color" content="#B9DCD3" />
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/manifest.json">
	<!-- Load the service worker if possible -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/register-sw.js" type="text/javascript" charset="utf-8"></script>
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/sw.js" type="text/javascript" charset="utf-8"></script>
	
	<!-- iOS Web App View Meta -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> | Enyware">
	<link rel="apple-touch-icon" href="<?php echo home_url(); ?>/images/touch-icon-iphone.png">

	<!-- Windows Tile Meta -->
	<meta name="application-name" content="Enyware" />
	<meta name="msapplication-TileColor" content=" #B9DCD3" />
	<!-- Logos for all four tile sizes -->
	<meta name="msapplication-square70x70logo" content="<?php echo home_url(); ?>/images/smalltile.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo home_url(); ?>/images/mediumtile.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo home_url(); ?>/images/widetile.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo home_url(); ?>/images/largetile.png" />

	<!-- Web App View Link Management -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/compressed.js" type="text/javascript" charset="utf-8"></script>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="shortcut icon" href="<?php echo home_url(); ?>/images/favicon.ico">

	<?php include('includes/all/ga.php'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<?php include('includes/config/member-details.php'); ?>
		<div class="slide-menu">
			<div class="btn-close" id="menu-trigger">
				<?php $logo = get_field('logo_svg_so', 'options'); ?>
				<img src="<?php echo $logo['url']; ?>">
				<div class="menu-button open">
					<span class="burger-icon open"></span>
				</div>
				<div id="menu-mobile"></div>
			</div>
			<nav class="side-nav">
				<div class="sub-page-list">
					<ul>
						<li class="home"><a href="<?php echo home_url(); ?>">Home</a></li>
					</ul>
				</div>
			</nav>
			
			<?php
				$user = wp_get_current_user();
				$allowed_roles = array('deity_iykaa', 'admin_iykaa');
				if( array_intersect($allowed_roles, $user->roles ) ) {  ?> 
				<ul class="admin-menu">
					<li class="admin-area">
						<a href="<?php echo home_url(); ?>/wp-admin">Admin Area</a>
					</li>
				</ul>
			<?php } ?>
			<div class="login">
				<?php if(is_user_logged_in()) { ?>
					<a href="<?php echo wp_logout_url(home_url('/logged-out')); ?>" class="logout">
						Logout
					</a>
				<?php } else { ?>
					<a href="<?php echo home_url('/login'); ?>" class="sign-in">
						Sign In
					</a>
				<?php } ?>
				<small>Powered by <a href="https://www.meetiykaa.com" target="_blank">Enyware</a>.</small>
			</div>
		</div>
	</header>