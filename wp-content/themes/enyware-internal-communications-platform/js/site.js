// @codekit-prepend 'html5shiv.min.js';
// @codekit-prepend 'jquery.cookie.js';

jQuery(document).ready(function($) {
	$('html').removeClass('no-js');
	$('html').addClass('js');
	
	// Remove empty p tags
	$('p:empty').remove();
	
	$('p').filter(function(){
		return $.trim(this.innerHTML) === '&nbsp;'
	}).remove();
	
	$('span').filter(function(){
		return $.trim(this.innerHTML) === '&nbsp;'
	}).remove();
	
	// Responsive navigation
	function responsiveMenu() {
		var windowWidth = $(window).innerWidth();
		
		if(windowWidth < 1441) {
			$('#menu-mobile').html($('#menu-desktop').html());
		} else {
			$('#menu-mobile').empty();
		}
		if(windowWidth < 1024) {
			$('#menu-mobile-library').html($('#library-menu').html());
		} else {
			$('#menu-mobile-library').empty();
		}
	}
	responsiveMenu();
	
	$('#menu-trigger').click(function() {
		if ($('#menu-mobile').hasClass('expanded')) {
			$('#menu-mobile.expanded').removeClass('expanded').slideUp(1000);
			$('#menu-trigger span').addClass('open');
			$('.menu-button').addClass('open');
			$('html').removeClass('body-overflow');
		} else {
			$('#menu-mobile').addClass('expanded').slideDown(1000);
			$('#menu-trigger span').removeClass('open');
			$('.menu-button').removeClass('open');
			$('html').addClass('body-overflow');
		}
	});
	
	// Guidelines menu
	$('.guidelines-menu span').click(function() {
		if($('.library-menu').hasClass('expanded')) {
			$('.library-menu.expanded').removeClass('expanded').slideUp(1000);
			$(this).addClass('open');
			$('.menu-button').addClass('open');
		} else {
			$('.library-menu').addClass('expanded').slideDown(1000);
			$(this).removeClass('open');
			$('.menu-button').removeClass('open');
		}
	});
	
	// Guidelines advanced search reveal
	$('.advanced span').click(function() {
		if($('.advanced .fields').hasClass('expanded')) {
			$('.advanced .fields').removeClass('expanded').slideUp(500);
		} else {
			$('.advanced .fields').addClass('expanded').slideDown(500);
		}
	});
	
	// Checks to see if a link is external
	var comp = new RegExp(location.host);
	$('.single-iykaa_library_custom a').each(function() {
		// Zip file
		if(this.pathname.substr(-4) === '.zip') {
			$(this).addClass('download-file zip');
		}
		
		// Doc file
		if(this.pathname.substr(-4) === '.doc') {
			$(this).addClass('download-file doc');
		}
		
		// Docx file
		if(this.pathname.substr(-4) === 'docx') {
			$(this).addClass('download-file doc');
		}
		
		// PDF file
		if(this.pathname.substr(-4) === '.pdf') {
			$(this).addClass('download-file pdf');
		}
		
		// XLSX file
		if(this.pathname.substr(-5) === '.xlsx') {
			$(this).addClass('download-file xlsx');
		}
	});
	
	$('.single-nursing_competencies a').each(function() {
		// Zip file
		if(this.pathname.substr(-4) === '.zip') {
			$(this).addClass('download-file zip');
		}
		
		// Doc file
		if(this.pathname.substr(-4) === '.doc') {
			$(this).addClass('download-file doc');
		}
		
		// Docx file
		if(this.pathname.substr(-4) === 'docx') {
			$(this).addClass('download-file doc');
		}
		
		// PDF file
		if(this.pathname.substr(-4) === '.pdf') {
			$(this).addClass('download-file pdf');
		}
		
		// XLSX file
		if(this.pathname.substr(-5) === '.xlsx') {
			$(this).addClass('download-file xlsx');
		}
	});
	
	$('.guidelines a').each(function() {
		// Zip file
		if(this.pathname.substr(-4) === '.zip') {
			$(this).addClass('download-file zip');
		}
		
		// Doc file
		if(this.pathname.substr(-4) === '.doc') {
			$(this).addClass('download-file doc');
		}
		
		// Docx file
		if(this.pathname.substr(-4) === 'docx') {
			$(this).addClass('download-file doc');
		}
		
		// PDF file
		if(this.pathname.substr(-4) === '.pdf') {
			$(this).addClass('download-file pdf');
		}
		
		// XLSX file
		if(this.pathname.substr(-5) === '.xlsx') {
			$(this).addClass('download-file xlsx');
		}
	});
	
	$('.mobile-library-menu > p').click(function() {
		if($('.mobile-library-menu > ul').hasClass('expanded')) {
			$('.mobile-library-menu > ul.expanded').removeClass('expanded').slideUp(500);
			$('html').removeClass('body-overflow');
		} else {
			$('.mobile-library-menu > ul').addClass('expanded').slideDown(500);
			$('html').addClass('body-overflow');
		}
	});
	
	function loadNav() {
		var windowWidth = $(window).innerWidth();
		
		if(windowWidth >= 1260) {
			$('.slide-menu').css('left', 0);
			$('.btn-close').addClass('open');
			
			if($.cookie('menu-state') == 'open') {
				$('body.logged-in').css('padding-left', '280px');
				$('.slide-menu').css('left', 0);
				$('#menu-mobile').removeClass('expanded');
				$('.btn-close').addClass('open');
				$('.menu-button').addClass('open');
				$('#menu-trigger span').addClass('open');
			} else if($.cookie('menu-state') == 'closed') {
				$('body.logged-in').css('padding-left', '45px');
				$('.slide-menu').css('left', '-234px');
				$('#menu-mobile').addClass('expanded');
				$('.btn-close').removeClass('open');
				$('.menu-button').removeClass('open');
				$('#menu-trigger span').removeClass('open');
			} else {
				$('body.logged-in').css('padding-left', '280px');
				$('.slide-menu').css('left', 0);
				$('#menu-mobile').removeClass('expanded');
				$('.btn-close').addClass('open');
				$('.menu-button').addClass('open');
				$('#menu-trigger span').addClass('open');
			}
		} else {
			$('.slide-menu').css('right', '-280px');
			$('#menu-mobile').addClass('expanded');
			$('.btn-close').removeClass('open');
			$('.menu-button').removeClass('open');
			$('#menu-trigger span').removeClass('open');
	    }
	}
	loadNav();
	
	function openNav() {
		var windowWidth = $(window).innerWidth();
		
		$.cookie('menu-state', 'open', { expires: 365, path: '/' });
		
		$('body.logged-in').css('transition', '0.5s');
		
		if(windowWidth >= 1260) {
			$('body.logged-in').css('padding-left', '280px');
			$('#menu-mobile').removeClass('expanded');
			$('.btn-close').addClass('open');
			$('.menu-button').addClass('open');
			$('#menu-trigger span').addClass('open');
			
			$(".super-search").css('width','calc(100vw - 280px)');
			
		} else {
			$('#menu-mobile').addClass('expanded');
			$('.btn-close').removeClass('open');
			$('.menu-button').removeClass('open');
			$('#menu-trigger span').removeClass('open');
		}
	    
		$('.slide-menu').css('left', 0);
		$('.btn-close').addClass('open');
	}
	
	function closeNav() {
		var windowWidth = $(window).innerWidth();
		
		$.cookie('menu-state', 'closed', { expires: 365, path: '/' });
		$('body.logged-in').css('padding-left', '46px');
		$('.slide-menu').css('left', '-234px');
		
		$('body.logged-in').css('transition', '0.5s');
		
		if(windowWidth >= 1260) {
			$('body.logged-in').css('padding-left', '46px');
			$('#menu-mobile').removeClass('expanded');
			$('.btn-close').removeClass('open');
			$('.menu-button').removeClass('open');
			$('#menu-trigger span').removeClass('open');
			
			$(".super-search").css('width','calc(100vw - 45px)');	
			
		} else {
			$('#menu-mobile').addClass('expanded');
			$('.btn-close').removeClass('open');
			$('.menu-button').removeClass('open');
			$('#menu-trigger span').removeClass('open');
		}
	}
	
	$('body.page-template-page-introduction').css('padding-left', '0px');
	
	$('.btn-close').click(function() {
		var windowWidth = $(window).innerWidth();
		
		if(windowWidth >= 1260) {
			if($(this).hasClass('open')) {
				$(this).removeClass('open');
				closeNav();
			} else {
				openNav();
			}
		}
	});
	
	$('.mobile-bottom .menu').click(function() {
		if($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('body.logged-in').css('padding-right', '0');
			$('.slide-menu').css('right', '-280px');
		} else {
			$(this).addClass('open');
			$('.slide-menu').css('right', 0);
			$('.slide-menu').css('opacity', 1);
			$('.btn-close').addClass('open');
		}
		
		return false;
	});
	
	$('.header-availability').change(function() {
		this.form.submit();
	});
	
	function blocks() {	
		// These are for proportionate sizing
		// var windowWidth = $(window).innerWidth();
		// var windowHeight = $(window).innerHeight();
		
		//Avatar resizing
		$('.avatar').css('height', $('.avatar').outerWidth());
		$('.avatar-similar').css('height', $('.avatar-similar').outerWidth());
	}
	blocks();
	
	if($('body.page-template-page-first-login').length > 0 || $('body.page-template-page-register').length > 0) {
	
		// Password strength
		var strPassword;
		var charPassword;
		// var complexity = $('#password-one');
		var progressBar = $('.progress-bar');
		var minPasswordLength = 6;
		var baseScore = 0, score = 0;
		
		var num 			= {};
		num.Excess 			= 0;
		num.Upper 			= 0;
		num.Numbers 		= 2;
		num.Symbols 		= 0;
		
		var bonus 			= {};
		bonus.Excess 		= 3;
		bonus.Upper 		= 4;
		bonus.Numbers 		= 5;
		bonus.Symbols 		= 5;
		bonus.Combo 		= 0;
		bonus.FlatLower 	= 0;
		bonus.FlatNumber 	= 0;
		
		outputResult();
		
		$('#password-one').bind('keyup', checkVal);
		$('#password-two').bind('keyup', passwordMatch);
		
		function passwordMatch() {
			if($('#password-one').val() == $('#password-two').val() && $('#password-one').val() != '') {
				$('.password-match').addClass('match');
			} else {
				$('.password-match').removeClass('match');
			}
		}
		
		function checkVal() {
			init();
			
			if(charPassword.length >= minPasswordLength) {
				baseScore = 50;
				analyzeString();
				calcComplexity();	
			} else {
				baseScore = 0;
			}
			
			outputResult();
		}
		
		function init() {
			strPassword 		= $('#password-one').val();
			charPassword 		= strPassword.split('');
			num.Excess 			= 0;
			num.Upper 			= 0;
			num.Numbers 		= 0;
			num.Symbols 		= 0;
			bonus.Combo 		= 0;
			bonus.FlatLower 	= 0;
			bonus.FlatNumber 	= 0;
			baseScore 			= 0;
			score 				= 0;
		}
		
		function analyzeString() {
			for(i =0; i < charPassword.length;i++) {
				if(charPassword[i].match(/[A-Z]/g)) {
					num.Upper++;
				}
				
				if(charPassword[i].match(/[0-9]/g)) {
					num.Numbers++;
				}
				
				if(charPassword[i].match(/(.*[!,@,#,$,%,^,&,*,?,_,~])/)) {
					num.Symbols++;
				}
			}
			
			num.Excess = charPassword.length - minPasswordLength;
			
			if(num.Upper && num.Numbers && num.Symbols) {
				bonus.Combo = 25;
			} else if((num.Upper && num.Numbers) || (num.Upper && num.Symbols) || (num.Numbers && num.Symbols)) {
				bonus.Combo = 15;
			}
			
			if(strPassword.match(/^[\sa-z]+$/)) {
				bonus.FlatLower = -15;
			}
			
			if(strPassword.match(/^[\s0-9]+$/)) {
				bonus.FlatNumber = -35;
			}
		}
		
		function calcComplexity() {
			score = baseScore + (num.Excess * bonus.Excess) + (num.Upper * bonus.Upper) + (num.Numbers * bonus.Numbers) + (num.Symbols * bonus.Symbols) + bonus.Combo + bonus.FlatLower + bonus.FlatNumber;
		}
		
		function outputResult() {
			if($('#password-one').val()== '') {
				progressBar.removeClass('weak good strong').addClass('default');
				$('.password-strength').val('default');
			} else if(charPassword.length < minPasswordLength) {
				progressBar.removeClass('weak good strong').addClass('weak');
				$('.password-strength').val('weak');
			} else if(score < 25) {
				progressBar.removeClass('weak good strong').addClass('weak');
				$('.password-strength').val('weak');
			} else if(score >= 25 && score < 84) {
				progressBar.removeClass('weak good strong').addClass('good');
				$('.password-strength').val('good');
			} else if(score >= 84) {
				progressBar.removeClass('weak good strong').addClass('good strong');
				$('.password-strength').val('strong');
			}
		}
	}
	
	// Adds a link to a tables tr if needed
	$('.row-link').click(function() {
		window.location = $(this).data('href');
	});
	
	// Toggle the Super Search Slide Down
	$( ".icon-ii-search, .find-out-more-search").click(function() {
		$( ".super-search" ).slideToggle( "slow", function() {
			// Animation complete.
		});
		
		$('.search-field').focus();
	});
	
	$( ".super-search .close").click(function() {
		$( ".super-search" ).slideToggle( "slow", function() {
			// Animation complete.
		});
		
		$('.search-field').focus();
	});
	
	// Toggle the Advanced Filters
	$('.open-filters').on('click touch', function() {
		$('.filters').toggle('slow');
	});
	
	// A to Z Guidelines
		var navTop = $('.guidelines .a-z').offset().top;
	function azGuidelines() {
		var scrollY = $(window).scrollTop();
		
		var azPositionTop = $('.guidelines .results').offset().top;
		var azPositionLeft = $('.guidelines .results').offset().left;
		
		if(scrollY >= navTop) {
			$('.guidelines .a-z').css('position', 'fixed');
			$('.guidelines .a-z').css('top', '10px');
			$('.guidelines .a-z').css('left', azPositionLeft);
		} else if(scrollY < navTop) {
			$('.guidelines .a-z').css('position', 'absolute');
			$('.guidelines .a-z').css('top', '10px');
			$('.guidelines .a-z').css('left', '0px');
		}
	}
	azGuidelines();
	
	$(window).scroll(function() {
		azGuidelines();
	});
	
	$(window).resize(function() {
		blocks();
		responsiveMenu();
	});
});

// iFrame Document Viewer

jQuery(document).ready(function($){
	var windowWidth = $(window).innerWidth();
	var windowHeight = $(window).innerHeight();
	
	$('.document-frame-trigger').on('click touch', function () {
		// Get the link to the document
		var content = $(this).attr('data-document-location');
		
		// Create the iFrame box
		$('body').before('<div class="document-frame"></div>');
		$('.document-frame').html('<iframe src=""></iframe>');
		$('.document-frame iframe').attr('src', content);
		$('.document-frame iframe').attr('width', windowWidth);
		$('.document-frame').css('width', windowWidth);
		$('.document-frame').css('height', windowHeight);
		
		// Add the tools to the frame box
		$('.document-frame > iframe').css('height', windowHeight - 88);
		$('.document-frame iframe').before('<div class="tool-bar top"><span id="document-frame-close" class="frame-close"></span></div>');
		$('.document-frame iframe').after('<div class="tool-bar bottom"><ul><li class="back" id="document-frame-close-bottom"><a href=""> </a></li><li class="download"><a href=""></a></li></ul></div>');
		
		$('.document-frame .download a').attr('href', content);
		
		// Add a close to the frame box
		$('.document-frame').on('click touch', '#document-frame-close', function() {
			$('.document-frame').remove();
		});
		$('.document-frame').on('click touch', '#document-frame-close-bottom', function() {
			$('.document-frame').remove();
		});
		
		$('.document-frame iframe').contents().find('body').addClass('myClass');
		$('.document-frame iframe').contents().find('.myClass').css('width', windowWidth);
		$('.document-frame iframe').contents().find('.myClass').css('margin', 0);
		
	});
});

jQuery(document).ready(function($){

	// add the menu down span after the link for links with children
	$('.sub-page-list li').find('ul.children').siblings('a').before('<span class="menu_updown menu_down"><div class="chevron">&#x63;</div></span>');
	
	$('.mobile-library-menu li').find('ul.children').siblings('a').before('<span class="menu_updown menu_down"><div class="chevron">p</div></span>');
	
	// auto drop down the sidebar menu on the right for the li.current_page_item on page load
	$('.sub-page-list > li.current_page_item').parents('ul.children').show().siblings('span.menu_updown').removeClass('menu_down').addClass('menu_up');
	//if($('li.current_page').hasClass('top_level_page') && $('li.current_page').hasClass('page_item_has_children')){
		$('.sub-page-list > li.current_page_item').children('span.menu_updown').addClass('menu_down').removeClass('menu_up');
		$('.sub-page-list > li.current_page_item').children('.children').show();
		$('.sub-page-list > li.current_page_item').children('span.menu_updown').removeClass('menu_down').addClass('menu_up');
	//}
	$(this).removeClass('menu_down').addClass('menu_up');
	
	// when the plus is clicked on an li in the sidebar menu, open up sub menu
	$('span.menu_updown').click(function() {
		$(this).parents('li').siblings('li').find('ul.children').hide().siblings('span.menu_updown').addClass('menu_down').removeClass('menu_up');
		if($(this).hasClass('menu_down')) {
			$(this).siblings('.children').show();
			$(this).removeClass('menu_down').addClass('menu_up');
		} else if($(this).hasClass('menu_up')) {
			$(this).siblings('.children').hide();
			$(this).removeClass('menu_up').addClass('menu_down');
		}
	});
});