jQuery('.comp-00601 .slides').owlCarousel({
	loop: true,
	margin: 20,
	responsive:{
		0: {
			items: 1
		},
		600: {
			items: 2
		},
		1025: {
			items: 3
		}
	},
	autoplay: true,
	autoplayTimeout: 5000,
	slideSpeed: 1500,
	nav: false,
	lazyLoad: true,
});

jQuery('.comp-00101 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
});

jQuery('.comp-00102 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
});

jQuery('.comp-00103 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
});

jQuery('.comp-00104 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
});

jQuery('.comp-00105 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
	autoHeight: true,
});

jQuery('.comp-00106 .slides').owlCarousel({
	loop: true,
	margin: 0,
	items: 1,
	autoplay: true,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
	autoHeight: true,
});

function callBack() {
	if($('.owl-carousel .owl-item').last().hasClass('active')) {
		$('.owl-prev').css('opacity', '1');
		$('.owl-next').css('opacity', '0.2');
	} else if($('.owl-carousel .owl-item').first().hasClass('active')) {
		$('.owl-prev').css('opacity', '0.2');
		$('.owl-next').css('opacity', '1');
	} else {
		$('.owl-prev').css('opacity', '1');
		$('.owl-next').css('opacity', '1');
	}
}

jQuery('.reg-slider').owlCarousel({
	loop: false,
	margin: 0,
	items: 1,
	autoplay: false,
	autoplayTimeout: 1500,
	slideSpeed: 1500,
	navigation: true,
	lazyLoad: true,
	autoHeight: true,
	mouseDrag: false,
	navText: ["Go Back", "Continue"],
	onTranslated:callBack,
});

/* Slick Slides for Dashboard 2*/
jQuery(document).ready(function($){
	$('.slider').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		],
		arrows: false,
//		fade: true,
		infinite: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 6000,
	});
	
	$('.slider2').slick({
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		],
		arrows: false,
//		fade: true,
		infinite: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 4500,
	});
	
	$('.showcase').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
//		fade: true,
		infinite: true,
		speed: 1000,
		autoplay: true,
		autoplaySpeed: 4500,
	});
});