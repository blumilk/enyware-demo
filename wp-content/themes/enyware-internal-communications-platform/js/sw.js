var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
/* '/',
	'/iykaa-internal-communications-platform/dashboard',
	'/iykaa-internal-communications-platform/wp-content/themes/iykaa-internal-communications-platform/css/style.css',
	'/iykaa-internal-communications-platform/wp-content/themes/iykaa-internal-communications-platform/js/min/site-min.js', */
	'/wp-content/themes/iykaa-internal-communications-platform/css/fonts/iykaa-icons.woff' 
];

self.addEventListener('install', function(event) {
	// Perform install steps
	event.waitUntil(
		caches.open(CACHE_NAME)
		.then(function(cache) {
			console.log('Opened cache');
			return cache.addAll(urlsToCache);
		})
	);
});