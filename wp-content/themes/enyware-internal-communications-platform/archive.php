<?php get_header(); ?>
<section class="page-load">
	<?php if(have_posts()) { ?>
		<div class="wrapper">
			<div class="row">
				<div class="dt-12">
					<?php if(is_day()) { ?>
						<h3>Archive: <?php echo get_the_date('D M Y'); ?></h3>							
					<?php } elseif(is_month()) { ?>
						<h3>Archive: <?php echo get_the_date('M Y'); ?></h3>	
					<?php } elseif(is_year()) { ?>
						<h3>Archive: <?php echo get_the_date('Y'); ?></h3>								
					<?php } else { ?>
						<h3>Archive</h3>	
					<?php } ?>
					<?php while(have_posts()) {
						the_post(); ?>
						<article>
							<h4>
								<a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
									<?php the_title(); ?>
								</a>
							</h4>
							<?php the_content(); ?>
						</article>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="wrapper">
			<div class="row">
				<div class="dt-12">
					<h1>No posts to display</h1>
				</div>
			</div>
		</div>
	<?php } ?>
</section>
<?php get_footer(); ?>