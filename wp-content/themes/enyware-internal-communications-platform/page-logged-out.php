<?php
	/*
		Template Name: Logged Out
	*/
	
	get_header('logged-out');
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			include('includes/public/logged-out.inc.php');
		}
	}
	
	get_footer();
?>