<?php
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/all/member-profile.inc.php');
			} else { // Not logged in
				wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
			}
		}
	}
	
	get_footer();
?>