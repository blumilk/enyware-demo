<div class="admin-tools">
	<h2>Quick Tools</h2>
	<hr>
	<ul class="content-options">
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/post-new.php?post_type=iykaa_notifications" class="notification">
			<span>Post a notification</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/post-new.php" class="happening">
			<span>Update What's Happening</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/edit.php?post_type=iykaa_locations" class="location">
			<span>Edit location info</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/edit.php?post_type=iykaa_departments" class="clipboard">
			<span>Update department info</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/edit.php?post_type=iykaa_library" class="library">
			<span>Update the library</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/edit.php?post_type=iykaa_members" class="user">
			<span>Edit member profile info</span>
		</a></li>
	</ul>
	
	<h2>System Settings</h2>
	<hr>
	<ul class="system-content">
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/admin.php?page=acf-options-system-options" class="edit">
			<span>Change dashboard buttons</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/admin.php?page=iykaa-library-index" class="libary">
			<span>Library Index</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/users.php" class="key">
			<span>Change a user's password</span>
		</a></li>
		<li><a href="<?php echo bloginfo('url');?>/wp-admin/users.php" class="user">
			<span>Remove a user</span>
		</a></li>
	</ul>
</div>