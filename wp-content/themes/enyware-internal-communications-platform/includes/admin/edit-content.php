<?php
	if(is_user_logged_in()) {
		// Only Allow Admins to edit pages. 
		$user = wp_get_current_user();
		$allowed_roles = array('deity_iykaa', 'admin_iykaa');
		if( array_intersect($allowed_roles, $user->roles ) ) {
			
			// Gets the pages ID
			$postID = get_queried_object_id();
			// Gets the url of the website
			$homeURL = home_url();
			// Gets the path of the admin
			$showPost = "/wp-admin/post.php?post=";
			// Places the edit action
			$editAction = "&action=edit";
			// Ties the variables together
			$contentEdit = $homeURL . $showPost . $postID . $editAction;			
			// The HTML
			$theHTML = '
			<div class="edit">
				<a href="' . $contentEdit . '">
					<div class="wrapper no-gaps nested">
						<div class="row">
							<div class="mp-2">
								<div class="i-icon large icon-ii-edit"></div>
							</div>
							<div class="mp-10">
							<p>Edit this content</p>
							</div>
						</div>
					</div>
				</a>
			</div>
			<hr class="secondary">';
			
			// Bringing it all together
			echo $theHTML;
		}
	}
?>