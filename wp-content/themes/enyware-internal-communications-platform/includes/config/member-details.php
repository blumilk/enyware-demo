<?php
	global $current_user;
	
	$args = array(
		'author' => $current_user->ID,
		'post_type' => 'iykaa_members',
		'post_status' => array('pending', 'draft', 'publish'),
	);
	
	$membersProfile = new WP_Query($args);
?>