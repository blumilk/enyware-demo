<aside class="dt-3 tl-4 force">
	<section class="inner popular-search">
		<h3>Search</h3>
		<hr class="secondary size-l">
		<form action="<?php echo home_url(); ?>/clinical-document-type/clinical-guidelines-a-z/" method="get" name="resources">
			<div class="flex-search">
				<input type="hidden" class="paged" value="1" name="paged">
				<?php
					if(isset($_GET['keywords'])) {
						$keywords = $_GET['keywords'];
					} else {
						$keywords = '';
					}
				?>
				<input class=" flex-item" type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
				<button class="cta secondary flex-item" type="submit">Search</button>
			</div>
		</form>
	</section>
	
	<section class="inner calculators">
		<h3>Calculators</h3>
		<hr class="secondary size-l">
		<a href="http://intranet/grace/" target="_blank">
			GRACE - Calculator for ACS Risk
		</a>
		<a href="http://pdmedcalc.co.uk/" target="_blank">
			Parkinson's Disease - Dose Calculator
		</a>
	</section>
	
	<section class="inner">
		<h3>Tags</h3>
		<hr class="secondary size-l">
		<?php
			$args = array(
				'smallest'                  => 8,
				'largest'                   => 18,
				'unit'                      => 'pt',
				'number'                    => 24,
				'format'                    => 'flat',
				'orderby'                   => 'name',
				'order'                     => 'ASC',
				'exclude'                   => null,
				'include'                   => null,
				'link'                      => 'view',
				'taxonomy'                  => 'post_tag',
				'echo'                      => true,
				'child_of'                  => null,
			);
			
			wp_tag_cloud($args);
		?>
	</section>
	
	<?php if(function_exists('mdst_search_results')) { ?>
		<section class="inner popular-search">
			<h3>Popular Searches</h3>
			<hr class="secondary size-l">
			<?php mdst_search_results(); ?>
		</section>
	<?php } ?>
</aside>