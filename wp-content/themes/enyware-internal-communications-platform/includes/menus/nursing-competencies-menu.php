<aside class="dt-3 tl-4 force">
	<section class="inner popular-search">
		<h3>Search</h3>
		<hr class="secondary size-l">
		<form action="<?php echo home_url(); ?>/nursing-competencies/" method="get" name="resources">
			<div class="flex-search">
				<input type="hidden" class="paged" value="1" name="paged">
				<?php
					if(isset($_GET['keywords'])) {
						$keywords = $_GET['keywords'];
					} else {
						$keywords = '';
					}
				?>
				<input class=" flex-item" type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
				<section class="advanced">
					<span><p>Advanced Search</p></span>
					<?php if(isset($_GET['document-type']) && $_GET['document-type'] != 'false' || isset($_GET['orderby']) && $_GET['orderby'] != 'false' || isset($_GET['order']) && $_GET['order'] != 'false') { ?>
						<div class="fields expanded" style="display: block;">
					<?php } else { ?>
						<div class="fields">
					<?php } ?>
						<label>Document Type</label>
						<?php
							if(isset($_GET['document-type'])) {
								$documentType = $_GET['document-type'];
							} else {
								$documentType = 0;
							}
							
							$args = array(
								'show_option_all'    => '',
								'show_option_none'   => 'Document Type',
								'option_none_value'  => 'false',
								'orderby'            => 'title',
								'order'              => 'ASC',
								'show_count'         => 0,
								'hide_empty'         => 1,
								'child_of'           => 0,
								'exclude'            => '',
								'echo'               => 1,
								'selected'           => $documentType,
								'hierarchical'       => 0,
								'name'               => 'document-type',
								'id'                 => '',
								'class'              => 'document-type',
								'depth'              => 0,
								'tab_index'          => 0,
								'taxonomy'           => 'document_type_tax',
								'hide_if_empty'      => false,
								'value_field'	     => 'slug',
							);
						?>
						<?php wp_dropdown_categories($args); ?>
						<div class="row">
							<div class="mp-6">
								<label>Order By</label>
								<?php
									if(isset($_GET['orderby'])) {
										$orderBy = $_GET['orderby'];
									} else {
										$orderBy = 'false';
									}
								?>
								<select name="orderby">
									<option value="false" <?php if($orderBy == 'false') { echo 'selected="selected"'; } ?>>Order By</option>
									<option value="title" <?php if($orderBy == 'title') { echo 'selected="selected"'; } ?>>Title</option>
									<option value="date" <?php if($orderBy == 'date') { echo 'selected="selected"'; } ?>>Date</option>
								</select>
							</div>
							<div class="mp-6">
								<label>Order</label>
								<?php
									if(isset($_GET['order'])) {
										$order = $_GET['order'];
									} else {
										$order = 'false';
									}
								?>
								<select name="order">
									<option value="false" <?php if($order == 'false') { echo 'selected="selected"'; } ?>>Order</option>
									<option value="asc" <?php if($order == 'asc') { echo 'selected="selected"'; } ?>>Ascending</option>
									<option value="desc" <?php if($order == 'desc') { echo 'selected="selected"'; } ?>>Descending</option>
								</select>
							</div>
						</div>
					</div>
				</section>
				<button class="cta secondary flex-item" type="submit">Search</button>
			</div>
		</form>
	</section>
	
	<section class="inner">
		<h3>Tags</h3>
		<hr class="secondary size-l">
		<?php
			$args = array(
				'smallest'                  => 8,
				'largest'                   => 24,
				'unit'                      => 'pt',
				'number'                    => 50,
				'format'                    => 'flat',
				'orderby'                   => 'name',
				'order'                     => 'ASC',
				'exclude'                   => 4463,
				'include'                   => null,
				'link'                      => 'view',
				'taxonomy'                  => 'nursing_competencies_tax',
				'echo'                      => true,
				'child_of'                  => null,
			);
			
			wp_tag_cloud($args);
		?>
	</section>
</aside>