<section class="registration-glimpse dashboard">
	<div class="enyware">
		<div class="wrapper no-gaps">
			<div class="register">
				<div class="row">
					<div class="dt-7 tp-6">
						<h2><small>Welcome to</small> Enyware</h2>
						<h3>The information resource to make your job easier. Access Enyware from any device.</h3>
						<ul>
							<li>
								<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-white-01.png">
								<span>24/7 access to resources on desktop, mobile and tablet</span>
							</li>
							<li>
								<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-white-02.png">
								<span>Tailored content and central contact directory for all staff</span>
							</li>
							<li>
								<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-white-03.png">
								<span>A one stop resource for your company information</span>
							</li>
						</ul>
						<a href="<?php echo home_url(); ?>/register" class="cta secondary size-l">
							Register for Enyware Today
						</a>
					</div>
					<div class="dt-3 tp-5 force">
						<div class="nhs">
							<img src="<?php echo home_url(); ?>/images/black-logo.png">
							<form class="sign-in" action="<?php echo home_url(); ?>/login" method="post">
								<label>
									Email Address
									<input name="iykaa-member-email" id="iykaa-member-email" class="required" type="text" value="<?php if(isset($_GET['email'])) { echo $_GET['email']; } ?>">
								</label>
								<label>
									Password <a href="<?php echo home_url(); ?>/login?action=lostpassword">(Forgot password?)</a>
									<input name="iykaa-member-password" id="iykaa-member-password" class="required" type="password" placeholder="Password" <?php if(isset($_GET['email'])) { echo 'autofocus'; } ?>>
								</label>
								<button class="cta primary">Login</button>
								<input type="hidden" name="iykaa-login-nonce" value="<?php echo wp_create_nonce('iykaa-login-nonce'); ?>">
								<h4 class="light">Simply Enyware</h4>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dashboard-v2">
	<div class="wrapper">
		<div class="row">
			<div class="mp-12">
			</div>
		</div>
	</div>
	
	<?php 
		$args = array(
			'posts_per_page' 	=> 12,
			'post_type' 		=> 'post',
			'post_status' 		=> 'publish',
			'order'				=> 'DESC',
			'tax_query' => array (
				array (
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => 'promoted',
					'operator' => 'NOT IN'
				)
			)
		);

		$regular = new WP_Query($args);
	?>
	
	<?php if($regular->have_posts()) { ?>
		<section class="feed regular">
			<div class="wrapper">
				<div class="row">
					<div class="mp-12">
						<h4>What's Happening</h4>
						<hr class="secondary size-l">
					</div>
				</div>
				<div class="row">
					<div class="slider">
						<?php while($regular->have_posts()){ ?>
						<?php $regular->the_post(); 
							$image			= get_field('slide_image');
							$size			= 'happening-tile';
							$regularImage	= $image['sizes'][$size];
							$defaultImage 	= home_url() . '/images/default-img.jpg';
						?>
						<article>
							<div class="mp-3 tp-12">
								<a href="<?php the_permalink();?>">
									<div class="article-image">
										<div class="image-wrapper">
											<?php if($regularImage){ ?>
												<img src="<?php echo $regularImage; ?>" />
											<?php } elseif($defaultImage) { ?>
												<img src="<?php echo $defaultImage; ?>">
											<?php } ?>
										</div>
									</div>
								</a>
							</div>
							<div class="mp-7 tp-12">
								<a href="<?php the_permalink();?>">
									<?php if(get_field('article_title')){ ?>
										<h4><?php the_field('article_title'); ?></h4>
									<?php } else { ?>
										<h4><?php the_title(); ?></h4>
									<?php } ?>
								</a>
								<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
								<p class="time-stamp"><?php echo get_the_date('l jS F, Y'); ?></p>
								
								<div class="excerpt ml-hide">
									<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
									<?php if(get_field('article_snippet')){ ?>
										<p><?php the_field('article_snippet');?></p>
									<?php } else { ?>
										<?php the_excerpt(); ?>
									<?php } ?>
								</div>
							</div>
							<div class="mp-2 tp-12">
								<a href="<?php the_permalink();?>">
									<div class="find-out-more">
										<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
											Find Out More
										</div>
										<div class="chevron">
											<p>&#x63;</p>
										</div>
									</div>
								</a>
							</div>
						</article>
						<?php } ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	
	<?php 
		$args = array(
			'posts_per_page' 	=> 12,
			'post_type' 		=> 'iykaa_notifications',
			'post_status' 		=> 'publish',
			'order'				=> 'DESC',
		);

		$notification = new WP_Query($args);
	?>
	
	<?php if($notification->have_posts()) { ?>
		<section class="feed notifications">
			<div class="wrapper">
				<div class="row">
					<div class="mp-12">
						<h4>Latest Notifications</h4>
						<hr class="secondary size-l">
					</div>
				</div>
				
				<div class="row">
					<div class="slider2">
					<?php while($notification->have_posts()) {
						$notification->the_post(); ?>
						<?php
							$priority 		= get_the_term_list($post->ID, 'notification_priority_tax');
							$priorityClean 	= strip_tags( $priority );
							$noteTax		= strtolower($priorityClean);
						?>
						<article class="<?php echo $noteTax; ?>">
							<div class="mp-12">
								<a href="<?php the_permalink();?>">
									<h4><?php the_title(); ?></h4>
								</a>
								<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
								<p class="time-stamp"><?php echo get_the_date('H:i | l jS F, Y'); ?></p>
								<div class="ml-hide">
									<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
								</div>
							</div>
							<div class="mp-2 tp-12">
								<a href="<?php the_permalink();?>">
									<div class="find-out-more">
										<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
											Find Out More
										</div>
										<div class="chevron">
											<p>&#x63;</p>
										</div>
									</div>
								</a>
							</div>
						</article>
					<?php } ?>
					<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>	
		</section>
	<?php } ?>
	
	<?php if(have_rows('dashboard_buttons', 'option')) { ?>
		<section class="buttons">
			<div class="wrapper">
				<div class="row">
					<div class="mp-12">
						<h4>Quick Links</h4>
						<hr class="secondary size-l">
					</div>
					<ul class="dash-buttons">
						<?php while(have_rows('dashboard_buttons', 'option')) {
							the_row(); ?>
							<li>
								<a href="<?php the_sub_field('link'); ?>">
									<div class="mp-4 icon-container">
										<div class="icon">
											<?php if(get_sub_field('icon')){ ?>
												<span><?php the_sub_field('icon'); ?></span>
											<?php } ?>
										</div>
									</div>
									<div class="mp-8">
										<div class="label">
											<?php if(get_sub_field('title')){ ?>
												<p><?php the_sub_field('title'); ?></p>
											<?php } ?>
											<?php if(get_sub_field('description')){ ?>
												<p class="description"><?php the_sub_field('description'); ?></p>
											<?php } ?>
										</div>
									</div>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</section>
	<?php } ?>
</section>