<div class="comp-00601">
	<div class="row">
		<div class="mp-12">
			<h4>Recent Notifications</h4>
			<hr class="secondary size-l">
		</div>
	</div>
	<div class="row results similar-notifications">
		<?php
			$postID	= get_the_ID();
			
			include(get_template_directory() . '/includes/search/custom-taxonomies/notifications.php');
			
			$args = array(
				'post_type' 		=> 'iykaa_notifications',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> 3,
				'offset' 			=> 0,
				'orderby' 			=> 'date',
				'order' 			=> 'DESC',
				'post__not_in' 		=> array($postID),
				'tax_query' => $taxQuery,
			);
			
			$similarContent = new WP_Query($args);
		?>
		
		<?php if($similarContent->have_posts()) { ?>
			<div class="results">
				<?php while($similarContent->have_posts()) {
					$similarContent->the_post(); 
					$image = the_post_thumbnail(); ?>
					<a href="<?php the_permalink(); ?>">
						<div class="tl-4 single-notification">
							<div class="row">
								<div class="tl-12">
									<div class="inner <?php the_field('notification_priority'); ?>">
										<h4 class="title"><?php the_title(); ?></h4>
										<hr class="size-s">
										<?php the_excerpt(); ?>
									</div>
								</div>
								<div class="mp-12">
									<div class="find-out-more">
										<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
											Find Out More
										</div>
										<div class="chevron <?php the_field('notification_priority'); ?>">
											&#x63;
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				<?php } ?>
			</div>
			
			<?php wp_reset_query(); ?>
		<?php } else { ?>
			<div class="mp-12">
				<p>Sorry but no results were found.</p>
			</div>
		<?php } ?>
	</div>
</div>