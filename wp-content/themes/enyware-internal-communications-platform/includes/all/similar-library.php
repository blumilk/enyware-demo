<?php if(get_field('related_content')) { ?>
	<div class="comp-00601">
		<div class="row">
			<div class="mp-12">
				<h4>Related Content</h4>
				<hr class="secondary size-l">
			</div>
		</div>
		<div class="row results further-reading-library">
		<?php
			$relatedContent = get_field('related_content');
			
			foreach($relatedContent as $content) {
				setup_postdata($content); ?>
				
				<a href="<?php the_permalink($content->ID); ?>">
					<div class="tl-4">
						<div class="row">
							<div class="mp-12">
								<div class="inner <?php the_sub_field('inner_colour', $content->ID); ?>">
									<h4 class="title"><?php echo($content->post_title); ?></h4>
									<hr class="size-s <?php the_sub_field('cta_colour', $content->ID); ?>">
									<?php the_excerpt($content->ID); ?>
									<?php the_sub_field('body', $content->ID); ?>
								</div>
							</div>
							<div class="mp-12">
								<div class="find-out-more">
									<div class="cta <?php the_sub_field('cta_colour', $content->ID); ?> size-s">
										Find Out More
									</div>
									<div class="chevron">
										&#x63;
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			<?php } ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
<?php } ?>

<div class="comp-00601">
	<div class="row">
		<div class="mp-12">
			<h4>Similar Information</h4>
			<hr class="secondary size-l">
		</div>
	</div>
	<div class="row results further-reading-library">
	<?php
		$postID	= get_the_ID();
		//get the post's taxonomies
		$custom_terms = wp_get_post_terms($post->ID, 'library_tag');
		
		if( $custom_terms ){
		
		// going to hold our tax_query params
		$tax_query = array();
		
		// add the relation parameter
		if( count( $custom_terms > 1 ) )
		$tax_query['relation'] = 'OR' ;
		
		// loop through taxonomies and build a tax query
		foreach( $custom_terms as $custom_term ) {
		
			$tax_query[] = array(
				'taxonomy' => 'library_tag',
				'field' => 'slug',
				'terms' => $custom_term->slug,
			);
		
		}
		
		// put all the WP_Query args together
		$args = array( 
			'post_type' => 'iykaa_library',
			'posts_per_page' => 3,
			'tax_query' => $tax_query,
			'post__not_in' => array($postID),
		);
		
		// finally run the query
		$loop = new WP_Query($args);
	
		if( $loop->have_posts() ) {
		
		while( $loop->have_posts() ) {
				$loop->the_post(); ?>
			<a href="<?php the_permalink(); ?>">
				<div class="tl-4">
					<div class="row">
						<div class="mp-12">
							<div class="inner <?php the_sub_field('inner_colour'); ?>">
								<h4 class="title"><?php the_title(); ?></h4>
								<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
								<?php the_excerpt(); ?>
								<?php the_sub_field('body'); ?>
							</div>
						</div>
						<div class="mp-12">
							<div class="find-out-more">
								<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
									Find Out More
								</div>
								<div class="chevron">
									&#x63;
								</div>
							</div>
						</div>
					</div>
				</div>
			</a>
		<?php }
			wp_reset_query();
		} else { ?>
			<div class="mp-12">
				<p>Sorry but no results were found.</p>
			</div>
		<?php }
	} ?>
	</div>
</div>