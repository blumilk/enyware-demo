<section class="page-load">
	<div class="wrapper page-content <?php the_field('notification_priority'); ?>">
		<div class="row">
			<div class="dt-12">
				<h2><?php the_title(); ?></h2>
				<hr class="secondary size-m">
				<article>
					<div class="row">
						<div class="tp-8 indent-tl-1 force">
							<div class="main">
								<?php the_content(); ?>
							</div>
						</div>
						<?php include(get_stylesheet_directory() .'/includes/all/single-sidebar.php'); ?>
					</div>
				</article>
				<?php include('further-content.php'); ?>
			</div>
		</div>
	</div>
</section>