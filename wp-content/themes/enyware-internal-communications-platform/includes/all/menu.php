<div class="slide-menu">
	<div class="btn-close" id="menu-trigger">
		<?php $logo = get_field('logo_svg_so', 'options'); ?>
		<img src="<?php echo $logo['url']; ?>">
		<div class="menu-button open">
			<span class="burger-icon open"></span>
		</div>
		<div id="menu-mobile"></div>
	</div>
	<?php 
		include('menu-manual.php');
//		wp_nav_menu(array('theme_location' => 'iykaa-menu')); 
	?>
	
	<?php
		$user = wp_get_current_user();
		$allowed_roles = array('deity_iykaa', 'admin_iykaa');
		if( array_intersect($allowed_roles, $user->roles ) ) {  ?> 
		<ul class="admin-menu">
			<li class="admin-area">
				<a href="<?php echo home_url(); ?>/wp-admin">Admin Area</a>
			</li>
		</ul>
	<?php } ?>
	<div class="login">
		<?php
			if(function_exists('iykaa_read_later_shortcode_add')) {
				echo do_shortcode('[iykaa_read_later_add]');
			}
		?>
		<?php if(is_user_logged_in()) { ?>
			<a href="<?php echo wp_logout_url(home_url('/logged-out')); ?>" class="logout">
				Logout
			</a>
		<?php } else { ?>
			<a href="<?php echo home_url('/login'); ?>" class="sign-in">
				Sign In
			</a>
		<?php } ?>
		<small>Powered by <a href="https://www.meetiykaa.com" target="_blank">Enyware</a>.</small>
	</div>
</div>
<?php
	while($membersProfile->have_posts()) {
		$membersProfile->the_post();
		$availabilityTax = get_the_terms($post->ID, 'status_tax');
	}
	
	$availability = '';
	
	if(is_array($availabilityTax)) {
		foreach($availabilityTax as $status) {
			$availability = $status->name;
		}
	}
	
	if(isset($_GET['header-availability'])) {
		$status = $_GET['header-availability'];
		
		wp_delete_object_term_relationships($post->ID, 'status_tax');
		wp_set_object_terms($post->ID, (int)$status, 'status_tax', true);
		
		if($status == 27) {
			$availability = 'Available';
		} elseif($status == 28) {
			$availability = 'Unavailable';
		} elseif($status == 53) {
			$availability = 'Holiday';
		} elseif($status == 54) {
			$availability = 'Travelling';
		}
	} else {
		$status = 0;
	}
	
	$args = array(
		'show_option_all'    => '',
		'show_option_none'   => '',
		'option_none_value'  => 'false',
		'orderby'            => 'title',
		'order'              => 'ASC',
		'show_count'         => 0,
		'hide_empty'         => 0,
		'child_of'           => 0,
		'exclude'            => '',
		'echo'               => 1,
		'selected'           => $status,
		'hierarchical'       => 0,
		'name'               => 'header-availability',
		'id'                 => '',
		'class'              => 'header-availability',
		'depth'              => 0,
		'tab_index'          => 0,
		'taxonomy'           => 'status_tax',
		'hide_if_empty'      => false,
		'value_field'	     => 'term_id',
	);
	
	wp_reset_query();
?>

<?php
	/* Dynamic back button */
	if(is_single()){
		if (!empty($_SERVER['HTTP_REFERER'])){
			$home = home_url();
			if (false !== stripos($_SERVER['HTTP_REFERER'], "$home")){
				$menuSize = 'mobile-bottom with-back';
			} else {
				
			}
		} else {
			$menuSize = 'mobile-bottom';
		}
	} else {
		$menuSize = 'mobile-bottom';
	}
?>

<div class="<?php echo $menuSize ?>">
	<ul>
		<?php
			/* Dynamic back button */
			if(is_single()){
				if (!empty($_SERVER['HTTP_REFERER'])){
					$home = home_url();
					if (false !== stripos($_SERVER['HTTP_REFERER'], "$home")){
						  $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
						  echo "<li class='back'><a href='$url'></a></li>";
					} else {
						
					}
				}
			}
		?>
		
		<li class="home">
			<a href="<?php echo home_url(); ?>/dashboard">
				
			</a>
		</li>
		<li class="notifications">
			<a href="<?php echo home_url(); ?>/notifications">
				
			</a>
		</li>
		<li class="contacts">
			<a href="<?php echo home_url(); ?>/members">
				
			</a>
		</li>
		<li class="library">
			<a href="<?php echo home_url(); ?>/library">
				
			</a>
		</li>
		<li class="menu">
			<a href="<?php echo home_url(); ?>">
				
			</a>
		</li>
	</ul>
</div>