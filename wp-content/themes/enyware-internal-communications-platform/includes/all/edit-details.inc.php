<section class="page-load">
	<div class="wrapper team page-content">
		<div class="row">
			<div class="dt-12">
				<div class="row">
					<div class="tp-12">
						<h2>Edit your details</h2>
					</div>
				</div>
				<hr class="secondary size-m">
				<article>
					<div class="row">
						<div class="tp-12">
							<div class="main">
								<div class="details edit-profile">
									<?php
										if($membersProfile->have_posts()) {
											while($membersProfile->have_posts()) {
												$membersProfile->the_post();
												$memberProfileID = $membersProfile->post->ID;
											}
											
											if($_SERVER['REQUEST_METHOD'] == 'POST') {
												include('edit-details-submit.inc.php');
											} else {
												include('edit-details-form.inc.php');
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>