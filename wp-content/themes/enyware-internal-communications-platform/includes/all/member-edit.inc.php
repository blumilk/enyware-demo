<section class="page-load">
	<div class="wrapper team page-content">
		<div class="row">
			<div class="dt-12">
				<div class="row">
					<div class="tp-12">
						<h2>Edit your profile</h2>
					</div>
				</div>
				<hr class="secondary size-m">
				<article>
					<div class="row">
						<div class="tp-12">
							<div class="main">
								<div class="details edit-profile">
									<div class="row">
										<?php
											if($membersProfile->have_posts()) {
												while($membersProfile->have_posts()) {
													$membersProfile->the_post();
													$memberProfileID = $membersProfile->post->ID;
												}
											}
											
											$args = array(
												'post_id' 			=> $memberProfileID,
												'post_type' 		=> 'iykaa_members',
												'submit_value' 		=> 'Update Your Profile',
												'updated_message' 	=> 'Profile Updated.',
												'uploader' 			=> 'basic',
												'fields' 			=> 
												array(
													'profile_image',
													'member_forename',
													'member_middle_name',
													'member_surname',
													'date_of_birth',
													'job_role',
													'job_title_full',
													'outside_job_role',
													'email_addresses',
													'contact_numbers',
													'working_hours',
													'mobile_phone',
													'bleep_number',
													'biography',
													'accreditations_and_qualifications',
													'alternative_contacts',
													'similar_contacts',
													'member_opt_out',
													'location_profile_taxonomy',
													'position_profile_taxonomy',
													'department_profile_taxonomy',
													'band_profile_taxonomy',
													'occupational_unit_profile_taxonomy',
													'business_unit_profile_taxonomy',
													'you_are_profile_taxonomy',
													'apply_to_you_profile_taxonomy',
													'systems_used_profile_taxonomy',
												),
											);
											
											acf_form($args);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>