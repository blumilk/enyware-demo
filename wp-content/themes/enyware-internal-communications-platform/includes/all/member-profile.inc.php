<?php
	$availabilityTax = get_the_terms($post->ID, 'status_tax');
	$availability = '';
	
	if(is_array($availabilityTax)) {
		foreach($availabilityTax as $status) {
			$availability = $status->name;
		}
	}
?>
<section class="page-load">
	<div class="wrapper team page-content">
		<div class="row">
			<div class="dt-12">
				<div class="row">
					<div class="tp-9">
						<h2><?php the_title(); ?></h2>
					</div>
					<div class="tp-3">
						<div class="status top <?php echo strtolower($availability); ?> align-right">
							<p><span>User Availability:</span> <?php echo $availability; ?></p>
						</div>
					</div>
				</div>
				<hr class="secondary size-m">
				<article>
					<div class="row">
						<div class="tp-4 tl-3">
							<?php
								$profileImage = get_field('profile_image');
								$size = 'thumbnail';
								$profileImage = $profileImage['sizes'][ $size ];
							?>
							
							<div class="avatar">
								<?php
									if($profileImage) {
										echo '<img src="' . $profileImage . '">';
									} else {
										echo '<img src="' . home_url() . '/images/avatar-default.svg">';
									}
								?>
							</div>
							<hr class="secondary">
							<?php
								$availabilityTax = get_the_terms($post->ID, 'status_tax');
								$availability = '';
								
								if(is_array($availabilityTax)) {
									foreach($availabilityTax as $status) {
										$availability = $status->name;
									}
								}
							?>
							
							<?php if(!empty($availability)) { ?>
								<div class="status <?php echo strtolower($availability); ?>">
									<?php echo $availability; ?>
									<?php if($current_user->ID == $post->post_author) { ?>
										<?php if(get_field('status_note')) { ?>
											<a href="<?php echo home_url(); ?>/status-note?TB_iframe=true&width=280&height=144" class="thickbox">
												<p><?php the_field('status_note'); ?></p>
											</a>
										<?php } ?>
									<?php } else { ?>
										<?php if(get_field('status_note')) { ?>
											<p><?php the_field('status_note'); ?></p>
										<?php } ?>
									<?php } ?>
								</div>
							<?php } ?>
<!--							<hr class="secondary">
								<div class="profile-updated">
									<p>Profile last updated:</p>
									<p><?php the_modified_date('l, jS F, Y'); ?></p>
								</div>-->
							<hr class="secondary">
							<?php if($current_user->ID == $post->post_author) { ?>
								<div class="edit">
									<a href="<?php echo home_url(); ?>/member-edit">
										<div class="wrapper no-gaps nested">
											<div class="row">
												<div class="mp-2">
													<div class="i-icon large icon-ii-edit"></div>
												</div>
												<div class="mp-10">
													<p>Edit Profile</p>
												</div>
											</div>
										</div>
									</a>
								</div>
								<hr class="secondary">
							<?php } else { ?>
								<?php include(get_stylesheet_directory() . '/includes/admin/edit-content.php'); ?>
							<?php } ?>
							<?php
								if(function_exists('iykaa_read_later_shortcode_add')) {
									echo do_shortcode('[iykaa_read_later_add]');
								}
								
								// Share with a contact
								include('share-with-contact.php');
								
								// Nudge this member
								include('nudge-member.php');
							?>
						</div>
						<div class="tp-8 indent-tl-1">
							<div class="main">
								<div class="details">
									<div class="row">
										<?php if(get_field('member_name')) { ?>
											<section class="title-divider first">
												<div class="tp-12">
													<h4>Details</h4>
													<hr class="secondary size-m">
												</div>
											</section>
											
											<div class="tp-6">
												<div class="i-icon icon-ii-name"></div>
												<?php the_field('member_name'); ?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<?php if(get_field('job_title_full')) { ?>
											<div class="tp-6">
												<div class="i-icon icon-ii-user"></div>
												<small>Title: </small>
												<?php the_field('job_title_full'); ?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<?php if(have_rows('email_addresses')) { ?>
											<section class="title-divider">
												<div class="tp-12">
													<h4>Contact Details</h4>
													<hr class="secondary size-m">
												</div>
											</section>
											<?php while(have_rows('email_addresses')) {
												the_row(); ?>
												<?php if(get_sub_field('email_address')) { ?>
													<div class="tp-6">
														<div class="i-icon icon-ii-envelope"></div>
														<a href="mailto:<?php the_sub_field('email_address'); ?>">
															<small><?php the_sub_field('label'); ?>:</small> <?php the_sub_field('email_address'); ?>
														</a>
														<hr class="secondary size-xs">
													</div>
												<?php } ?>
											<?php } ?>
										<?php } ?>
										
										<?php if(have_rows('contact_numbers')) { ?>
											<?php while(have_rows('contact_numbers')) {
												the_row(); ?>
												<?php if(get_sub_field('contact_number')) { ?>
													<div class="tp-6">
														<div class="i-icon icon-ii-phone"></div>
														<a href="tel:<?php the_sub_field('contact_number'); ?>">
															<small><?php the_sub_field('label'); ?>:</small> <?php the_sub_field('contact_number'); ?>
														</a>
														<hr class="secondary size-xs">
													</div>
												<?php } ?>
											<?php } ?>
										<?php } ?>
										
										<?php $taxonomies = get_the_terms($post->ID, 'location_tax'); ?>
										<?php if(is_array($taxonomies)) { ?>
											<section class="title-divider">
												<div class="tp-12">
													<h4>Job Details</h4>
													<hr class="secondary size-m">
												</div>
											</section>
											<div class="tp-6">
												<div class="i-icon icon-ii-location"></div>
												<small>Main Location: </small>
												<?php
													$taxCount = count($taxonomies);
													$taxInc = 1;
													
													foreach($taxonomies as $taxonomy) {
														if($taxInc >= $taxCount) {
															echo $taxonomy->name;
														} else {
															echo $taxonomy->name . ', ';
														}
														
														$taxInc++;
													}
												?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<?php $taxonomies = get_the_terms($post->ID, 'position_tax'); ?>
										<?php if(is_array($taxonomies)) { ?>
											<div class="tp-6">
												<div class="i-icon icon-ii-user-group"></div>
												<small>Position: </small>
												<?php
													$taxCount = count($taxonomies);
													$taxInc = 1;
													
													foreach($taxonomies as $taxonomy) {
														if($taxInc >= $taxCount) {
															echo $taxonomy->name;
														} else {
															echo $taxonomy->name . ', ';
														}
														
														$taxInc++;
													}
												?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<?php $taxonomies = get_the_terms($post->ID, 'department_tax'); ?>
										<?php if(is_array($taxonomies)) { ?>
											<div class="tp-6">
												<div class="i-icon icon-ii-user-group"></div>
												<small>Department: </small>
												<?php
													$taxCount = count($taxonomies);
													$taxInc = 1;
													
													foreach($taxonomies as $taxonomy) {
														if($taxInc >= $taxCount) {
															echo $taxonomy->name;
														} else {
															echo $taxonomy->name . ', ';
														}
														
														$taxInc++;
													}
												?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<?php $taxonomies = get_the_terms($post->ID, 'business_tax'); ?>
										<?php if(is_array($taxonomies)) { ?>
											<div class="tp-6">
												<div class="i-icon icon-ii-user-group"></div>
												<small>Business Unit: </small>
												<?php
													$taxCount = count($taxonomies);
													$taxInc = 1;
													
													foreach($taxonomies as $taxonomy) {
														if($taxInc >= $taxCount) {
															echo $taxonomy->name;
														} else {
															echo $taxonomy->name . ', ';
														}
														
														$taxInc++;
													}
												?>
												<hr class="secondary size-xs">
											</div>
										<?php } ?>
										
										<!-- Custom Taxonomies -->

										<!-- End Custom Taxonomies -->
										
										<?php if(get_field('working_hours')) { ?>
											<section class="title-divider">
												<div class="tp-12">
													<h4>Working Hours</h4>
													<hr class="secondary size-m">
												</div>
											</section>
											<?php while(have_rows('working_hours')) {
												the_row(); ?>
												<div class="tp-12 working-hours">
													<?php
														date_default_timezone_set('Europe/London');
														$currentDay = date('l');
														$currentDayLower = strtolower(date('l'));
														
														$workingHours = array(
															'Monday' 		=> get_sub_field('monday_start') . ' - ' . get_sub_field('monday_end'),
															'Tuesday' 		=> get_sub_field('tuesday_start') . ' - ' . get_sub_field('tuesday_end'),
															'Wednesday' 	=> get_sub_field('wednesday_start') . ' - ' . get_sub_field('wednesday_end'),
															'Thursday' 		=> get_sub_field('thursday_start') . ' - ' . get_sub_field('thursday_end'),
															'Friday' 		=> get_sub_field('friday_start') . ' - ' . get_sub_field('friday_end'),
															'Saturday' 		=> get_sub_field('saturday_start') . ' - ' . get_sub_field('saturday_end'),
															'Sunday' 		=> get_sub_field('sunday_start') . ' - ' . get_sub_field('sunday_end'),
														);
														
														// Gets todays day and the value
														$firstDay = array(
															$currentDay => get_sub_field($currentDayLower . '_start') . ' - ' . get_sub_field($currentDayLower . '_end')
														);
														
														// Adds the two arrays together so that todays day is first
														$workingHours = $firstDay + $workingHours;
													?>
													
													<div class="row">
														<?php foreach($workingHours as $key => $value) { ?>
															<?php
																if(empty($value) || $value == ' - ') {
																	$value = 'Unavailable';
																}
															?>
															
															<small class="tp-3">
																<?php if($key == $currentDay) { ?>
																	<strong>
																		<p><?php echo $key; ?>: <?php echo $value; ?></p>
																	</strong>
																<?php } else { ?>
																	<p><?php echo $key; ?>: <?php echo $value; ?></p>
																<?php } ?>
																<hr class="secondary size-xs">
															</small>
														<?php } ?>
													</div>
												</div>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
				<?php
					include(get_stylesheet_directory() . '/includes/all/member-documents.php');
					
					include(get_stylesheet_directory() . '/includes/all/further-content.php');
				?>
			</div>
		</div>
	</div>
</section>