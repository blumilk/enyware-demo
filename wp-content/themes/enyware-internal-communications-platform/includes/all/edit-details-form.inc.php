<form method="post" action="" class="registration">
	<fieldset>
		<div class="row">
			<div class="tl-4">
				<h3>Location</h3>
				<hr class="tertiary size-s">
				<?php
					$args = array(
						'type'                     => 'iykaa_members',
						'child_of'                 => 0,
						'parent'                   => '',
						'orderby'                  => 'Title',
						'order'                    => 'ASC',
						'hide_empty'               => 0,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => 'location_tax',
						'pad_counts'               => false
					);
				 	
					$categories = get_categories($args);
					
					$selected = get_the_terms($memberProfileID, 'location_tax');
					
					if(!empty($selected)) {
						foreach($selected as $select) {
							$selection[] = $select->term_id;
						}
					}
				?>
				<div class="checkbox">
					<div class="row">
						<?php foreach($categories as $category) { ?>
							<div class="tp-6">
								<div class="row">
									<div class="mp-2">
										<input type="checkbox" id="<?php echo $category->term_id; ?>" name="location[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
									</div>
									<div class="mp-10">
										<label for="<?php echo $category->term_id; ?>">
											<?php echo $category->cat_name; ?>
										</label>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="tl-4">
				<h3>Position</h3>
				<hr class="tertiary size-s">
				<?php
					$args = array(
						'type'                     => 'iykaa_members',
						'child_of'                 => 0,
						'parent'                   => '',
						'orderby'                  => 'Title',
						'order'                    => 'ASC',
						'hide_empty'               => 0,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => 'position_tax',
						'pad_counts'               => false
					);
				 	
					$categories = get_categories($args);
					
					$selected = get_the_terms($memberProfileID, 'position_tax');
					
					if(!empty($selected)) {
						foreach($selected as $select) {
							$selection[] = $select->term_id;
						}
					}
				?>
				<div class="checkbox">
					<div class="row">
						<?php foreach($categories as $category) { ?>
							<?php if($category->slug != 'all') { // Ignores the 'All' option as this is only to be used within the DMS. I would have done this by the exclude but ID's will be changing when it comes to different deployments ?>
								<div class="tp-6">
									<div class="row">
										<div class="mp-2">
											<input type="checkbox" id="<?php echo $category->term_id; ?>" name="position[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
										</div>
										<div class="mp-10">
											<label for="<?php echo $category->term_id; ?>">
												<?php echo $category->cat_name; ?>
											</label>
										</div>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="tl-4">
				<h3>Department</h3>
				<hr class="tertiary size-s">
				<?php
					$args = array(
						'type'                     => 'iykaa_members',
						'child_of'                 => 0,
						'parent'                   => '',
						'orderby'                  => 'Title',
						'order'                    => 'ASC',
						'hide_empty'               => 0,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => 'department_tax',
						'pad_counts'               => false
					);
				 	
					$categories = get_categories($args);
					
					$selected = get_the_terms($memberProfileID, 'department_tax');
					
					if(!empty($selected)) {
						foreach($selected as $select) {
							$selection[] = $select->term_id;
						}
					}
				?>
				<div class="checkbox">
					<div class="row">
						<?php foreach($categories as $category) { ?>
							<?php if($category->slug != 'all') { // Ignores the 'All' option as this is only to be used within the DMS. I would have done this by the exclude but ID's will be changing when it comes to different deployments ?>
								<div class="tp-6">
									<div class="row">
										<div class="mp-2">
											<input type="checkbox" id="<?php echo $category->term_id; ?>" name="department[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
										</div>
										<div class="mp-10">
											<label for="<?php echo $category->term_id; ?>">
												<?php echo $category->cat_name; ?>
											</label>
										</div>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
	<div class="row">
		<div class="tp-12">
			<input type="submit" name="user-submit" value="Update your Details" class="cta secondary">
		</div>
	</div>
</form>