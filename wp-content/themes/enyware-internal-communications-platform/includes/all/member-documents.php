<?php if(have_rows('iykaa_member_documents_2')) { ?>
	<div class="further-info">
		<div class="row">
			<div class="mp-12">
				<h4>My Documents</h4>
				<hr class="secondary size-l">
				<div class="row">
					<?php while(have_rows('iykaa_member_documents_2')) {
						the_row(); ?>
						<article class="tl-4 tp-6">
							<a href="<?php the_sub_field('iykaa_member_documents_document_url'); ?>">
								<strong class="title"><?php the_sub_field('iykaa_member_documents_document_title'); ?></strong>
								<small>
									<?php the_sub_field('iykaa_member_documents_document_timestamp'); ?>
								</small>
							</a>
							<hr class="secondary size-s">
							<p><?php the_sub_field('iykaa_member_documents_document_description'); ?></p>
							<a class="cta secondary size-s corners" href="<?php the_sub_field('iykaa_member_documents_document_url'); ?>">
								Download Now
							</a>
							<hr class="secondary size-s">
						</article>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>