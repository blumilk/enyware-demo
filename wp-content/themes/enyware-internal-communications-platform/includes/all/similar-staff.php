<?php if(get_field('alternative_contacts')) { ?>
	<div class="further-info">
		<div class="row">
			<div class="mp-12">
				<h4>Alternative Contacts</h4>
				<hr class="secondary size-l">
			</div>
		</div>
		<div class="row results">
			<?php $similarMembers = get_field('alternative_contacts'); ?>
			
			<?php foreach($similarMembers as $member) {
				setup_postdata($member); ?>
				<div class="tl-2 tp-4 mp-6 profile-preview">
					<a href="<?php the_permalink($member->ID); ?>">
						<?php
							$image = get_field('profile_image', $member->ID);
							$size = 'large';
							$large = $image['sizes'][$size];
							$profileImage = $large;
						?>
						
						<div class="avatar avatar-similar">
							<?php
								if($profileImage) {
									echo '<img src="' . $profileImage . '">';
								} else {
									echo '<img src="' . home_url() . '/images/avatar-default.svg">';
								}
							?>
						</div>
						
						<?php	
							$name = $member->post_title;
							$name = preg_replace('/\s+/', ' ', $name);
						?>
						
						<span class="title"><?php echo $name; ?></span>
						
						<?php
							$availabilityTax = get_the_terms($member->ID, 'status_tax');
							$availability = '';
							
							if(is_array($availabilityTax)) {
								foreach($availabilityTax as $status) {
									$availability = $status->name;
								}
							}
						?>
						<hr class="secondary size-s">
						
						<?php
							if(get_field('status_note')) {
								$statusNote = ' note';
							} else {
								$statusNote = '';
							}
						?>
						
						<?php if(!empty($availability)) { ?>
							<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
								<?php echo $availability; ?>
							</div>
						<?php } ?>
					</a>
				</div>
			<?php } ?>
			
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
<?php } ?>

<?php if(get_field('similar_contacts')) { ?>
	<div class="further-info">
		<div class="row">
			<div class="mp-12">
				<h4>Similar Contacts</h4>
				<hr class="secondary size-l">
			</div>
		</div>
		<div class="row results">
			<?php $similarMembers = get_field('similar_contacts'); ?>
			
			<?php foreach($similarMembers as $member) {
				setup_postdata($member); ?>
				<div class="tl-2 tp-4 mp-6 profile-preview">
					<a href="<?php the_permalink($member->ID); ?>">
						<?php
							$image = get_field('profile_image', $member->ID);
							$size = 'large';
							$large = $image['sizes'][$size];
							$profileImage = $large;
						?>
						
						<div class="avatar avatar-similar">
							<?php
								if($profileImage) {
									echo '<img src="' . $profileImage . '">';
								} else {
									echo '<img src="' . home_url() . '/images/avatar-default.svg">';
								}
							?>
						</div>
						
						<?php	
							$name = $member->post_title;
							$name = preg_replace('/\s+/', ' ', $name);
						?>
						
						<span class="title"><?php echo $name; ?></span>
						
						<?php
							$availabilityTax = get_the_terms($member->ID, 'status_tax');
							$availability = '';
							
							if(is_array($availabilityTax)) {
								foreach($availabilityTax as $status) {
									$availability = $status->name;
								}
							}
						?>
						<hr class="secondary size-s">
						
						<?php
							if(get_field('status_note')) {
								$statusNote = ' note';
							} else {
								$statusNote = '';
							}
						?>
						
						<?php if(!empty($availability)) { ?>
							<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
								<?php echo $availability; ?>
							</div>
						<?php } ?>
					</a>
				</div>
			<?php } ?>
			
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
<?php } ?>