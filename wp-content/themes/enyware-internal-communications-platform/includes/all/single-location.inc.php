<section class="page-load">
	<div class="wrapper page-content">
		<div class="row">
			<div class="dt-12">
				<?php if(get_field('title')){?>
					<h2><?php the_field('title');?></h2>
				<?php } else { ?>
					<h2><?php the_title(); ?></h2>
				<?php } ?>
				
				<hr class="secondary size-m">
				
				<section class="location-details">
					<section class="location-info">
						<div class="row">
							<?php if(get_field( 'map' )){ 
								$location = get_field( 'map' );
							?>
								<div class="ml-6 tp-8">	
									<div class="acf-map">
										<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
									</div>
									<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAWrXyk7pqnJhF_ywrIcb7Pw02eBwItLak"></script>
									<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/acf-maps.js"></script>
								</div>
								<div class="ml-6 tp-4 force">
								<?php if(get_field('image')){
									$image = get_field('image');
								?>
									<img class="side-image" src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'];?>" />
								<?php } ?>
								
							<?php } else { ?>
								 <div class="ml-6 tp-8">
								<?php if(get_field('image')){
									$image = get_field('image');
								?>
									<img class="side-image" src="<?php echo $image['url'] ?>" alt="<?php echo $image['title'];?>" />
								<?php } ?>
								 </div>
								<div class="ml-6 tp-4 side-details force">
							<?php } ?>		
								
								<?php if(get_field('address')){ ?>
								<div class="i-icon icon-ii-location"></div>
									<address>
										<?php if(have_rows('address')){ ?>
											<?php while(have_rows( 'address' )){ ?>
												<?php the_row(); ?>
												<p><?php the_sub_field('line');?></p>
											<?php } ?>
										<?php } ?>
									</address>
								<?php } ?>
								
								<?php if(get_field('main_phone')){
									$telephone = get_field( 'main_phone' );
									$phone = preg_replace('/\s+/ ', '', $telephone);
								?>
								<div class="i-icon icon-ii-phone"></div>
									<p class="phone"><a href="tel:<?php echo $phone ?>"><?php the_field('main_phone');?></a></p>
								<?php } ?>
								
								<?php if(get_field('main_email')){?>
								<div class="i-icon icon-ii-envelope"></div>
									<p class="email"><a href="mailto:<?php the_field('main_email');?>"><?php the_field('main_email');?></a></p>
								<?php } ?>
								
							</div>
						</div>
					</section>
					
					<?php if(get_field('information')){?>
						<section class="information">
							<div class="row">
								<div class="tp-12">
									<h2>Information</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12 main">
									<?php the_field('information');?>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php if(get_field('opening_times')){?>
						<section class="opening-times">
							<div class="row">
								<div class="tp-12">
									<h2>Opening Times</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12">
									<?php if(have_rows('opening_times')){ ?>
										<ul>
											<?php while(have_rows('opening_times')){ ?>
												<?php the_row(); ?>
												<li>
													<p><?php the_sub_field('day'); ?></p>
													<p>Open: <?php the_sub_field('opening_time'); ?></p>
													<p>Close: <?php the_sub_field('closing_time'); ?></p>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php 
					$postID	= get_the_ID();
					
					$terms = get_the_terms($post->ID , 'location_tax', 'string');
					$term_ids = wp_list_pluck($terms,'term_id');
	
					$args = array(
						'posts_per_page' 	=> -1,
						'post_type' 		=> 'iykaa_departments',
						'post_status' 		=> 'publish',
						'order'				=> 'ASC',
						'tax_query' 		=> array(
							array(
								'taxonomy' 	=> 'location_tax',
								'field' 	=> 'id',
								'terms' 	=> $term_ids,
								'operator'	=> 'IN'
							)
						),
						'post__not_in' 		=> array($postID),
					);
	
					$locations = new WP_Query($args); 		
					if($locations->have_posts()) { ?>
						<section class="departments">
							<div class="row">
								<div class="tp-12">
									<h2>Departments</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12">
									<?php while($locations->have_posts()) { ?>
										<ul>
											<?php $locations->the_post(); ?>
												<?php setup_postdata( $locations ); ?>
												<li>
													<p class="dept">
														<a href="<?php the_permalink();?>">
															<?php if(get_field('title')){ ?>
																<?php the_field('title');?>
															<?php } else { ?>
																<?php the_title(); ?>
															<?php } ?>
														</a>
													</p>
													<?php if(get_field('main_phone_number')){ 
														$telephone2 = get_field( 'main_phone_number' );
														$phone2 = preg_replace('/\s+/ ', '', $telephone2);
													?>
														<p class="phone">
															<a href="tel:<?php echo $phone2 ?>"><?php the_field('main_phone_number');?></a>
														</p>
													<?php } ?>
													
													<?php if(get_field('main_email_address')){?>
														<p class="email">
															<a href="mailto:<?php the_field('main_email_address');?>"><?php the_field('main_email_address');?></a>
														</p>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
										<?php wp_reset_query(); ?>
								</div>
							</div>
						</section>
					<?php } ?>
					
				</section>
				
				<?php include('further-content.php'); ?>
			</div>
		</div>
	</div>
</section>