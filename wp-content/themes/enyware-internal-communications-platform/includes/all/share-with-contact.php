<script>
	function showHint(str) {
		if(str.length == 0) {
			document.getElementById("get-user").innerHTML = "";
			return;
		} else {
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200) {
					document.getElementById("get-user").innerHTML = this.responseText;
				}
			};
			
			xmlhttp.open("GET", "<?php echo home_url(); ?>/get-users?resource-url=<?php the_permalink(); ?>&resource-name=<?php the_title(); ?>&keywords=" + str, true);
			xmlhttp.send();
		}
	}
</script>
<section class="get-user">
	<div class="wrapper no-gaps nested">
		<div class="row">
			<div class="mp-2">
				<div class="i-icon large icon-ii-share"></div>
			</div>
			<div class="mp-10">
				<form>
					<input type="text" onkeyup="showHint(this.value)" placeholder="Share with a contact" autocomplete="off">
				</form>
				<div>
					<ul id="search">
						<span id="get-user"></span>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<hr class="secondary">