<section class="super-search">
	<div class="search">
		<span>
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'What are you looking for?', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" autocomplete="off">
				<button class="cta secondary" type="submit">Search</button>
				<button class="cta secondary close">&times;</button>
			</form>
		</span>
	</div>
</section>