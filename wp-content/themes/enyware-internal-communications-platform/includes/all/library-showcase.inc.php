<section class="page-load">
	<div class="wrapper page-content">
		<div class="row">
			<div class="dt-12">
				<h3><?php the_title(); ?></h3>
				<hr class="secondary size-m">
				<?php include(get_stylesheet_directory() . '/includes/search/showcase-menu.php'); ?>
			</div>
			<div class="tl-8">
				<?php if(have_rows('download')) { ?>
					<section class="download">
						<div class="row">
							<div class="tl-4">
							<?php if(count(get_field('download')) >= 2) { ?>
								<h3>Downloads</h3>
							<?php } else { ?>
								<h3>Download</h3>
							<?php } ?>
							</div>
							<div class="tl-8">
								<ul>
								<?php while(have_rows('download')) {
									the_row(); 
									$file 		= get_sub_field( 'file' );
									$ext 		= pathinfo($file, PATHINFO_EXTENSION);
									?>
									<?php if($ext == 'doc'){ ?>
										<li>
										<a href="<?php the_sub_field('file'); ?>">
											<p class="<?php echo $ext; ?>">
												<?php the_sub_field('file_name'); ?>
											</p>
										</a>
										</li>
									<?php } ?>
									
									<?php if($ext == 'docx'){ ?>
										<li>
										<a href="<?php the_sub_field('file'); ?>">
											<p class="<?php echo $ext; ?>">
												<?php the_sub_field('file_name'); ?>
											</p>
										</a>
										</li>
									<?php } ?>
									
									<?php if($ext == 'pdf'){ ?>
										<li>
											<p class="document-frame-trigger <?php echo $ext; ?>" data-document-location="<?php the_sub_field('file'); ?>">
												<?php the_sub_field('file_name'); ?>
											</p>
										</li>
									<?php } ?>
									
								<?php } ?>
								</ul>
							</div>
						</div>
					</section>
				<?php } ?>
				<div class="main">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>