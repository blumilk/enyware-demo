<section class="welcome">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<div class="row">
					<div class="mp-6 ml-7">
						<a href="<?php echo home_url();?>/dashboard"><img src="<?php echo home_url(); ?>/images/enyware-i-icon.svg" class="iykaa-icon"/></a>
						<h1>
							<?php
								$currentHour = date('H');
								$currentHour++;
								
								if($currentHour >= '00' && $currentHour <= '12') {
									$timeOfDay = 'morning';
								} elseif($currentHour >= '12' && $currentHour <= '18') {
									$timeOfDay = 'afternoon';
								} elseif($currentHour >= '18') {
									$timeOfDay = 'evening';
								} else {
									$timeOfDay = 'day';
								}
								
								$currentUser = wp_get_current_user();
								
								if($currentUser->user_firstname) {
									echo 'Good ' . $timeOfDay . ', ' . $currentUser->user_firstname . '.';
								} else {
									echo 'Good ' . $timeOfDay . '.';
								}
								
								while($membersProfile->have_posts()) {
									$membersProfile->the_post();
									$availabilityTax = get_the_terms($post->ID, 'status_tax');
								}
								
								$availability = '';
								
								if(is_array($availabilityTax)) {
									foreach($availabilityTax as $status) {
										$availability = $status->name;
										$availabilityID = $status->term_id;
									}
								}
								
								if(isset($_GET['header-availability'])) {
									$status = $_GET['header-availability'];
									
									wp_delete_object_term_relationships($post->ID, 'status_tax');
									wp_set_object_terms($post->ID, (int)$status, 'status_tax', true);
									
									if($status == 27) {
										$availability = 'Available';
									} elseif($status == 28) {
										$availability = 'Unavailable';
									} elseif($status == 53) {
										$availability = 'Holiday';
									} elseif($status == 54) {
										$availability = 'Travelling';
									}
								} else {
									$status = 0;
									$availabilityID = 0;
								}
								
								if(!isset($_GET['header-availability'])) {
									$status = $availabilityID;
								}
								
								if(isset($availabilityID)) {
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => '',
										'option_none_value'  => 'false',
										'orderby'            => 'title',
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $status,
										'hierarchical'       => 0,
										'name'               => 'header-availability',
										'id'                 => '',
										'class'              => 'header-availability',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'status_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								}
								
								wp_reset_query();
							?>
						</h1>
					</div>
					<?php if(isset($availabilityID)) { ?>
					<?php add_thickbox(); ?>
					<div class="mp-4">
						<div class="status">
							
							<div class="current">
								<span>Your status is:<br/></span> 
								<span class="status <?php echo strtolower($availability); ?>"><?php echo $availability ?></span>
							</div>
							
							<a href="<?php echo home_url(); ?>/status-note?TB_iframe=true&width=280&height=260" class="thickbox update <?php echo strtolower($availability); ?>"><span class="hide">Update your </span>status</a>
							
						</div>
					</div>
					<?php } ?>
					<div class="ml-1 mp-2">
						<div class="status <?php echo strtolower($availability); ?> align-right status-search">
							<small>
								<div class="i-icon icon-ii-search"></div>
							</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Full Search -->
<?php include('searchform-super.php'); ?>