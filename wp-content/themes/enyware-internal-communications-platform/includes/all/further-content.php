<?php
	if(is_singular('post')) {
		include(get_stylesheet_directory() . '/includes/all/further-reading.php');
	} elseif(is_single() && is_post_type('iykaa_notifications')) {
		include(get_stylesheet_directory() . '/includes/all/similar-notifications.php');
	} elseif(is_single() && is_post_type('iykaa_members')) {
		include(get_stylesheet_directory() . '/includes/all/similar-staff.php');
	} elseif(is_single() && is_post_type('iykaa_library')) {
		include(get_stylesheet_directory() . '/includes/all/similar-library.php');
	} elseif(is_single() && is_post_type('iykaa_locations')) {
		include(get_stylesheet_directory() . '/includes/all/similar-staff-location.php');
	} elseif(is_single() && is_post_type('iykaa_library_custom')) {
		include(get_stylesheet_directory() . '/includes/all/similar-library-custom.php');
	} elseif(is_single() && is_post_type('iykaa_departments')) {
		include(get_stylesheet_directory() . '/includes/all/department-staff.php');
	}
?>