<?php 
	$postID	= get_the_ID();
	
	$terms = get_the_terms($post->ID , 'department_tax', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	
	$args = array(
		'post_type' 		=> 'iykaa_members',
		'post_status' 		=> 'publish',
		'posts_per_page' 	=> 6,
		'offset' 			=> 0,
		'order' 			=> 'rand',
		'tax_query' 		=> array(
			array(
				'taxonomy' 	=> 'department_tax',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			)
		),
		'post__not_in' 		=> array($postID),
	);
	
	$similarMembers = new WP_Query($args);
?>
			
<?php if($similarMembers->have_posts()) { ?>
<div class="further-info">
	<div class="row">
		<div class="mp-12">
			<h4>Department Members</h4>
			<hr class="secondary size-l">
		</div>
	</div>
	<div class="row results">
	<?php while($similarMembers->have_posts()) {
		$similarMembers->the_post(); ?>
		<div class="tl-2 tp-4 mp-6 profile-preview">
			<a href="<?php the_permalink(); ?>">
				<?php
					$profileImage = get_field('profile_image');
					$size = 'thumbnail';
					$profileImage = $profileImage['sizes'][$size];
				?>
				
				<div class="avatar avatar-similar">
					<?php
						if($profileImage) {
							echo '<img src="' . $profileImage . '">';
						} else {
							echo '<img src="' . home_url() . '/images/avatar-default.svg">';
						}
					?>
				</div>
				
				<?php	
					$name = get_the_title();
					
					$name = preg_replace('/\s+/', ' ', $name);
				?>
				
				<span class="title"><?php echo $name; ?></span>
				
				<?php
					$availabilityTax = get_the_terms($post->ID, 'status_tax');
					$availability = '';
					
					if(is_array($availabilityTax)) {
						foreach($availabilityTax as $status) {
							$availability = $status->name;
						}
					}
				?>
				<hr class="secondary size-s">
				
				<?php
					if(get_field('status_note')) {
						$statusNote = ' note';
					} else {
						$statusNote = '';
					}
				?>
				
				<?php if(!empty($availability)) { ?>
					<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
						<?php echo $availability; ?>
					</div>
				<?php } ?>
			</a>
		</div>
	<?php } ?>
	
	<?php wp_reset_query(); ?>
	</div>
</div>
<?php } else { ?>
<div class="further-info">
	<div class="row">
		<div class="mp-12">
			<h4>Department Members</h4>
			<p>No department members have been allocated yet. <a href="<?php echo home_url();?>/members">You can search for people here</a>.</p>
		</div>
	</div>
</div>
<?php } ?>