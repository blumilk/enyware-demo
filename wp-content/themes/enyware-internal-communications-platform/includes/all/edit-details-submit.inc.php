<?php
	wp_delete_object_term_relationships($memberProfileID, 'location_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['location'])) {
		$location = $_POST['location'];
		
		foreach($location as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'location_tax', true);
		}
	}
	
	wp_delete_object_term_relationships($memberProfileID, 'position_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['position'])) {
		$position = $_POST['position'];
		
		foreach($position as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'position_tax', true);
		}
	}
	
	wp_delete_object_term_relationships($memberProfileID, 'department_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['department'])) {
		$department = $_POST['department'];
		
		foreach($department as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'department_tax', true);
		}
	}
	
	
	// Custom Taxonomies 
	
	wp_delete_object_term_relationships($memberProfileID, 'band_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['band'])) {
		$department = $_POST['band'];
		
		foreach($department as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'band_tax', true);
		}
	}
	
	wp_delete_object_term_relationships($memberProfileID, 'business_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['business'])) {
		$department = $_POST['business'];
		
		foreach($department as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'business_tax', true);
		}
	}
	
	wp_delete_object_term_relationships($memberProfileID, 'occupational_tax');
	
	// Set the management and governance taxonomy
	if(isset($_POST['occupational'])) {
		$department = $_POST['occupational'];
		
		foreach($department as $value) {
			wp_set_object_terms($memberProfileID, (int)$value, 'occupational_tax', true);
		}
	}
?>

<div id="message" class="updated">
	<p>Your details have been updated.</p>
</div>

<?php include('edit-details-form.inc.php'); ?>