<section class="page-load">
	<div class="wrapper page-content">
		<div class="row">
			<div class="dt-12">
				<?php if(get_field('title')){ ?>
					<h2><?php the_field('title');?></h2>
				<?php } else { ?>
					<h2><?php the_title(); ?></h2>
				<?php } ?>
				
				<hr class="secondary size-m">
				
				<section class="location-details department">
					<section class="location-info">
						
						<?php 
							$postID	= get_the_ID();
							
							$terms = get_the_terms($post->ID , 'location_tax', 'string');
							$term_ids = wp_list_pluck($terms,'term_id');
			
							$args = array(
								'posts_per_page' 	=> 1,
								'post_type' 		=> 'iykaa_locations',
								'post_status' 		=> 'publish',
								'order'				=> 'ASC',
								'tax_query' 		=> array(
									array(
										'taxonomy' 	=> 'location_tax',
										'field' 	=> 'id',
										'terms' 	=> $term_ids,
										'operator'	=> 'IN'
									)
								),
								'post__not_in' 		=> array($postID),
							);
			
							$locations = new WP_Query($args); 		
							if($locations->have_posts()) { ?>
							<?php while($locations->have_posts()) {
										$locations->the_post(); 
										setup_postdata( $locations ); ?>
										<div class="row">
											<div class="ml-4">
												<p class="title">Location: </p>
											</div>
											<div class="ml-8">
												<div class="i-icon icon-ii-location"></div>
												<p class="location"><a href="<?php the_permalink(); ?>">
													<?php if(get_field('title')){ ?>
														<?php the_field('title');?>
													<?php } else { ?>
														<?php the_title(); ?>
													<?php } ?>
												</a></p>
											</div>
										</div>
								<?php } ?>
								<?php wp_reset_query(); ?>
							<?php } ?>
														
						<?php if(get_field('main_phone_number')){
							$telephone = get_field( 'main_phone_number' );
							$phone = preg_replace('/\s+/ ', '', $telephone);
						?>
						<div class="row">
							<div class="ml-4">
								<p class="title">Primary Phone Number: </p>
							</div>
							<div class="ml-8">
								<div class="i-icon icon-ii-phone"></div>
								<p class="phone"><a href="tel:<?php echo $phone; ?>"><?php the_field('main_phone_number');?></a></p>
							</div>
						</div>
						<?php } ?>
						
						<?php if(get_field('main_email_address')){?>
						<div class="row">
							<div class="ml-4">
								<p class="title">Primary Email Address: </p>
							</div>
							<div class="ml-8">
								<div class="i-icon icon-ii-envelope"></div>
								<p class="email"><a href="mailto:<?php the_field('main_email_address');?>"><?php the_field('main_email_address');?></a></p>
							</div>
						</div>
						<?php } ?>
					</section>
					
					<?php if(get_field('department_information')){ ?>
						<section class="information">
							<div class="row">
								<div class="tp-12">
									<h2>Information</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12 main">
									<?php the_field('department_information');?>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php if(get_field('additional_phone_numbers')){ ?>
						<section class="opening-times phone-numbers">
							<div class="row">
								<div class="tp-12">
									<h2>Additional Phone Numbers</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12">
									<?php if(have_rows('additional_phone_numbers')){ ?>
										<ul>
											<?php while(have_rows('additional_phone_numbers')){ ?>
												<?php the_row(); ?>
												<li>
													<p><?php the_sub_field('name'); ?></p>
														<?php if(get_sub_field('number')){
															$telephoneAdd = get_sub_field( 'number' );
															$phoneAdd = preg_replace('/\s+/ ', '', $telephoneAdd);
														?>
													<p class="phone"><a href="tel:<?php echo $phoneAdd; ?>"><?php the_sub_field('number');?></a></p>
													<?php } ?>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php if(get_field('additional_email_addresses')){ ?>
						<section class="opening-times emails">
							<div class="row">
								<div class="tp-12">
									<h2>Additional Email Addresses</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12">
									<?php if(have_rows('additional_email_addresses')){ ?>
										<ul>
											<?php while(have_rows('additional_email_addresses')){ ?>
												<?php the_row(); ?>
												<li>
													<p><?php the_sub_field('name'); ?></p>
													<p class="email"><a href="mailto:<?php the_sub_field('address');?>"><?php the_sub_field('address');?></a></p>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>
							</div>
						</section>
					<?php } ?>
					
					<?php if(get_field('opening_times')){ ?>
						<section class="opening-times">
							<div class="row">
								<div class="tp-12">
									<h2>Opening Times</h2>
									<hr class="secondary size-m">
								</div>
								<div class="tp-12">
									<?php if(have_rows('opening_times')){ ?>
										<ul>
											<?php while(have_rows('opening_times')){ ?>
												<?php the_row(); ?>
												<li>
													<p><?php the_sub_field('day'); ?></p>
													<p>Open: <?php the_sub_field('opening_time'); ?></p>
													<p>Close: <?php the_sub_field('closing_time'); ?></p>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>
							</div>
						</section>
					<?php } ?>
					
				</section>

				<?php include('further-content.php'); ?>
			</div>
		</div>
	</div>
</section>