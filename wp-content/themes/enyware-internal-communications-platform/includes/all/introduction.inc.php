<section class="introduction">
	<div class="wrapper no-gaps full intro">
		<div class="row">
			<div class="mp-12" id="intro-slides">
				<section class="slide one">
					<div class="wrapper">
						<div class="row">
							<div class="ml-6 indent-ml-3">
								<h1>Welcome to Enyware</h1>
								<hr class="secondary size-s">
								<p>I’m here to make your work life easier!</p>
								<p>I’ve got three main sections that are designed to help you everyday:</p>
								<ul class="comms-types">
									<li class="news">What's Happening</li>
									<li class="contacts">Contacts</li>
									<li class="library">For Me</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<section class="slide two">
					<div class="wrapper">
						<div class="row">
							<div class="ml-6 indent-ml-3">
								<h2><div class="i-icon icon-ii-whats-happening"></div>What's Happening</h2>
								<hr class="secondary size-s">
								<p>The latest news and updates, exclusively for you as a member of the team.</p>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/desktop-dashboard.jpg" class="screenshot desktop"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/tablet-dashboard.jpg" class="screenshot tablet"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/phone-dashboard.jpg" class="screenshot phone"/>
							</div>
						</div>
					</div>
				</section>
				<section class="slide three">
					<div class="wrapper">
						<div class="row">
							<div class="ml-6 indent-ml-3">
								<h2><div class="i-icon icon-ii-contacts"></div>Contacts</h2>
								<hr class="secondary size-s">
								<p>Looking for someone? Not sure of their department or location, well I know and I’ll help you find them.</p>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/desktop-contacts.jpg" class="screenshot desktop"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/tablet-contacts.jpg" class="screenshot tablet"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/phone-contacts.jpg" class="screenshot phone"/>
							</div>
						</div>
					</div>
				</section>
				<section class="slide four">
					<div class="wrapper">
						<div class="row">
							<div class="ml-6 indent-ml-3">
								<h2><div class="i-icon icon-ii-library"></div>For Me</h2>
								<hr class="secondary size-s">
								<p>Access to resources for you such as my benefits, staff lottery, employee self service, NHS pensions and much more.</p>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/desktop-library.jpg" class="screenshot desktop"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/tablet-library.jpg" class="screenshot tablet"/>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/phone-library.jpg" class="screenshot phone"/>
							</div>
						</div>
					</div>
				</section>
				<section class="slide five">
					<div class="wrapper">
						<div class="row">
							<div class="ml-6 indent-ml-3">
								<h2>Keep me close</h2>
								<hr class="secondary size-s">
								<p>'I’m here to be as helpful as possible so it's best to keep me just a tap away on your mobile devices as well as accessing on desktop.</p>
								<p>To do this, add me to your home screen when you're on the next page.</p>	
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/css/assets/intro-slides/homescreen.png" class="screenshot space" />
								<a href="<?php echo home_url(); ?>/member-edit">
									<div class="find-out-more">
										<div class="cta size-s">
											Let's get started
										</div>
										<div class="chevron">
											&#x63;
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
<script>
	jQuery(document).ready(function($) {
		$("#intro-slides").owlCarousel({
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			items: 1,
			loop: true,
		});
		
		// Hides the next button if on the last slide
		$('#intro-slides').on('change.owl.carousel', function(e) {
			if(e.namespace && e.property.name === 'position' && e.relatedTarget.relative(e.property.value) === e.relatedTarget.items().length - 1) {
				$('.owl-next').css('visibility', 'hidden');
			} else {
				$('.owl-next').css('visibility', 'visible');
			}
		});
	});
	
	jQuery(document).ready(function($) {
		vph = $(window).height();
		$("section").css({'height':vph+'px'});
		$('body').css('padding-left', '0px');
	});
</script>