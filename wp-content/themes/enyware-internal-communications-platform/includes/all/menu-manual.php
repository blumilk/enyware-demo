<?php if(have_rows('side_menu', 'option')) { ?>
	<nav class="side-nav">
		<div class="sub-page-list">
		<ul>
		<?php while(have_rows( 'side_menu', 'option' )){ ?> 
			<?php the_row(); ?>
			<li class="<?php the_sub_field( 'icon' ); ?>"><a href="<?php the_sub_field( 'link' ); ?>"><?php the_sub_field( 'text' ); ?></a>			
				<?php if(have_rows( 'sub_menu' )){ ?>
					<ul class="sub-menu children">
						<?php while(have_rows('sub_menu')){ ?>
							<?php the_row(); ?>
								<li class="<?php the_sub_field( 'icon' ); ?>"><a href="<?php the_sub_field( 'link' ); ?>"><?php the_sub_field( 'text' ); ?></a></li>
						<?php } ?>
					</ul>
				<?php } ?>
			</li>
		<?php } ?>
		</ul>
		</div>
	</nav>
<?php } ?>