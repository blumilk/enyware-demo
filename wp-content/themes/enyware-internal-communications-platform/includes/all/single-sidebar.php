<div class="tp-4 tl-3 sidebar single-sidebar">
<?php 
	$image 				= get_field('slide_image'); 
	$size				= 'medium';
	$sidebarImage		= $image['sizes'][$size];
?>
	<?php if($sidebarImage) { ?>
		<div class="hero-img" id="desktop-image">
			<img src="<?php echo $sidebarImage; ?>">
		</div>
	<?php } ?>
	
	<hr class="secondary">
	<small>Posted by <?php the_author(); ?></small>
	<hr class="secondary">
	<small><?php the_time('l | jS F, Y'); ?></small>
	<hr class="secondary">
	<?php
		include(get_stylesheet_directory() . '/includes/admin/edit-content.php');
		
		include(get_stylesheet_directory() . '/includes/all/share-with-contact.php');
		
		if(function_exists('iykaa_read_later_shortcode_add')) {
			echo do_shortcode('[iykaa_read_later_add]');
		}
	?>
</div>