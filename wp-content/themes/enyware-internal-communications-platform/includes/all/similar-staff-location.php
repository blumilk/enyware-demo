<div class="further-info">
	<div class="row">
		<div class="mp-12">
			<h4>People at this location</h4>
			<hr class="secondary size-l">
		</div>
	</div>
	<div class="row results">
		<?php if(get_field('similar_contacts')) { ?>
			<?php
				$similarMembers = get_field('similar_contacts');
				
				foreach($similarMembers as $member) {
					setup_postdata($member); ?>
					
					<div class="tl-2 tp-4 mp-6 profile-preview">
						<a href="<?php the_permalink($member->ID); ?>">
							<?php
								$image = get_field('profile_image', $member->ID);
								$size = 'large';
								$large = $image['sizes'][$size];
								$profileImage = $large;
							?>
							
							<div class="avatar avatar-similar">
								<?php
									if($profileImage) {
										echo '<img src="' . $profileImage . '">';
									} else {
										echo '<img src="' . home_url() . '/images/avatar-default.svg">';
									}
								?>
							</div>
							
							<?php	
								$name = $member->post_title;
								
								$name = preg_replace('/\s+/', ' ', $name);
							?>
							
							<span class="title"><?php echo $name; ?></span>
							
							<?php
								$availabilityTax = get_the_terms($member->ID, 'status_tax');
								$availability = '';
								
								if(is_array($availabilityTax)) {
									foreach($availabilityTax as $status) {
										$availability = $status->name;
									}
								}
							?>
							<hr class="secondary size-s">
							
							<?php
								if(get_field('status_note')) {
									$statusNote = ' note';
								} else {
									$statusNote = '';
								}
							?>
							
							<?php if(!empty($availability)) { ?>
								<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
									<?php echo $availability; ?>
								</div>
							<?php } ?>
						</a>
					</div>
				<?php }
				
				wp_reset_postdata();
			?>
		<?php } else { ?>
			<?php 
				$postID	= get_the_ID();
				
				$terms = get_the_terms($post->ID , 'location_tax', 'string');
				$term_ids = wp_list_pluck($terms,'term_id');
				
				$args = array(
					'post_type' 		=> 'iykaa_members',
					'post_status' 		=> 'publish',
					'posts_per_page' 	=> 6,
					'offset' 			=> 0,
					'order' 			=> 'rand',
					'tax_query' 		=> array(
						array(
							'taxonomy' 	=> 'location_tax',
							'field' 	=> 'id',
							'terms' 	=> $term_ids,
							'operator'	=> 'IN'
						)
					),
					'post__not_in' 		=> array($postID),
				);
				
				$similarMembers = new WP_Query($args);
			?>
			
			<?php if($similarMembers->have_posts()) { ?>
				<?php while($similarMembers->have_posts()) {
					$similarMembers->the_post(); ?>
					<div class="tl-2 tp-4 mp-6 profile-preview">
						<a href="<?php the_permalink(); ?>">
							<?php
								$profileImage = get_field('profile_image');
								$size = 'thumbnail';
								$profileImage = $profileImage['sizes'][$size];
							?>
							
							<div class="avatar avatar-similar">
								<?php
									if($profileImage) {
										echo '<img src="' . $profileImage . '">';
									} else {
										echo '<img src="' . home_url() . '/images/avatar-default.svg">';
									}
								?>
							</div>
							
							<?php	
								$name = get_the_title();
								
								$name = preg_replace('/\s+/', ' ', $name);
							?>
							
							<span class="title"><?php echo $name; ?></span>
							
							<?php
								$availabilityTax = get_the_terms($post->ID, 'status_tax');
								$availability = '';
								
								if(is_array($availabilityTax)) {
									foreach($availabilityTax as $status) {
										$availability = $status->name;
									}
								}
							?>
							<hr class="secondary size-s">
							
							<?php
								if(get_field('status_note')) {
									$statusNote = ' note';
								} else {
									$statusNote = '';
								}
							?>
							
							<?php if(!empty($availability)) { ?>
								<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
									<?php echo $availability; ?>
								</div>
							<?php } ?>
						</a>
					</div>
				<?php } ?>
				
				<?php wp_reset_query(); ?>
			<?php } else { ?>
				<div class="mp-12">
					<p>Sorry but no results were found.</p>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>