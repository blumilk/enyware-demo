<?php
	// Gets the ID for the members profile page
	if($membersProfile->have_posts()) {
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberProfileID = $membersProfile->post->ID;
		}
	}
	
	$args = array(
		'post_type' => 'iykaa_notifications',
		'orderby' => 'date',
		'order' => 'DESC',
		'posts_per_page' => 1,
		'post_status' => 'publish',
	);
	
	$latestNews = new WP_Query($args);
?>

<?php while($latestNews->have_posts()) {
	$latestNews->the_post(); ?>
	
	<?php
		// Dictates how long the notification will be displayed for
		$notificationsDate = get_the_time('YmdHis');
		$lastLogin = get_field('member_last_login', $memberProfileID);
		
		if(get_field('days_available')) {
			$daysAvailable = get_field('days_available');
		} else {
			$daysAvailable = 0;
		}
		
		$stopDate = new DateTime($notificationsDate);
		$stopDate->modify('+' . $daysAvailable . ' day');
		
		// Shows the taxonomies the notification should appear to
		$memberLocation = array();
		$notificationLocation = array();
		$memberPosition = array();
		$notificationPosition = array();
		$memberDepartment = array();
		$notificationDepartment = array();
		$memberAvailability = array();
		$notificationAvailability = array();
		
		$locations = get_the_terms($memberProfileID, 'location_tax');
		if(isset($locations) && !empty($locations)) {
			foreach($locations as $location) {
				array_push($memberLocation, $location->name);
			}
		}
		
		$postions = get_the_terms($memberProfileID, 'position_tax');
		if(isset($postions) && !empty($postions)) {
			foreach($postions as $postion) {
				array_push($memberLocation, $location->name);
			}
		}
		
		$departments = get_the_terms($memberProfileID, 'department_tax');
		if(isset($departments) && !empty($departments)) {
			foreach($departments as $department) {
				array_push($memberLocation, $department->name);
			}
		}
		
		$availability = get_the_terms($memberProfileID, 'status_tax');
		if(isset($availability) && !empty($availability)) {
			foreach($availability as $status) {
				array_push($memberLocation, $status->name);
			}
		}
		
		$locations = get_the_terms($post->ID, 'location_tax');
		if(isset($locations) && !empty($locations)) {
			foreach($locations as $location) {
				array_push($notificationLocation, $location->name);
			}
		}
		
		$postions = get_the_terms($post->ID, 'position_tax');
		if(isset($postions) && !empty($postions)) {
			foreach($postions as $postion) {
				array_push($notificationLocation, $postion->name);
			}
		}
		
		$departments = get_the_terms($post->ID, 'department_tax');
		if(isset($departments) && !empty($departments)) {
			foreach($departments as $department) {
				array_push($notificationLocation, $department->name);
			}
		}
		
		$availability = get_the_terms($post->ID, 'status_tax');
		if(isset($availability) && !empty($availability)) {
			foreach($availability as $status) {
				array_push($notificationLocation, $status->name);
			}
		}
		
		if(count($notificationLocation) > 0) {
			$locationCheck = array_intersect($memberLocation, $notificationLocation);
		} else {
			$locationCheck = ' ';
		}
		
		if(count($notificationPosition) > 0) {
			$positionCheck = array_intersect($memberPosition, $notificationPosition);
		} else {
			$positionCheck = ' ';
		}
		
		if(count($notificationDepartment) > 0) {
			$departmentCheck = array_intersect($memberDepartment, $notificationDepartment);
		} else {
			$departmentCheck = ' ';
		}
		
		if(count($notificationAvailability) > 0) {
			$availabilityCheck = array_intersect($memberAvailability, $notificationAvailability);
		} else {
			$availabilityCheck = ' ';
		}
		
		$errors = array();
		
		if(!empty($locationCheck)) {
			array_push($errors, 'Value');
		} else {
			array_push($errors, 'No Match');
		}
		
		if(!empty($positionCheck)) {
			array_push($errors, 'Value');
		} else {
			array_push($errors, 'No Match');
		}
		
		if(!empty($departmentCheck)) {
			array_push($errors, 'Value');
		} else {
			array_push($errors, 'No Match');
		}
		
		if(!empty($availabilityCheck)) {
			array_push($errors, 'Value');
		} else {
			array_push($errors, 'No Match');
		}
	?>
	
	<?php if(!in_array('No Match', $errors)) { ?>
		<?php if($stopDate->format('YmdHis') > $lastLogin) { ?>
			<a href="<?php the_permalink(); ?>">
				<?php
					$notifications = array();
					$notificationPriority = get_the_terms($post->ID, 'notification_priority_tax');
					if(isset($notificationPriority) && !empty($notificationPriority)) {
						foreach($notificationPriority as $notification) {
							array_push($notifications, $notification->slug);
						}
					}
					
					// Defaults the priority to low if it isn't set
					if(empty($notifications)) {
						array_push($notifications, 'low');
					}
					
					$richDate 		= get_the_date('H:i | l jS F, Y');
					$mobileDate		= get_the_date('H:i | d-m-Y');
				?>
				<section class="notification <?php echo $notifications[0]; ?>" role="alert">
					<div class="wrapper">
						<div class="row">
							<div class="ml-12">
								<div class="inner detials">
									<h3><?php the_title(); ?> <span class="rich"><?php echo $richDate; ?></span> <span class="mobile"><?php echo $mobileDate; ?></span> </h3>
								</div>
							</div>
							<div class="ml-12">
								<div class="inner excerpt">
									<p><?php echo get_the_excerpt(); ?></p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</a>
		<?php } ?>
	<?php } ?>
<?php } ?>

<?php wp_reset_query(); ?>