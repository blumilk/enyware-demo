<?php
	if (is_user_logged_in()) {
		$user = wp_get_current_user();
		$userName = $user->user_login;
	}
?>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php the_field( 'google_analytics','options' ); ?>', 'auto');
	<?php if (isset($userName)) : ?>
		ga('set', 'userId', <?php echo(json_encode($userName)); ?>); // Set the User ID using signed-in user name.
	<?php endif; ?>
	ga('send', 'pageview');
</script>