<?php
	$user = wp_get_current_user();
	$allowed_roles = array(
		'deity_iykaa',
		'admin_iykaa'
	);
?>

<?php if(array_intersect($allowed_roles, $user->roles)) {  ?> 
	<section class="nudge-member">
		<div class="wrapper no-gaps nested edit">
			<div class="row">
				<div class="mp-12">
					<form method="post">
						<?php if(isset($_POST['submitid']) && isset($_SESSION['submitid']) && $_POST["submitid"] == $_SESSION["submitid"]) { ?>
							<?php
								$_SESSION["submitid"] = '';
								
								$email = get_field('email_address');
								$subject = 'Please fill out your profile.';
								
								if(!empty($_SERVER['HTTPS'])) {
									$protocol = 'https://';
								} else {
									$protocol = 'http://';
								}
								$message = 'Please fill out your profile.<br />' . $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
								wp_mail($email, $subject, $message);
							?>
							<p>This member has been nudged.</p>
						<?php } else { ?>
							<?php $_SESSION["submitid"] = md5(rand(0,10000000)); ?>
							<input type="hidden" name="submitid" value="<?php echo htmlspecialchars($_SESSION["submitid"]); ?>">
							<div class="mp-2">
								<div class="i-icon large icon-ii-envelope"></div>
							</div>
							<div class="mp-10">
								<button>
									Send a Profile Update Reminder
								</button>
							</div>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>
	</section>
	<hr class="secondary">
<?php } ?>