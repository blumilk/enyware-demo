<?php
	while($membersProfile->have_posts()) {
		$membersProfile->the_post();
		$memberID = $membersProfile->post->ID;
	}
	
	if(isset($_POST['status-note'])) {
		update_post_meta($memberID, 'status_note', $_POST['status-note']);
	}
?>

<?php if(isset($_POST['status-note'])) { ?>
	<div id="message" class="updated">
		<p>Your status note has been updated.</p>
	</div>
<?php } else { ?>
		<p>Add a note for your colleagues:</p>
<?php } ?>
<form action="" method="post" class="status-form">
	<input type="text" name="status-note" placeholder="Status Note" value="<?php if(get_field('status_note')) { the_field('status_note'); } ?>" id="status-note">
	<input type="button" value="Clear note" class="reset" />
	<input type="submit" value="Save note" id="status_submit">
</form>

<script>
	jQuery(".reset").click(function() {
		jQuery(this).closest('form').find("input[type=text], textarea").val("");
		jQuery('#status_submit').click();
	});	
</script>