<section class="comp comp-006 comp-00601 <?php the_sub_field('background_colour'); ?> page-content">
	<div class="wrapper <?php the_sub_field('width'); ?> further-reading">
		<div class="row nested">
			<div class="dt-12">
				<h3>See what else is happening</h3>
				<hr class="secondary size-l">
			</div>
			<?php
				$postID	= get_the_ID();
				$terms = get_the_terms($post->ID , 'category', 'string');
				$term_ids = wp_list_pluck($terms, 'term_id');
				
				$args = array(
					'post_type' => 'post',
					'orderby' => 'date',
					'order' => 'DESC',
					'posts_per_page' => 3,
					'post_status' => 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'id',
							'terms' => $term_ids,
							'operator'=> 'IN',
						)
					),
					'post__not_in' => array($postID),
				);
				
				$latestNews = new WP_Query($args);
			?>
		</div>
		<div class="row whats-happening">
			<?php while($latestNews->have_posts()) {
				$latestNews->the_post();
				$image = get_field('slide_image'); 
				$defaultImage = get_field('logo_svg_so', 'options'); ?>
				<a href="<?php the_permalink(); ?>">
					<div class="tl-4">
						<div class="row">
							<?php if($image) {?>
								<div class="mp-4 ml-12 news-thumbnail">
									<img src="<?php echo $image['url']; ?>">
								</div>
								<div class="mp-4 news-thumbnail-mobile" style="background-image: url(<?php echo $image['url']; ?>);">
								</div>
								<div class="mp-8 ml-12">
							<?php } else if($defaultImage) { ?>
								<div class="mp-4 ml-12 news-thumbnail default">
									<img src="<?php echo $defaultImage['url']; ?>">
								</div>
								<div class="mp-4 news-thumbnail-mobile default" style="background-image: url(<?php echo $defaultImage['url']; ?>);">
								</div>
								<div class="mp-8 ml-12">	
							<?php } else { ?>
								<div class="mp-12">
							<?php } ?>
								
								<div class="inner <?php the_sub_field('inner_colour'); ?>">
									<h4 class="title"><?php the_title(); ?></h4>
									<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
									<p><?php the_time('l jS F, Y'); ?></p>
									<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
									<?php the_excerpt(); ?>
									<?php the_sub_field('body'); ?>
								</div>
							</div>
							<div class="mp-12">
								<div class="find-out-more">
									<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
										Find Out More
									</div>
									<div class="chevron">
										&#x63;
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			<?php } ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>