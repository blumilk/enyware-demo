<section class="page-load">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<p>You're already logged in to your account. Access your <a class="cta primary size-xs" href="<?php echo home_url(); ?>/dashboard">dashboard</a> to continue.</p>
			</div>
		</div>
	</div>
</section>