<tr class="<?php echo $accessClass; ?>" data-href="<?php the_permalink(); ?>">
	<td>
		<?php the_title(); ?>
	</td>
	<td>
		<?php
			if(!empty($fileSummary)) {
				echo $fileSummary;
			} else {
				echo '–';
			}
		?>
	</td>
	<td>
		<?php
			if(!empty($fileSizes)) {
				echo $fileSizes;
			} else {
				echo '–';
			}
		?>
	</td>
	<td>
		<?php
			if(!empty($fileType)) {
				switch($fileType) {
					case 'pdf':
						$fileIcon = 'pdf';
						break;
					case 'doc':
						$fileIcon = 'word-doc';
						break;
					case 'docx':
						$fileIcon = 'word-doc';
						break;
					case 'jpg':
						$fileIcon = 'image';
						break;
					case 'jpeg':
						$fileIcon = 'image';
						break;
					case 'png':
						$fileIcon = 'image';
						break;
					case 'gif':
						$fileIcon = 'image';
						break;
					case 'xlsx':
						$fileIcon = 'excel';
						break;
					case 'zip':
						$fileIcon = 'zip';
						break;
					default:
						$fileIcon = 'file';
				}
			}
		?>
		
		<?php if(!empty($fileType)) { ?>
			<i class="fa fa-<?php echo $fileIcon; ?>"></i> <?php echo strtoupper($fileType); ?>
		<?php } else { ?>
			–
		<?php } ?>
		
		<?php $fileType = ''; ?>
	</td>
	<td>
		<?php
			if(get_field('file_settings')) {
				echo count(get_field('file_settings'));
			} else {
				echo '–';
			}
		?>
	</td>
	<td>
		<?php the_modified_date('d/m/Y - G:i'); ?>
	</td>
</tr>