<?php if($dmsSettings->max_num_pages > 1) { ?>
	<nav class="pagination">
		<ul>
			<?php
				if(isset($_GET['page'])) {
					$currentPage = $_GET['page'];
					$currentPagePlus = $page;
				} else {
					$currentPagePlus = 0;
				}
			?>
			
			<?php if(isset($currentPage) && $currentPage > 1) { ?>
				<a href="?page=1">
					<li>
						<i class="fa fa-double-left"></i>
					</li>
				</a>
			<?php } ?>
			<?php if(isset($currentPage) && $currentPage > 1) { ?>
				<a href="?page=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
					<li>
						<i class="fa fa-single-left"></i>
					</li>
				</a>
			<?php } ?>
			<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
				<?php if($i < $dmsSettings->max_num_pages - 1 && $i > 0) { ?>
					<a href="?page=<?php echo $i; ?>">
						<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
							<?php echo $i; ?>
						</li>
					</a>
				<?php } ?>
			<?php } ?>
			<?php if($dmsSettings->max_num_pages > 5) { ?>
				<li>
					...
				</li>
			<?php } ?>
			<?php for($i = $dmsSettings->max_num_pages - 1; $i <= $dmsSettings->max_num_pages; $i++) { ?>
				<a href="?page=<?php echo $i; ?>">
					<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
						<?php echo $i; ?>
					</li>
				</a>
			<?php } ?>
			<?php if(isset($currentPage) && $currentPage < $dmsSettings->max_num_pages) { ?>
				<a href="?page=<?php if(isset($currentPage) && $currentPage < $dmsSettings->max_num_pages) { echo $currentPage + 1; } ?>">
					<li>
						<i class="fa fa-single-right"></i>
					</li>
				</a>
			<?php } ?>
			<?php if(isset($currentPage) && $currentPage < $dmsSettings->max_num_pages) { ?>
				<a href="?page=<?php if(isset($currentPage) && $currentPage < $dmsSettings->max_num_pages) { echo $dmsSettings->max_num_pages; } ?>">
					<li>
						<i class="fa fa-double-right"></i>
					</li>
				</a>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>