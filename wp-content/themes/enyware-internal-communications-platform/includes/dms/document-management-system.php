<?php
	// Sets and stores the number of documents per page
	if(isset($_GET['count']) || isset($_COOKIE['count'])) {
		if(isset($_GET['count'])) {
			setcookie('count', $_GET['count']);
			$_COOKIE['count'] = $_GET['count'];
		}
		
		$pageCount = $_COOKIE['count'];
	} else {
		$pageCount = 10;
	}
	
	// Sets and stores the document order
	if(isset($_GET['order']) || isset($_COOKIE['order'])) {
		if(isset($_GET['order'])) {
			if($_GET['order'] == 'asc') {
				setcookie('order', 'desc');
				$_COOKIE['order'] = 'desc';
			} else {
				setcookie('order', 'asc');
				$_COOKIE['order'] = 'asc';
			}
		}
		
		$dmsOrder = $_COOKIE['order'];
	} else {
		$dmsOrder = 'desc';
	}
	
	// Sets and stores the document order by settings
	$dmsOrderByValue = '';
	if(isset($_GET['orderby']) || isset($_COOKIE['orderby'])) {
		if(isset($_GET['orderby'])) {
			if($_GET['orderby'] == 'dms_fn') {
				setcookie('orderby', 'title');
				$_COOKIE['orderby'] = 'title';
				setcookie('orderbyvalue', '');
				$_COOKIE['orderbyvalue'] = '';
				setcookie('orderbyshow', 'title');
				$_COOKIE['orderbyshow'] = 'title';
			} elseif($_GET['orderby'] == 'dms_lu') {
				setcookie('orderby', 'modified');
				$_COOKIE['orderby'] = 'modified';
				setcookie('orderbyvalue', '');
				$_COOKIE['orderbyvalue'] = '';
				setcookie('orderbyshow', 'modified');
				$_COOKIE['orderbyshow'] = 'modified';
			} elseif($_GET['orderby'] == 'dms_ft') {
				setcookie('orderby', 'meta_value');
				$_COOKIE['orderby'] = 'meta_value';
				setcookie('orderbyvalue', 'dms_file_type');
				$_COOKIE['orderbyvalue'] = 'dms_file_type';
				setcookie('orderbyshow', 'dms_file_type');
				$_COOKIE['orderbyshow'] = 'dms_file_type';
			} elseif($_GET['orderby'] == 'dms_r') {
				setcookie('orderby', 'meta_value_num');
				$_COOKIE['orderby'] = 'meta_value_num';
				setcookie('orderbyvalue', 'dms_revision_count');
				$_COOKIE['orderbyvalue'] = 'dms_revision_count';
				setcookie('orderbyshow', 'dms_revision_count');
				$_COOKIE['orderbyshow'] = 'dms_revision_count';
			} else {
				setcookie('orderby', 'meta_value_num');
				$_COOKIE['orderby'] = 'meta_value_num';
				setcookie('orderbyvalue', 'dms_file_size');
				$_COOKIE['orderbyvalue'] = 'dms_file_size';
				setcookie('orderbyshow', 'dms_file_size');
				$_COOKIE['orderbyshow'] = 'dms_file_size';
			}
		}
		
		$dmsOrderBy = $_COOKIE['orderby'];
		
		if(isset($_COOKIE['orderbyvalue'])) {
			$dmsOrderByValue = $_COOKIE['orderbyvalue'];
		} else {
			$dmsOrderByValue = '';
		}
	} else {
		$dmsOrderBy = 'modified';
	}
	
	// Clears out the keywords cookie
	if(count($_GET) < 1) {
		setcookie('keywords', '');
		$_COOKIE['keywords'] = '';
	}
	
	// Member details
	global $current_user;
	
	$args = array(
		'author' => $current_user->ID,
		'post_type' => 'iykaa_members',
		'post_status' => array('pending', 'draft', 'publish'),
	);
	
	$membersProfile = new WP_Query($args);
	
	if($membersProfile->have_posts()) {
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberProfileID = $membersProfile->post->ID;
		}
	}
	
	$taxQuery = array();
	$metaQuery = array();
	
	$user = wp_get_current_user();
	$allowedRoles = array(
		'deity_iykaa'
	);
	
	// if(!array_intersect($allowedRoles, $user->roles)) {
		$departmentSlugs = array(
			'all'
		);
		
		$departmentTerms = get_the_terms($memberProfileID, 'department_tax');
		
		foreach($departmentTerms as $departmentTerm) {
			array_push($departmentSlugs, $departmentTerm->slug);
		}
		
		array_push($taxQuery,
			array('relation' => 'OR',
				array(
					'taxonomy' => 'department_tax',
					'field'    => 'slug',
					'terms'    => $departmentSlugs,
				)
			)
		);
		
		$positionSlugs = array(
			'all'
		);
		
		$positionTerms = get_the_terms($memberProfileID, 'position_tax');
		
		foreach($positionTerms as $positionTerm) {
			array_push($positionSlugs, $positionTerm->slug);
		}
		
		array_push($taxQuery,
			array('relation' => 'OR',
				array(
					'taxonomy' => 'position_tax',
					'field'    => 'slug',
					'terms'    => $positionSlugs,
				)
			)
		);
		
		if(isset($_GET['documents']) && $_GET['documents'] == 'authored') {
			array_push($metaQuery,
				array(
					'relation' 		=> 'OR',
					array(
						'key' 		=> 'author_access',
						'value' 	=> '"' . get_the_ID() . '"',
						'compare' 	=> 'LIKE',
					),
				)
			);
		} else {
			array_push($metaQuery,
				array(
					'relation' 		=> 'OR',
					array(
						'key' 		=> 'member_access',
						'value' 	=> '"' . get_the_ID() . '"',
						'compare' 	=> 'LIKE',
					),
					array(
						'key' 		=> 'member_access',
						'value' 	=> false,
						'type' 		=> 'BOOLEAN',
					),
				),
				array(
					'relation' 		=> 'OR',
					array(
						'key' 		=> 'author_access',
						'value' 	=> '"' . get_the_ID() . '"',
						'compare' 	=> 'LIKE',
					),
					array(
						'key' 		=> 'author_access',
						'value' 	=> false,
						'type' 		=> 'BOOLEAN',
					),
				)
			);
		}
	// }
	if(isset($_GET['keywords']) || isset($_COOKIE['keywords'])) {
		if(isset($_GET['keywords'])) {
			$keywords = $_GET['keywords'];
			setcookie('keywords', $_GET['keywords']);
			$_COOKIE['keywords'] = $_GET['keywords'];
		} elseif(isset($_COOKIE['keywords'])) {
			$keywords = $_COOKIE['keywords'];
		}
		
		array_push($metaQuery,
			array(
				'relation' 		=> 'OR',
				array(
					'key' 		=> 'document_summary',
					'value' 	=> $keywords,
					'compare' 	=> 'LIKE',
				),
				array(
					'key' 		=> 'search_terms',
					'value' 	=> $keywords,
					'compare' 	=> 'LIKE',
				),
				array(
					'key' 		=> 'search_terms_raw',
					'value' 	=> $keywords,
					'compare' 	=> 'LIKE',
				),
			)
		);
	} else {
		$keywords = '';
	}
	
	$page = (get_query_var('page')) ? get_query_var('page') : 1;
	
	$args = array(
		'post_type' 		=> 'iykaa_dms',
		'order' 			=> $dmsOrder,
		'orderby' 			=> $dmsOrderBy,
		'posts_per_page' 	=> $pageCount,
		'post_status' 		=> 'publish',
		'tax_query' 		=> $taxQuery,
		'meta_key' 			=> $dmsOrderByValue,
		'meta_query' 		=> $metaQuery,
		'paged' 			=> $page,
	);
	
	$dmsSettings = new WP_Query($args);
	
	$memberID = array();
	$authorID = array();
	$filePostData = array();
?>

<section class="page-load">
	<div class="wrapper page-content dms">
		<div class="row">
			<div class="tp-12">
				<h1>Document Management System</h1>
				<hr class="secondary size-l">
				<a class="cta secondary outline" href="<?php echo home_url(); ?>/wp-admin/post-new.php?post_type=iykaa_dms">
					<i class="fa fa-dms-add"></i> Add a New Document
				</a>
				<a class="cta secondary outline" href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=iykaa_dms">
					<i class="fa fa-dms-manage"></i> Manage Documents
				</a>
				<a class="cta secondary <?php if(!isset($_GET['documents'])) { echo 'outline'; } ?>" href="?documents=authored">
					<i class="fa fa-dms-edit"></i> Authored Documents
				</a>
				<hr class="secondary size-l">
				<div class="row">
					<form action="" method="get">
						<div class="tp-7">
							<input type="text" name="keywords" placeholder="Keywords" value="<?php if(isset($_GET['keywords'])) { echo $_GET['keywords']; } elseif(isset($_COOKIE['keywords'])) { echo $_COOKIE['keywords']; } ?>">
						</div>
						<div class="tp-3">
							<input type="submit" value="Search Documents">
						</div>
						<div class="tp-2">
							<a class="cta primary full" href="<?php echo home_url(); ?>/document-management-system">
								Reset
							</a>
						</div>
					</form>
				</div>
				<hr class="secondary size-l">
				<div class="document-info">
					<div class="row">
						<div class="ml-8">
							<?php
								$foundDocuments = $dmsSettings->found_posts;
								$currentPage = $page;
								$maxPages = $dmsSettings->max_num_pages;
								
								$message = 'Page ' . $currentPage . ' of ' . $maxPages . '.<br />' . $foundDocuments . ' documents found.';
								
								echo $message;
							?>
						</div>
						<div class="ml-2">
							<select class="dms-page-count">
								<option value="10"># Per Page</option>
								<?php for($i = 1; $i <= 100; $i++) { ?>
									<?php if($i % 12 == 0) { ?>
										<option value="<?php echo $i; ?>" <?php if(isset($_GET['count']) && $_GET['count'] == $i || isset($_COOKIE['count']) && $_COOKIE['count'] == $i) { echo 'selected'; } ?>><?php echo $i; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<script>
								jQuery('.dms-page-count').on('change', function() {
									window.location.replace('?count=' + jQuery('.dms-page-count').val());
								});
							</script>
						</div>
						<div class="ml-2">
							<select class="dms-page-select">
								<option value="1">Page Navigation</option>
								<?php for($i = 1; $i <= $dmsSettings->max_num_pages; $i++) { ?>
									<option value="<?php echo $i; ?>" <?php if(isset($_GET['page']) && $_GET['page'] == $i) { echo 'selected'; } ?>><?php echo $i; ?> of <?php echo $maxPages; ?></option>
								<?php } ?>
							</select>
							<script>
								jQuery('.dms-page-select').on('change', function() {
									window.location.replace('?page=' + jQuery('.dms-page-select').val());
								});
							</script>
						</div>
					</div>
				</div>
				<hr class="secondary size-l">
				
				<?php include('templates/pagination.php'); ?>
				
				<?php if($dmsSettings->have_posts()) { ?>
					<table class="dms-files" width="100%">
						<thead>
							<tr>
								<td width="25%">
									<a href="?orderby=dms_fn&order=<?php echo $dmsOrder; ?>">
										File Name <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fn' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'title') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="35%">
									File Summary
								</td>
								<td width="10%">
									<a href="?orderby=dms_fs&order=<?php echo $dmsOrder; ?>">
										File Size <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fs' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_file_size') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="10%">
									<a href="?orderby=dms_ft&order=<?php echo $dmsOrder; ?>">
										File Type <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_ft' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_file_type') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="10%">
									<a href="?orderby=dms_r&order=<?php echo $dmsOrder; ?>">
										Revisions <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_r' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_revision_count') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="15%">
									<a href="?orderby=dms_lu&order=<?php echo $dmsOrder; ?>">
										Last Updated <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_lu' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'modified') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php
								while($dmsSettings->have_posts()) {
									$dmsSettings->the_post();
									
									$authorAccess = get_field('author_access');
									
									if($authorAccess && is_array($authorAccess)) {
										foreach($authorAccess as $author) {
											array_push($authorID, $author->ID);
										}
										
										if(in_array($memberProfileID, $authorID)) {
											$accessClass = 'is-author';
										} else {
											$accessClass = 'not-author';
										}
									}
									
									$memberAccess = get_field('member_access');
									
									$fileSummary = get_field('document_summary');
									$fileSizes = array();
									
									while(have_rows('file_settings')) {
										the_row();
										
										if(get_sub_field('summary_notes')) {
											$fileSummary = get_sub_field('summary_notes');
										} else {
											$fileSummary = get_field('document_summary');
										}
										
										$fileID = get_sub_field('select_file')['id'];
										$fileSize = filesize(get_attached_file($fileID));
										$fileSize = size_format($fileSize, 1);
										$fileSizes = $fileSize;
										$fileType = pathinfo(get_attached_file($fileID), PATHINFO_EXTENSION);
									}
									
									include('templates/table-list-all-tmp.php');
								}
								
								$memberID = array();
								$authorID = array();
							?>
						</tbody>
						<tfoot>
							<tr>
								<td width="25%">
									<a href="?orderby=dms_fn&order=<?php echo $dmsOrder; ?>">
										File Name <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fn' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'title') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="35%">
									File Summary
								</td>
								<td width="10%">
									<a href="?orderby=dms_fs&order=<?php echo $dmsOrder; ?>">
										File Size <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fs' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_file_size') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="10%">
									<a href="?orderby=dms_ft&order=<?php echo $dmsOrder; ?>">
										File Type <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_ft' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_file_type') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="10%">
									<a href="?orderby=dms_r&order=<?php echo $dmsOrder; ?>">
										Revisions <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_r' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'dms_revision_count') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
								<td width="15%">
									<a href="?orderby=dms_lu&order=<?php echo $dmsOrder; ?>">
										Last Updated <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_lu' || isset($_COOKIE['orderbyshow']) && $_COOKIE['orderbyshow'] == 'modified') { ?><i class="fa fa-<?php echo $dmsOrder; ?>"></i><?php } ?>
									</a>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php } else { ?>
					<h2>No documents have been found.</h2>
				<?php } ?>
				
				<?php include('templates/pagination.php'); ?>
			</div>
		</div>
	</div>
</section>

<script>
	jQuery(document).ready(function($) {
		$('.dms-files tbody tr').click(function() {
			window.location = $(this).data('href');
		});
	});
</script>