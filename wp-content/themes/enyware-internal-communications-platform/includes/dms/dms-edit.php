<?php
	if(!isset($_GET['post'])) {
		wp_redirect(home_url('/document-management-system/'));
	} else {
		setcookie('dmsdisplay', 'iframe');
		$_COOKIE['dmsdisplay'] = 'iframe';
	}
?>
<section class="page-load">
	<div class="wrapper page-content dms">
		<div class="row">
			<div class="tp-12">
				<h1>EDIT: <?php echo get_the_title($_GET['post']); ?></h1>
				<hr class="secondary size-l">
				<a class="cta secondary outline" href="<?php echo home_url(); ?>/wp-admin/post-new.php?post_type=iykaa_dms">
					<i class="fa fa-dms-add"></i> Add a New Document
				</a>
				<a class="cta secondary outline" href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=iykaa_dms">
					<i class="fa fa-dms-manage"></i> Manage Documents
				</a>
				<a class="cta secondary <?php if(!isset($_GET['documents'])) { echo 'outline'; } ?>" href="?documents=authored">
					<i class="fa fa-dms-edit"></i> Authored Documents
				</a>
				<hr class="secondary size-l">
				<iframe src="<?php echo home_url(); ?>/wp-admin/post.php?post=<?php echo $_GET['post']; ?>&action=edit&display=iframe"></iframe>
			</div>
		</div>
	</div>
</section>