<?php
	$dmsOrderByValue = '';
	
	if(isset($_GET['order'])) {
		if($_GET['order'] == 'asc') {
			$dmsOrder = 'desc';
		} else {
			$dmsOrder = 'asc';
		}
	} else {
		$dmsOrder = 'desc';
	}
	
	if(isset($_GET['orderby'])) {
		if($_GET['orderby'] == 'dms_fn') {
			$dmsOrderBy = 'title';
		} elseif($_GET['orderby'] == 'dms_lu') {
			$dmsOrderBy = 'modified';
		} elseif($_GET['orderby'] == 'dms_ft') {
			$dmsOrderBy = 'meta_value';
			$dmsOrderByValue = 'dms_file_type';
		} elseif($_GET['orderby'] == 'dms_r') {
			$dmsOrderBy = 'meta_value_num';
			$dmsOrderByValue = 'dms_revision_count';
		} else {
			$dmsOrderBy = 'meta_value_num';
			$dmsOrderByValue = 'dms_file_size';
		}
	} else {
		$dmsOrderBy = 'modified';
	}
?>

<section class="page-load">
	<div class="wrapper page-content">
		<div class="row">
			<div class="tp-12">
				<h3>Document Management System</h3>
				<hr class="secondary size-l">
				<table class="dms-files" width="100%">
					<thead>
						<tr>
							<td width="25%">
								<a href="?orderby=dms_fn&order=<?php echo $dmsOrder; ?>">
									File Name <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fn') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="35%">
								File Summary
							</td>
							<td width="10%">
								<a href="?orderby=dms_fs&order=<?php echo $dmsOrder; ?>">
									File Size <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fs') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="10%">
								<a href="?orderby=dms_ft&order=<?php echo $dmsOrder; ?>">
									File Type <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_ft') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="10%">
								<a href="?orderby=dms_r&order=<?php echo $dmsOrder; ?>">
								Revisions <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fr') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
							</td>
							<td width="15%">
								<a href="?orderby=dms_lu&order=<?php echo $dmsOrder; ?>">
									Last Updated <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_lu') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
						</tr>
					</thead>
					<?php
						// Member details
						global $current_user;
						
						$args = array(
							'author' => $current_user->ID,
							'post_type' => 'iykaa_members',
							'post_status' => array('pending', 'draft', 'publish'),
						);
						
						$membersProfile = new WP_Query($args);
						
						if($membersProfile->have_posts()) {
							while($membersProfile->have_posts()) {
								$membersProfile->the_post();
								$memberProfileID = $membersProfile->post->ID;
							}
						}
						
						$departmentIDs = array(134);
						
						$user = wp_get_current_user();
						$allowedRoles = array('deity_iykaa');
						
						if(array_intersect($allowedRoles, $user->roles)) {
							$terms = get_terms('department_tax');
							
							foreach($terms as $term) {
								array_push($departmentIDs, $term->term_id);
							}
						}
						
						$departmentTerms = get_the_terms($memberProfileID, 'department_tax');
						
						foreach($departmentTerms as $departmentTerm) {
							array_push($departmentIDs, $departmentTerm->term_id);
						}
						
						$taxQuery = array();
						
						array_push($taxQuery,
							array('relation' => 'OR',
								array(
									'taxonomy' => 'department_tax',
									'field'    => 'term_id',
									'terms'    => $departmentIDs,
								)
							),
							array(
								'taxonomy' => 'iykaa_members',
								'field'    => 'term_id',
								'terms'    => $memberProfileID,
								'operator' => 'IN'
							)
						);
						
/*
						array_push($taxQuery,
							array('relation' => 'OR',
								array(
									'taxonomy' => 'department_tax',
									'field'    => 'term_id',
									'terms'    => $departmentIDs,
								),
								array(
									'taxonomy' => 'iykaa_members',
									'field'    => 'ID',
									'terms'    => $departmentIDs,
								)
							)
						);
*/
						
						$metaQuery = array(
							array(
								'key' 		=> 'member_access',
								'value' 	=> $memberProfileID,
								'compare' 	=> 'IN',
							)
						);
						
						$args = array(
							'post_type' 		=> 'iykaa_dms',
							'order' 			=> $dmsOrder,
							'orderby' 			=> $dmsOrderBy,
							'posts_per_page' 	=> -1,
							'post_status' 		=> 'publish',
							'tax_query' 		=> $taxQuery,
							'meta_key' 			=> $dmsOrderByValue,
							// 'meta_query' 		=> $metaQuery,
						);
						
						$dmsSettings = new WP_Query($args);
						
						$memberID = array();
						$authorID = array();
						$filePostData = array();
					?>
					
					<tbody>
						<?php
							while($dmsSettings->have_posts()) {
								$dmsSettings->the_post();
								
								$fileSummary = array();
								$fileSizes = array();
								
								while(have_rows('file_settings')) {
									the_row();
									
									if(get_field('document_summary')) {
										$fileSummary = get_field('document_summary');
									} else {
										$fileSummary = get_sub_field('summary_notes');
									}
									
									$fileID = get_sub_field('select_file')['id'];
									$fileSize = filesize(get_attached_file($fileID));
									$fileSize = size_format($fileSize, 1);
									$fileSizes = $fileSize;
									$fileType = pathinfo(get_attached_file($fileID), PATHINFO_EXTENSION);
								}
								
								include('templates/table-list-all-tmp.php');
							}
							
							$memberID = array();
							$authorID = array();
						?>
					</tbody>
					<tfoot>
						<tr>
							<td width="25%">
								<a href="?orderby=dms_fn&order=<?php echo $dmsOrder; ?>">
									File Name <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fn') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="35%">
								File Summary
							</td>
							<td width="10%">
								<a href="?orderby=dms_fs&order=<?php echo $dmsOrder; ?>">
									File Size <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fs') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="10%">
								<a href="?orderby=dms_ft&order=<?php echo $dmsOrder; ?>">
									File Type <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_ft') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
							<td width="10%">
								<a href="?orderby=dms_r&order=<?php echo $dmsOrder; ?>">
								Revisions <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_fr') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
							</td>
							<td width="15%">
								<a href="?orderby=dms_lu&order=<?php echo $dmsOrder; ?>">
									Last Updated <?php if(isset($_GET['orderby']) && $_GET['orderby'] == 'dms_lu') { ?><i class="fa fa-<?php echo $_GET['order']; ?>"></i><?php } ?>
								</a>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</section>

<script>
	jQuery(document).ready(function($) {
		$('.dms-files tbody tr').click(function() {
			window.location = $(this).data('href');
		});
	});
</script>