<section class="page-load">
	<div class="wrapper">
		<div class="row dashbaord-blocks">
			<div class="dt-4">
				<a href="#">
					Edit your member details
				</a>
			</div>
			<?php while($membersProfile->have_posts()) {
				$membersProfile->the_post(); ?>
				<div class="dt-4">
					<div class="inner">
						<a href="<?php the_permalink(); ?>">
							<h6>View your profile page</h6>
						</a>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>