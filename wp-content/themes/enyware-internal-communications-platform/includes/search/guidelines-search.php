<h3>Advanced Search</h3>
<hr class="secondary size-l">
<form class="row advance-search" action="" method="get">
	<div class="tp-6">
		<label>Keywords</label>
		<input type="text" name="keywords" value="<?php if(isset($_REQUEST['keywords'])) { echo $_REQUEST['keywords']; } ?>" placeholder="Keywords...">
	</div>
	<div class="tp-6">
		<label>Document Type</label>
		<?php
			if(isset($_GET['document-type'])) {
				$documentType = $_GET['document-type'];
			} else {
				$documentType = 0;
			}
			
			$args = array(
				'show_option_all'    => '',
				'show_option_none'   => 'Document Type',
				'option_none_value'  => 'false',
				'orderby'            => 'title',
				'order'              => 'ASC',
				'show_count'         => 0,
				'hide_empty'         => 1,
				'child_of'           => 0,
				'exclude'            => '',
				'echo'               => 1,
				'selected'           => $documentType,
				'hierarchical'       => 0,
				'name'               => 'document-type',
				'id'                 => '',
				'class'              => 'document-type',
				'depth'              => 0,
				'tab_index'          => 0,
				'taxonomy'           => 'document_type_tax',
				'hide_if_empty'      => false,
				'value_field'	     => 'slug',
			);
		?>
		<?php wp_dropdown_categories($args); ?>
	</div>
	<div class="tp-4">
		<label>Order By</label>
		<?php
			if(isset($_GET['orderby'])) {
				$orderBy = $_GET['orderby'];
			} else {
				$orderBy = 'false';
			}
		?>
		<select name="orderby">
			<option value="false" <?php if($orderBy == 'false') { echo 'selected="selected"'; } ?>>ORDER BY</option>
			<option value="title" <?php if($orderBy == 'title') { echo 'selected="selected"'; } ?>>Title</option>
			<option value="date" <?php if($orderBy == 'date') { echo 'selected="selected"'; } ?>>Date</option>
		</select>
	</div>
	<div class="tp-4">
		<label>Order</label>
		<?php
			if(isset($_GET['order'])) {
				$order = $_GET['order'];
			} else {
				$order = 'false';
			}
		?>
		<select name="order">
			<option value="false" <?php if($order == 'false') { echo 'selected="selected"'; } ?>>ORDER</option>
			<option value="asc" <?php if($order == 'asc') { echo 'selected="selected"'; } ?>>Ascending</option>
			<option value="desc" <?php if($order == 'desc') { echo 'selected="selected"'; } ?>>Descending</option>
		</select>
	</div>
	<div class="tp-4">
		<label>&nbsp;</label>
		<button type="submit">Filter</button>
	</div>
</form>
<hr class="secondary size-l">