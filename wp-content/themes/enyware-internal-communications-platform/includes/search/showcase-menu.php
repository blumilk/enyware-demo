<nav class="guidelines-menu">
	<span>For Me Menu</span>
	<?php
		wp_nav_menu(
			array(
				'menu' 				=> 'For Me',
				'container_class' 	=> 'library-menu'
			)
		);
	?>
</nav>