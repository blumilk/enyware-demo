<?php
	include(get_template_directory() . '/includes/config/member-details.php');
	if($membersProfile->have_posts()) {
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberProfileID = $membersProfile->post->ID;
		}
	}
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Checks the taxonomies so the system can show the correct notifications
	 *
	 */
	$taxQuery = array();
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Location
	 *
	 */
	$locations = get_the_terms($memberProfileID, 'location_tax');
	if(isset($locations) && !empty($locations)) {
		$locationIDs = array();
		
		foreach($locations as $location) {
			array_push($locationIDs, $location->term_id);
		}
	} else {
		$locationIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'location_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $locationIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'location_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $locationIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Position
	 *
	 */
	$positions = get_the_terms($memberProfileID, 'position_tax');
	if(isset($positions) && !empty($positions)) {
		$positionIDs = array();
		
		foreach($positions as $position) {
			array_push($positionIDs, $position->term_id);
		}
	} else {
		$positionIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'position_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $positionIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'position_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $positionIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Department
	 *
	 */
	$departments = get_the_terms($memberProfileID, 'department_tax');
	if(isset($departments) && !empty($departments)) {
		$departmentIDs = array();
		
		foreach($departments as $department) {
			array_push($departmentIDs, $department->term_id);
		}
	} else {
		$departmentIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'department_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $departmentIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'department_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $departmentIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Band
	 *
	 */
	$bands = get_the_terms($memberProfileID, 'band_tax');
	if(isset($bands) && !empty($bands)) {
		$bandIDs = array();
		
		foreach($bands as $band) {
			array_push($bandIDs, $band->term_id);
		}
	} else {
		$bandIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'band_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $bandIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'band_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $bandIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Occupational Unit
	 *
	 */
	$occupationals = get_the_terms($memberProfileID, 'occupational_tax');
	if(isset($occupationals) && !empty($occupationals)) {
		$occupationalIDs = array();
		
		foreach($occupationals as $occupational) {
			array_push($occupationalIDs, $occupational->term_id);
		}
	} else {
		$occupationalIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'occupational_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $occupationalIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'occupational_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $occupationalIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Business Unit
	 *
	 */
	$businesses = get_the_terms($memberProfileID, 'business_tax');
	if(isset($businesses) && !empty($businesses)) {
		$businessIDs = array();
		
		foreach($businesses as $business) {
			array_push($businessIDs, $business->term_id);
		}
	} else {
		$businessIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'business_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $businessIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'business_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $businessIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * You Are
	 *
	 */
	$youAres = get_the_terms($memberProfileID, 'you_are_tax');
	if(isset($youAres) && !empty($youAres)) {
		$youAreIDs = array();
		
		foreach($youAres as $youAre) {
			array_push($youAreIDs, $youAre->term_id);
		}
	} else {
		$youAreIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'you_are_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $youAreIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'you_are_tax',
				'field'    	=> 'term_id',
				'terms'    	=> $youAreIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
	
	/*
	 *
	 * Custom Taxonomies Checker
	 * Systems Used
	 *
	 */
	$systems = get_the_terms($memberProfileID, 'systems_used');
	if(isset($systems) && !empty($systems)) {
		$systemIDs = array();
		
		foreach($systems as $system) {
			array_push($systemIDs, $system->term_id);
		}
	} else {
		$systemIDs = -1;
	}
	array_push($taxQuery,
		array('relation' => 'OR',
			array(
				'taxonomy' 	=> 'systems_used',
				'field'    	=> 'term_id',
				'terms'    	=> $systemIDs,
				'operator' 	=> 'IN',
			),
			// Allows for results to show if none are set against the post type
			array(
				'taxonomy' 	=> 'systems_used',
				'field'    	=> 'term_id',
				'terms'    	=> $systemIDs,
				'operator' 	=> 'NOT EXISTS',
			)
		)
	);
?>