<section class="page-load">
	<div class="wrapper team page-content">
		<div class="row">
			<div class="tp-12">
				<h3>Contacts</h3>
				<hr class="secondary size-l">
				<div class="search">
					<div class="row nested">
						<form action="<?php echo home_url(); ?>/members" method="get" name="resources">
							<input type="hidden" class="paged" value="1" name="paged">
							<div class="tl-12">
								<?php
									if(isset($_GET['keywords'])) {
										$keywords = $_GET['keywords'];
									} else {
										$keywords = '';
									}
								?>
								<div class="i-icon large icon-ii-search"></div>
								<input type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Name...">
								<button class="cta secondary" type="submit">Search</button>
							</div>
							<div class="advanced-search">
								<div class="ml-12">
									<div class="i-icon large icon-ii-search"></div>
									<p class="open-filters">Advanced Search</p>
								</div>
								<div class="ml-12 filters">
									<div class="tl-4">
										<div class="i-icon large icon-ii-location"></div>
										<?php
											if(isset($_GET['member-location'])) {
												$location = $_GET['member-location'];
											} else {
												$location = 0;
											}
											
											$args = array(
												'show_option_all'    => '',
												'show_option_none'   => 'Location',
												'option_none_value'  => 'false',
												'orderby'            => 'title',
												'order'              => 'ASC',
												'show_count'         => 0,
												'hide_empty'         => 1,
												'child_of'           => 0,
												'exclude'            => '',
												'echo'               => 1,
												'selected'           => $location,
												'hierarchical'       => 0,
												'name'               => 'member-location',
												'id'                 => '',
												'class'              => 'member-location',
												'depth'              => 0,
												'tab_index'          => 0,
												'taxonomy'           => 'location_tax',
												'hide_if_empty'      => false,
												'value_field'	     => 'term_id',
											);
										?>
										<?php wp_dropdown_categories($args); ?>
									</div>
									<div class="tl-4">
										<div class="i-icon large icon-ii-status"></div>
										<?php
											if(isset($_GET['member-availability'])) {
												$status = $_GET['member-availability'];
											} else {
												$status = 0;
											}
											
											$args = array(
												'show_option_all'    => '',
												'show_option_none'   => 'Availability',
												'option_none_value'  => 'false',
												'orderby'            => 'title',
												'order'              => 'ASC',
												'show_count'         => 0,
												'hide_empty'         => 0,
												'child_of'           => 0,
												'exclude'            => '',
												'echo'               => 1,
												'selected'           => $status,
												'hierarchical'       => 0,
												'name'               => 'member-availability',
												'id'                 => '',
												'class'              => 'member-availability',
												'depth'              => 0,
												'tab_index'          => 0,
												'taxonomy'           => 'status_tax',
												'hide_if_empty'      => false,
												'value_field'	     => 'term_id',
											);
										?>
										<?php wp_dropdown_categories($args); ?>
									</div>
									<div class="tl-4">
										<div class="i-icon large icon-ii-user-group"></div>
										<?php
											if(isset($_GET['member-department'])) {
												$department = $_GET['member-department'];
											} else {
												$department = 0;
											}
											
											$args = array(
												'show_option_all'    => '',
												'show_option_none'   => 'Department',
												'option_none_value'  => 'false',
												'orderby'            => 'title',
												'order'              => 'ASC',
												'show_count'         => 0,
												'hide_empty'         => 1,
												'child_of'           => 0,
												'exclude'            => '',
												'echo'               => 1,
												'selected'           => $department,
												'hierarchical'       => 0,
												'name'               => 'member-department',
												'id'                 => '',
												'class'              => 'member-department',
												'depth'              => 0,
												'tab_index'          => 0,
												'taxonomy'           => 'department_tax',
												'hide_if_empty'      => false,
												'value_field'	     => 'term_id',
											);
										?>
										<?php wp_dropdown_categories($args); ?>
									</div>
									<!-- Custom Taxonomies -->
									<?php include('custom-search-taxonomies.php'); ?>
								</div>
							</div>
						</form>
					</div>
				</div>
								
				<div class="results">
					<?php
						// Get the deity member page ID's so they can be excluded from results
						$memberArgs = array(
							'role' 	=> 'deity_iykaa',
						);
						
						$memberExcludes = array();
						
						$memberDeities = get_users($memberArgs);
						
						foreach($memberDeities as $member) {
							$args = array(
								'author' 		=> $member->ID,
								'post_type' 	=> 'iykaa_members',
							);
							
							$memberPages = get_posts($args);
							
							foreach($memberPages as $member) {
								array_push($memberExcludes, $member->ID);
							}
						}
						
						if(count($_GET) > 0 && isset($_REQUEST['keywords']) && isset($_REQUEST['member-location']) && isset($_REQUEST['member-availability']) && isset($_REQUEST['member-department'])) {
							$keywords = $_REQUEST['keywords'];
							$memberLocation = $_REQUEST['member-location'];
							$memberStatus = $_REQUEST['member-availability'];
							$memberDepartment = $_REQUEST['member-department'];
							
							// Meta Query search options
							$metaQuery = array();
							/*
							array_push($metaQuery,
								array('relation' => 'OR',
									array(
										'key' => 'overview_resource',
										'value' => $keywords,
										'compare' => 'LIKE'
									),
									array(
										'key' => 'workshop_title',
										'value' => $keywords,
										'compare' => 'LIKE'
									)
								)
							);
							*/
							
							$taxQuery = array();
							
							// Searches by the improvement areas
							if($memberLocation !== 'false') {
								array_push($taxQuery,
									array(
										'taxonomy' => 'location_tax',
										'field'    => 'term_id',
										'terms'    => $memberLocation,
									)
								);
							}
							
							if($memberStatus !== 'false') {
								array_push($taxQuery,
									array(
										'taxonomy' => 'status_tax',
										'field'    => 'term_id',
										'terms'    => $memberStatus,
									)
								);
							}
							
							if($memberDepartment !== 'false') {
								array_push($taxQuery,
									array(
										'taxonomy' => 'department_tax',
										'field'    => 'term_id',
										'terms'    => $memberDepartment,
									)
								);
							}
							
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
							
							
							
							$args = array(
								'post_type' => 'iykaa_members',
								'orderby' => 'modified',
								'order' => 'DESC',
								'posts_per_page' => -1,
								'post_parent' => 0,
								'post_status' => 'publish',
								'meta_query' => $metaQuery,
								'tax_query' => $taxQuery,
								'paged' => $paged,
								's' => $keywords,
								'post__not_in' => $memberExcludes
							);
						} else {
							if(isset($columnCount)) {
								$args = array(
									'post_type' => 'iykaa_members',
									'orderby' => 'rand',
									'order' => 'ASC',
									'posts_per_page' => 6,
									'post_parent' => 0,
									'post_status' => 'publish',
									'post__not_in' => $memberExcludes
								);
							} else {
								$paged = (get_query_var('page')) ? get_query_var('page') : 1;
								
								$args = array(
									'post_type' => 'iykaa_members',
									'orderby' => 'modified',
									'order' => 'DESC',
									'posts_per_page' => 30,
									'post_parent' => 0,
									'post_status' => 'publish',
									'post__not_in' => $memberExcludes,
									'paged'		=>	$paged,
								);
							}
						}
						
						$memberSearch = new WP_Query($args);
					?>
					
					<?php if($memberSearch->have_posts()) { ?>
						<div class="row">
							<?php while($memberSearch->have_posts()) {
								$memberSearch->the_post(); ?>
								<div class="tl-2 tp-4 mp-6 profile-preview">
									<a href="<?php the_permalink(); ?>">
										<?php
											$profileImage = get_field('profile_image');
											$size = 'thumbnail';
											$profileImage = $profileImage['sizes'][ $size ];
										?>
										
										<div class="avatar">
											<?php
												if($profileImage) {
													echo '<img src="' . $profileImage . '">';
												} else {
													echo '<img src="' . home_url() . '/images/avatar-default.svg">';
												}
											?>
										</div>
										
										<?php	
											$name = get_the_title();
											
											$name = preg_replace('/\s+/', ' ', $name);
										?>
										
										<span class="title"><?php echo $name; ?></span>
										
										<?php
											$availabilityTax = get_the_terms($post->ID, 'status_tax');
											$availability = '';
											
											if(is_array($availabilityTax)) {
												foreach($availabilityTax as $status) {
													$availability = $status->name;
												}
											}
										?>
										
										<hr class="secondary size-s">
										
										<?php
											if(get_field('status_note')) {
												$statusNote = ' note';
											} else {
												$statusNote = '';
											}
										?>
										
										<?php if(!empty($availability)) { ?>
											<div class="status <?php echo strtolower($availability) . $statusNote; ?>">
												<?php echo $availability; ?>
											</div>
										<?php } ?>
									</a>
								</div>
							<?php } ?>
							
							<?php wp_reset_query(); ?>
							<?php
								if(is_page_template( 'page-dashboard.php' )){
									
								} else {
								if($memberSearch->max_num_pages > 1) { ?>
								<div class="tl-12">
									<nav class="pagination">
										<ul>
											<?php
												if(isset($_GET['page'])) {
													$currentPage = $_GET['page'];
													$currentPagePlus = $paged;
												} else {
													$currentPage = 1;
													$currentPagePlus = 0;
												}
											?>
											
											<?php if(isset($currentPage) && $currentPage > 1) { ?>
												<a href="?page=1">
													<li>
														<i class="fa fa-double-left"></i>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage > 1) { ?>
												<a href="?page=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
													<li>
														<i class="fa fa-single-left"></i>
													</li>
												</a>
											<?php } ?>
											<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
												<?php if($i < $memberSearch->max_num_pages - 1 && $i > 0) { ?>
													<a href="?page=<?php echo $i; ?>">
														<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
															<?php echo $i; ?>
														</li>
													</a>
												<?php } ?>
											<?php } ?>
											<?php if($memberSearch->max_num_pages > 5) { ?>
												<li>
													...
												</li>
											<?php } ?>
											<?php for($i = $memberSearch->max_num_pages - 1; $i <= $memberSearch->max_num_pages; $i++) { ?>
												<a href="?page=<?php echo $i; ?>">
													<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
														<?php echo $i; ?>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage < $memberSearch->max_num_pages) { ?>
												<a href="?page=<?php if(isset($currentPage) && $currentPage < $memberSearch->max_num_pages) { echo $currentPage + 1; } ?>">
													<li>
														<i class="fa fa-single-right"></i>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage < $memberSearch->max_num_pages) { ?>
												<a href="?page=<?php if(isset($currentPage) && $currentPage < $memberSearch->max_num_pages) { echo $memberSearch->max_num_pages; } ?>">
													<li>
														<i class="fa fa-double-right"></i>
													</li>
												</a>
											<?php } ?>
										</ul>
									</nav>
								</div>
							<?php } ?>
						<?php } ?>
						</div>
					<?php } else { ?>
						<p>Sorry but no results were found.</p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>