<nav class="guidelines-menu">
	<span>Nursing Competencies Menu</span>
	<?php
		wp_nav_menu(
			array(
				'menu' 				=> 'Nursing Competencies',
				'container_class' 	=> 'library-menu'
			)
		);
	?>
</nav>