<section>
	<div class="row">
		<div class="tp-12">
			<?php
				// Parent Page Back Button
				$parent = wp_get_post_parent_id($post_ID);
				if($parent){ 
					$parent_post_id = $parent;
					$parent_post = get_post($parent_post_id);
					$parent_post_title = $parent_post->post_title;
					$parent_post_url = get_permalink($parent_post);	
					?>
						<a class="menu-back" href="<?php echo $parent_post_url;?>">Go back to: <span><?php echo $parent_post_title;?></span></a>
			<?php } ?>
			
			<?php
				// Auto Populating Libarary Menu
				if (is_single() && is_post_type('iykaa_library_custom')){
					if(!$post->post_parent) {
						// will display the subpages of this top level page
						$children = wp_list_pages("post_type=iykaa_library_custom&title_li=&child_of=" . $post->ID . "&echo=0");
						$title = get_the_title($post->ID);
					} else {
						// diplays only the subpages of parent level
						if($post->ancestors) {
							// now you can get the the top ID of this page
							// wp is putting the ids DESC, thats why the top level ID is the last one
							$ancestors = end($post->ancestors);
							$children = wp_list_pages("post_type=iykaa_library_custom&title_li=&child_of=".$post->post_parent."&echo=0");
							// you will always get the whole subpages list
							$title = get_the_title( $post->post_parent );
							$parentLink = get_permalink($post->post_parent);
						}
					}
				} else {
					if(!$post->post_parent) {
						// will display the subpages of this top level page
						$children = wp_list_pages("post_type=iykaa_library&title_li=&child_of=" . $post->ID . "&echo=0");
						$title = get_the_title($post->ID);
					} else {
						// diplays only the subpages of parent level
						if($post->ancestors) {
							// now you can get the the top ID of this page
							// wp is putting the ids DESC, thats why the top level ID is the last one
							$ancestors = end($post->ancestors);
							$children = wp_list_pages("post_type=iykaa_library&title_li=&child_of=".$post->post_parent."&echo=0");
							// you will always get the whole subpages list
							$title = get_the_title( $post->post_parent );
							$parentLink = get_permalink($post->post_parent);
						}
					}	
				}
			
			if($children) { ?>
			<div id="library-menu">
				<ul class="library-menu">
					<?php echo $children; ?>
				</ul>
			</div>
			
			<div class="mobile-library-menu" id="menu-trigger-library">
				<p>Menu</p>
				<ul>
					<?php echo $children; ?>
				</ul>
			</div>
			
			<?php } else { ?>
			
			<?php } ?>
		</div>
	</div>
</section>