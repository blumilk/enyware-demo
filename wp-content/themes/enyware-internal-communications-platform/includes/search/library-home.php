<section class="page-load">
	<div class="wrapper library page-content">
		<?php include('guidelines-menu.php'); ?>
		<div class="row">
			<main class="tl-8">
				<div class="main">
					<h3><?php the_title(); ?></h3>
					<hr class="secondary size-l">
					<?php the_content();?>
				</div>
			</main>
			<?php include(get_template_directory() . '/includes/menus/guidelines-menu.php'); ?>
		</div>
	</div>
</section>