<section class="page-load">
	<div class="wrapper page-content dms">
		<div class="row">
			<div class="tp-12">
				<h3>Document Management System</h3>
				<hr class="secondary size-l">
				<a href="<?php echo home_url(); ?>/add-new-dms" class="cta secondary">
					Add New Document
				</a>
				<a href="<?php echo home_url(); ?>/add-new-dms" class="cta secondary">
					My Documents
				</a>
				<hr class="secondary size-l">
				<div class="search">
					<div class="row nested">
						<form action="<?php echo home_url(); ?>/library" method="get" name="resources">
							<input type="hidden" class="paged" value="1" name="paged">
							<div class="tl-12">
								<?php
									if(isset($_GET['keywords'])) {
										$keywords = $_GET['keywords'];
									} else {
										$keywords = '';
									}
								?>
								<div class="i-icon large icon-ii-search"></div>
								<input type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
								<button class="cta secondary" type="submit">Search</button>
							</div>
						</form>
					</div>
				</div>
				<div class="results">
					<?php
						if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
							$keywords = $_REQUEST['keywords'];
							
							// Meta Query search options
							$metaQuery = array();
							
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							
							$args = array(
								'post_type' => 'iykaa_library',
								'order' => 'ASC',
								'posts_per_page' => -1,
								'post_parent' => 0,
								'post_status' => 'publish',
								'meta_query' => $metaQuery,
								'paged' => $paged,
								's' => $keywords,
							);
						} else {
							if(isset($columnCount)) {
								$args = array(
									'post_type' => 'iykaa_library',
									'order' => 'ASC',
									'posts_per_page' => 3,
									'post_parent' => 0,
									'post_status' => 'publish',
								);
							} else {
								$args = array(
									'post_type' => 'iykaa_library',
									'order' => 'ASC',
									'posts_per_page' => -1,
									'post_parent' => 0,
									'post_status' => 'publish',
								);
							}
						}
						
						$librarySearch = new WP_Query($args);
					?>
					
					<?php if($librarySearch->have_posts()) { ?>
						<div class="row">
							<?php if(isset($columnCount)) {
								$terms = get_terms('download_type_tax',
									array(
										'orderby'    => 'ID',
										'hide_empty' => 1
									)
								);
								
								foreach($terms as $term) {
									$args = array(
										'post_type' 		=> 'iykaa_library',
										'download_type_tax' => $term->slug,
										'posts_per_page' 	=> 3,
									);
									
									$query = new WP_Query($args); ?>
									<div class="tl-4">
										<hr class="grey">
										<h3>
											<a href="<?php echo get_term_link($term->slug, 'download_type_tax'); ?>">
												<?php echo $term->name; ?>
											</a>
										</h3>
										<hr class="secondary size-l">
										<div class="row">
											<?php while($query->have_posts()) {
												$query->the_post(); ?>
												<div class="tl-12">
													<a href="<?php the_permalink(); ?>">
														<div class="inner">
															<h4 class="title"><?php the_title(); ?></h4>
															<hr class="secondary size-s">
															<?php the_excerpt(); ?>
														</div>
														<div class="find-out-more">
															<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
																Find Out More
															</div>
															<div class="chevron">
																&#x63;
															</div>
														</div>
													</a>
												</div>
											<?php } ?>
										</div>
										<?php wp_reset_query(); ?>
									</div>
								<?php } ?>
							<?php } else { ?>
								<?php while($librarySearch->have_posts()) {
									$librarySearch->the_post();
									if(get_sub_field('cta_colour')) {
										$ctaColour = the_sub_field('cta_colour');
									} else {
										$ctaColour = 'secondary';
									} ?>
									<a href="<?php the_permalink(); ?>">
										<div class="tl-4">
											<div class="inner">
												<h4 class="title"><?php the_title(); ?></h4>
												<hr class="secondary size-s">
												<?php the_excerpt(); ?>
											</div>
											<div class="find-out-more">
												<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
													Find Out More
												</div>
												<div class="chevron">
													&#x63;
												</div>
											</div>
										</div>
									</a>
								<?php } ?>
								<?php wp_reset_query(); ?>
							<?php } ?>
						</div>
					<?php } else { ?>
						<p>Sorry but no results were found.</p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>