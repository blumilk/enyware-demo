<?php
	$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
	$TaxTerm = $term->slug;
	$TaxName = $term->name;
?>
<section class="page-load">
	<div class="wrapper guidelines page-content <?php echo $TaxTerm ?>">
		<div class="row">
			<div class="dt-12">
				<?php include(get_stylesheet_directory() . '/includes/search/guidelines-menu.php'); ?>
			</div>
			<div class="tl-8">
				<?php
					$paged = (get_query_var('page')) ? get_query_var('page') : 1;
					
					$args = array(
						'post_type' 		=> 'iykaa_library_custom',
						'order' 			=> 'ASC',
						'orderby' 			=> 'title',
						'posts_per_page' 	=> 30,
						'post_status' 		=> 'publish',
						'tax_query' 		=> array(
							'relation' => 'AND',
							array(
								'taxonomy' 	=> 'document_type_tax',
								'field' 	=> 'slug',
								'terms' 	=> $TaxTerm,
							),
							array(
								'taxonomy' 	=> 'document_type_tax',
								'field' 	=> 'slug',
								'terms' 	=> 'useful-document',
							)
						),
						'paged'				=> $paged,
					);
					
					$usefulDocuments = new WP_Query($args);
				?>
				
				<?php if($usefulDocuments->have_posts()) { ?>
					<section class="useful-docs">
						<h3>Useful Documents <i class="i-icon icon-ii-chevron-down"></i></h3>
						<article>
							<?php while($usefulDocuments->have_posts()) {
								$usefulDocuments->the_post(); ?>
								<p>
									<a href="<?php the_permalink(); ?>">
										<?php the_title();?>
									</a>
								</p>
							<?php } ?>
						</article>
					</section>
				<?php } ?>
				
				<div class="results">
					<?php if(count($_GET) == 0) { ?>
						<h3><?php echo($TaxName);?></h3>
						<hr class="secondary size-l">
					<?php } else { ?>
						<h3>Search Results</h3>
						<hr class="secondary size-l">
					<?php } ?>
					
					<?php
						if(count($_GET) > 0) {
							if(isset($_REQUEST['document-type']) && $_REQUEST['document-type'] != 'false') {
								$TaxTerm = $_REQUEST['document-type'];
							}
							
							if(isset($_REQUEST['orderby']) && $_REQUEST['orderby'] != 'false') {
								$orderBy = $_REQUEST['orderby'];
							} else {
								$orderBy = 'title';
							}
							
							if(isset($_REQUEST['order']) && $_REQUEST['order'] != 'false') {
								$order = $_REQUEST['order'];
							} else {
								$order = 'ASC';
							}
							
							$tax_query[] = array(
								'taxonomy' 			=> 'document_type_tax',
								'field' 			=> 'slug',
								'terms' 			=> $TaxTerm,
							);
							
							$metaQuery = array();
							
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
							
							if(isset($_REQUEST['keywords']) && $_REQUEST['keywords'] != '') {
								$keywords = $_REQUEST['keywords'];
								
								array_push($metaQuery,
									array(
										'relation' 		=> 'OR',
										array(
											'key' 		=> 'search_priority',
											'value' 	=> $keywords,
											'compare' 	=> 'LIKE',
										),
									)
								);
								
								$args = array(
									'post_type' 		=> 'iykaa_library_custom',
									'order' 			=> $order,
									'orderby' 			=> $orderBy,
									'posts_per_page' 	=> -1,
									'post_status' 		=> 'publish',
									// 'meta_query' 		=> $metaQuery,
									'tax_query' 		=> $tax_query,
									'paged' 			=> $paged,
									's' 				=> $keywords,
								);
							} else {
								$args = array(
									'post_type' 		=> 'iykaa_library_custom',
									'order' 			=> $order,
									'orderby' 			=> $orderBy,
									'posts_per_page' 	=> -1,
									'post_status' 		=> 'publish',
									'tax_query' 		=> $tax_query,
									'paged' 			=> $paged,
								);
							}
						} else {
							$tax_query[] = array(
								'taxonomy' 			=> 'document_type_tax',
								'field' 			=> 'slug',
								'terms' 			=> $TaxTerm,
							);
							
							$paged = (get_query_var('page')) ? get_query_var('page') : 1;
							
							$args = array(
								'post_type' 		=> 'iykaa_library_custom',
								'order' 			=> 'ASC',
								'orderby' 			=> 'title',
								'posts_per_page' 	=> -1,
								'post_status' 		=> 'publish',
								'tax_query' 		=> $tax_query,
								'paged'				=> $paged,
							);
						}
						
						$librarySearch = new WP_Query($args);
						
						if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
							if(function_exists('mdst_search_execute')) {
								$resultsFound = $librarySearch->found_posts;
								$postTypeName = 'Clinical Guidelines';
								
								if(isset($_REQUEST['document-type']) && $_REQUEST['document-type'] != 'false') {
									$TaxTerm = ucwords(str_replace('-', ' ', $_REQUEST['document-type']));
								} else {
									$TaxTerm = '–';
								}
								
								mdst_search_execute($resultsFound, $TaxTerm, $postTypeName);
							}
						}
					?>
					
					<?php if($librarySearch->have_posts()) { ?>
						<section class="a-z">
							<?php
								$alpha = array();
								$currentLetter = '';
								
								while($librarySearch->have_posts()) {
									$librarySearch->the_post();
									$thisLetter = strtoupper(substr(get_the_title(), 0, 1));
									
									if($thisLetter != $currentLetter) {
										array_push($alpha, $thisLetter);
										$currentLetter = $thisLetter;
									}
								}
								
								foreach($alpha as $letter) {
									echo '<a href="#' . $letter . '">' . $letter . '</a>';
								}
							?>
						</section>
						<?php $currentLetter = ''; ?>
						<?php while($librarySearch->have_posts()) {
							$librarySearch->the_post(); ?>
							<?php
								$thisLetter = strtoupper(substr(get_the_title(), 0, 1));
								
								if($thisLetter != $currentLetter) {
									echo '<span id="' . $thisLetter . '"></span>';
									$currentLetter = $thisLetter;
								}
							?>
							
							<?php
								if(get_sub_field('cta_colour')) {
									$ctaColour = the_sub_field('cta_colour');
								} else {
									$ctaColour = 'secondary';
								}
							?>
							
							<h5>
								<a href="<?php the_permalink(); ?>">
									<?php the_title();?>
								</a>
							</h5>
							<?php
								// Doesn't show the full content for the A-Z taxonomy
								if(!is_tax('document_type_tax', 'clinical-guidelines-a-z')) {
									the_content();
								}
							?>
							<hr class="secondary">
						<?php } ?>
						<?php wp_reset_query(); ?>
						<div class="row">
							<?php if($librarySearch->max_num_pages > 1) { ?>
								<div class="tl-12">
									<nav class="pagination">
										<ul>
											<?php
												if(isset($_GET['page'])) {
													$currentPage = $_GET['page'];
													$currentPagePlus = $paged;
												} else {
													$currentPage = 1;
													$currentPagePlus = 0;
												}
											?>
											
											<?php if(isset($currentPage) && $currentPage > 1) { ?>
												<a href="?page=1">
													<li>
														<i class="fa fa-double-left"></i>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage > 1) { ?>
												<a href="?page=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
													<li>
														<i class="fa fa-single-left"></i>
													</li>
												</a>
											<?php } ?>
											<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
												<?php if($i < $librarySearch->max_num_pages - 1 && $i > 0) { ?>
													<a href="?page=<?php echo $i; ?>">
														<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
															<?php echo $i; ?>
														</li>
													</a>
												<?php } ?>
											<?php } ?>
											<?php if($librarySearch->max_num_pages > 5) { ?>
												<li>
													...
												</li>
											<?php } ?>
											<?php for($i = $librarySearch->max_num_pages - 1; $i <= $librarySearch->max_num_pages; $i++) { ?>
												<a href="?page=<?php echo $i; ?>">
													<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
														<?php echo $i; ?>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage < $librarySearch->max_num_pages) { ?>
												<a href="?page=<?php if(isset($currentPage) && $currentPage < $librarySearch->max_num_pages) { echo $currentPage + 1; } ?>">
													<li>
														<i class="fa fa-single-right"></i>
													</li>
												</a>
											<?php } ?>
											<?php if(isset($currentPage) && $currentPage < $librarySearch->max_num_pages) { ?>
												<a href="?page=<?php if(isset($currentPage) && $currentPage < $librarySearch->max_num_pages) { echo $librarySearch->max_num_pages; } ?>">
													<li>
														<i class="fa fa-double-right"></i>
													</li>
												</a>
											<?php } ?>
										</ul>
									</nav>
								</div>
							<?php } ?>
						</div>
					<?php } else { ?>
						<p>Sorry but no results were found.</p>
					<?php } ?>
				</div>
			</div>
			<?php include(get_template_directory() . '/includes/menus/guidelines-menu.php'); ?>
		</div>
	</div>
</section>