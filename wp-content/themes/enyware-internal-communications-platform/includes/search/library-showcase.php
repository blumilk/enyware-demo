<section class="page-load">
	<div class="wrapper library page-content library-showcase">
		
		<?php if(get_field('page_title')){ ?>
			<section>
				<div class="row">
					<div class="tp-12">
						<h3><?php the_field('page_title'); ?></h3>
						<hr class="secondary size-l">
					</div>
				</div>
			</section>
		<?php } else { ?>
			<section>
				<div class="row">
					<div class="tp-12">
						<h3><?php the_title(); ?></h3>
						<hr class="secondary size-l">
					</div>
				</div>
			</section>
		<?php } ?>
		
		<?php include(get_stylesheet_directory() . '/includes/search/showcase-menu.php'); ?>
		
		<?php if(have_rows( 'banners' )){ ?>
			<section class="banner-wrap">
				<div class="row">
					<div class="mp-12">
						<div class="row results tap-throughs large showcase">
							<?php while(have_rows('banners')) { ?>
								<?php the_row(); 
									$image = get_sub_field('image');
									$size = 'large';
									
									$bgImage = $image['sizes'][$size];
									
									if(get_sub_field('external_link')) {
										$linkURL = get_sub_field('link');
										$linkTarget = '_blank';
									} else {
										$linkURL = get_sub_field('page_link');
										$linkTarget = '_self';
									}
								?>
								
								<a href="<?php echo $linkURL; ?>" target="<?php echo $linkTarget; ?>">
									<div class="mp-12">
										<div class="inner">
											<h4 class="title"><?php the_sub_field( 'title' ); ?></h4>
											<hr class="size-s ">
											<p><?php the_sub_field( 'description_text' ); ?></p>
										</div>
									</div>
									<div class="mp-12">
										<?php if(get_sub_field('image')) { ?>
											<img src="<?php echo $bgImage; ?>">
										<?php } ?>
									</div>
									<div class="mp-12">
										<div class="find-out-more">
											<div class="cta size-s">
												Find Out More
											</div>
											<div class="chevron">
												&#x63;
											</div>
										</div>
									</div>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
		<?php } ?>
		
		<?php if(have_rows('small_block')) { ?>
			<section>
				<div class="row">
					<div class="mp-12">
						<div class="row results tap-throughs">
							<?php while(have_rows('small_block')) {
								the_row(); ?>
								<?php
									$image = get_sub_field('image');
									$size = 'large';
									
									$bgImage = $image['sizes'][$size];
									
									if(get_sub_field('external_link')) {
										$linkURL = get_sub_field('link');
										$linkTarget = '_blank';
									} else {
										$linkURL = get_sub_field('page_link');
										$linkTarget = '_self';
									}
								?>
								
								<a href="<?php echo $linkURL; ?>" target="<?php echo $linkTarget; ?>">
									<div class="tp-4 show-block">
										<?php if(get_sub_field('image_only')) { ?>
											<?php if(get_sub_field('image')) { ?>
												<img src="<?php echo $bgImage; ?>">
											<?php } ?>
											<div class="row">
												<div class="mp-12">
													<div class="find-out-more">
														<div class="cta size-s">
															Find Out More
														</div>
														<div class="chevron">
															&#x63;
														</div>
													</div>
												</div>
											</div>
										<?php } else { ?>
											<div class="row">
												<div class="mp-12">
													<div class="inner">
														<h4 class="title"><?php the_sub_field('title'); ?></h4>
														<hr class="size-s">
													</div>
												</div>
												<div class="mp-12">
													<?php if(get_sub_field('image')) { ?>
														<img src="<?php echo $bgImage; ?>">
													<?php } ?>
												</div>
												<div class="mp-12">
													<div class="find-out-more">
														<div class="cta size-s">
															Find Out More
														</div>
														<div class="chevron">
															&#x63;
														</div>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
								</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
		<?php } ?>
	</div>
</section>