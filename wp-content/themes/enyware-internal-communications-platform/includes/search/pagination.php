<?php if($newsArticles->max_num_pages > 1) { ?>
	<nav class="pagination">
		<ul>
			<?php
				if(isset($_GET['page'])) {
					$currentPage = $_GET['page'];
					$currentPagePlus = $page;
				} else {
					$currentPagePlus = 0;
				}
			?>
			
			<?php if(isset($currentPage) && $currentPage > 1) { ?>
				<a href="?page=1">
					<li>
						<< First
					</li>
				</a>
			<?php } ?>
			
			<?php if(isset($currentPage) && $currentPage > 1) { ?>
				<a href="?page=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
					<li>
						< Previous
					</li>
				</a>
			<?php } ?>
			
			<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
				<?php if($i < $newsArticles->max_num_pages - 1 && $i > 0) { ?>
					<a href="?page=<?php echo $i; ?>">
						<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
							<?php echo $i; ?>
						</li>
					</a>
				<?php } ?>
			<?php } ?>
			
			<?php if($newsArticles->max_num_pages > 5) { ?>
				<li>
					...
				</li>
			<?php } ?>
			
			<?php for($i = $newsArticles->max_num_pages - 1; $i <= $newsArticles->max_num_pages; $i++) { ?>
				<a href="?page=<?php echo $i; ?>">
					<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
						<?php echo $i; ?>
					</li>
				</a>
			<?php } ?>
			
			<?php if(isset($currentPage) && $currentPage < $newsArticles->max_num_pages) { ?>
				<a href="?page=<?php if(isset($currentPage) && $currentPage < $newsArticles->max_num_pages) { echo $currentPage + 1; } ?>">
					<li>
						Next >
					</li>
				</a>
			<?php } elseif(!isset($currentPage)) { ?>
				<a href="?page=2">
					<li>
						Next >
					</li>
				</a>
			<?php } ?>
			
			<?php if(isset($currentPage) && $currentPage < $newsArticles->max_num_pages) { ?>
				<a href="?page=<?php if(isset($currentPage) && $currentPage < $newsArticles->max_num_pages) { echo $newsArticles->max_num_pages; } ?>">
					<li>
						Last >>
					</li>
				</a>
			<?php } elseif(!isset($currentPage)) { ?>
				<a href="?page=<?php echo $newsArticles->max_num_pages; ?>">
					<li>
						Last >>
					</li>
				</a>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>