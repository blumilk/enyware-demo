<section class="page-load">
	<div class="wrapper notifications page-content">
		<div class="row">
			<div class="tp-12">
				<h3>Notifications</h3>
				<hr class="secondary size-l">
				<div class="search">
					<div class="row nested">
						<form action="<?php echo home_url(); ?>/notifications" method="get" name="resources">
							<input type="hidden" class="paged" value="1" name="paged">
							<div class="tl-4">
								<div class="i-icon large icon-ii-notification"></div>
								<?php
									if(isset($_GET['notification-priority'])) {
										$notificationPriority = $_GET['notification-priority'];
									} else {
										$notificationPriority = 0;
									}
									
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => 'Notification Priority',
										'option_none_value'  => 'false',
										'orderby'            => 'ID',
										'order'              => 'DESC',
										'show_count'         => 0,
										'hide_empty'         => 0,
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => $notificationPriority,
										'hierarchical'       => 0,
										'name'               => 'notification-priority',
										'id'                 => '',
										'class'              => 'notification-priority',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'notification_priority_tax',
										'hide_if_empty'      => false,
										'value_field'	     => 'term_id',
									);
								?>
								<?php wp_dropdown_categories($args); ?>
							</div>
							<div class="tl-8 small-search">
								<div class="i-icon large icon-ii-search"></div>
								<?php
									if(isset($_GET['keywords'])) {
										$keywords = $_GET['keywords'];
									} else {
										$keywords = '';
									}
								?>
								
								<input type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
								<button class="cta secondary" type="submit">SEARCH</button>
							</div>
						</form>
					</div>
				</div>
				<div class="results">
					<?php
						if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
							$keywords = $_REQUEST['keywords'];
							$notificationPriority = $_REQUEST['notification-priority'];
							
							// Meta Query search options
							$metaQuery = array();
							
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							
							$taxQuery = array();
							
							// Searches by the improvement areas
							if($notificationPriority !== 'false') {
								array_push($taxQuery,
									array(
										'taxonomy' => 'notification_priority_tax',
										'field'    => 'term_id',
										'terms'    => $notificationPriority,
									)
								);
							}
							
							$args = array(
								'post_type' => 'iykaa_notifications',
								'order' => 'DESC',
								'orderby' => 'date',
								'posts_per_page' => -1,
								'post_parent' => 0,
								'post_status' => 'publish',
								'meta_query' => $metaQuery,
								'tax_query' => $taxQuery,
								'paged' => $paged,
								's' => $keywords,
							);
						} else {
							if(isset($columnCount)) {
								$args = array(
									'post_type' => 'iykaa_notifications',
									'order' => 'DESC',
									'orderby' => 'date',
									'posts_per_page' => 6,
									'post_parent' => 0,
									'post_status' => 'publish',
								);
							} else {
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								
								$args = array(
									'post_type' => 'iykaa_notifications',
									'order' => 'DESC',
									'orderby' => 'date',
									'posts_per_page' => 30,
									'post_parent' => 0,
									'post_status' => 'publish',
									'paged'		=>	$paged,
								);
							}
						}
						
						$notificationsSearch = new WP_Query($args);
					?>
					
					<?php if($notificationsSearch->have_posts()) { ?>
						<div class="row">
							<?php if(isset($columnCount)) {
								$terms = get_terms('download_type_tax',
									array(
										'orderby'    => 'ID',
										'hide_empty' => 1
									)
								);
								
								foreach($terms as $term) {
									$args = array(
										'post_type' 		=> 'iykaa_notifications',
										'download_type_tax' => $term->slug
									);
									
									$query = new WP_Query($args); ?>
									<div class="tl-4">
										<hr class="grey">
										<h3>
											<?php echo $term->name; ?>
										</h3>
										<hr class="secondary size-l">
										<div class="row">
											<?php while($query->have_posts()) {
												$query->the_post(); ?>
												<div class="tl-12">
													<a href="<?php the_permalink(); ?>">
														<div class="inner">
															<h4 class="title"><?php the_title(); ?></h4>
															<hr class="secondary size-s">
															<p class="time-stamp"><?php echo get_the_date('l jS F, Y | g:i a'); ?></p>
															<hr class="secondary size-s">
															<?php the_excerpt(); ?>
														</div>
													</a>
												</div>
											<?php } ?>
										</div>
										<?php wp_reset_query(); ?>
									</div>
								<?php } ?>
							<?php } else { ?>
								<?php while($notificationsSearch->have_posts()) {
									$notificationsSearch->the_post(); ?>
									<a href="<?php the_permalink(); ?>">
										<div class="mp-12 notifications <?php the_field('notification_priority'); ?>">
											<div class="inner">
												<h4 class="title"><?php the_title(); ?></h4>
												<hr class="secondary size-s">
												<p class="time-stamp"><?php echo get_the_date('l jS F, Y | g:i a'); ?></p>
												<hr class="secondary size-s">
												<?php the_excerpt(); ?>
											</div>
										</div>
									</a>
								<?php } ?>
								
								<?php wp_reset_query(); ?>
								<?php if($notificationsSearch->max_num_pages > 1) { ?>
									<div class="tl-12">
										<nav class="pagination">
											<ul>
												<?php
													if(isset($_GET['paged'])) {
														$currentPage = $_GET['paged'];
														$currentPagePlus = $paged;
													} else {
														$currentPage = 1;
														$currentPagePlus = 0;
													}
												?>
												
												<?php if(isset($currentPage) && $currentPage > 1) { ?>
													<a href="?paged=1">
														<li>
															<i class="fa fa-double-left"></i>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage > 1) { ?>
													<a href="?paged=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
														<li>
															<i class="fa fa-single-left"></i>
														</li>
													</a>
												<?php } ?>
												<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
													<?php if($i < $notificationsSearch->max_num_pages - 1 && $i > 0) { ?>
														<a href="?paged=<?php echo $i; ?>">
															<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
																<?php echo $i; ?>
															</li>
														</a>
													<?php } ?>
												<?php } ?>
												<?php if($notificationsSearch->max_num_pages > 5) { ?>
													<li>
														...
													</li>
												<?php } ?>
												<?php for($i = $notificationsSearch->max_num_pages - 1; $i <= $notificationsSearch->max_num_pages; $i++) { ?>
													<a href="?paged=<?php echo $i; ?>">
														<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
															<?php echo $i; ?>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage < $notificationsSearch->max_num_pages) { ?>
													<a href="?paged=<?php if(isset($currentPage) && $currentPage < $notificationsSearch->max_num_pages) { echo $currentPage + 1; } ?>">
														<li>
															<i class="fa fa-single-right"></i>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage < $notificationsSearch->max_num_pages) { ?>
													<a href="?paged=<?php if(isset($currentPage) && $currentPage < $notificationsSearch->max_num_pages) { echo $notificationsSearch->max_num_pages; } ?>">
														<li>
															<i class="fa fa-double-right"></i>
														</li>
													</a>
												<?php } ?>
											</ul>
										</nav>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } else { ?>
						<p>Sorry but no results were found.</p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>