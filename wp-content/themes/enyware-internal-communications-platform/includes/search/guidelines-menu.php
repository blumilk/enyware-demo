<nav class="guidelines-menu">
	<span>Clinical Guidelines Menu</span>
	<?php
		wp_nav_menu(
			array(
				'menu' 				=> 'Clinical Guidelines',
				'container_class' 	=> 'library-menu'
			)
		);
	?>
</nav>