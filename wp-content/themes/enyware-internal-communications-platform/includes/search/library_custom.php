<section class="page-load">
	<div class="wrapper library page-content">
		
		<?php include('guidelines-menu.php'); ?>
		
		<div class="row">
			<div class="tl-8">
				<h3>Clinical Documents Library</h3>
				<hr class="secondary size-l">
				<div class="results">
					<?php
						if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
							$keywords = $_REQUEST['keywords'];
							
							// Meta Query search options
							$metaQuery = array();
							
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							
							$args = array(
								'post_type' 		=> 'iykaa_library_custom',
								'order' 			=> 'ASC',
								'posts_per_page' 	=> -1,
								'post_parent' 		=> 0,
								'post_status' 		=> 'publish',
								'meta_query' 		=> $metaQuery,
								'paged' 			=> $paged,
								's' 				=> $keywords,
							);
						} else {
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							
							$args = array(
								'post_type' 		=> 'iykaa_library_custom',
								'order' 			=> 'ASC',
								'posts_per_page' 	=> -1,
								'post_parent' 		=> 0,
								'post_status' 		=> 'publish',
								'paged'				=>	$paged,
							);
						}
						
						$librarySearch = new WP_Query($args);
						
						if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
							if(function_exists('mdst_search_execute')) {
								$resultsFound = $librarySearch->found_posts;
								$postTypeName = 'Clinical Guidelines';
								
								if(isset($_REQUEST['document-type']) && $_REQUEST['document-type'] != 'false') {
									$TaxTerm = ucwords(str_replace('-', ' ', $_REQUEST['document-type']));
								} else {
									$TaxTerm = '–';
								}
								
								mdst_search_execute($resultsFound, $TaxTerm, $postTypeName);
							}
						}
					?>
					
					<?php if($librarySearch->have_posts()) { ?>
						<?php while($librarySearch->have_posts()) {
							$librarySearch->the_post(); ?>
							<?php
								if(get_sub_field('cta_colour')) {
									$ctaColour = the_sub_field('cta_colour');
								} else {
									$ctaColour = 'secondary';
								}
							?>
							<p>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>
								</a>
							</p>
						<?php } ?>
						<?php wp_reset_query(); ?>
					<?php } else { ?>
						<p>Sorry but no results were found.</p>
					<?php } ?>
				</div>
			</div>
			<?php include(get_template_directory() . '/includes/menus/guidelines-menu.php'); ?>
		</div>
	</div>
</section>