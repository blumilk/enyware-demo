<div class="tl-6">
	<div class="i-icon large icon-ii-user"></div>
	<?php
		if(isset($_GET['occupational-unit'])) {
			$occupational = $_GET['occupational-unit'];
		} else {
			$occupational = 0;
		}
		
		$args = array(
			'show_option_all'    => '',
			'show_option_none'   => 'Occupational Unit',
			'option_none_value'  => 'false',
			'orderby'            => 'title',
			'order'              => 'ASC',
			'show_count'         => 0,
			'hide_empty'         => 1,
			'child_of'           => 0,
			'exclude'            => '',
			'echo'               => 1,
			'selected'           => $occupational,
			'hierarchical'       => 0,
			'name'               => 'occupational-unit',
			'id'                 => '',
			'class'              => 'occupational-unit',
			'depth'              => 0,
			'tab_index'          => 0,
			'taxonomy'           => 'occupational_tax',
			'hide_if_empty'      => false,
			'value_field'	     => 'term_id',
		);
	?>
	<?php wp_dropdown_categories($args); ?>
</div>
<div class="tl-6">
	<div class="i-icon large icon-ii-user"></div>
	<?php
		if(isset($_GET['business-unit'])) {
			$business = $_GET['business-unit'];
		} else {
			$business = 0;
		}
		
		$args = array(
			'show_option_all'    => '',
			'show_option_none'   => 'Business Unit',
			'option_none_value'  => 'false',
			'orderby'            => 'title',
			'order'              => 'ASC',
			'show_count'         => 0,
			'hide_empty'         => 0,
			'child_of'           => 0,
			'exclude'            => '',
			'echo'               => 1,
			'selected'           => $business,
			'hierarchical'       => 0,
			'name'               => 'business-unit',
			'id'                 => '',
			'class'              => 'business-unit',
			'depth'              => 0,
			'tab_index'          => 0,
			'taxonomy'           => 'business_tax',
			'hide_if_empty'      => false,
			'value_field'	     => 'term_id',
		);
	?>
	<?php wp_dropdown_categories($args); ?>
</div>