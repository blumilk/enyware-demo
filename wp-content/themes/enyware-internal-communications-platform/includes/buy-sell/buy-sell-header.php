<div class="tl-12">
	<div class="row">
		<div class="tp-6">
			<h1>Buy and Sell</h1>
		</div>
		<div class="tp-2">
			<a class="cta secondary size-s full" href="<?php echo home_url(); ?>/buy-sell/">All Items</a>
		</div>
		<div class="tp-2">
			<a class="cta secondary size-s full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=my-items">My Items</a>
		</div>
		<div class="tp-2">
			<a class="cta success size-s full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=new-item">Add New Item</a>
		</div>
	</div>
	<hr class="secondary size-l">
</div>