<aside class="dt-3 tl-4">
	<section class="inner popular-search">
		<h3>Search</h3>
		<hr class="secondary size-l">
		<form action="<?php echo home_url(); ?>/buy-sell/" method="get" name="resources">
			<div class="flex-search">
				<input type="hidden" class="paged" value="1" name="paged">
				<input type="hidden" value="<?php echo ((isset($_GET['my-items']) && $_GET['my-items'] != '') ? $_GET['my-items'] : '' ) ?>" name="my-items">
				<?php
					if(isset($_GET['keywords'])) {
						$keywords = $_GET['keywords'];
					} else {
						$keywords = '';
					}
				?>
					
				<?php
					if(isset($_GET['orderby'])) {
						$orderBy = $_GET['orderby'];
					} else {
						$orderBy = '';
					}
				?>
				
				<?php
					if(isset($_GET['order'])) {
						$order = $_GET['order'];
					} else {
						$order = '';
					}
				?>

				<input class=" flex-item" type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
				<button class="cta secondary flex-item" type="submit">Search</button>
			</div>
		</form>
	</section>
	
	<section class="inner popular-search category">
		<h3>Advert Type</h3>
		<hr class="secondary size-l">
		<?php
			$args = array(
				'type'                     => 'buy_sell',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'title',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'buy_sell_tax',
				'pad_counts'               => false
			);
		 	
			$categories = get_categories($args);
		?>
		
		<?php foreach($categories as $category) { ?>
			<a  href="<?php echo home_url(); ?>/buy-sell/?advert-type=<?php echo $category->slug; ?>" class="cta secondary size-s">
				<i class="fa fa-<?php echo $category->slug; ?>"></i>
				<span><?php echo $category->cat_name; ?></span>
			</a>
		<?php } ?>
	</section>
	
	<section class="inner popular-search category">
		<h3>Item Type</h3>
		<hr class="secondary size-l">
		<?php
			$args = array(
				'type'                     => 'buy_sell',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'title',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'buy_sell_item',
				'pad_counts'               => false
			);
		 	
			$categories = get_categories($args);
		?>
		
		<?php foreach($categories as $category) { ?>
			<a  href="<?php echo home_url(); ?>/buy-sell/?item-type=<?php echo $category->slug; ?>" class="cta secondary size-s">
				<i class="fa fa-<?php echo $category->slug; ?>"></i>
				<span><?php echo $category->cat_name; ?></span>
			</a>
		<?php } ?>
	</section>
</aside>