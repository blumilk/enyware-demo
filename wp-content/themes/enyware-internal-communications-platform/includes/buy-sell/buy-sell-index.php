<section class="page-load">
	<div class="wrapper buy-sell page-content">
		<div class="row">
			<?php include(get_template_directory() . '/includes/buy-sell/buy-sell-header.php'); ?>
			<div class="tl-8 force">
				<div class="results">
					<?php
						// Confirms authorship of the items
						if(isset($_GET['buy-sell-action'])) {
							$args = array(
								'author' => get_current_user_id(),
								'post_type' => 'buy_sell',
								'post_status' => array('pending', 'draft', 'publish'),
							);
							
							$itemDetails = new WP_Query($args);
							
							$itemIDs = array();
							
							if($itemDetails->have_posts()) {
								while($itemDetails->have_posts()) {
									$itemDetails->the_post();
									
									array_push($itemIDs, $itemDetails->post->ID);
								}
							}
						}
					?>
					
					<?php if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'delete-item') { ?>
						<?php if(in_array($_REQUEST['itemid'], $itemIDs)) { ?>
							<section class="sent">
								<p>Your item has been successfully deleted.</p>
							</section>
							
							<?php wp_delete_post($_REQUEST['itemid'], true); ?>
						<?php } else { ?>
							<section class="sent stop">
								<p>You do not have permission to delete this item.</p>
							</section>
						<?php } ?>
					<?php } else if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'sold-item') { ?>
						<?php if(in_array($_REQUEST['itemid'], $itemIDs)) { ?>
							<section class="sent">
								<p>Your item has been successfully been marked as sold.</p>
							</section>
							
							<?php
								$itemID = $_REQUEST['itemid'];
							
								// Unsets the taxonomy
								wp_delete_object_term_relationships($itemID, 'buy_sell_status');
								wp_set_object_terms($itemID, 'sold', 'buy_sell_status', true);
							?>
						<?php } else { ?>
							<section class="sent stop">
								<p>You do not have permission to edit this item.</p>
							</section>
						<?php } ?>
					<?php } else if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'not-sold-item') { ?>
						<?php if(in_array($_REQUEST['itemid'], $itemIDs)) { ?>
							<section class="sent">
								<p>Your item has been successfully been marked as not sold.</p>
							</section>
							
							<?php
								$itemID = $_REQUEST['itemid'];
							
								// Unsets the taxonomy
								wp_delete_object_term_relationships($itemID, 'buy_sell_status');
							?>
						<?php } else { ?>
							<section class="sent stop">
								<p>You do not have permission to edit this item.</p>
							</section>
						<?php } ?>
					<?php } ?>
					
					<?php if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'new-item') { ?>
						<?php if($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
							<?php
								$newItem = array(
									'post_type' => 'buy_sell',
									'post_title' => $_REQUEST['item-name'],
									'post_status' => 'publish',
								);
								
								$newItemDetails = wp_insert_post($newItem);
								
								if(isset($_POST['advert-type'])) {
									$itemCategory = $_POST['advert-type'];
									
									foreach($itemCategory as $value) {
										wp_set_object_terms($newItemDetails, (int)$value, 'buy_sell_tax', true);
									}
								}
								
								if(isset($_POST['item-type'])) {
									$itemCategory = $_POST['item-type'];
									
									foreach($itemCategory as $value) {
										wp_set_object_terms($newItemDetails, (int)$value, 'buy_sell_item', true);
									}
								}
								
								update_post_meta($newItemDetails, 'price', $_REQUEST['item-price']);
								update_post_meta($newItemDetails, 'preview_description', $_REQUEST['preview-description']);
								update_post_meta($newItemDetails, 'item_location', $_REQUEST['item-location']);
								update_post_meta($newItemDetails, 'full_description', stripslashes(nl2br($_REQUEST['full-description'])));
								
								// File upload
								if(isset($_POST['file_upload_nonce']) && wp_verify_nonce($_POST['file_upload_nonce'], 'file_upload')) {
									require_once(ABSPATH . 'wp-admin/includes/image.php');
									require_once(ABSPATH . 'wp-admin/includes/file.php');
									require_once(ABSPATH . 'wp-admin/includes/media.php');
									
									$attachmentID = media_handle_upload('file_upload', $newItemDetails);
									
									if(is_wp_error($attachmentID)) {
										// There was an error uploading the image.
									} else {
										update_field('featured_image', $attachmentID, $newItemDetails);
									}
								}
							?>
							
							<section class="sent">
								<p>Your item has been successfully added. <a href="<?php echo get_permalink($newItemDetails); ?>">View your item</a>.</p>
							</section>
						<?php } else { ?>
							<form class="row" action="" method="post" name="add-item" enctype="multipart/form-data">
								<?php
									$args = array(
										'type'                     => 'buy_sell',
										'child_of'                 => 0,
										'parent'                   => '',
										'orderby'                  => 'title',
										'order'                    => 'ASC',
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '',
										'include'                  => '',
										'number'                   => '',
										'taxonomy'                 => 'buy_sell_tax',
										'pad_counts'               => false
									);
								 	
									$categories = get_categories($args);
								?>
								
								<section class="category">
									<div class="dt-12">
										<h3>Advert Type</h3>
									</div>
									<?php foreach($categories as $category) { ?>
										<div class="tp-3 mp-6">
											<label>
												<input type="radio" id="<?php echo $category->term_id; ?>" name="advert-type[]" value="<?php echo $category->term_id; ?>">
												<span><i class="far fa-checked"></i> <?php echo $category->cat_name; ?></span>
											</label>
										</div>
									<?php } ?>
								</section>
								
								<?php
									$args = array(
										'type'                     => 'buy_sell',
										'child_of'                 => 0,
										'parent'                   => '',
										'orderby'                  => 'title',
										'order'                    => 'ASC',
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'exclude'                  => '',
										'include'                  => '',
										'number'                   => '',
										'taxonomy'                 => 'buy_sell_item',
										'pad_counts'               => false
									);
								 	
									$categories = get_categories($args);
								?>
								
								<section class="category">
									<div class="dt-12">
										<h3>Item Type</h3>
									</div>
									<?php foreach($categories as $category) { ?>
										<div class="tp-3 mp-6">
											<label>
												<input type="checkbox" id="<?php echo $category->term_id; ?>" name="item-type[]" value="<?php echo $category->term_id; ?>">
												<span><i class="far fa-checked"></i> <?php echo $category->cat_name; ?></span>
											</label>
										</div>
									<?php } ?>
								</section>
								
								<div class="tp-12">
									<label>
										Add Item Image
										<input type="file" name="file_upload" id="file_upload"  multiple="false">
										<?php wp_nonce_field('file_upload', 'file_upload_nonce'); ?>
									</label>
								</div>
								<div class="tp-6">
									<label>
										Item Name
										<input type="text" name="item-name">
									</label>
								</div>
								<div class="tp-6">
									<label>
										Price
										<input type="text" name="item-price">
									</label>
								</div>
								<div class="tp-6">
									<label>
										Preview Description
										<input type="text" name="preview-description">
									</label>
								</div>
								<div class="tp-6">
									<label>
										Collection/Drop-off Info
										<input type="text" name="item-location">
									</label>
								</div>
								<div class="tp-12">
									<label>
										Full Description
										<textarea name="full-description"></textarea>
									</label>
									<input type="submit" value="Add Item">
								</div>
							</form>
						<?php } ?>
					<?php } else if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'edit-item') { ?>
						<?php
							$args = array(
								'author' => get_current_user_id(),
								'post_type' => 'buy_sell',
								'post_status' => array('pending', 'draft', 'publish'),
							);
							
							$itemDetails = new WP_Query($args);
							
							$itemIDs = array();
							
							if($itemDetails->have_posts()) {
								while($itemDetails->have_posts()) {
									$itemDetails->the_post();
									
									array_push($itemIDs, $itemDetails->post->ID);
								}
							}
						?>
						
						<?php if(in_array($_REQUEST['itemid'], $itemIDs)) { ?>
							<?php if($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
								<?php
									$newItemDetails = $_REQUEST['itemid'];
									
									$newItem = array(
										'ID' => $newItemDetails,
										'post_title' => $_REQUEST['item-name'],
									);
									
									wp_update_post($newItem);
									
									// Unsets the taxonomy
									wp_delete_object_term_relationships($newItemDetails, 'buy_sell_tax');
									if(isset($_POST['advert-type'])) {
										$itemCategory = $_POST['advert-type'];
										
										foreach($itemCategory as $value) {
											wp_set_object_terms($newItemDetails, (int)$value, 'buy_sell_tax', true);
										}
									}
									
									// Unsets the taxonomy
									wp_delete_object_term_relationships($newItemDetails, 'buy_sell_item');
									if(isset($_POST['item-type'])) {
										$itemCategory = $_POST['item-type'];
										
										foreach($itemCategory as $value) {
											wp_set_object_terms($newItemDetails, (int)$value, 'buy_sell_item', true);
										}
									}
									
									update_post_meta($newItemDetails, 'price', $_REQUEST['item-price']);
									update_post_meta($newItemDetails, 'preview_description', $_REQUEST['preview-description']);
									update_post_meta($newItemDetails, 'item_location', $_REQUEST['item-location']);
									update_post_meta($newItemDetails, 'full_description', stripslashes(nl2br($_REQUEST['full-description'])));
									
									// File upload
									if(isset($_POST['file_upload_nonce']) && wp_verify_nonce($_POST['file_upload_nonce'], 'file_upload')) {
										require_once(ABSPATH . 'wp-admin/includes/image.php');
										require_once(ABSPATH . 'wp-admin/includes/file.php');
										require_once(ABSPATH . 'wp-admin/includes/media.php');
										
										$attachmentID = media_handle_upload('file_upload', $newItemDetails);
										
										if(is_wp_error($attachmentID)) {
											// There was an error uploading the image.
										} else {
											update_field('featured_image', $attachmentID, $newItemDetails);
										}
									}
								?>
								
								<section class="sent">
									<p>Your item has been successfully updated.</p>
									<p><a href="<?php echo get_permalink($newItemDetails); ?>">View your item</a>.</p>
								</section>
							<?php } else { ?>
								<form class="row" action="" method="post" name="add-item" enctype="multipart/form-data">
									<?php
										$args = array(
											'type'                     => 'buy_sell',
											'child_of'                 => 0,
											'parent'                   => '',
											'orderby'                  => 'title',
											'order'                    => 'ASC',
											'hide_empty'               => 0,
											'hierarchical'             => 1,
											'exclude'                  => '',
											'include'                  => '',
											'number'                   => '',
											'taxonomy'                 => 'buy_sell_tax',
											'pad_counts'               => false
										);
									 	
										$categories = get_categories($args);
										
										$selected = get_the_terms($_REQUEST['itemid'], 'buy_sell_tax');
										
										if(!empty($selected)) {
											foreach($selected as $select) {
												$selection[] = $select->term_id;
											}
										}
									?>
									
									<section class="category">
										<div class="dt-12">
											<h3>Advert Type</h3>
										</div>
										<?php foreach($categories as $category) { ?>
											<div class="tp-3 mp-6">
												<label>
													<input type="radio" id="<?php echo $category->term_id; ?>" name="advert-type[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
													<span><i class="far fa-checked"></i> <?php echo $category->cat_name; ?></span>
												</label>
											</div>
										<?php } ?>
									</section>
									
									<?php
										$args = array(
											'type'                     => 'buy_sell',
											'child_of'                 => 0,
											'parent'                   => '',
											'orderby'                  => 'title',
											'order'                    => 'ASC',
											'hide_empty'               => 0,
											'hierarchical'             => 1,
											'exclude'                  => '',
											'include'                  => '',
											'number'                   => '',
											'taxonomy'                 => 'buy_sell_item',
											'pad_counts'               => false
										);
									 	
										$categories = get_categories($args);
										
										$selected = get_the_terms($_REQUEST['itemid'], 'buy_sell_item');
										
										if(!empty($selected)) {
											foreach($selected as $select) {
												$selection[] = $select->term_id;
											}
										}
									?>
									
									<section class="category">
										<div class="dt-12">
											<h3>Item Type</h3>
										</div>
										<?php foreach($categories as $category) { ?>
											<div class="tp-3 mp-6">
												<label>
													<input type="checkbox" id="<?php echo $category->term_id; ?>" name="item-type[]" value="<?php echo $category->term_id; ?>" <?php if(isset($selection) && in_array($category->term_id, $selection)) { ?> checked <?php } ?>>
													<span><i class="far fa-checked"></i> <?php echo $category->cat_name; ?></span>
												</label>
											</div>
										<?php } ?>
									</section>
									
									<div class="tp-12">
										<label>
											Add New Image
											<input type="file" name="file_upload" id="file_upload"  multiple="false">
											<?php wp_nonce_field('file_upload', 'file_upload_nonce'); ?>
										</label>
									</div>
									<div class="tp-6">
										<label>
											Item Name
											<input type="text" name="item-name" value="<?php echo get_the_title($_REQUEST['itemid']); ?>">
										</label>
									</div>
									<div class="tp-6">
										<label>
											Price
											<input type="text" name="item-price" value="<?php the_field('price', $_REQUEST['itemid']); ?>">
										</label>
									</div>
									<div class="tp-6">
										<label>
											Preview Description
											<input type="text" name="preview-description" value="<?php echo strip_tags(get_field('preview_description', $_REQUEST['itemid'])); ?>">
										</label>
									</div>
									<div class="tp-6">
										<label>
											Collection/Drop-off Info: 
											<input type="text" name="item-location" value="<?php echo strip_tags(get_field('item_location', $_REQUEST['itemid'])); ?>">
										</label>
									</div>
									<div class="tp-12">
										<label>
											Full Description
											<textarea name="full-description"><?php echo strip_tags(get_field('full_description', $_REQUEST['itemid'])); ?></textarea>
										</label>
										<input type="submit" value="Update Item">
									</div>
								</form>
							<?php } ?>
						<?php } elseif(get_post_status($_REQUEST['itemid']) == FALSE) { ?>
							<section class="sent stop">
								<p>This item doesn't exist.</p>
							</section>
						<?php } else { ?>
							<section class="sent stop">
								<p>You do not have permission to edit this item.</p>
							</section>
						<?php } ?>
					<?php } else { ?>
						<?php
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							
							$taxQuery = array();
							
							if(isset($_REQUEST['advert-type']) && !empty($_REQUEST['advert-type'])) {
								array_push($taxQuery,
									array('relation' => 'OR',
										array(
											'taxonomy' => 'buy_sell_tax',
											'field'    => 'slug',
											'terms'    => $_REQUEST['advert-type'],
										)
									)
								);
							}
							
							if(isset($_REQUEST['item-type']) && !empty($_REQUEST['item-type'])) {
								array_push($taxQuery,
									array('relation' => 'OR',
										array(
											'taxonomy' => 'buy_sell_item',
											'field'    => 'slug',
											'terms'    => $_REQUEST['item-type'],
										)
									)
								);
							}
							
							$args = array();
							$args['post_type'] = 'buy_sell';
							$args['posts_per_page'] = -1;
							$args['post_parent'] = 0;
							$args['post_status'] = 'publish';
							$args['paged'] = $paged;
							$args['orderby'] = 'modified';
							$args['order'] = 'DESC';
							$args['tax_query'] = $taxQuery;
							
							if(isset($_GET['buy-sell-action']) && $_GET['buy-sell-action'] == 'my-items') {
								$args['author'] = get_current_user_id();
							}
	
							if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
								$keywords = $_REQUEST['keywords'];
								$args['s'] = $keywords;
								
								$metaQuery = array();
								$args['meta_query'] = $metaQuery;
							}
							
							$buySell = new WP_Query($args);
							
							if(count($_GET) > 0 && isset($_REQUEST['keywords'])) {
								if(function_exists('mdst_search_execute')) {
									$resultsFound = $buySell->found_posts;
									$postTypeName = 'Buy and Sell';
									
									if(isset($_REQUEST['advert-type']) && $_REQUEST['advert-type'] != '') {
										$TaxTerm = ucwords(str_replace('-', ' ', $_REQUEST['advert-type']));
									} else {
										$TaxTerm = '–';
									}
								}
							}
						?>
						
						<?php if($buySell->have_posts()) { ?>
							<div class="row">
								<div class="tp-12">
									<?php while($buySell->have_posts()) {
										$buySell->the_post();
										$image = get_field('featured_image');
										$size = 'square-img-small';
										$imageSize = $image['sizes'][$size]; ?>
										
										<?php
											$itemStatus = get_the_terms($post->ID, 'buy_sell_status');
											
											if(isset($itemStatus) && !empty($itemStatus)) {
												$itemStatusSold = 'sold';
											} else {
												$itemStatusSold = '';
											}
										?>
										<a href="<?php the_permalink(); ?>">
											<article class="<?php if($buySell->current_post % 2 == 0) { echo 'even'; } else { echo 'odd'; } ?>">
												<div class="tp-2">
													<?php if($image) { ?>
														<img src="<?php echo $imageSize; ?>">
													<?php } else { ?>
														<img src="<?php echo home_url(); ?>/images/buy-sell-no-img.jpg">
													<?php } ?>
												</div>
												<div class="tp-9 force">
													<div class="row">
														<div class="tp-6">
															<?php
																if($itemStatusSold) {
																	$itemTitle = '<span class="sold"><i class="fa fa-sold"></i> Item Sold</span><br />' . get_the_title();
																} else {
																	$itemTitle = get_the_title();
																}
															?>
															<h3 class="title"><?php echo $itemTitle; ?></h3>
															
															
															
															<p>
																<?php the_field('preview_description'); ?>
															</p>
															
															<?php if(get_field('price')) { ?>
																<?php $price = preg_replace('/[^0-9]/', '', get_field('price')); ?>
																<?php if($price) { ?>
																	<span class="price">£<?php echo $price; ?></span>
																<?php } ?>
															<?php } ?>
															
															<?php if(get_field('item_location')) { ?>
																<p>Collection/Drop-off Info: <br /><?php the_field('item_location'); ?></p><br />
															<?php } ?>
															
															<date>Last updated: <?php echo the_modified_time('G:i jS F, Y'); ?></date>
														</div>
														<div class="tp-6">
															<?php $categories = get_the_terms($post->ID, 'buy_sell_tax'); ?>
															
															<?php if(isset($categories) && !empty($categories)) { ?>
																<?php foreach($categories as $category) { ?>
																	<p class="item-type <?php echo $category->slug; ?>">
																		<i class="fa fa-<?php echo $category->slug; ?>"></i> <?php echo $category->name; ?>
																	</p>
																<?php } ?>
															<?php } ?>
														</div>
													</div>
													
												</div>
											</article>
										</a>
									<?php } ?>
								</div>
							</div>
							
							<?php wp_reset_query(); ?>
						<?php } else { ?>
							<p>Sorry but no items were found.</p>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<?php include(get_template_directory() . '/includes/buy-sell/buy-sell-menu.php'); ?>
		</div>
	</div>
</section>