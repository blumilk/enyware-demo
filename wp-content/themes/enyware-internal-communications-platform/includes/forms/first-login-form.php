<?php
	$errors = 0;
	
	if(!empty($_POST['password-one'])) {
		if($_POST['password-one'] == $_POST['password-two'] && $_POST['password-strength'] == 'strong') {
			wp_update_user(
				array(
					'ID' => $current_user->ID,
					'user_pass' => $_POST['password-one']
				)
			);
			
			wp_redirect(home_url());
		} else {
			if($_POST['password-strength'] != 'strong') {
				$passwordStrength = true;
			} else {
				$errors++;
			}
		}
	} elseif(isset($_POST['password-one']) && empty($_POST['password-one']) && isset($_POST['password-two']) && empty($_POST['password-two'])) {
		wp_redirect(home_url());
	}
?>

<section class="page-load">
	<div class="wrapper page-content">
		<div class="row">
			<div class="dt-6 indent-dt-3">
				<fieldset>
					<form action="<?php the_permalink(); ?>" method="post" class="password-reset">
						<?php if($errors > 0) { ?>
							<div class="notifications error">
								<div class="inner">
									<p>The passwords you entered do not match. Your password was not updated.</p>
								</div>
							</div>
						<?php } ?>
						
						<?php if(isset($passwordStrength)) { ?>
							<div class="notifications error">
								<div class="inner">
									<p>Your password isn't strong enough to use. Your password was not updated.</p>
								</div>
							</div>
						<?php } ?>
						
						<h4>Please change your password</h4>
						<hr class="size-l">
							<p>The password we sent you is very secure but hard to remember.</p>
							<p>Please enter a password that you don’t use anywhere else, that you will also remember easily.</p>
						<br/>
						<div class="mp-2 ml-1 icon">
							<div class="i-icon large icon-ii-key"></div>
						</div>
						<div class="mp-10 ml-11">
							<input name="password-one" type="password" id="password-one" placeholder="New Password">
							<div class="progress-bar"></div>
						</div>
						<div class="mp-2 ml-1 icon">
							<div class="i-icon large icon-ii-key"></div>
						</div>
						<div class="mp-10 ml-11">					
							<input name="password-two" type="password" id="password-two" placeholder="Repeat New Password">
							<div class="password-match"></div>
						</div>
						<input class="password-strength" type="hidden" name="password-strength">
						<input class="primary" name="update-password" type="submit" value="Update Password">
					</form>
				</fieldset>
			</div>
		</div>
	</div>
</section>