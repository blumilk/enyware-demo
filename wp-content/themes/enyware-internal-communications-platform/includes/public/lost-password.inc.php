<section class="page-load">
	<div class="wrapper login-register">
		<div class="row">
			<div class="mp-10 indent-mp-1">
				<h6><a href="<?php echo home_url(); ?>">Return to Login</a></h6>
				<hr class="secondary size-s width-s">
				<h1>Password Reset</h1>
				<div id="password-lost-form" class="widecolumn">
					<?php if(isset($_GET['errors'])) { ?>
						<div class="row">
							<div class="mp-12">
								<div class="errors">
									<?php if($_GET['errors'] == 'empty_username') { ?>
										<span class="member-error">
											<strong>Error:</strong> Nothing was entered.
										</span>
									<?php } elseif($_GET['errors'] == 'invalidcombo') { ?>
										<span class="member-error">
											<strong>Error:</strong> There are no members registered with this information.
										</span>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>
					
					<form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
						<div class="row">
							<div class="mp-2 ml-1 icon">
								<div class="i-icon x-large icon-ii-user"></div>
							</div>
							<div class="mp-10 ml-11">
								<input type="text" name="user_login" id="user_login" placeholder="Email Address">
							</div>
							<div class="tp-6">
								<div class="row">
									<div class="mp-2 ml-1 icon">
										<div class="i-icon x-large icon-ii-open-lock"></div>
									</div>
									<div class="mp-10 ml-11">
										<input type="text" name="nhs-payroll" id="nhs-payroll" placeholder="Payroll Number">
									</div>
								</div>
							</div>
							<div class="tp-6">
								<div class="row">
									<div class="mp-2 ml-1 icon">
										<div class="i-icon x-large icon-ii-open-lock"></div>
									</div>
									<div class="mp-10 ml-11">
										<input type="text" name="alternative-payroll" id="alternative-payroll" placeholder="Alternative Payroll Number">
									</div>
								</div>
							</div>
							<div class="mp-11 indent-mp-1">
								<input type="submit" name="submit" class="lostpassword-button" value="<?php _e('Reset Password', 'personalize-login'); ?>">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>