<section class="page-load">
	<div class="wrapper login-page">
		<div class="row">
			<div class="tp-8 indent-tp-2">
				<?php echo do_shortcode('[login_form]'); ?>
			</div>
		</div>
	</div>
</section>