<?php
	/*
		Template Name: Status Note
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/config/member-details.php');
							
				while($membersProfile->have_posts()) {
					$membersProfile->the_post();
					$availabilityTax = get_the_terms($post->ID, 'status_tax');
				}
				
				$availability = '';
				
				if(is_array($availabilityTax)) {
					foreach($availabilityTax as $status) {
						$availability = $status->name;
						$availabilityID = $status->term_id;
					}
				}
				
				if(isset($_GET['header-availability'])) {
					$status = $_GET['header-availability'];
					
					wp_delete_object_term_relationships($post->ID, 'status_tax');
					wp_set_object_terms($post->ID, (int)$status, 'status_tax', true);
					
					if($status == 27) {
						$availability = 'Available';
					} elseif($status == 28) {
						$availability = 'Unavailable';
					} elseif($status == 53) {
						$availability = 'Holiday';
					} elseif($status == 54) {
						$availability = 'Travelling';
					}
				} else {
					$status = 0;
				}
				
				if(!isset($_GET['header-availability'])) {
					$status = $availabilityID;
				}
				
				if(isset($availabilityID)) {
					$args = array(
						'show_option_all'    => '',
						'show_option_none'   => '',
						'option_none_value'  => 'false',
						'orderby'            => 'title',
						'order'              => 'ASC',
						'show_count'         => 0,
						'hide_empty'         => 0,
						'child_of'           => 0,
						'exclude'            => '',
						'echo'               => 1,
						'selected'           => $status,
						'hierarchical'       => 0,
						'name'               => 'header-availability',
						'id'                 => '',
						'class'              => 'header-availability',
						'depth'              => 0,
						'tab_index'          => 0,
						'taxonomy'           => 'status_tax',
						'hide_if_empty'      => false,
						'value_field'	     => 'term_id',
					);
				}
				
				wp_reset_query(); 
				
				add_thickbox();
				?>
				
				<section class="page-load fast-animation status-updater">
					<div class="wrapper">
						<div class="row">
							<div class="dt-12">
								<div class="status status-select status-form <?php echo strtolower($availability); ?>">
									<p>Update your availability:</p>
									<form action="?header-availability=<?php echo $availability ?>">
											<?php wp_dropdown_categories($args); ?>
									</form>
								</div>
							</div>
							<div class="dt-12">
								<?php include('includes/all/status-note.php'); ?>	
							</div>
						</div>
					</div>
				</section>
				
			<?php } else { // Not logged in
				wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
			}
		}
	}
	
	get_footer();
?>