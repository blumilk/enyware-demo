<?php
	get_header();

	if(is_home()) {
		wp_redirect(home_url('/dashboard'));
	} 
				
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				wp_redirect(home_url('/dashboard'));
			} else { // Not logged in
				include('includes/public/enter.inc.php');
			}
		}
	}
	
	get_footer();
?>