<?php get_header(); ?>
<?php if(is_user_logged_in()) { ?>
	<section class="page-load">
		<div class="wrapper news page-content">
			<div class="row">
				<div class="tp-12">
					<h3>What's Happening</h3>
					<hr class="secondary size-l">
					<div class="search">
						<div class="row nested">
							<form action="<?php echo home_url(); ?>/news" method="get" name="resources">
								<input type="hidden" class="page" value="1" name="page">
								<div class="tl-12">
									<?php
										if(isset($_GET['keywords'])) {
											$keywords = $_GET['keywords'];
										} else {
											$keywords = '';
										}
									?>
									<div class="i-icon large icon-ii-search"></div>
									<input type="text" name="keywords" value="<?php echo $keywords; ?>" placeholder="Keywords...">
									<button class="cta secondary" type="submit">Search</button>
								</div>
							</form>
						</div>
					</div>
					<div class="results whats-happening">
						<?php
							if(count($_GET) > 0) {
								if(isset($_REQUEST['keywords'])) {
									$keywords = $_REQUEST['keywords'];
								} else {
									$keywords = '';
								}
								
								// Meta Query search options
								$metaQuery = array();
								
								$page = (get_query_var('page')) ? get_query_var('page') : 1;
								
								$args = array(
									'post_type' => 'post',
									'orderby' => 'date',
									'order' => 'DESC',
									'posts_per_page' => 12,
									'post_parent' => 0,
									'post_status' => 'publish',
									'meta_query' => $metaQuery,
									'paged' => $page,
									's' => $keywords,
								);
							} else {
								$page = (get_query_var('page')) ? get_query_var('page') : 1;
								
								$args = array(
									'post_type' => 'post',
									'orderby' => 'date',
									'order' => 'DESC',
									'posts_per_page' => 12,
									'post_parent' => 0,
									'post_status' => 'publish',
									'paged' 	=> $page
								);
							}
							
							$newsSearch = new WP_Query($args);
						?>
						<?php if($newsSearch->have_posts()) { ?>
							<div class="row">
								<?php while($newsSearch->have_posts()) {
									$newsSearch->the_post(); ?>
									<?php
										$image = get_field('slide_image'); 
										$size = 'happening-tile';
										$regularImage = $image['sizes'][$size];
										$defaultImage 	= home_url() . '/images/default-img.jpg';
									?>
									<a href="<?php the_permalink(); ?>">
										<div class="tl-4">
												<?php if($image) {?>
													<div class="ml-12 news-thumbnail">
														<div class="image-wrapper">
															<?php if($regularImage){ ?>
																<img src="<?php echo $regularImage; ?>" alt="<?php echo $image['title'];?>" title="<?php echo $image['title'];?>" />
															<?php } elseif($defaultImage) { ?>
																<img src="<?php echo $defaultImage; ?>">
															<?php } ?>
														</div>
													</div>
													<div class="news-thumbnail-mobile" style="background-image: url(<?php echo $image['url']; ?>);"></div>
													<div class="mp-12">
												<?php } else if($defaultImage) {?>
													<div class="ml-12 news-thumbnail">
														<div class="image-wrapper">
															<img src="<?php echo $defaultImage; ?>">
														</div>
													</div>
													<div class="mp-12">
												<?php } else { ?>
												<div class="mp-12">
												<?php } ?>
													<div class="inner">
														<h4 class="title"><?php the_title(); ?></h4>
														<hr class="secondary size-s">
														<p><?php echo get_the_date('l jS F, Y'); ?></p>
														<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
														<?php the_excerpt(); ?>
													</div>
												</div>
											<div class="find-out-more">
												<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
													Find Out More
												</div>
												<div class="chevron">
													&#x63;
												</div>
											</div>
										</div>
									</a>
								<?php } ?>
								
								<?php wp_reset_query(); ?>
								<?php if($newsSearch->max_num_pages > 1) { ?>
									<div class="tl-12">
										<nav class="pagination">
											<ul>
												<?php
													if(isset($_GET['page'])) {
														$currentPage = $_GET['page'];
														$currentPagePlus = $page;
													} else {
														$currentPage = 1;
														$currentPagePlus = 0;
													}
												?>
												
												<?php if(isset($currentPage) && $currentPage > 1) { ?>
													<a href="?page=1">
														<li>
															<i class="fa fa-double-left"></i>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage > 1) { ?>
													<a href="?page=<?php if(isset($currentPage)) { echo $currentPage - 1; } else { echo 2; } ?>">
														<li>
															<i class="fa fa-single-left"></i>
														</li>
													</a>
												<?php } ?>
												<?php for($i = $currentPagePlus - 3; $i <= 5 + $currentPagePlus; $i++) { ?>
													<?php if($i < $newsSearch->max_num_pages - 1 && $i > 0) { ?>
														<a href="?page=<?php echo $i; ?>">
															<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } elseif(!isset($currentPage) && $i == 1) { echo 'active'; } ?>">
																<?php echo $i; ?>
															</li>
														</a>
													<?php } ?>
												<?php } ?>
												<?php if($newsSearch->max_num_pages > 5) { ?>
													<li>
														...
													</li>
												<?php } ?>
												<?php for($i = $newsSearch->max_num_pages - 1; $i <= $newsSearch->max_num_pages; $i++) { ?>
													<a href="?page=<?php echo $i; ?>">
														<li class="<?php if(isset($currentPage) && $currentPage == $i) { echo 'active'; } ?>">
															<?php echo $i; ?>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage < $newsSearch->max_num_pages) { ?>
													<a href="?page=<?php if(isset($currentPage) && $currentPage < $newsSearch->max_num_pages) { echo $currentPage + 1; } ?>">
														<li>
															<i class="fa fa-single-right"></i>
														</li>
													</a>
												<?php } ?>
												<?php if(isset($currentPage) && $currentPage < $newsSearch->max_num_pages) { ?>
													<a href="?page=<?php if(isset($currentPage) && $currentPage < $newsSearch->max_num_pages) { echo $newsSearch->max_num_pages; } ?>">
														<li>
															<i class="fa fa-double-right"></i>
														</li>
													</a>
												<?php } ?>
											</ul>
										</nav>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } else { // Not logged in ?>
	<?php wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI'])); ?>
<?php } ?>
<?php get_footer(); ?>