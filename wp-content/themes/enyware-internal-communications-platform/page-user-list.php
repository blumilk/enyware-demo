<?php
	/*
		Template Name: User List
	*/
	
	$args = array(
		'post_type' 		=> 'iykaa_members',
		'orderby' 			=> 'title',
		'order' 			=> 'ASC',
		'posts_per_page' 	=> -1,
	);
	
	$userList = array();
	
	$adminSearch = new WP_Query($args);
	
	$profileImage = get_field('profile_image');
	
	if(!$profileImage) {
		$profileImage = home_url() . '/images/avatar-default.svg';
	} else {
		$profileImage = $profileImage;
	}
	
	if($adminSearch->have_posts()) {
		while($adminSearch->have_posts()) {
			$adminSearch->the_post();
			array_push($userList, array(
				'title' => get_the_title(),
				'email' => get_field('email_address'),
				'image' => get_field('profile_image'),
				'id' 	=> $post->ID,
			));
		}
	}
	
	$keywords = $_REQUEST['keywords'];
	$url = $_REQUEST['resource-url'];
	$name = $_REQUEST['resource-name'];
	
	$hint = '';
	
	if($keywords !== '') {
		$keywords = strtolower($keywords);
		$len = strlen($keywords);
		
		foreach($userList as $getUser) {
			if(stripos($getUser['title'], $keywords) !== FALSE) {
				$profileImage = $getUser['image'];
				
				if($profileImage) {
					$profileImage = $profileImage['url'];
				} else {
					$profileImage = home_url() . '/images/avatar-default.svg';
				}
				
				$hint .= '<option name="member" value="' . $getUser['id'] . '" id="member-' . $getUser['id'] . '">' . $getUser['title'] . '</option>';
			}
		}
	}
	
	if($hint === '') {
		echo '<option>No results found.</option>';
	} else {
		echo $hint;
	}
?>