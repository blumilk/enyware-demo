<?php
	/*
		Template Name: Get Users
	*/
	
	$args = array(
		'post_type' 		=> 'iykaa_members',
		'orderby' 			=> 'title',
		'order' 			=> 'ASC',
		'posts_per_page' 	=> -1,
	);
	
	$userList = array();
	
	$adminSearch = new WP_Query($args);
	
	$profileImage = get_field('profile_image');
	
	if(!$profileImage) {
		$profileImage = home_url() . '/images/avatar-default.svg';
	} else {
		$profileImage = $profileImage;
	}
	
	if($adminSearch->have_posts()) {
		while($adminSearch->have_posts()) {
			$adminSearch->the_post();
			array_push($userList, array(
				'title' => get_the_title(),
				'email' => get_field('email_address'),
				'image' => get_field('profile_image'),
			));
		}
	}
	
	$keywords = $_REQUEST['keywords'];
	$url = $_REQUEST['resource-url'];
	$name = $_REQUEST['resource-name'];
	
	$hint = '';
	
	if($keywords !== '') {
		$keywords = strtolower($keywords);
		$len = strlen($keywords);
		
		foreach($userList as $getUser) {
			if(stripos($getUser['title'], $keywords) !== FALSE) {
				$profileImage = $getUser['image'];
				
				if($profileImage) {
					$profileImage = $profileImage['url'];
				} else {
					$profileImage = home_url() . '/images/avatar-default.svg';
				}
				
				$hint .= '<a href="mailto:' . $getUser['email'] . '?Subject=' . get_bloginfo('name') . ': ' . $name . '&body=I found this resource and thought you might like it ' . $url . '"><li class="gaps"><div class="row"><div class="mp-10">' . $getUser['title'] . '</div><div class="mp-2"><img src="' . $profileImage . '"></div></div></li></a>';
			}
		}
	}
	
	if($hint === '') {
		echo '<li>No results found.</li>';
	} else {
		echo $hint;
	}
?>