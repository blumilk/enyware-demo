<!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title>
		<?php if(!is_front_page()) { wp_title(''); echo ' | '; } ?><?php bloginfo('name');  ?> | Enyware
	</title>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Manifest for PWA view -->
	<meta name="theme-color" content="#B9DCD3" />
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/manifest.json">
	<!-- Load the service worker if possible -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/register-sw.js" type="text/javascript" charset="utf-8"></script>
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/sw.js" type="text/javascript" charset="utf-8"></script>
	
	<!-- iOS Web App View Meta -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> | Enyware">
	<link rel="apple-touch-icon" href="<?php echo home_url(); ?>/images/touch-icon-iphone.png">

	<!-- Windows Tile Meta -->
	<meta name="application-name" content="Enyware" />
	<meta name="msapplication-TileColor" content=" #B9DCD3" />
	<!-- Logos for all four tile sizes -->
	<meta name="msapplication-square70x70logo" content="<?php echo home_url(); ?>/images/smalltile.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo home_url(); ?>/images/mediumtile.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo home_url(); ?>/images/widetile.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo home_url(); ?>/images/largetile.png" />

	<!-- Web App View Link Management -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/compressed.js" type="text/javascript" charset="utf-8"></script>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="shortcut icon" href="<?php echo home_url(); ?>/images/favicon.ico">

	<?php include('includes/all/ga.php'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<?php
			include('includes/config/member-details.php');
			
			// Menu
			if(!is_page(array(1347))) {
				include('includes/all/menu.php');
			}
		?>
	</header>
	<section class="browser-glimpse-bg"></section>
	
	<?php $desktop = ( ! wp_is_mobile() ? 1 : 0); ?>
	<?php $not_chrome = ( ! preg_match('/Chrome[\/\s](\d+\.\d+)/', $_SERVER['HTTP_USER_AGENT']) ? 1 : 0); ?>
	
	<section class="browser-glimpse float">
		<div class="browser">
			<div class="close">
				<h3>&times;</h3>
			</div>
			<div class="bg message">
				<h2>We have detected that your current browser isn't Google Chrome, please change your browser.</h2>
			</div>
		</div>
	</section>
	
	<section class="registration-glimpse-bg"></section>
	<section class="registration-glimpse float">
		<div class="enyware">
			<div class="close">
				<h3>&times;</h3>
			</div>
			<div class="bg register">
				<h2><small>Register for</small> Enyware</h2>
				<h3>The information resource to make your job easier. Access Enyware from any device.</h3>
				<ul>
					<li>
						<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-01.png">
						<span>24/7 access to resources on desktop, mobile and tablet</span>
					</li>
					<li>
						<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-02.png">
						<span>Tailored content and central contact directory for all staff</span>
					</li>
					<li>
						<img class="icon" src="<?php echo home_url(); ?>/images/glimpse/glimpse-icon-03.png">
						<span>Your one stop resource for company information</span>
					</li>
				</ul>
				<a href="<?php echo home_url(); ?>/register" class="cta primary size-l">Register for Enyware Today</a>
				<h4 class="light">Simply Enyware</h4>
			</div>
			<div class="choose">
				<h3>OR</h3>
			</div>
			<form class="bg sign-in" action="<?php echo home_url(); ?>/login" method="post">
				<div class="wrapper">
					<div class="row">
						<div class="tp-12">
							<h2>Login</h2>
						</div>
						<div class="tp-5">
							<label>
								Email Address
								<input name="iykaa-member-email" id="iykaa-member-email" class="required" type="email" value="<?php if(isset($_GET['email'])) { echo $_GET['email']; } ?>" placeholder="Email Address">
							</label>
						</div>
						<div class="tp-4">
							<label>
								Password <a href="<?php echo home_url(); ?>/login?action=lostpassword">(Forgot password?)</a>
								<input name="iykaa-member-password" id="iykaa-member-password" class="required" type="password" placeholder="Password" <?php if(isset($_GET['email'])) { echo 'autofocus'; } ?>>
							</label>
						</div>
						<div class="tp-3">
							<button class="cta primary">Login</button>
						</div>
					</div>
				</div>
				<input type="hidden" name="iykaa-login-nonce" value="<?php echo wp_create_nonce('iykaa-login-nonce'); ?>">
			</form>
		</div>
	</section>
	<script>
		jQuery(document).ready(function($) {
			function sideNavGlimpse(e) {
				if($(this).hasClass('library')) {
					
				} else {
					e.preventDefault();
					$('.registration-glimpse-bg').fadeIn('slow');
					$('.registration-glimpse').fadeIn('slow');
				}
			}
/*
			var desktop = <?php echo $desktop ?>;
			var not_chrome = <?php echo $not_chrome ?>;

			if (desktop == 1 && not_chrome == 1) {
				$('.browser-glimpse-bg').fadeIn('slow');
				$('.browser-glimpse.float').fadeIn('slow');
			}
*/			
			$('.browser .close').click(function(e) {
				console.log('clicked');
				e.preventDefault();
				$('.browser-glimpse-bg').fadeOut('slow');
				$('.browser-glimpse.float').fadeOut('slow');
			});
			

			$('.side-nav li').click(sideNavGlimpse);
			
			$('.feed a').click(function(e) {
				e.preventDefault();
				$('.registration-glimpse-bg').fadeIn('slow');
				$('.registration-glimpse').fadeIn('slow');
			});
			
			$('.buttons a').click(function(e) {
				e.preventDefault();
				$('.registration-glimpse-bg').fadeIn('slow');
				$('.registration-glimpse').fadeIn('slow');
			});
			
			$('.enyware .close').click(function(e) {
				e.preventDefault();
				$('.registration-glimpse-bg').fadeOut('slow');
				$('.registration-glimpse.float').fadeOut('slow');
			});
		});
	</script>