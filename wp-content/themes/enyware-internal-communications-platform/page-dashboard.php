<?php
	/*
		Template Name: Dashboard
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('components/components.php');
			} else { // Not logged in
				wp_redirect(home_url()); ?>
				<!-- Microsoft Browser Redirect -->
				<script type='text/javascript'>window.top.location='<?php echo home_url()?>';</script>
				<?php
			}
		}
	}
	
	get_footer();
?>