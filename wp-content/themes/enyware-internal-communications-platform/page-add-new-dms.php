<?php
	/*
		Template Name: Add New DMS
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/config/member-details.php');
				include('includes/dms/add-new.php');
			} else { // Not logged in
				wp_redirect(home_url());
			}
		}
	}
	
	get_footer();
?>