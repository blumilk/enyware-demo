<!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
	<title><?php wp_title(''); ?> | <?php bloginfo('name'); ?> | Enyware</title>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Manifest for PWA view -->
	<meta name="theme-color" content="#B9DCD3" />
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/manifest.json">
	<!-- Load the service worker if possible -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/register-sw.js" type="text/javascript" charset="utf-8"></script>
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/sw.js" type="text/javascript" charset="utf-8"></script>
	
	<!-- iOS Web App View Meta -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> | Enyware">
	<link rel="apple-touch-icon" href="<?php echo home_url(); ?>/images/touch-icon-iphone.png">

	<!-- Windows Tile Meta -->
	<meta name="application-name" content="Enyware" />
	<meta name="msapplication-TileColor" content=" #B9DCD3" />
	<!-- Logos for all four tile sizes -->
	<meta name="msapplication-square70x70logo" content="<?php echo home_url(); ?>/images/smalltile.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo home_url(); ?>/images/mediumtile.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo home_url(); ?>/images/widetile.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo home_url(); ?>/images/largetile.png" />

	<!-- Web App View Link Management -->
	<script async src="<?php echo get_stylesheet_directory_uri(); ?>/js/compressed.js" type="text/javascript" charset="utf-8"></script>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="shortcut icon" href="<?php echo home_url(); ?>/images/favicon.ico">

	<?php include('includes/all/ga.php'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header>
		<?php if(is_user_logged_in()) { ?>
			<?php
				include('includes/config/member-details.php');
				
				// EXCLUDES
				// 371 Introduction
				// 1347 Status Note
				
				if(!is_page(array(371, 1347))) {
					// Welcome message
					include('includes/all/welcome.inc.php');
					
					include('includes/all/notifications.inc.php');
				}
				
				// Menu
				if(!is_page(array(1347))) {
					include('includes/all/menu.php');
				}
			?>
		<?php } ?>
	</header>