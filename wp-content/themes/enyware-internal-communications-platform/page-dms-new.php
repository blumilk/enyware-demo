<?php
	/*
		Template Name: Document Management System New
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/dms/display.php');
			} else { // Not logged in
				wp_redirect(home_url());
			}
		}
	}
	
	get_footer();
?>