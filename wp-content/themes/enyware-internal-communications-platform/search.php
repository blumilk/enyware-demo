<?php get_header(); ?>
<section class="page-load">

	<div class="wrapper news page-content">	
		<?php if(have_posts()) { 
			 
		?> 
		<div class="row">
			<div class="mp-12">
				<?php if($wp_query->found_posts < 2){ ?>
					<h3>There was <?global $wp_query; echo $wp_query->found_posts; ?> Result for '<?php echo get_search_query(); ?>'</h3>
				<?php } else { ?>
					<h3>There were <?global $wp_query; echo $wp_query->found_posts; ?> Results for '<?php echo get_search_query(); ?>'</h3>
				<?php } ?>
				<hr class="secondary size-l results-spacer">
			</div>
		</div>
		<div class="results">
			<?php			
			$last_type="";
			$typecount = 0;
			
			while (have_posts()){
				the_post();
				if ($last_type != $post->post_type){
					$typecount = $typecount + 1;
					if ($typecount > 1){
						echo '</div></div>'; //close type container
					}
					// save the post type.
					$last_type = $post->post_type;
					//open type container
					switch ($post->post_type) {
						
						case 'post':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from What's Happening</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'page':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Pages</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_members':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Members</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_notifications':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Notifications</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_library':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Library</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_library_custom':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Library</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_locations':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Locations</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
						case 'iykaa_departments':
							echo "<div class=\"row search-results-row\"><div class=\"tp-12\">
								<h3>Results from the Departments</h3>
								<hr class=\"secondary size-l\">
								</div>
								<div class=\"results whats-happening\">";
							break;
					}
				} ?>
				<a href="<?php the_permalink(); ?>">
					<div class="tl-4">
							<div class="mp-12 gapless">
								<div class="inner">
									<h4 class="title"><?php the_title(); ?></h4>
									<hr class="secondary size-s">
									<?php the_excerpt(); ?>
								</div>
							</div>
						<div class="find-out-more">
							<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
								Find Out More
							</div>
							<div class="chevron">
								&#x63;
							</div>
						</div>
					</div>
				</a>
		<?php } ?>
		</div>
	<?php } else { ?>
	<div class="wrapper">
		<div class="row">
			<div class="mp-12">
				<h3>No results found for '<?php echo get_search_query(); ?>'</h3>
				<hr class="secondary size-l">
			</div>
			<div class="mp-12">
				<div class="find-out-more find-out-more-search">
					<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
						Search Again
					</div>
					<div class="chevron">
						&#x63;
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
		
	</div>

</section>
<?php get_footer(); ?>