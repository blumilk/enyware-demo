<?php get_header(); ?>
<section class="page-load">
	<div class="wrapper library page-content <?php echo $TaxTerm ?>">
		<div class="row">
			<div class="dt-12">
				<?php include(get_stylesheet_directory() . '/includes/search/guidelines-menu.php'); ?>
			</div>
			<div class="dt-8">
				<h3>Tag Archive: <?php echo single_tag_title('', false); ?></h3>
				<hr class="secondary size-l">
				<?php while(have_posts()) {
					the_post(); ?>
					<p>
						<a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
							<?php the_title(); ?>
						</a>
					</p>
				<?php } ?>
			</div>
			<?php include(get_template_directory() . '/includes/menus/guidelines-menu.php'); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>