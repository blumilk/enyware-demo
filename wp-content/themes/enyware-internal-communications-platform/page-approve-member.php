<?php
	/*
		Template Name: Approve Member
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(isset($_REQUEST['email']) && isset($_REQUEST['hash'])) {
				$memberEmailLocal = $_REQUEST['email'];
				$memberEmailDomain = $_REQUEST['domain'];
				$memberEmail = $memberEmailLocal . $memberEmailDomain;
				
				// Setting up of the emails
				$headers = array('Content-Type: text/html; charset=UTF-8');
				
				$signupSubject = get_field('member_signup_subject', 'options');
				$signupMessage = get_field('member_signup_message', 'options');
				
				$linkText = get_string_between($signupMessage, '[login-link]', '[/login-link]');
				
				$signupMessage = str_replace('[login-link]', '<a class="cta" href="' . home_url() . '/login?email=' . $memberEmailLocal . '&domain=' . $memberEmailDomain . '&hash=' . $_REQUEST['hash'] . '" style="background-color: #28333C; color: #ffffff; font-size: 24px; font-weight: bold; text-align: center; display: block; margin-top: 20px; padding: 20px; border-radius: 3px;">', $signupMessage);
				$signupMessage = str_replace('[/login-link]', '</a>', $signupMessage);
				
				@wp_mail($memberEmail, $signupSubject, $signupMessage, $headers);
				
				include('includes/public/logged-out.inc.php');
			} else {
				wp_redirect(home_url());
			}
		}
	}
	
	get_footer();
?>