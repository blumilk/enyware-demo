<?php
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/all/library-archive.inc.php');
			} else {
				ipLoginChecker('includes/all/library-archive.inc.php');
			}
		}
	}
	
	get_footer();
?>