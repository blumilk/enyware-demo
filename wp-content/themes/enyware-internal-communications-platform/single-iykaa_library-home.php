<?php 
	/*
	Template Name: Library Home Page
	Template Post Type: iykaa_library, iykaa_library_custom
	*/
	?>

<?php
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			if(is_user_logged_in()) {
				include('includes/search/library-home.php');
			} else { // Not logged in
				ipLoginChecker('includes/search/library-home.php');
			}
		}
	}
	
	get_footer();
?>