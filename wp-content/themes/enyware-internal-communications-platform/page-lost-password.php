<?php
	/*
		Template Name: Lost Password
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			include('includes/public/lost-password.inc.php');
		}
	}
	
	get_footer();
?>