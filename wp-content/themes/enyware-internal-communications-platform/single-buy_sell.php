<?php get_header(); ?>
<section class="page-load">
	<?php if(is_user_logged_in()) { ?>
		<div class="wrapper buy-sell page-content">
			<div class="row">
				<?php include(get_template_directory() . '/includes/buy-sell/buy-sell-header.php'); ?>
				
				<?php while(have_posts()) {
					the_post(); ?>
					<?php include(get_template_directory() . '/includes/buy-sell/buy-sell-menu.php'); ?>
					<div class="tl-8 force">
						<?php
							global $current_user;
							$currentUser = wp_get_current_user();
							
							$itemStatus = get_the_terms($post->ID, 'buy_sell_status');
							
							if(isset($itemStatus) && !empty($itemStatus)) {
								$itemStatusSold = 'sold';
							} else {
								$itemStatusSold = '';
							}
						?>
						
						<?php if(get_the_author_meta('ID') == get_current_user_id()) { ?>
							<div class="row">
								<div class="tp-3 mp-6 force">
									<?php if($itemStatusSold) { ?>
										<a class="cta success size-xs full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=not-sold-item&itemid=<?php echo get_the_ID(); ?>">Mark Item as Not Sold</a>
									<?php } else { ?>
										<a class="cta success size-xs full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=sold-item&itemid=<?php echo get_the_ID(); ?>">Mark Item as Sold</a>
									<?php } ?>
								</div>
								<div class="tp-3 mp-6 force">
									<a class="cta warning size-xs full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=edit-item&itemid=<?php echo get_the_ID(); ?>">Edit Item</a>
								</div>
								<div class="tp-3 mp-6 force">
									<a class="cta stop size-xs full" href="<?php echo home_url(); ?>/buy-sell/?buy-sell-action=delete-item&itemid=<?php echo get_the_ID(); ?>">Delete Item</a>
								</div>
							</div>
							<hr class="secondary size-l">
						<?php } ?>

						<?php if(isset($_POST['send-message'])) { ?>
							<?php
								$commentdata = array(
								    'comment_post_ID'      => $post->ID, 
								    'comment_author'       => $_POST['comment-author'], 
								    'comment_author_url'   => $_POST['comment-author-url'],
								    'comment_author_email' => $_POST['comment-author-email'],
								    'comment_content'      => $_POST['message'], 
								    'user_id'              => $current_user->ID, 
								);
	 
								// Insert new comment and get the comment ID.
								$comment_id = wp_new_comment( $commentdata );
							?>
							
						<?php } ?>

						<?php
							if($itemStatusSold) {
								$itemTitle = '<span class="sold"><i class="fa fa-sold"></i> Item Sold</span><br />' . get_the_title();
							} else {
								$itemTitle = get_the_title();
							}
						?>
						<h2><?php echo $itemTitle; ?></h2>
						<hr class="secondary size-m">
						<?php if(get_field('item_location')) { ?>
							<p>Item location: <?php the_field('item_location'); ?></p>
						<?php } ?>
						
						<?php if(get_field('price')) { ?>
							<?php $price = preg_replace('/[^0-9]/', '', get_field('price')); ?>
							<?php if($price) { ?>
								<span class="price">£<?php echo $price; ?></span>
							<?php } ?>
						<?php } ?>
						
						<?php the_field('preview_description'); ?>
						
						<?php $image = get_field('featured_image'); ?>
						<?php if($image) { ?>
							<div class="preview-img">
								<img src="<?php echo $image['url']; ?>">
							</div>
						<?php } ?>
						
						<?php
							if(get_field('full_description')) {
								the_field('full_description');
							}
						?>
						
						<section class="comments">
							<?php
								$comments = get_comments(
									array(
										'post_id' => $post->ID,
										'order' => 'ASC',
									)
								);
							?>
							
							<?php if($comments) { ?>
								<?php foreach($comments as $key => $comment) { ?>
									<?php $userInfo = get_userdata($comment->user_id); ?>
									<div class="comment">
										<h6>
											<?php echo $userInfo->nickname; ?> - 
											<time datetime="<?php comment_time( 'c' ); ?>">
												<?php printf(__('%1$s at %2$s'), get_comment_date('', $comment), get_comment_time()); ?>
											</time>
										</h6>
										<p><?php echo $comment->comment_content; ?></p>
									</div>
								<?php } ?>
							<?php } else { ?>
								No comments found.
							<?php } ?>
						</section>
						<section class="message">
							<h3>Ask a question</h3>
							<form action="" method="post">
								<textarea name="message" cols="8"></textarea>
								<input type="hidden" name="comment-author" value=" <?php echo $current_user->user_nicename ?>">
								<input type="hidden" name="comment-author-email" value=" <?php echo $current_user->comment_author_email ?>">
								<input type="hidden" name="comment-author-url" value=" <?php echo $current_user->comment_author_url ?>">
								<input type="hidden" name="send-message">
								<input type="submit" value="Post Comment">
							</form>
						</section>

					</div>
				<?php } ?>
			</div>
		</div>
		<?php include( get_stylesheet_directory() . '/includes/all/further-content.php'); ?>
	<?php } else { // Not logged in ?>
		<?php wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI'])); ?>
	<?php } ?>
</section>
<?php get_footer(); ?>