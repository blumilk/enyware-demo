<?php
	/*
		Template Name: Login
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/all/logged-in.inc.php');
			} else { // Not logged in
				include('includes/public/enter.inc.php');
			}
		}
	}
	
	get_footer();
?>