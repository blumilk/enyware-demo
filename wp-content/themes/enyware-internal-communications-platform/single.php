<?php get_header(); ?>
<section class="page-load">
	<?php if(is_user_logged_in()) { ?>
		<div class="wrapper page-content">
			<div class="row">
				<div class="dt-12">
					<?php while(have_posts()) {
						the_post(); ?>
						<h2><?php the_title(); ?></h2>
						<hr class="secondary size-m">
						<article>
							<div class="row">
								<!-- Sidebar -->
								<?php include(get_stylesheet_directory() .'/includes/all/single-sidebar.php'); ?>
								
								<?php
									if(get_field( 'add_video' )){ 
										// Poster Image	
										$image 				= get_field('video_poster'); 
										$size				= 'large';
										$posterImage		= $image['sizes'][$size];
										// Videos
										$mp4Video			= get_field( 'mp4_video' );
										$webmVideo			= get_field( 'webm_video' );
										$oggVideo			= get_field( 'ogg_video' );
									?>
								<!-- Video -->
								<div class="tp-8 indent-tl-1 force">
									<div class="main">
										<div class="video-container top">
											<video preload controls poster="<?php echo $posterImage; ?>" autoplay="<?php the_field( 'autoplay_video' ); ?>" >
												<?php if($mp4Video){ ?>
													<source src="<?php echo $mp4Video['url']; ?>" type="video/mp4" alt="<?php echo $mp4Video['title']; ?>">
												<?php } ?>
												<?php if($webmVideo){ ?>
													<source src="<?php echo $webmVideo['url']; ?>" type="video/webm" alt="<?php echo $webmVideo['title']; ?>">
												<?php } ?>
												<?php if($oggVideo){ ?>
													<source src="<?php echo $oggVideo['url']; ?>" type="video/ogg" alt="<?php echo $oggVideo['title']; ?>">
												<?php } ?>
												<?php if($mp4Video){ ?>
													<a href="<?php echo $mp4Video['url']; ?>">Your device is not compatible with modern web video. Click here to download the video, <?php echo $mp4Video['title']; ?>.  </a>
												<?php } ?>												
											</video>
										</div>
									</div>
								</div>
								<?php }	?>
								<!-- Content -->
								<div class="tp-8 indent-tl-1 force">
									<div class="main">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</article>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php include( get_stylesheet_directory() . '/includes/all/further-content.php'); ?>
	<?php } else { // Not logged in ?>
		<?php wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI'])); ?>
	<?php } ?>
</section>
<?php get_footer(); ?>