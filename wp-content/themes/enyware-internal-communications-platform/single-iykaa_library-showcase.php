<?php 
	/*
	Template Name: Showcase Page
	Template Post Type: iykaa_library, iykaa_library_custom
	*/
	?>

<?php
	get_header();
	
	if(have_posts()) {
		if(is_user_logged_in()) {
			include('includes/search/library-showcase.php');
		} else { // Not logged in
			wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
		}
	}
	
	get_footer();
?>