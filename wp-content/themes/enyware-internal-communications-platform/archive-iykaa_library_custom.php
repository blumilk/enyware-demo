<?php
	get_header();
	
	if(have_posts()) {
		if(is_user_logged_in()) {
			include('includes/search/library_custom.php');
		} else {
			ipLoginChecker('includes/search/library_custom.php');
		}
	}
	
	get_footer();
?>