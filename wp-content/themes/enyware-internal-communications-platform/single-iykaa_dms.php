<?php get_header(); ?>
<?php
	// Member details
	global $current_user;
	
	$args = array(
		'author' => $current_user->ID,
		'post_type' => 'iykaa_members',
		'post_status' => array('pending', 'draft', 'publish'),
	);
	
	$membersProfile = new WP_Query($args);
	
	if($membersProfile->have_posts()) {
		while($membersProfile->have_posts()) {
			$membersProfile->the_post();
			$memberProfileID = $membersProfile->post->ID;
		}
		wp_reset_query();
	}
	
	$memberID = array();
	
	$fileInfo = get_field('file_settings');
	
	if($fileInfo) {
		$fileOrder = array_reverse($fileInfo);
		$file = end($fileInfo);
	}
	
	$authorAccess = get_field('author_access');
	
	if($authorAccess && is_array($authorAccess)) {
		foreach($authorAccess as $author) {
			array_push($memberID, $author->ID);
		}
	}
	
	$accessErrors = 0;
	
	$departmentSlugs = array(
		'all'
	);
	
	$departmentTerms = get_the_terms($post->ID, 'department_tax');
	
	foreach($departmentTerms as $departmentTerm) {
		array_push($departmentSlugs, $departmentTerm->slug);
	}
	
	$departmentMemberSlugs = array(
		'all'
	);
	
	$departmentMemberTerms = get_the_terms($post->ID, 'department_tax');
	
	foreach($departmentMemberTerms as $departmentMemberTerm) {
		array_push($departmentMemberSlugs, $departmentMemberTerm->slug);
	}
	
	if(!in_array($departmentMemberSlugs, $departmentSlugs)) {
		// $accessErrors++;
	}
?>

<section class="page-load">
	<?php if(is_user_logged_in()) { ?>
		<?php if($accessErrors == 0) { ?>
			<div class="wrapper page-content dms">
				<div class="row">
					<div class="dt-12">
						<a href="<?php echo home_url(); ?>/document-management-system">
							<h1>Document Management System</h1>
						</a>
						<hr class="secondary size-l">
						<?php if(get_field('file_settings')) { ?>
							<?php while(have_posts()) {
								the_post(); ?>
								<div class="row">
									<?php if(in_array($memberProfileID, $memberID)) { ?>
										<div class="tp-3 force">
											<h2>Document Options</h2>
											<a class="cta secondary outline full" href="<?php echo home_url(); ?>/dms-edit?post=<?php echo $post->ID; ?>">
												<i class="fa fa-dms-edit"></i> Edit Document
											</a>
										</div>
										<div class="tp-5 indent-tp-1 force">
											<div class="main">
												<h2><?php the_title(); ?></h2>
												<?php the_field('document_summary'); ?>
											</div>
										</div>
									<?php } else { ?>
										<div class="tp-8 indent-tl-1 force">
											<div class="main">
												<h2><?php the_title(); ?></h2>
												<?php the_field('document_summary'); ?>
											</div>
										</div>
									<?php } ?>
									<div class="tp-3 sidebar single-sidebar">
										<hr class="secondary">
										<?php
											$departments = get_the_terms($post->ID, 'department_tax');
											$departmentList = '';
											$departmentCount = count($departments);
											$count = 1;
											
											foreach($departments as $department) {
												if($departmentCount == $count) {
													$departmentList .= $department->name;
												} else {
													$departmentList .= $department->name . ', ';
												}
												
												$count++;
											}
										?>
										<small>Departments: <?php echo $departmentList; ?></small>
										<hr class="secondary">
										<small>Created: <?php the_time('d/m/Y'); ?></small>
										<hr class="secondary">
										<small>Last Updated: <?php echo date('d/m/Y', strtotime($file['select_file']['modified'])); ?></small>
										<hr class="secondary">
										<?php
											if(function_exists('iykaa_read_later_shortcode_add')) {
												echo do_shortcode('[iykaa_read_later_add]');
											}
										?>
									</div>
								</div>
								<hr class="secondary size-m">
								<h3>Latest Document Version</h3>
								<hr class="secondary size-m">
								<table class="dms-files" width="100%">
									<thead>
										<tr>
											<td width="20%">
												File Name
											</td>
											<td width="30%">
												File Summary
											</td>
											<td width="8%">
												Version #
											</td>
											<td width="8%">
												File Size
											</td>
											<td width="8%">
												File Type
											</td>
											<td width="15%">
												Last Modified
											</td>
											<td width="25%">
												Actions
											</td>
										</tr>
									</thead>
									<tbody>
										<tr data-href="<?php echo $file['select_file']['url']; ?>">
											<td>
												<?php
													if($file['file_name_dms']) {
														echo $file['file_name_dms'];
													} elseif($file['select_file']['filename']) {
														echo $file['select_file']['filename'];
													} else {
														the_title();
													}
												?>
											</td>
											<td>
												<?php
													if($file['summary_notes']) {
														echo $file['summary_notes'];
													} else {
														echo '–';
													}
												?>
											</td>
											<td>
												<?php
													if($file['version_number']) {
														echo $file['version_number'];
													} else {
														echo '–';
													}
												?>
											</td>
											<td>
												<?php
													$fileID = $file['select_file']['ID'];
													$fileSize = filesize(get_attached_file($fileID));
													$fileSize = size_format($fileSize, 1);
													echo $fileSize;
												?>
											</td>
											<td>
												<?php
													$fileType = pathinfo(get_attached_file($file['select_file']['ID']), PATHINFO_EXTENSION);
													
													switch($fileType) {
														case 'pdf':
															$fileIcon = 'pdf';
															break;
														case 'doc':
															$fileIcon = 'word-doc';
															break;
														case 'docx':
															$fileIcon = 'word-doc';
															break;
														case 'jpg':
															$fileIcon = 'image';
															break;
														case 'jpeg':
															$fileIcon = 'image';
															break;
														case 'png':
															$fileIcon = 'image';
															break;
														case 'gif':
															$fileIcon = 'image';
															break;
														case 'xlsx':
															$fileIcon = 'excel';
															break;
														case 'zip':
															$fileIcon = 'zip';
															break;
														default:
															$fileIcon = 'file';
													}
												?>
												
												<i class="fa fa-<?php echo $fileIcon; ?>"></i> <?php echo strtoupper($fileType); ?>
											</td>
											<td>
												<?php echo date('d/m/Y - G:i', strtotime($file['select_file']['modified'])); ?>
											</td>
											<td>
												<a href="<?php echo $file['select_file']['url']; ?>">
													<i class="fa icon-ii-download"></i> Download
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<?php
									$user = wp_get_current_user();
									$allowedRoles = array(
										'deity_iykaa'
									);
								?>
								
								<?php if(array_intersect($allowedRoles, $user->roles)) { ?>
									<section class="dms-revisions">
										<?php if($fileOrder && count($fileInfo) > 1) { ?>
											<hr class="secondary size-m">
											<h3>Document Revisions</h3>
											<hr class="secondary size-m">
											<table class="dms-files" width="100%">
												<thead>
													<tr>
														<td width="20%">
															File Name
														</td>
														<td width="30%">
															File Summary
														</td>
														<td width="8%">
															Version #
														</td>
														<td width="8%">
															File Size
														</td>
														<td width="8%">
															File Type
														</td>
														<td width="15%">
															Last Modified
														</td>
														<td width="25%">
															Actions
														</td>
													</tr>
												</thead>
												<tbody>
													<?php foreach($fileOrder as $file) { ?>
														<tr data-href="<?php echo $file['select_file']['url']; ?>">
															<td>
																<?php
																	if($file['file_name_dms']) {
																		echo $file['file_name_dms'];
																	} elseif($file['select_file']['filename']) {
																		echo $file['select_file']['filename'];
																	} else {
																		the_title();
																	}
																?>
															</td>
															<td>
																<?php
																	if($file['summary_notes']) {
																		echo $file['summary_notes'];
																	} else {
																		echo '–';
																	}
																?>
															</td>
															<td>
																<?php
																	if($file['version_number']) {
																		echo $file['version_number'];
																	} else {
																		echo '–';
																	}
																?>
															</td>
															<td>
																<?php
																	$fileID = $file['select_file']['ID'];
																	$fileSize = filesize(get_attached_file($fileID));
																	$fileSize = size_format($fileSize, 1);
																	echo $fileSize;
																?>
															</td>
															<td>
																<?php
																	$fileType = pathinfo(get_attached_file($file['select_file']['ID']), PATHINFO_EXTENSION);
																	
																	switch($fileType) {
																		case 'pdf':
																			$fileIcon = 'pdf';
																			break;
																		case 'doc':
																			$fileIcon = 'word-doc';
																			break;
																		case 'docx':
																			$fileIcon = 'word-doc';
																			break;
																		case 'jpg':
																			$fileIcon = 'image';
																			break;
																		case 'jpeg':
																			$fileIcon = 'image';
																			break;
																		case 'png':
																			$fileIcon = 'image';
																			break;
																		case 'gif':
																			$fileIcon = 'image';
																			break;
																		case 'xlsx':
																			$fileIcon = 'excel';
																			break;
																		case 'zip':
																			$fileIcon = 'zip';
																			break;
																		default:
																			$fileIcon = 'file';
																	}
																?>
																
																<i class="fa fa-<?php echo $fileIcon; ?>"></i> <?php echo strtoupper($fileType); ?>
															</td>
															<td>
																<?php echo date('d/m/Y - G:i', strtotime($file['select_file']['modified'])); ?>
															</td>
															<td>
																<a href="<?php echo $file['select_file']['url']; ?>">
																	<i class="fa icon-ii-download"></i> Download
																</a>
															</td>
														</tr>
													<?php } ?>
												</tbody>
												<tfoot>
													<tr>
														<td width="20%">
															File Name
														</td>
														<td width="30%">
															File Summary
														</td>
														<td width="8%">
															Version #
														</td>
														<td width="8%">
															File Size
														</td>
														<td width="8%">
															File Type
														</td>
														<td width="15%">
															Last Modified
														</td>
														<td width="25%">
															Actions
														</td>
													</tr>
												</tfoot>
											</table>
										<?php } ?>
									</section>
								<?php } ?>
							<?php } ?>
						<?php } else { ?>
							<p>This document currently has no files attached.</p>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<div class="wrapper page-content dms">
				<div class="row">
					<div class="dt-12">
						<h1>You do not have permission to view this document.</h1>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } else { // Not logged in ?>
		<?php wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI'])); ?>
	<?php } ?>
</section>
<?php get_footer(); ?>