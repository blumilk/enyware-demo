<?php
	/*
		Template Name: Member Document Uploader
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				the_content();
			} else { // Not logged in
				include('includes/public/enter.inc.php');
			}
		}
	}
	
	get_footer();
?>