<?php
	/*
		Template Name: Thank You
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			include('includes/public/logged-out.inc.php');
		}
	}
	
	get_footer();
?>