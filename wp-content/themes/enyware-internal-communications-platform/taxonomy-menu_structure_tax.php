<?php
	get_header();
	
	if(is_user_logged_in()) {
		include('includes/search/taxonomy-menu.php');
	} else { // Not logged in
		wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
	}
	
	get_footer();
?>