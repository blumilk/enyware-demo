<?php
	get_header();
	
	if(is_user_logged_in()) {
		include('includes/search/nursing-competencies-tax.php');
	} else { // Not logged in
		ipLoginChecker('includes/search/nursing-competencies-tax.php');
	}
	
	get_footer();
?>