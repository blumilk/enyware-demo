<?php
	get_header();
	
	if(is_user_logged_in()) {
		include('includes/search/taxonomy-custom.php');
	} else { // Not logged in
		ipLoginChecker('includes/search/taxonomy-custom.php');
	}
	
	get_footer();
?>