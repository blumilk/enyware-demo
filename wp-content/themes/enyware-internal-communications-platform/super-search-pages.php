<?php
	/*
		Template Name: Super Search Pages
	*/
	
	$excludeIDs = array();
	
	array_push($excludeIDs, '24310');
	
	$args = array(
		'post_type' 		=> array(
			'post',
			'page',
			'iykaa_members',
			'iykaa_notifications',
			'iykaa_library'
		),
		'posts_per_page' 	=> -1,
		'post__not_in' 		=> $excludeIDs,
	);
	
	$pageList = array();
	
	$superSearch = new WP_Query($args);
	
	if($superSearch->have_posts()) {
		while($superSearch->have_posts()) {
			$superSearch->the_post();
			
			if(get_field('exclude_from_search_results') != 'yes') {
				array_push($pageList, array(
					'title' 		=> get_the_title(),
					'permalink' 	=> get_the_permalink(),
					'the_excerpt'	=> get_the_excerpt(),
				));
			}
		}
	}
	
	$keywordsSearch = $_REQUEST['keywords-search'];
	
	$hint = '';
	
	if($keywordsSearch !== '') {
		$keywordsSearch = strtolower($keywordsSearch);
		$len = strlen($keywordsSearch);
		
		foreach($pageList as $superSearchResults) {
			if(stripos($superSearchResults['title'], $keywordsSearch) !== FALSE) {
				$hint .= '<a href="' . $superSearchResults['permalink'] . '"><li class="gaps"><div class="row"><div class="dt-12">' . $superSearchResults['title'] . '<br>' . $superSearchResults['the_excerpt'] . '</div></div></li></a>';
			}
		}
	}
	
	if($hint === '') {
		echo '<li>No results found.</li>';
	} else {
		echo $hint;
	}
?>
