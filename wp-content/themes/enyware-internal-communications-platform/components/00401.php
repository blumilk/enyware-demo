<section class="comp comp-00401 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="tl-7">
				<strong><?php the_sub_field('bold_text'); ?></strong>
				<?php the_sub_field('content'); ?>
			</div>
			<div class="tl-4 indent-tl-1">
				<?php $image = get_sub_field('image'); ?>
				<img src="<?php echo $image['url']; ?>">
			</div>
		</div>
	</div>
</section>