<section class="comp comp-00106 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<ul class="slider slides">
					<?php
						$args = array(
							'post_type' => 'post',
							'orderby' => 'date',
							'order' => 'DESC',
							'posts_per_page' => 4,
							'post_status' => 'publish',
						);
						
						$latestNews = new WP_Query($args);
					?>
					
					<?php while($latestNews->have_posts()) {
						$latestNews->the_post();
						$image = get_field('slide_image'); ?>
						<a href="<?php the_permalink(); ?>">
							<div class="wrapper full no-gaps">
								<div class="row">
									<div class="dt-12">
										<div class="inner <?php the_sub_field('inner_colour'); ?>">
											<div class="wrapper">
												<div class="row">
													<div class="dt-12">
														<h4><?php the_title(); ?></h4>
														<?php the_sub_field('body'); ?>
														<div class="cta <?php the_sub_field('cta_colour'); ?>">
															Find Out More
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if($image) { ?>
								<li style="background-image: url('<?php echo $image['url']; ?>');"></li>
							<?php } ?>
						</a>
					<?php } ?>
					
					<?php wp_reset_query(); ?>
				</ul>
			</div>
		</div>
	</div>
</section>