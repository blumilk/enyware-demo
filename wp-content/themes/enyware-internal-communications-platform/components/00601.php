<section class="comp comp-006 comp-00601 <?php the_sub_field('background_colour'); ?> whats-happening">
	<div class="wrapper <?php the_sub_field('width'); ?>">
		<div class="row nested">
			<div class="dt-12">
				<h3>What's Happening</h3>
				<hr class="secondary size-l">
			</div>
			<?php
				$args = array(
					'post_type' => 'post',
					'orderby' => 'date',
					'order' => 'DESC',
					'posts_per_page' => 12,
					'post_status' => 'publish',
					'offset'	=> 0,
				);
				
				$latestNews = new WP_Query($args);
			?>
			
			<div class="tp-12">
				<ul class="slides">
					<?php while($latestNews->have_posts()) {
						$latestNews->the_post();
						$image = get_field('slide_image'); 
						$defaultImage = get_field('logo_svg_so', 'options'); ?>
						<li>
							<a href="<?php the_permalink(); ?>">
								<div class="row">
									<?php if($image) {?>
										<div class="mp-4 ml-12 news-thumbnail">
											<img src="<?php echo $image['url']; ?>">
										</div>
										<div class="mp-4 news-thumbnail-mobile" style="background-image: url(<?php echo $image['url']; ?>);">
										</div>
										<div class="mp-8 ml-12">
									<?php } else if($defaultImage) { ?>
										<div class="mp-4 ml-12 news-thumbnail default">
											<img src="<?php echo $defaultImage['url']; ?>">
										</div>
										<div class="mp-4 news-thumbnail-mobile default" style="background-image: url(<?php echo $defaultImage['url']; ?>);">
										</div>
										<div class="mp-8 ml-12">	
									<?php } else { ?>
										<div class="mp-12">
									<?php } ?>
										
										<div class="inner <?php the_sub_field('inner_colour'); ?>">
											<h4 class="title"><?php the_title(); ?></h4>
											<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
											<p class="time-stamp"><?php echo get_the_date('l jS F, Y'); ?></p>
											<hr class="size-s <?php the_sub_field('cta_colour'); ?>">
											<?php the_excerpt(); ?>
											<?php the_sub_field('body'); ?>
										</div>
									</div>
									<div class="mp-12">
										<div class="find-out-more">
											<div class="cta <?php the_sub_field('cta_colour'); ?> size-s">
												Find Out More
											</div>
											<div class="chevron">
												&#x63;
											</div>
										</div>
									</div>
								</div>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
			
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>