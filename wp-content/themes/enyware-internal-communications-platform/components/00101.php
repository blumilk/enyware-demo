<section class="comp comp-00101 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<?php
					$sliderCount = count(get_sub_field('slider'));
					
					if($sliderCount > 1) {
						$slider = ' slides';
					} else {
						$slider = '';
					}
				?>
				<ul class="slider<?php echo $slider; ?>">
					<?php while(have_rows('slider')) {
						the_row();
						$image = get_sub_field('image'); ?>
						<li style="background-image: url('<?php echo $image['url']; ?>');"></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</section>