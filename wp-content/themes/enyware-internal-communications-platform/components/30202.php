<section class="comp comp-30202 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12 align-central">
				<h3>Get in touch</h3>
			</div>
			<div class="tl-6">
				<?php
					$url = "https://maps.googleapis.com/maps/api/geocode/xml?address=" . get_sub_field('postcode') . "+UK&sensor=false";
						
					$result = simplexml_load_file($url);
					
					foreach($result->result as $mydata) {
						foreach($mydata->geometry as $values) {
							foreach($values->location as $bodies) {
								$lat = (string) $bodies->lat;
								$long = (string) $bodies->lng;
							}
						}
					}
				?>
				<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						initialize();
					});
					
					function initialize() {
						var myLatLng = {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>};
					
						var map_options = {
							center: myLatLng,
							zoom: 15,
							mapTypeId: google.maps.MapTypeId.MAP,
							scrollwheel: false,
						};
					
						var google_map = new google.maps.Map(document.getElementById("map"), map_options);
					
						var info_window = new google.maps.InfoWindow({
							content: 'loading'
						});
					
						var marker = new google.maps.Marker({
							position: myLatLng,
							map: google_map,
							icon: '<?php the_sub_field('pin_icon'); ?>',
							draggable: false
						});
					}
				</script>
				<div id="map" style="width: 100%; height: 300px;">
					Google Map
				</div>
			</div>
			<div class="tl-6">
				<?php if(have_rows('address', 'options')) { ?>
					<div class="address">
						<?php while(have_rows('address', 'options')) {
							the_row(); ?>
							<p><?php the_sub_field('line'); ?></p>
						<?php } ?>
					</div>
				<?php } ?>
				<?php if(get_field('telephone_number', 'options')) { ?>
					<div class="contact">
						<strong>Telephone</strong>
						<p>
							<a href="tel:<?php the_field('telephone_number', 'options'); ?>">
								<?php the_field('telephone_number', 'options'); ?>
							</a>
						</p>
					</div>
				<?php } ?>
				<?php if(get_field('fax_number', 'options')) { ?>
					<div class="contact">
						<strong>Fax</strong>
						<p><?php the_field('fax_number', 'options'); ?></p>
					</div>
				<?php } ?>
				<?php if(get_field('email_address', 'options')) { ?>
					<div class="contact">
						<strong>Email</strong>
						<p>
							<a href="mailto:<?php the_field('email_address', 'options'); ?>">
								<?php the_field('email_address', 'options'); ?>
							</a>
						</p>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>