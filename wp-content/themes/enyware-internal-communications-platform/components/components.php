<?php
	while(have_rows('layouts')) {
		the_row();
		if(get_row_layout() == '00101') {
			include('00101.php');
		} elseif(get_row_layout() == '00102') {
			include('00102.php');
		} elseif(get_row_layout() == '00103') {
			include('00103.php');
		} elseif(get_row_layout() == '00104') {
			include('00104.php');
		} elseif(get_row_layout() == '00105') {
			include('00105.php');
		} elseif(get_row_layout() == '00106') {
			include('00106.php');
		} elseif(get_row_layout() == '00201') {
			include('00201.php');
		} elseif(get_row_layout() == '00202') {
			include('00202.php');
		} elseif(get_row_layout() == '00301') {
			include('00301.php');
		} elseif(get_row_layout() == '00302') {
			include('00302.php');
		} elseif(get_row_layout() == '00303') {
			include('00303.php');
		} elseif(get_row_layout() == '00304') {
			include('00304.php');
		} elseif(get_row_layout() == '00401') {
			include('00401.php');
		} elseif(get_row_layout() == '00402') {
			include('00402.php');
		} elseif(get_row_layout() == '00501') {
			include('00501.php');
		} elseif(get_row_layout() == '00502') {
			include('00502.php');
		} elseif(get_row_layout() == '00502a') {
			include('00502a.php');
		} elseif(get_row_layout() == '00601') {
			include('00601.php');
		} elseif(get_row_layout() == '30201') {
			include('30201.php');
		} elseif(get_row_layout() == '30202') {
			include('30202.php');
		} elseif(get_row_layout() == '70101') {
			include('70101.php');
		}
	}
?>