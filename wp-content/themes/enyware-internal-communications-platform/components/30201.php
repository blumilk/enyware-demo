<section class="comp comp-30201">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="tl-12">
				<?php
					$url = "https://maps.googleapis.com/maps/api/geocode/xml?address=" . get_sub_field('postcode') . "+UK&sensor=false";
						
					$result = simplexml_load_file($url);
					
					foreach($result->result as $mydata) {
						foreach($mydata->geometry as $values) {
							foreach($values->location as $bodies) {
								$lat = (string) $bodies->lat;
								$long = (string) $bodies->lng;
							}
						}
					}
				?>
				<script src="https://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						initialize();
					});
					
					function initialize() {
						var myLatLng = {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>};
					
						var map_options = {
							center: myLatLng,
							zoom: 15,
							mapTypeId: google.maps.MapTypeId.MAP,
							scrollwheel: false,
						};
					
						var google_map = new google.maps.Map(document.getElementById("map"), map_options);
					
						var info_window = new google.maps.InfoWindow({
							content: 'loading'
						});
					
						var marker = new google.maps.Marker({
							position: myLatLng,
							map: google_map,
							icon: '<?php the_sub_field('pin_icon'); ?>',
							draggable: false
						});
					}
				</script>
				<div id="map" style="width: 100%; height: 300px;">
					Google Map
				</div>
			</div>
		</div>
	</div>
</section>