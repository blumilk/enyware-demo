<section class="comp comp-00104 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<?php
					$sliderCount = count(get_sub_field('slider'));
					
					if($sliderCount > 1) {
						$slider = ' slides';
					} else {
						$slider = '';
					}
				?>
				<ul class="slider<?php echo $slider; ?>">
					<?php while(have_rows('slider')) {
						the_row();
						$image = get_sub_field('image'); ?>
						<a href="<?php the_sub_field('link'); ?>">
							<div class="wrapper full no-gaps">
								<div class="row">
									<div class="dt-12 <?php the_sub_field('position'); ?>">
										<div class="inner <?php the_sub_field('inner_colour'); ?>">
											<div class="wrapper">
												<div class="row">
													<div class="dt-12">
														<h4><?php the_sub_field('heading'); ?></h4>
														<?php the_sub_field('body'); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<li style="background-image: url('<?php echo $image['url']; ?>');"></li>
						</a>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</section>