<section class="comp comp-00102 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<?php
					$sliderCount = count(get_sub_field('slider'));
					
					if($sliderCount > 1) {
						$slider = ' slides';
					} else {
						$slider = '';
					}
				?>
				<ul class="slider<?php echo $slider; ?>">
					<?php while(have_rows('slider')) {
						the_row();
						$image = get_sub_field('image'); ?>
						<a href="<?php the_sub_field('link'); ?>">
							<li style="background-image: url('<?php echo $image['url']; ?>');">
								<div class="wrapper no-gaps">
									<div class="row">
										<div class="tl-5 <?php the_sub_field('position'); ?>">
											<div class="inner <?php the_sub_field('inner_colour'); ?>">
												<h4><?php the_sub_field('heading'); ?></h4>
												<?php the_sub_field('body'); ?>
												<div class="cta <?php the_sub_field('cta_colour'); ?>">
													Find Out More
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
						</a>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</section>