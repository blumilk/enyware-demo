<section class="comp comp-00202 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?>">
		<div class="row">
			<?php while(have_rows('gallery')) {
				the_row(); ?>
				<div class="tl-3 tp-6 img">
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo $image['url']; ?>">
				</div>
			<?php } ?>
		</div>
	</div>
</section>