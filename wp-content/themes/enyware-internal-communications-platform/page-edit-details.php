<?php
	/*
		Template Name: Edit Details
	*/
	
	acf_form_head();
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/config/member-details.php');
				
				include('includes/all/edit-details.inc.php');
			} else { // Not logged in
				wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
			}
		}
	}
	
	get_footer();
?>