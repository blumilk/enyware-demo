<?php
	/*
		Template Name: Glimpse
	*/
	
	get_header('glimpse');

	if(is_home()) {
		wp_redirect(home_url('/dashboard'));
	} 
				
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				wp_redirect(home_url('/dashboard'));
			} else { // Not logged in
				include('includes/all/dashboard-glimpse.php');
			}
		}
	}
	
	get_footer();
?>