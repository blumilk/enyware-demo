<?php
	/*
		Template Name: My Profile
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
				include('includes/config/member-details.php');
				
				while($membersProfile->have_posts()) {
					$membersProfile->the_post();
					wp_redirect(get_the_permalink());
				}
			} else { // Not logged in
				wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
			}
		}
	}
	
	get_footer();
?>