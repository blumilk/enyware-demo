<?php get_header(); ?>
<section class="page-load">
	<?php if(have_posts()) {
		the_post(); ?>
		<div class="wrapper">
			<div class="row">
				<div class="dt-12">
					<h2>Author Archives: <?php echo get_the_author(); ?></h2>
					<?php if(get_the_author_meta('description')) { ?>
						<?php echo get_avatar(get_the_author_meta('user_email')); ?>
						<h3>About <?php echo get_the_author();?></h3>
						<?php the_author_meta('description'); ?>
					<?php } ?>
					<?php rewind_posts(); ?>
					<?php while(have_posts()) {
						the_post(); ?>
						<article>
							<h2>
								<a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
									<?php the_title(); ?>
								</a>
							</h2>
							<?php the_content(); ?>
						</article>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="wrapper">
			<div class="row">
				<div class="dt-12">
					<h1>No posts to display for <?php echo get_the_author() ; ?></h1>
				</div>
			</div>
		</div>
	<?php } ?>
</section>
<?php get_footer(); ?>