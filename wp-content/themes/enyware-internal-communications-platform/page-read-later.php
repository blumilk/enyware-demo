<?php
	/*
		Template Name: Read Later
	*/
	
	get_header();
	
	if(have_posts()) {
		while(have_posts()) {
			the_post();
			
			if(is_user_logged_in()) {
//				include('includes/all/read-later-list.php');
				the_content();
			} else { // Not logged in
				wp_redirect(home_url('/login/?redirect_to=' . $_SERVER['REQUEST_URI']));
			}
		}
	}
	
	get_footer();
?>