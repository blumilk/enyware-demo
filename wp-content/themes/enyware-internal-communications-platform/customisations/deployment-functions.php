<?php 
	// Deployment Specific Functions 
	
	// Add scripts to wp_head()
	function custom_functions_header_load() {
		// Your PHP goes here
	}
	add_action( 'wp_head', 'custom_functions_header_load' );
	

	// Custom post types - Such as an extra library
	function create_post_type_custom() {
		register_post_type ('iykaa_library_custom',
			array(
				'labels' => array(
					'name' => __('Documents'),
					'singular_name' => __('Document'),
					'add_new_item' => __('Add New Item'),
					'add_new' => _x('Add New', ''),
					'edit_item' => __('Edit Item'),
					'new_item' => __('New Item'),
					'all_items' => __('All Items'),
					'view_item' => __('View Item'),
					'search_items' => __('Search'),
					'not_found' =>  __('No Items Found'),
					'not_found_in_trash' => __('No Items Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Documents',
					'add_new' => 'Add Item',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-book-alt',
				'menu_position' => 6,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'taxonomies' => array('post_tag'),
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'document',
				),
				'show_in_rest' => true,
				'rest_base' => 'document-api',
				'rest_controller_class' => 'WP_REST_Posts_Controller',
			)
		);
/*
		register_post_type ('policies_procedures',
			array(
				'labels' => array(
					'name' => __('Policies and Procedures'),
					'singular_name' => __('Policies and Procedures'),
					'add_new_item' => __('Add New Item'),
					'add_new' => _x('Add New', ''),
					'edit_item' => __('Edit Item'),
					'new_item' => __('New Item'),
					'all_items' => __('All Items'),
					'view_item' => __('View Item'),
					'search_items' => __('Search'),
					'not_found' =>  __('No Items Found'),
					'not_found_in_trash' => __('No Items Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Policies and Procedures',
					'add_new' => 'Add Item',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-book-alt',
				'menu_position' => 7,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'taxonomies' => array('post_tag'),
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'policies-procedures',
				),
			)
		);
		
		register_post_type ('nursing_competencies',
			array(
				'labels' => array(
					'name' => __('Nursing Competencies'),
					'singular_name' => __('Nursing Competencies'),
					'add_new_item' => __('Add New Item'),
					'add_new' => _x('Add New', ''),
					'edit_item' => __('Edit Item'),
					'new_item' => __('New Item'),
					'all_items' => __('All Items'),
					'view_item' => __('View Item'),
					'search_items' => __('Search'),
					'not_found' =>  __('No Items Found'),
					'not_found_in_trash' => __('No Items Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Nursing Competencies',
					'add_new' => 'Add Item',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-book-alt',
				'menu_position' => 8,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'nursing-competencies',
				),
			)
		);
		
		register_post_type ('buy_sell',
			array(
				'labels' => array(
					'name' => __('Buy and Sell'),
					'singular_name' => __('Buy and Sell'),
					'add_new_item' => __('Add New Item'),
					'add_new' => _x('Add New', ''),
					'edit_item' => __('Edit Item'),
					'new_item' => __('New Item'),
					'all_items' => __('All Items'),
					'view_item' => __('View Item'),
					'search_items' => __('Search'),
					'not_found' =>  __('No Items Found'),
					'not_found_in_trash' => __('No Items Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Buy and Sell',
					'add_new' => 'Add Item',
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-cart',
				'menu_position' => 9,
				'hierarchical' => true,
				'delete_with_user'	=> false,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'buy-sell',
				),
			)
		);
		*/
	}
	
	add_action('init', 'create_post_type_custom');
	
	
	// Custom post type taxonomies
	function create_post_type_taxonomies_custom() {
		/*
		register_taxonomy('band_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Band',
					'add_new_item' => 'Add New Band',
					'new_item_name' => 'New Band'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('occupational_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Occupational Unit',
					'add_new_item' => 'Add New Occupational Unit',
					'new_item_name' => 'New Occupational Unit'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('business_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Business Unit',
					'add_new_item' => 'Add New Business Unit',
					'new_item_name' => 'New Business Unit'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		*/
		register_taxonomy('document_type_tax',
			array(
				'iykaa_library_custom',
			),
			array(
				'labels' => array(
					'name' => 'Document Type',
					'add_new_item' => 'Add Document Type',
					'new_item_name' => 'New Document Type'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'document-type',
				),
			)
		);
		/*
		register_taxonomy('nursing_competencies_tax',
			array(
				'nursing_competencies',
			),
			array(
				'labels' => array(
					'name' => 'Nursing Competencies Type',
					'add_new_item' => 'Add Nursing Competencies Type',
					'new_item_name' => 'New Nursing Competencies Type'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true,
				'rewrite' => array(
					'slug' => 'nursing-competencies-type',
				),
			)
		);
		
		register_taxonomy('you_are_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'You Are',
					'add_new_item' => 'Add You Are',
					'new_item_name' => 'New You Are'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('apply_to_you_tax',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Apply To You',
					'add_new_item' => 'Add Item',
					'new_item_name' => 'New Item'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		
		register_taxonomy('systems_used',
			array(
				'iykaa_members',
				'iykaa_notifications',
			),
			array(
				'labels' => array(
					'name' => 'Systems Used',
					'add_new_item' => 'Add System',
					'new_item_name' => 'New System'
				),
				'show_ui' => true,
				'show_tagcloud' => false,
				'hierarchical' => true
			)
		);
		*/
	}
	
	
	add_action('init', 'create_post_type_taxonomies_custom', 0);
	
?>