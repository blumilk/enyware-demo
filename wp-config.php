<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Applications/MAMP/htdocs/enyware-demo/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'blumilkd_nhsn-iykaa');
define('DB_NAME', 'enyware-demo');
/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uUjZJ+Bl8[|F(RgvFRCFkbX:F;faHqJHq7ba oUI_DhIJ*0s[DuJVqgm|j1Gi4P|');
define('SECURE_AUTH_KEY',  'U|lI|*|LtLZ{i:b*#M*fqggKOS<V67T53PTNkchD~Ks@l~-;COlxc&>O7A4q5{/W');
define('LOGGED_IN_KEY',    'd-.M_`3DWvy<Rpr @oAV)8SsL!_<}Y0];*dsejqo2@r1Z41uPk]pBT7+R0g#Y[P7');
define('NONCE_KEY',        '-`eX-)+v#Fr.TdJsiMb0T;wTC!j}6fJ|p1cAXV!`p:8d!WB>!zWfm0?&abs.M2T5');
define('AUTH_SALT',        '2~ ]6.wk-1|x[N&RML:J`XF[/j.Ou>FWH#/,K8%qf&66ZmDpVu)5j;a~T-/j~SDL');
define('SECURE_AUTH_SALT', 'wlX[E&^}:-y5N-2F#&VgK9 [e/oyP=#E~|wH?<%Ub[=Lt-PE*sFi^&<{-Y|,4BC[');
define('LOGGED_IN_SALT',   'XeFuYTq~}fkn:#K+#OGX DL4]]smrF_no6R-hE[G%Adf-V#2<Fc>B+-P]{XF7bn+');
define('NONCE_SALT',       'Oa3}Ukia R)$f#t[O5!s_|,x;r!3Ey.M`;s|i.#Ki F4`odJ1D|_dM|giG-O {02');

define('CONCATENATE_SCRIPTS', false);

/**#@-*/

// set max post revisions    
define('WP_POST_REVISIONS', 20);

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

define('DISALLOW_FILE_EDIT', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
